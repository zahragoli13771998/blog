@extends('layouts.marketing')
@section('title', 'نمایش')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <table id="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">
                                <thead>
                                <tr>
                                    <div class="search-container">
                                        <form action="{{route('marketing.faq.question_answer.show_questions')}}"
                                              method="GET"
                                              class="form-inline">
                                            <input type="search" placeholder="جستجو میان پرسش ها..."
                                                   name="question_title"
                                                   value="{{$request->question}}">
                                            <button type="submit"><i class="fa fa-search"></i></button>
                                        </form>
                                        <a href="{{route('marketing.faq.question_answer.create')}}" class="btn btn-primary">ایجاد </a>
                                    </div>
                                    <th scope="col">ترتیب نمایشی</th>
                                    <th scope="col">دسته بندی</th>
                                    <th scope="col">پر تکرار است؟</th>
                                    <th scope="col">دفعات مطرح شدن این پرسش</th>
                                    <th scope="col">پرسش</th>
                                    <th scope="col">پاسخ کوتاه</th>
                                    <th scope="col">پاسخ کامل</th>
                                    <th scope="col">تصویر</th>
                                    <th scope="col">وید‌‌ئو</th>
                                    <th scope="col">لینک وید‌‌ئو</th>
                                    <th scope="col">تعداد کامنت ها</th>
                                    <th scope="col">عملیات</th>
                                    <th scope="col">دیپ لینک</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($question_answers as $question_answer)
                                    <tr>
                                        <td class="text-sm">{{$question_answer->show_order}}</td>
                                        <td class="text-sm">{{$question_answer->FaqCategory->name_fa}}</td>
                                        <td class="text-sm">@if($question_answer->is_repetitive != 0)بله
                                            @else   خیر
                                            @endif
                                        </td>
                                        <td class="text-sm">{{$question_answer->question_fa}}</td>
                                        <td class="text-sm">{{$question_answer->short_answer_fa}}</td>
                                        <td class="text-sm">{!!  $question_answer->complete_answer_fa!!}</td>
                                        <td class="text-sm">
                                            @if(isset($question_answer->image))
                                                <img width="80px" src="{{$question_answer->image}}">
                                            @endif
                                        </td>
                                        <td class="text-sm">
                                            @if(isset($question_answer->video))
                                                <img width="80px" src="{{$question_answer->video}}">
                                            @endif
                                        </td>
                                        <td class="text-sm">{{$question_answer->external_video}}</td>

                                        <td> {{$question_answer->comments_count}}</td>
                                        <td class="text-sm">
                                            <a href="{{route('marketing.faq.question_answer.feedback',[$question_answer->id])}}" class="btn btn-success">نمایش</a>
                                            <a href="{{route('marketing.faq.question_answer.edit',[$question_answer->id])}}" class="btn btn-warning">ویرایش</a>
                                            <a href="{{route('marketing.faq.question_answer.delete',[$question_answer->id])}}"
                                               class="btn btn-danger">حذف سوال</a>
                                            <a href="{{route('marketing.faq.question_answer.deeplink.create', [$question_answer->id])}}" class="btn btn-success">اضافه کردن لینک</a>
                                        </td>
                                        <td class="text-sm">
                                            @if(isset($question_answer->deeplink->id))
                                                {{$question_answer->deeplink->link}}
                                                <a href="{{route('marketing.faq.question_answer.deeplink.delete',[$question_answer->deeplink->id])}}"
                                                   class="btn btn-danger">حذف دیپ لینک</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
        {{$question_answers->appends(request()->all())->links()}}
@endsection



