@extends('layouts.marketing')

@section('title', 'ایجاد questionAnswer')

@section('desktop')

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <div class="alert ">
                <li>{{ $error }}</li>
            </div>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <head>
        <style>
            #cke_text-editor {
                width: 1000px;
            }
            .ck.ck-content.ck-editor__editable.ck-rounded-corners.ck-editor__editable_inline.ck-blurred p
            {
                color: black;
            }
            .ck.ck-content.ck-editor__editable.ck-rounded-corners.ck-editor__editable_inline.ck-blurred p:active
            {
                color: black;
            }
        </style>
    </head>
    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <form action="{{route('marketing.faq.question_answer.create')}}" method="POST" class="form-inline"  enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="id">دسته بندی</label>
                            <select  class="form-control" name="faq_category_id">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name_fa}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_order">ترتیب نمایشی</label>
                            <input type="number" class="form-control"
                                   name="show_order" id="show_order"
                                   placeholder=" ...show_order"
                                   value="{{old('show_order')}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label mb-10"
                                   for="is_repetitive">پرتکرار است؟</label>
                            <select  class="form-control" name="is_repetitive">
                                <option value="1">پر تکرار است</option>
                                <option value="0">پرتکرار نیست</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="question_fa">پرسش</label>
                            <input type="text" class="form-control"
                                   name="question_fa" id="question_fa"
                                   placeholder=" ...question"
                                   value="{{old('question_fa')}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="question_en">پرسش انگلیسی</label>
                            <input type="text" class="form-control"
                                   name="question_en" id="question_en"
                                   placeholder=" ...question"
                                   value="{{old('question_en')}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="width: 100%">
                            <label for="short_answer_fa">پاسخ کوتاه</label>
                            <input type="text" class="form-control" style="width: 100%"
                                   name="short_answer_fa" id="short_answer_fa"
                                   placeholder=" ...short_answer"
                                   value="{{old('short_answer_fa')}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="width: 100%">
                            <label for="short_answer_en">پاسخ کوتاه انگلیسی</label>
                            <input type="text" class="form-control" style="width: 100%"
                                   name="short_answer_en" id="short_answer_en"
                                   placeholder=" ...short_answer"
                                   value="{{old('short_answer_en')}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="image">آپلود تصویر</label>
                            <input type="file" class="form-control"
                                   name="image" id="image"
                                   placeholder="آپلود تصویر...">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="video">آپلود فیلم</label>
                            <input type="file" class="form-control"
                                   name="video" id="video"
                                   placeholder="آپلود فیلم...">
                        </div>
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="external_video">لینک وید‌‌ئو</label>
                        <input type="url" class="form-control"
                               name="external_video" id="external_video"
                               placeholder="لینک وید‌‌ئو ..."
                               value="{{old('external_video')}}">
                    </div>
                    <div class="col-md-12">
                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="complete_answer">پاسخ کامل** *</label>
                            <textarea rows="4" cols="50"
                                      name="complete_answer_fa"  id="text-editor"
                                      class="form-control">


                            </textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group" style="width: 100%">
                            <label class="control-label"
                                   for="complete_answer_en">پاسخ کامل</label>
                            <textarea rows="4" cols="50" placeholder="پاسخ کامل..."
                                      name="complete_answer_en"  id="text-editor"
                                      class="form-control">{{old('complete_answer_en')}}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button id="update-profile" type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ایجاد
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="/js/ckeditor/ckeditor.js"></script>
    <script src="/js/ckeditor/starter.js"></script>
    <script src="/js/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('text-editor', {
            filebrowserUploadUrl: "{{route('marketing.faq.ckeditor.file.store-image',['_token' => csrf_token()])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
    <script src="/js/ckeditor/starter.js"></script>
@endsection



