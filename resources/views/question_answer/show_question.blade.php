@extends('layouts.marketing')
@section('title', 'نمایش')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="card" style="width: 40rem;">
        <div>
            @if(isset($question_answer->image))
                <img width="80px" src="{{$question_answer->image}}">
            @endif
        </div>
        <div>

            @if(isset($question_answer->video))
                <img width="80px" src="{{$question_answer->video}}">
            @endif
        </div>
        <div>
            {{$question_answer->external_video}}
        </div>
        <div class="card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <h1 class="card-title">{{$question_answer->question_fa}}</h1>
                </li>
                <h3>
                    <li class="list-group-item">
                        {{$question_answer->short_answer_fa}}
                    </li>

                </h3>
                <li class="list-group-item">
                    <p class="card-text">
                        {!! $question_answer->complete_answer_fa !!}
                    </p>
                </li>
                <li class="list-group-item"> دسته بندی : {{$question_answer->FaqCategory->name_fa}}</li>
                <li class="list-group-item"> دفعات مطرح شدن پرسش : {{$question_answer->question_view_counter}}</li>
                <li class="list-group-item"> سوال پر تکرار است؟ @if($question_answer->is_repetitive != 0)بله
                    @else   خیر
                    @endif
                </li>
                <li class="list-group-item">ترتیب نمایشی : {{$question_answer->show_order}}</li>
            </ul>
            <div class="form-group pa-5">

                <a href="{{route('marketing.faq.question_answer.show_questions')}}"
                   class="btn btn-danger">بازگشت</a>
            </div>
        </div>
    </div>
@endsection
