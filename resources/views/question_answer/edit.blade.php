@extends('layouts.marketing')

@section('title', ',ویرایش')

@section('desktop')

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    <!DOCTYPE html>
    <html>

    <head>
        <style>
            #cke_text-editor {
                width: 1500px;
            }
            .ck.ck-content.ck-editor__editable.ck-rounded-corners.ck-editor__editable_inline.ck-blurred p
            {
                color: black;
            }
            .ck-editor {
                max-width: 1500px;
            }
            .ck.ck-content.ck-editor__editable.ck-rounded-corners.ck-editor__editable_inline.ck-blurred p:active
            {
                color: black;
            }
        </style>
    </head>
    <div class="row">
        <div class="col-xlg-15 col-lg-15">
            <div class="panelGlass minH-240">
                <form action="{{route('marketing.faq.question_answer.update',[$question_answer->id])}}" method="POST"
                      class="form-inline" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="show_order">ترتیب نمایشی</label>
                            <input type="number" class="form-control"
                                   name="show_order" id="show_order"
                                   value="{{$question_answer->show_order}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="category_id">دسته بندی</label>
                            <select class="form-control" name="faq_category_id">
                                @foreach($categories as $category)
                                    <option @if($category->id == $question_answer->faq_category_id) selected="selected"
                                            @endif value="{{$category->id}}">
                                        {{$category->name_fa}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label mb-10"
                                   for="question_fa">پرسش</label>
                            <input type="text" class="form-control"
                                   name="question_fa" id="question_fa"
                                   value="{{$question_answer->question_fa}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label mb-10"
                                   for="question_en">پرسش انگلیسی</label>
                            <input type="text" class="form-control"
                                   name="question_en" id="question_en"
                                   value="{{$question_answer->question_en}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="width: 100%">
                            <label class="control-label mb-10"
                                   for="short_answer_fa">پاسخ کوتاه </label>
                            <input type="text" style="width: 100%" class="form-control"
                                   name="short_answer_fa" id="short_answer_fa" value=" {{$question_answer->short_answer_fa}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="width: 100%">
                            <label class="control-label mb-10"
                                   for="short_answer_en">پاسخ کوتاه انگلیسی</label>
                            <input type="text" style="width: 100%" class="form-control"
                                   name="short_answer_en" id="short_answer_en" value=" {{$question_answer->short_answer_en}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label mb-10"
                                   for="image">آپلود تصویر</label>
                            <input type="file" class="form-control"
                                   name="image" id="image"
                                   placeholder="آپلود تصویر...">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label mb-10"
                                   for="video">آپلود فیلم</label>
                            <input type="file" class="form-control"
                                   name="video" id="video"
                                   placeholder="آپلود فیلم...">
                        </div>
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="external_video">لینک وید‌‌ئو</label>
                        <input type="url" class="form-control"
                               name="external_video" id="external_video"
                               value="{{$question_answer->external_video}}">
                    </div>
                    <div class="col-md-12">
                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="complete_answer">پاسخ کامل** *</label>
                            <textarea rows="4" cols="50"
                                      name="complete_answer_fa"  id="text-editor"
                                      class="form-control">
                            {{$question_answer->complete_answer_fa}}

                            </textarea>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label mb-10"
                                   for="is_repetitive">پرتکرار است؟</label>
                            <select  class="form-control" name="is_repetitive">
                                <option value="1">پر تکرار است</option>
                                <option value="0">پرتکرار نیست</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="complete_answer_en">پاسخ کامل انگلیسی</label>
                            <textarea rows="4" cols="50"
                                      name="complete_answer_en"
                                      class="form-control">
                            {{$question_answer->complete_answer_en}}
                            </textarea>
                        </div>
                    </div>
                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button id="update-profile" type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ثبت تغییرات
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="/js/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('text-editor', {
            filebrowserUploadUrl: "{{route('marketing.faq.ckeditor.file.store-image',['_token' => csrf_token()])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
    <script src="/js/ckeditor/starter.js"></script>
@endsection
