@extends('layouts.marketing')
@section('title', 'نمایش')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li >{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif
<form action="{{route('marketing.faq.question_answer.feedback')}}" method="POST"
      class="form-inline" enctype="multipart/form-data">
    <div class="form-group pa-5">
        {{csrf_field()}}
        {{--                        <label class="control-label mb-10">آیا این پاسخ برای شما مفید بود؟</label>--}}
{{--        <div class="form-group pa-5">--}}
{{--            {{$question_answer->id}}--}}
{{--            <label class="control-label mb-10"--}}
{{--                   for="feedback">آیا این پاسخ برای شما مفید بود؟</label>--}}
{{--            <select  class="form-control" name="feedback">--}}
{{--                <option value="1"> است</option>--}}
{{--                <option value="0"> نیست</option>--}}
{{--            </select>--}}
{{--        </div>--}}
{{--       --}}
        <div class="form-group pa-10">
            <label for="faq_question_id">like</label>
            <input type="number" class="form-control"
                   name="faq_question_id" id="faq_question_id"
                   placeholder=" ...faq_question_id"
                   value="{{old('faq_question_id')}}">
        </div>
        <div class="form-group pa-10">
            <label for="user_id">like</label>
            <input type="number" class="form-control"
                   name="user_id" id="user_id"
                   placeholder=" ...user_id"
                   value="{{old('user_id')}}">
        </div>
        <div class="form-group pa-10">
            <label for="like_counter">like</label>
            <input type="number" class="form-control"
                   name="like_counter" id="like_counter"
                   placeholder=" ...like_counter"
                   value="{{old('like_counter')}}">
        </div>
        <div class="form-group pa-10">
            <label for="dislike_counter">dislike</label>
            <input type="number" class="form-control"
                   name="dislike_counter" id="dislike_counter"
                   placeholder=" ...dislike_counter"
                   value="{{old('dislike_counter')}}">
        </div>
        <div class="col-md-12 pa-20 text-center">
            <div class="form-actions mt-10">
                <button id="update-profile" type="submit"
                        class="btn btn-primary mr-10 mb-30">
                    ثبت
                </button>
            </div>
        </div>
    </div>
</form>
@endsection
