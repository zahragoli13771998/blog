@extends('layouts.marketing')

@section('title', 'ایجاد لینک')

@section('desktop')

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <div class="alert ">
                <li class="alert alert-danger">{{ $error }}</li>

            </div>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <div>
        <div class="card">
            <div class="card-body">
                <form action="{{ route('marketing.faq.question_answer.deeplink.store') }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">

                        <div class="col-sm-4">
                            <label>لینک</label>
                            <input type="text" name="link" class="form-control" value="{{ old('link') }}"
                                   placeholder="link" required>
                        </div>

                        <div class="col-sm-4">
                            <label>عنوان فارسی</label>
                            <input type="text" name="title_fa" class="form-control"
                                   value="{{ old('title_fa') }}"
                                   placeholder="title_fa" required>
                        </div>

                        <div class="col-sm-4">
                            <label>عنوان انگلیسی</label>
                            <input type="text" name="title_en" class="form-control"
                                   value="{{ old('title_en') }}"
                                   placeholder="title_en" required>
                        </div>

                        <div class="col-sm-4">
                            <label>وضعیت</label>
                            <select  class="form-control" name="status">
                                <option value="1">فعال</option>
                                <option value="0">غیر فعال</option>
                            </select>
                        </div>

                        <div class="col-sm-3">
                            <input type="hidden" name="question_answer_id" value={{$id}}>
                        </div>
                    </div>

                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ایجاد
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
