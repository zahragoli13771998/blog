
@extends('layouts.appAdmin')
@section('title', 'ایجاد سود و زیان جدید')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
        @if (session()->has('success'))
            <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
        @if (session()->has('success'))
            <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <form action="{{route('admin.currency_change_percent.store')}}" method="POST" class="form-inline" enctype="multipart/form-data">

                   {{csrf_field()}}

                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="value">مقدار</label>
                        <input type="number" class="form-control"
                               name="value"
                               placeholder="مقدار..." required>
                    </div>

                    <div class="form-group pa-10">

                        <label for="currency_id">نام ارز</label>

                        <select class="form-control" name="global_currency_id" required>
                            @foreach($currencies as $currency)

                                <option value="{{$currency->id}}">
                                    {{$currency->name_en}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group pa-10">
                        <label for="interval_id">نام اینتروال</label>
                        <select class="form-control" name="interval_id" required>
                            @foreach($intervals as $interval)
                                <option value="{{$interval->id}}">
                                    {{$interval->title_fa}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button id="update-profile" type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ایجاد سود و زیان
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
