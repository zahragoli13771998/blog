
@extends('layouts.appAdmin')
@section('title', 'ویرایش سود و زیان')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
        @if (session()->has('success'))
            <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
        @if (session()->has('error'))
            <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <form action="{{route('admin.currency_change_percent.update',[$currencyChange->id])}}" method="POST" class="form-inline">

                    {{csrf_field()}}

                    @method('PUT')

                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="value">مقدار</label>
                        <input type="number" class="form-control"
                               name="value" id="value"
                               value="{{$currencyChange->value}}">
                    </div>

                    <div class="form-group pa-10">
                        <label for="currency_id">نام ارز</label>
                        <select  class="form-control" name="currency_id">
                            <option value=""> </option>
                            @foreach($currencies as $currency)
                                <option value="{{$currency->id}}"
                                        @if($currency->id == $currencyChange->global_currency_id) selected = "selected" @endif>
                                    {{$currency->name_en}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group pa-10">
                        <label for="interval_id">نام اینتروال</label>
                        <select  class="form-control" name="interval_id">
                            <option value=""> </option>
                            @foreach($intervals as $interval)
                                <option value="{{$interval->id}}"
                                        @if($interval->id == $currencyChange->interval_id) selected = "selected" @endif>
                                    {{$interval->title_fa}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button id="update-profile" type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ویرایش سود و زیان
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
