
@extends('layouts.appAdmin')
@section('title', 'نمایش لیست سود و زیان ها')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    @if ( session()->has('error'))
        <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <form action="{{route('admin.currency_change_percent.show')}}" method="get" class="form-inline" enctype="multipart/form-data">

                        {{csrf_field()}}

                        <div class="form-group pa-10">
                            <label for="currency_id">نام ارز</label>

                            <select  class="form-control" name="global_currency_id">
                                <option value=""> </option>
                                @foreach($currencies as $currency)
                                    <option value="{{$currency->id}}">
                                        {{$currency->name_en}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group pa-10">
                            <label for="network_id">نام اینتروال</label>
                            <select  class="form-control" name="network_id">
                                <option value=""> </option>
                                @foreach($intervals as $interval)
                                    <option value="{{$interval->id}}">
                                        {{$interval->title_fa}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-12 pa-20 text-center">
                            <div class="form-actions mt-10">
                                <button id="update-profile" type="submit"
                                        class="btn btn-primary mr-10 mb-30">
                                    جستجوی نکات سود و زیان
                                </button>
                            </div>
                        </div>

                    </form>

                    <table id="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">

                                <thead>
                                <tr>
                                    <th scope="col"> مقدار</th>
                                    <th scope="col"> نام ارز</th>
                                    <th scope="col">نام اینتروال</th>
                                    <th>
                                        <a href="{{route('admin.currency_change_percent.create')}}" class="btn btn-primary">ایجاد نکات معاملات</a>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($currencyChangePercent as $change)
                                    <tr>
                                        <td class="text-sm">{{$change->value}}</td>
                                        <td class="text-sm">{{$change->currency->name_en}}</td>
                                        <td class="text-sm">{{$change->interval->title_fa}}</td>
                                        <td class="text-sm">
                                            <a href="{{route('admin.currency_change_percent.update',[$change->id])}}"
                                               class="btn btn-primary">ویرایش</a>

                                            <a href="{{route('admin.currency_change_percent.destroy',[$change->id])}}"
                                               onclick="return confirm('آیا مایل به حذف این فیلد هستید؟')"
                                               class="btn btn-danger">حذف</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{$currencyChangePercent->links()}}
@endsection



