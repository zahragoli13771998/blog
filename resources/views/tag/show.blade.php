@extends('layouts.appAdmin')
@section('title', 'نمایش تگ ها')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <table id="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">
                                <thead>
                                <tr>

                                    <th scope="col"> عنوان تگ</th>
                                    <th scope="col">related model</th>
                                    <th scope="col"></th>
                                    <th>
                                        <a href="{{route('admin.tag.global_currency.create')}}" class="btn btn-success">
                                            ایجاد تگ برای ارزها</a>
                                        <a href="{{route('admin.tag.banner.create')}}" class="btn btn-success"> ایجاد تگ
                                            برای بنرها</a>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tags as $tag)

                                    <tr>
                                        <td class="text-sm">{{$tag->title}}</td>
                                        <td class="text-sm">{{$tag->tag_type}}</td>
                                        <td class="text-sm">
                                            <a href="{{route('admin.tag.edit',[$tag->id])}}"
                                               class="btn btn-primary">ویرایش</a>
                                            <a href="{{route('admin.tag.delete',[$tag->id])}}"
                                               class="btn btn-danger">حذف</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{$tags->links()}}
@endsection



