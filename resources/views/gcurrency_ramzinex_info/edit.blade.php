@extends('layouts.appAdmin')
@section('title', 'ویرایش تغییرات رمزینکس')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    @if ( session()->has('error'))
        <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-15 col-lg-15">
            <div class="panelGlass minH-240">
                <div class="form-group pa-10">
                    <label class="control-label mb-10"
                           for="title">{{$g_currency_ramzinex_info->globalCurrency->name_en}}</label>
                </div>
                <form action="{{route('admin.ramzinex_info.update',[$g_currency_ramzinex_info->id])}}" method="POST"
                      class="form-inline">
                    {{csrf_field()}}
                    @method('PUT')

                    <div class="form-wrap">

                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="title">buy</label>
                            <input type="number" class="form-control"
                                   name="buy" id="buy"
                                   value="{{$g_currency_ramzinex_info->buy}}">
                        </div><div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="title">sell</label>
                            <input type="number" class="form-control"
                                   name="sell" id="sell"
                                   value="{{$g_currency_ramzinex_info->sell}}">
                        </div><div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="title">open</label>
                            <input type="number" class="form-control"
                                   name="open" id="open"
                                   value="{{$g_currency_ramzinex_info->open}}">
                        </div>
                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="title">close</label>
                            <input type="number" class="form-control"
                                   name="close" id="close"
                                   value="{{$g_currency_ramzinex_info->close}}">
                        </div>
                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="title">highest</label>
                            <input type="text" class="form-control"
                                   name="high" id="high"
                                   value="{{$g_currency_ramzinex_info->high}}">
                        </div>
                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="title">lowest</label>
                            <input type="text" class="form-control"
                                   name="low" id="low"
                                   value="{{$g_currency_ramzinex_info->low}}">
                        </div>
                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="title">quote volume</label>
                            <input type="text" class="form-control"
                                   name="quote_volume" id="quote_volume"
                                   value="{{$g_currency_ramzinex_info->quote_volume}}">
                        </div>
                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="title">base volume</label>
                            <input type="text" class="form-control"
                                   name="base_volume" id="base_volume"
                                   value="{{$g_currency_ramzinex_info->base_volume}}">
                        </div>
                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="title">change percent</label>
                            <input type="text" class="form-control"
                                   name="change_percent" id="change_percent"
                                   value="{{$g_currency_ramzinex_info->change_percent}}">
                        </div>

                        <div class="col-md-12 pa-20 text-center">
                            <div class="form-actions mt-10">
                                <button id="update-profile" type="submit"
                                        class="btn btn-primary mr-10 mb-30">
                                    ثبت تغییرات
                                </button>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
