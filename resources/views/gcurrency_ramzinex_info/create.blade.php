@extends('layouts.appAdmin')
@section('title', 'ایجاد تغیرات رمزینکس جدید')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    @if ( session()->has('error'))
        <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <form action="{{route('admin.ramzinex_info.store')}}" method="POST" class="form-inline">

                    {{csrf_field()}}

                    <div class="form-group pa-10">

                        <label for="currency_id">نام ارز</label>

                        <select class="form-control" name="global_currency_id" required>
                            @foreach($currencies as $currency)
                                <option value="{{$currency->id}}">
                                    {{$currency->name_en}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="title">buy</label>
                        <input type="number" class="form-control"
                               name="buy" id="buy"
                               value="">
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="title">sell</label>
                        <input type="number" class="form-control"
                               name="sell" id="sell"
                               value="">
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="title">open</label>
                        <input type="number" class="form-control"
                               name="open" id="open"
                               value="">
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="title">close</label>
                        <input type="number" class="form-control"
                               name="close" id="close"
                               value="">
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="title">high</label>
                        <input type="text" class="form-control"
                               name="high" id="high"
                               value="">
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="title">low</label>
                        <input type="text" class="form-control"
                               name="low" id="low"
                               value="">
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="title">quote volume</label>
                        <input type="text" class="form-control"
                               name="quote_volume" id="quote_volume"
                               value="">
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="title">base volume</label>
                        <input type="text" class="form-control"
                               name="base_volume" id="base_volume"
                               value="">
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="title">change percent</label>
                        <input type="text" class="form-control"
                               name="change_percent" id="change_percent"
                               value="">
                    </div>

                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button id="update-profile" type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ایجاد تغییرات
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection



