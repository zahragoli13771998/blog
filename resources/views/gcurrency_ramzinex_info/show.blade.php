@extends('layouts.appAdmin')
@section('title', 'نمایش لیست تغییرات رمزینکس')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    @if ( session()->has('error'))
        <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <table id="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">

                                <thead>
                                <th>
                                    <a href="{{route('admin.ramzinex_info.create')}}" class="btn btn-primary">ایجاد
                                        تغییرات</a>
                                </th>
                                <tr>
                                    <th scope="col">buy</th>
                                    <th scope="col">sell</th>
                                    <th scope="col">name</th>
                                    <th scope="col">open</th>
                                    <th scope="col">close</th>
                                    <th scope="col">highest</th>
                                    <th scope="col">lowest</th>
                                    <th scope="col">quote volume</th>
                                    <th scope="col">base volume</th>
                                    <th scope="col">change percent</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($g_currencies_ramzinex_info as $g_currency_ramzinex_info)
                                    <tr>
                                        <td class="text-sm">{{$g_currency_ramzinex_info->globalCurrency->name_en}}</td>
                                        <td class="text-sm">{{$g_currency_ramzinex_info->buy}}</td>
                                        <td class="text-sm">{{$g_currency_ramzinex_info->sell}}</td>
                                        <td class="text-sm">{{$g_currency_ramzinex_info->open}}</td>
                                        <td class="text-sm">{{$g_currency_ramzinex_info->close}}</td>
                                        <td class="text-sm">{{$g_currency_ramzinex_info->high}}</td>
                                        <td class="text-sm">{{$g_currency_ramzinex_info->low}}</td>
                                        <td class="text-sm">{{$g_currency_ramzinex_info->quote_volume}}</td>
                                        <td class="text-sm">{{$g_currency_ramzinex_info->base_volume}}</td>
                                        <td class="text-sm">{{$g_currency_ramzinex_info->change_percent}}</td>
                                        <td class="text-sm">
                                            <a href="{{route('admin.ramzinex_info.edit',[$g_currency_ramzinex_info->id])}}"
                                               class="btn btn-primary">ویرایش</a>

                                            <a href="{{route('admin.ramzinex_info.destroy',[$g_currency_ramzinex_info->id])}}"
                                               onclick="return confirm('آیا مایل به حذف این فیلد هستید؟')"
                                               class="btn btn-danger">حذف</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{$g_currencies_ramzinex_info->links()}}
@endsection



