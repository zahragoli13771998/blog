@extends('layouts.appAdmin')
@section('title', 'ویرایش ارز')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session('status'))
            <div class="alert alert-success">{{ session('status') }}</div>
        @endif
        <form action="{{ route('admin.global-currencies.edit',[$global_currency]) }}" enctype="multipart/form-data" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="title">rank</label>
                <input type="number" class="form-control" id="content" name="rank" value="{{$global_currency->rank}}">
            </div>

            <div class="form-group">
                <label for="title">name en</label>
                <input type="text" class="form-control" id="title" name="name_en" value="{{$global_currency->name_en}}">
            </div>

            <div class="form-group">
                <label for="title">name fa</label>
                <input type="text" class="form-control" id="title" name="name_fa" value="{{$global_currency->name_fa}}">
            </div>

            <div class="form-group">
                <label for="title">symbol</label>
                <input type="text" class="form-control" id="symbol" name="symbol" value="{{$global_currency->symbol}}">
            </div>

            <div class="form-group">
                <label for="title">price precision</label>
                <input type="" class="form-control" id="title" name="price_precision" value="{{$global_currency->price_precision}}">
            </div>

            <div class="form-group">
                <label for="title">price</label>
                <input type="" class="form-control" id="title" name="price" value="{{$global_currency->price}}">
            </div>

            <div class="form-group">
                <label for="title">btc_price</label>
                <input type="" class="form-control" id="title" name="btc_price" value="{{$global_currency->btc_price}}">
            </div>


            <div class="form-group">
                <label for="title">total supply</label>
                <input type="int" class="form-control" id="total_supply" name="total_supply"
                       value="{{$global_currency->total_supply}}">
            </div>
            <div class="form-group">
                <label for="">pair_id</label>
                <input type="number" class="form-control" min="1" step="1" value="{{$global_currency->pair_id}}" name="pair_id">
            </div>

            <div class="form-group">
                <label for="currency_id">currency_id</label>
                <input type="number" class="form-control" min="1" step="1" name="currency_id">
            </div>

            <div class="form-group">
                <label for="title">total volume</label>
                <input type="int" class="form-control" id="total_volume" name="total_volume"
                       value="{{$global_currency->total_volume}}">
            </div>

            <div class="form-group">
                <label for="title">market dominance</label>
                <input type="int" class="form-control" id="market_dominance" name="market_dominance"
                       value="{{$global_currency->market_dominance}}">
            </div>

            <div class="form-group">
                <label for="title">daily trading volume</label>
                <input type="int" class="form-control" id="daily_trading_volume" name="daily_trading_volume"
                       value="{{$global_currency->daily_trading_volume}}">
            </div>

            <div class="form-group">
                <label for="title">coin_circulation</label>
                <input type="int" class="form-control" id="coin_circulation" name="coin_circulation"
                       value="{{$global_currency->coin_circulation}}">
            </div>

            <div class="form-group">
                <label for="title">icon file</label>
                <input type="file" class="form-control" id="icon" name="icon">
            </div>

            <div class="form-group">
                <label for="title">Description Fa</label>
                <textarea class="form-control" name="description_fa">{{$global_currency->description_fa}}</textarea>
            </div>

            <div class="form-group">
                <label for="title">Description en</label>
                <textarea class="form-control" name="description_en">{{$global_currency->description_en}}</textarea>
            </div>

            <button type="submit" class="btn btn-primary">update</button>
        </form>
    </div>
@endsection

