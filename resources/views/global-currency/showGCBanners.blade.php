@extends('layouts.appAdmin')
@section('title', 'نمایش لیست ارزها')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif
    @if(session('status'))
        <div class="alert alert-success">{{ session('status') }}</div>
    @endif
    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <a href="{{ route('admin.global-currencies.edit',$global_currency->id) }}"
                       class="btn btn-primary btn-sm"><span>{{__('edit') }}</span></a>
                    <a href="{{route('admin.global-currencies.add_banners',[$global_currency->id])}}"
                       class="btn btn-success btn-sm">تخصیص بنر به ارزها</a>
                    <a href="{{route('admin.global-currencies.index')}}" class="btn btn-primary btn-sm">نمایش تمام ارز
                        ها</a>
                </div>
                <table id="bonusTable" class="table mediumThead">
                    <div class="row">
                        <div class="form-wrap">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">رتبه</th>
                                <th scope="col"> نام انگلیسی ارز</th>
                                <th scope="col"> نام فارسی ارز</th>
                                <th scope="col"> نماد</th>
                                <th scope="col">pair_id</th>
                                <th scope="col">currency_id</th>
                                <th scope="col">بنرها</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-sm">{{$global_currency->id ?? '--'}}</td>
                                <td class="text-sm">{{$global_currency->rank ?? '--'}}</td>
                                <td class="text-sm"><a
                                        href="{{route('admin.global-currencies.show',$global_currency->id)}}"
                                        class="text-primary">{{$global_currency->name_en}}</a></td>
                                <td class="text-sm">{{$global_currency->name_fa ?? '--' }}</td>
                                <td class="text-sm">{{$global_currency->symbol ?? '--' }}</td>
                                <td class="text-sm">{{$global_currency->pair_id ?? '--'}}</td>
                                <td class="text-sm">{{$global_currency->currency_id ?? '--'}}</td>
                                @if($global_currency->banners)
                                    @foreach($global_currency->banners as $banner)
                                        <td class="text-sm">{{$banner->title['fa']?? '--'}}</td>
                                    @endforeach
                                @endif
                            </tr>
                            </tbody>
                        </div>
                    </div>
                </table>
            </div>
        </div>
    </div>
    </div>
@endsection
