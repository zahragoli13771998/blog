<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
        body {
            height: 95vh;
        }
    </style>
    <title>TrendingView</title>
</head>
<body>
<div class="tredingview-widget-container">
    <div id="trending_view"></div>
</div>
<script>
    function loadScript(url, callback) {
        var script = document.createElement("script");
        script.type = "text/javascript";
        if (script.readyState) {
            script.onreadystatechange = function () {
                if (script.readyState === "loaded" || script.readyState === "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {
            script.onload = function () {
                callback();
            };
        }
        script.setAttribute("src", url);
        script.setAttribute("id", "world-chart");
        document.getElementsByTagName("head")[0].appendChild(script);
    }
    loadScript("https://s3.tradingview.com/tv.js", function () {
        new TradingView.widget({
            width: "100%",
            height: "100%",
            @if($symbol=='REV' || $symbol== 'TEL' || $symbol=='BSV')
            symbol: "kucoin:{{$symbol}}USDT",
            @elseif($symbol=='SAFEMOON')
            symbol: "bitforex:{{$symbol}}USDT",
            @elseif($symbol=='DAI')
            symbol: "binance:{{"USDT"}}DAI",
            @else
            symbol: "binance:{{$symbol}}USDT",
            @endif
            interval: "60",
            timezone: "Asia/Tehran",

            @if(isset($theme) && $theme=='dark')
            theme: "Dark",
            @endif
            style: "1",
            locale: "en",
            //toolbar_bg: "#f1f3f6",
            enable_publishing: false,
            hide_side_toolbar: false,
            allow_symbol_change: true,
            container_id: "trending_view",
        });
    });
</script>
</body>
</html>
