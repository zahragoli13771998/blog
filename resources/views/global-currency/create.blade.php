@extends('layouts.appAdmin')
@section('title', 'ایجاد ارز جدید')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="container">
        <div class="mb-4 d-flex align-items-center justify-content-between">
            <h1>New Currency</h1>
            <a href="{{ route('admin.global-currencies.index') }}" class="btn btn-secondary btn-lg btn-block">back</a>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('admin.global-currencies.create') }}" enctype="multipart/form-data" method="post">
            @csrf

            <div class="form-group">
                <label for="title">rank</label>
                <input type="number" class="form-control" id="content" name="rank" value="{{old('rank', null)}}">
            </div>

            <div class="form-group">
                <label for="title">name persian</label>
                <input type="text" class="form-control" id="title" name="name_fa" value="{{old('name_fa', null)}}">
            </div>

            <div class="form-group">
                <label for="title">name english</label>
                <input type="text" class="form-control" id="title" name="name_en" value="{{old('name_en', null)}}">
            </div>

            <div class="form-group">
                <label for="title">symbol</label>
                <input type="text" class="form-control" id="symbol" name="symbol" value="{{old('symbol', null)}}">
            </div>

            <div class="form-group">
                <label for="title">price precision</label>
                <input type="" class="form-control" id="title" name="price_precision">
            </div>

            <div class="form-group">
                <label for="title">price</label>
                <input type="" class="form-control" id="title" name="price" value="{{old('price', null)}}">
            </div>

            <div class="form-group">
                <label for="title">btc_price</label>
                <input type="" class="form-control" id="title" name="btc_price" value="{{old('btc_price', null)}}">
            </div>

            <div class="form-group">
                <label for="title">total supply</label>
                <input type="int" class="form-control" id="total_supply" name="total_supply" value="{{old('total_supply', null)}}">
            </div>
            <div class="form-group">
                <label for="pair_id">Pair Id</label>
                <input type="number" class="form-control" min="1" step="1" name="pair_id">
            </div>
            <div class="form-group">
                <label for="currency_id">currency id</label>
                <input type="number" class="form-control" min="1" step="1" name="currency_id">
            </div>
            <div class="form-group">
                <label for="title">total volume</label>
                <input type="int" class="form-control" id="total_supply" name="total_volume" value="{{old('total_volume', null)}}">
            </div>

            <div class="form-group">
                <label for="title">market dominance</label>
                <input type="int" class="form-control" id="market_dominance" name="market_dominance" value="{{old('market_dominance')}}">
            </div>

            <div class="form-group">
                <label for="title">daily trading volume</label>
                <input type="int" class="form-control" id="daily_trading_volume" name="daily_trading_volume" value="{{old('daily_trading_volume')}}">
            </div>

            <div class="form-group">
                <label for="title">coin_circulation</label>
                <input type="int" class="form-control" id="coin_circulation" name="coin_circulation" value="{{old('coin_circulation')}}">
            </div>

            <div class="form-group">
                <label for="title">icon file</label>
                <input type="file" class="form-control" id="icon" name="icon">
            </div>

            <div class="form-group">
                <label for="title">Description Fa</label>
                <textarea class="form-control" name="description_fa">{{old('description_fa')}}</textarea>
            </div>

            <div class="form-group">
                <label for="title">Description en</label>
                <textarea class="form-control" name="description_en">{{old('description_en')}}</textarea>
            </div>

            <button type="submit" class="btn btn-primary">ADD</button>
        </form>
    </div>
@endsection

