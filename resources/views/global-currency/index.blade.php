@extends('layouts.appAdmin')
@section('title', 'نمایش لیست ارزها')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif
    @if(session('status'))
        <div class="alert alert-success">{{ session('status') }}</div>
    @endif
    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>

                    <form action="{{route('admin.global-currencies.index')}}" method="GET" class="form-inline">
                        <div class="search-container">
                            <label>نماد :</label><br>
                            <input type="search" placeholder="جستجو..."
                                   name="request_symbol"
                                   value="{{old('symbol')}}">
                        </div>
                        <div class="search-container">
                            <label>عنوان فارسی :</label><br>
                            <input type="search" placeholder="جستجو..."
                                   name="request_name_fa"
                                   value="{{old('name_fa')}}">
                        </div>
                        <div class="search-container">
                            <label>عنوان انگلیسی :</label><br>
                            <input type="search" placeholder="جستجو..."
                                   name="request_name_en"
                                   value="{{old('name_en')}}">
                        </div>
                        <select class="form-select" aria-label="Default select example" name="sort_by">
                            <option selected>مرتب سازی بر اساس...</option>
                            <option value="includes">درخواست های افزوده شدن به رمزینکس</option>
                        </select>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="ramzinex_currencies"
                                   id="flexRadioDefault1" value="1">
                            <label class="form-check-label" for="flexRadioDefault1">
                                ارز های موجود در رمزینکس
                            </label>
                        </div>
                        <button type="submit">ثبت</button>
                    </form>
                </div>
                <div>
                    <a href="{{route('admin.global-currencies.index')}}" class="btn btn-primary btn-sm">نمایش تمام ارز ها</a>
                        </div>

                <table id="bonusTable" class="table mediumThead">
                    <div class="row">
                        <div class="form-wrap">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">رتبه</th>
                                <th scope="col"> نام انگلیسی ارز</th>
                                <th scope="col"> نام فارسی ارز</th>
                                <th scope="col"> نماد</th>
                                <th scope="col">قیمت</th>
                                <th scope="col">قیمت بیت کوین</th>
                                <th scope="col">ارزش کل بازار</th>
                                <th scope="col">حجم معاملات روزانه</th>
                                <th scope="col">تسلط به بازار</th>
                                <th scope="col">عرضه کل</th>
                                <th scope="col">سکه در گردش</th>
                                <th scope="col">pair_id</th>
                                <th scope="col">currency_id</th>
                                <th scope="col">addtoramzinex</th>
                                <th scope="col">icon</th>
                                <th scope="col">حذف</th>
                                <th scope="col">ویرایش</th>
                                <th scope="col">عدم نمایش</th>
                                <th scope="col"></th>
                                <th scope="col">show banner_gc </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($global_currencies as $global_currency)
                                <tr>

                                    <td class="text-sm">{{$global_currency->id ?? '--'}}</td>
                                    <td class="text-sm">{{$global_currency->rank ?? '--'}}</td>
                                    <td class="text-sm"><a
                                            href="{{route('admin.global-currencies.show',$global_currency->id)}}"
                                            class="text-primary">{{$global_currency->name_en}}</a></td>
                                    <td class="text-sm">{{$global_currency->name_fa ?? '--' }}</td>
                                    <td class="text-sm">{{$global_currency->symbol ?? '--' }}</td>
                                    <td class="text-sm">{{$global_currency->price}}</td>
                                    <td class="text-sm">{{$global_currency->btc_price}}</td>
                                    <td class="text-sm">{{$global_currency->total_volume ?? '--' }}</td>
                                    <td class="text-sm">{{$global_currency->daily_trading_volume ?? '--' }}</td>
                                    <td class="text-sm">{{$global_currency->market_dominance ?? '--' }}</td>
                                    <td class="text-sm">{{$global_currency->total_supply ?? '--'}}</td>
                                    <td class="text-sm">{{$global_currency->coin_circulation ?? '--'}}</td>
                                    <td class="text-sm">{{$global_currency->pair_id ?? '--'}}</td>
                                    <td class="text-sm">{{$global_currency->currency_id ?? '--'}}</td>
                                    <td class="text-sm">{{$global_currency->includes_count}}</td>
                                    <td class="row">
{{--                                        <img width="20px" src="{{$global_currency->icon}}" alt="icon image">--}}
                                        {{$global_currency->icon}}
                                    </td>
                                    <td>
                                        <form action="{{route('admin.global-currencies.destroy', $global_currency)}}"
                                              method="post">
                                            {{csrf_field()}}
                                            @method('DELETE')
                                            <button class="btn btn-danger btn-sm"
                                                    onclick="return confirm('are you sure ?')">Delete Coin
                                            </button>
                                        </form>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.global-currencies.edit',$global_currency->id) }}"
                                           class="btn btn-primary btn-sm"><span>{{__('edit') }}</span></a>
                                    </td>
                                    <td>
                                        <a class="btn btn-primary btn-sm"
                                           @if($global_currency->is_hidden == 1)  href="{{ route('admin.global-currencies.make_hidden',[$global_currency->id]) }}"
                                           disabled
                                           @endif
                                           href="{{ route('admin.global-currencies.make_hidden',[$global_currency->id]) }}">
                                        <span class="iconify" data-icon="bi-eye-slash"
                                              data-inline="false"></span>

                                        </a>
                                        <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
                                    </td>
                                    <td>
                                        <a href="{{route('admin.global-currencies.add_banners',[$global_currency->id])}}"
                                           class="btn btn-success btn-sm">تخصیص بنر به ارزها</a>
                                    </td>
                                    <td>
                                        <a href="{{route('admin.global-currencies.show_banner_gc',[$global_currency->id])}}" class="btn btn-primary btn-sm">show banner gc</a>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                            {{$global_currencies->appends(request()->all())->links()}}
                        </div>
                    </div>
                </table>
            </div>
        </div>
    </div>
    </div>
@endsection
