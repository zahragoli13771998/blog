@extends('layouts.appAdmin')
@section('title', 'نمایش ارز')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif
    @if(session('status'))
        <div class="alert alert-success">{{ session('status') }}</div>
    @endif
    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <table id="bonusTable" class="table mediumThead">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">رتبه</th>
                            <th scope="col"> نام انگلیسی ارز</th>
                            <th scope="col"> نام فارسی ارز</th>
                            <th scope="col"> نماد </th>
                            <th scope="col">قیمت</th>
                            <th scope="col">قیمت بیت کوینی</th>
                            <th scope="col">قیمت ریالی</th>
                            <th scope="col">ارزش کل بازار</th>
                            <th scope="col">حجم معاملات روزانه</th>
                            <th scope="col">تسلط به بازار</th>
                            <th scope="col">عرضه کل</th>
                            <th scope="col">سکه در گردش</th>
                            <th scope="col">حذف</th>
                            <th scope="col">ویرایش</th>
                            <th scope="col"> تغیرات ارز</th>
                            <th scope="col"> pair_id</th>
                            <th scope="col"> currency_id</th>


                        </tr>
                        </thead>
                        <tr>
                            <td class="text-sm">{{$global_currency->id ?? '--'}}</td>
                            <td class="text-sm">{{$global_currency->rank ?? '--'}}</td>
                            <td class="text-sm"><a href="{{route('admin.global-currencies.show',$global_currency->id)}}"
                                                   class="text-primary">{{$global_currency->name_en}}</a></td>
                            <td class="text-sm">{{$global_currency->name_fa ?? '--' }}</td>
                            <td class="text-sm">{{$global_currency->symbol ?? '--' }}</td>
                            <td class="text-sm">{{$global_currency->price['usd'] ?? '--' }}</td>
                            <td class="text-sm">{{$global_currency->price['btc'] ?? '--' }}</td>
                            <td class="text-sm">{{$global_currency->rial_price ?? '--' }}</td>
                            <td class="text-sm">{{$global_currency->total_volume ?? '--' }}</td>
                            <td class="text-sm">{{$global_currency->daily_trading_volume ?? '--' }}</td>
                            <td class="text-sm">{{$global_currency->market_dominance ?? '--' }}</td>
                            <td class="text-sm">{{$global_currency->total_supply ?? '--'}}</td>
                            <td class="text-sm">{{$global_currency->coin_circulation ?? '--'}}</td>
                            <td class="text-sm">{{$global_currency->pair_id ?? '--'}}</td>
                            <td class="text-sm">{{$global_currency->currency_id ?? '--'}}</td>

                            <td>

                                <form action="{{route('admin.global-currencies.destroy', $global_currency)}}" method="post">
                                    {{csrf_field()}}
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm"
                                            onclick="return confirm('are you sure ?')">Delete Coin
                                    </button>
                                </form>
                            </td>
                            <td>
                                <a href="{{ route('admin.global-currencies.edit',$global_currency) }}"
                                   class="btn btn-primary btn-sm"><span>{{__('edit') }}</span></a>
                            </td>
                            <td class="text-sm">
                                @foreach($global_currency->changePercent as $change)
                                    {{$change->value}}::{{$change->interval->title_fa}}<a
                                            href="{{route('admin.currency_change_percent.edit',$change->id)}}"
                                            class="btn btn-primary btn-sm">
                                        <span>{{__('edit') }}</span></a>
                                    <form action="{{route('admin.currency_change_percent.destroy', $change->id)}}"
                                          method="post">
                                        {{csrf_field()}}
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm"
                                                onclick="return confirm('are you sure ?')">Delete Change Percent
                                        </button>
                                    </form>
                                @endforeach
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
