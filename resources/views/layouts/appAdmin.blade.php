@extends('layouts.desktop')

@section('sidebar')
    <div class="sidebar-top big-img">
        <div class="user-image cursor-pointer">
            <img src="{{ asset('img/logo-coin.png') }}" class="img-responsive img-circle flip animatex"
                 alt="friend 8">
        </div>
    </div>
    <ul class="nav nav-sidebar">
        <li>
            <a href="{{route('admin.dashboard')}}"><i
                    class="sidebar-icon flaticon-dashboard"></i><span>داشبورد مدیر</span>
            </a>
        </li>

{{--        <li>--}}
{{--            <a href="{{route('admin.dashboard_resume')}}"><i--}}
{{--                    class="sidebar-icon flaticon-dashboard"></i><span>رزومه</span>--}}
{{--            </a>--}}
{{--        </li>--}}
        <li>
            <a href="{{route('admin.global-currencies.index')}}"><i
                    class="sidebar-icon flaticon-bitcoin"></i><span>ارز های دیجیتال</span>
            </a>
        </li>
        <li>
            <a href="{{route('admin.banner.show')}}"><i
                    class="sidebar-icon flaticon-banknote"></i><span>بنر ها</span>
            </a>
        </li>
        <li>
            <a href="{{route('admin.currency_change_percent.show')}}"><i
                    class="sidebar-icon flaticon-transfer"></i><span>تغییرات ارز ها</span>
            </a>
        </li>
        <li>
            <a href="{{route('admin.intervals.show')}}">
                <span>بازه های زمانی</span>
            </a>
        </li>
{{--        <li>--}}
{{--            <a href="{{route('admin.faq.category.show_categories')}}">--}}
{{--                <i class="sidebar-icon flaticon-secure-shield"></i>--}}
{{--                <span>دسته بندی سوالات</span>--}}
{{--            </a>--}}
{{--        </li>--}}
{{--        <li>--}}
{{--            <a href="{{route('admin.faq.question_answer.show_questions')}}">--}}
{{--                <span>سوالات و جواب ها</span>--}}
{{--            </a>--}}
{{--        </li>--}}
        <li>
            <a href="{{route('admin.comments.show')}}">
                <i class="sidebar-icon flaticon-banknote"></i>
                <span>نظرات</span>
            </a>
        </li>
        <li>
            <a href="{{route('admin.comments.survey')}}">
                <i class="sidebar-icon flaticon-banknote"></i>
                <span>نظر سنجی</span>
            </a>
        </li>


        <li>
            <a href="{{route('admin.looser.show')}}">
                <span>Loosers</span>
            </a>
        </li>
        <li>
            <a href="{{route('admin.gainer.show')}}">
                <span>Gainers</span>
            </a>
        </li>
        <li>
            <a href="{{route('admin.trending_currency.show')}}">
                <span>Currency Trends</span>
            </a>
        </li>
        <li>
            <a href="{{route('admin.trend.show')}}">
                <span>Trends</span>
            </a>
        </li>
        <li>
            <a href="{{route('admin.sector.list')}}">
                <span>sectors</span>
            </a>
        </li>
        <li>
            <a href="{{route('admin.currency.show')}}">
                <span>Ramzinex currencies</span>
            </a>
        </li>
        <li>
            <a href="{{route('admin.game-users.index')}}">
                <span>game</span>
            </a>
        </li>
    </ul>
@endsection
