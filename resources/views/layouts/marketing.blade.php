@extends('layouts.desktop')

@section('sidebar')
    <div class="sidebar-top big-img">
        <div class="user-image cursor-pointer">
            <img src="{{ asset('img/logo-coin.png') }}" class="img-responsive img-circle flip animatex"
                 alt="friend 8">
        </div>
    </div>
    <ul class="nav nav-sidebar">

        <li>
            <a href="{{route('marketing.faq.category.show_categories')}}">
                <i class="sidebar-icon flaticon-dashboard"></i>
                <span>دسته بندی</span>
            </a>
        </li>
        <li>

            <a href="{{route('marketing.faq.question_answer.show_questions')}}">
                <i class="sidebar-top icheckbox_flat-green"></i>
                <span>سوالات و جواب ها</span>
            </a>
        </li>


    </ul>
@endsection
