@extends('site_layout.master')
@section('body')
<body class="offers-controller show" id="show-430488">
    <div class="header-component component" id="section-88535">
        <div class="header-cover-image white-center-logo" style="background-image: url(#39;/img/background_header.jpg&#39;);">
            <div class="overlay" style="background-color: #273952; opacity: 0.6;"></div>
        </div>
        <div class="white-center-logo">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container">
                    <h1 class="brand">
                        <a href="https://ramzinex.com/"> <img src="http://127.0.0.1:8000/img/logo-coin.png" class="img-responsive img-circle flip animatex mCS_img_loaded" alt="friend 8">
                        </a></h1>
                    <div class="pull-right hidden-xs hidden-sm">
                        <button class="navbar-toggle" data-target="#navbar-collapse" data-toggle="collapse" type="button">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="pull-left hidden-xs hidden-sm">
                        <a class="company-link btn btn-primary hidden-xs" href="https://ramzinex.com/">    وبسایت شرکت    <i class="fa fa-arrow-circle-right"></i>
                        </a>
                        <ul class="nav navbar-nav socials hidden-xs navbar-right">
                            <li>
                                <a target="_blank" href="https://www.linkedin.com/company/ramzinex/"> <i class="fa fa-linkedin-square"></i>
                                </a></li>
                            <li>
                                <a target="_blank" href="https://twitter.com/Ramzinex_com"> <i class="fa fa-twitter-square"></i>
                                </a></li>
                            <li>
                                <a target="_blank" href="https://www.instagram.com/ramzinex_com/"> <i class="fa fa-instagram"></i>
                                </a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="content container">
                <img src="http://127.0.0.1:8000/img/1619013027577.jfif" class="img-responsive flip animatex mCS_img_loaded" alt="friend 8"  style="background-color: #273952; opacity: 0.2; width: 100%">
                <div class="centered" style=""><h1>{{$job_vacancy->title}}</h1></div>
                <div class="bottom-center"><h4>
                <ul>
                        <i class="ion ion-ios-people"></i>  <strong>{{$job_vacancy->jobtype->title}}</strong><br>

                        <i class="ion ion-location"></i> تهران
                </ul>
                    </h4>
                </div>
                <div class="info-container">
                    <div class="info">


                    </div>
                </div>
            </div>
        </div>

    <div class="col-sm-12">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
    </div>

        <div class="breadcrumbs">
            <div class="container">
                <ul>
                    <li>
                        <a href="{{route('jobvacancy.list')}}">لیست موقعیت شغلی های فعال در رمزینکس</a>
                    </li>
                    <li>
                        <a class="active" >{{$job_vacancy->title}}</a></li>
                </ul>
            </div>
        </div>
        <div class="spacing"></div>
    </div>
    <section>
        <div class="offer-component">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 content">
                        <h2 class="title">
                            {{$job_vacancy->title}}
                        </h2>
                        <h3 class="alted">
                            شرح موقعیت شغلی
                        </h3>
                        <div class="description">
                            <p>{{$job_vacancy->description}}</p>
                            <p><br></p>
                        </div>
                        <h3 class="alted">
                            حداقل سابقه
                        </h3>
                        <div class="description">
                            <ul><li>{{$job_vacancy->min_experience}}</li></ul><p><br></p>
                            <p><br></p>
                        </div>
                        <h3 class="alted">
                            حقوق
                        </h3>
                        <div class="description">
                            <ul><li>{{$job_vacancy->salary}}</li></ul><p><br></p>
                            <p><br></p>
                        </div>
                        <h3 class="alted">
                            حداقل مدرک
                        </h3>
                        <div class="description">
                            <ul><li>{{$job_vacancy->degree->title}}</li></ul><p><br></p>
                            <p><br></p>
                        </div>
                        <div class="apply hidden-xs hidden-sm">
                            <a class="btn btn-lg btn-primary" href="{{ route('request.create',$job_vacancy->id)}}">ارسال در خواست برای این کار</a>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="sidebar">
                            <a class="btn btn-thebiggest" href="{{ route('request.create',$job_vacancy->id)}}">ارسال در خواست برای این کار</a>

                            <div class="socials-share">
                                <h4>
                                    اشتراک گذاری این موقعیت شغلی
                                </h4>
                                <div class="share clearfix">
                                    <ul class="rrssb-buttons clearfix rrssb-1">
                                        <li class="rrssb-linkedin" data-initwidth="33.333333333333336" data-size="57" style="width: calc(100% - 84px);">
                                            <a class="popup" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A%2F%2Fcareer.snapp.ir%2Fjob%2Fsenior-backend-software-engineer-fintech-java&amp;title=Senior%20Backend%20Software%20Engineer%20(Fintech-%20Java)%20Snapp&amp;summary=Senior%20Backend%20Software%20Engineer%20(Fintech-%20Java)%20Snapp">
      <span class="rrssb-icon">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28">
          <path d="M25.424 15.887v8.447h-4.896v-7.882c0-1.98-.71-3.33-2.48-3.33-1.354 0-2.158.91-2.514 1.802-.13.315-.162.753-.162 1.194v8.216h-4.9s.067-13.35 0-14.73h4.9v2.087c-.01.017-.023.033-.033.05h.032v-.05c.65-1.002 1.812-2.435 4.414-2.435 3.222 0 5.638 2.106 5.638 6.632zM5.348 2.5c-1.676 0-2.772 1.093-2.772 2.54 0 1.42 1.066 2.538 2.717 2.546h.032c1.71 0 2.77-1.132 2.77-2.546C8.056 3.593 7.02 2.5 5.344 2.5h.005zm-2.48 21.834h4.896V9.604H2.867v14.73z"></path>
        </svg>
      </span>
                                                <span class="rrssb-text">
        Linkedin
      </span>
                                            </a>
                                        </li>
                                        <li class="rrssb-facebook small" data-initwidth="33.333333333333336" data-size="58" style="width: 42px;">
                                            <a class="popup" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fcareer.snapp.ir%2Fjob%2Fsenior-backend-software-engineer-fintech-java">
      <span class="rrssb-icon">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 29">
          <path d="M26.4 0H2.6C1.714 0 0 1.715 0 2.6v23.8c0 .884 1.715 2.6 2.6 2.6h12.393V17.988h-3.996v-3.98h3.997v-3.062c0-3.746 2.835-5.97 6.177-5.97 1.6 0 2.444.173 2.845.226v3.792H21.18c-1.817 0-2.156.9-2.156 2.168v2.847h5.045l-.66 3.978h-4.386V29H26.4c.884 0 2.6-1.716 2.6-2.6V2.6c0-.885-1.716-2.6-2.6-2.6z"></path>
        </svg>
      </span>
                                                <span class="rrssb-text">
        Facebook
      </span>
                                            </a>
                                        </li>
                                        <li class="rrssb-twitter small" data-initwidth="33.333333333333336" data-size="45" style="width: 42px;">
                                            <a class="popup" href="https://twitter.com/intent/tweet?text=Senior%20Backend%20Software%20Engineer%20(Fintech-%20Java)%20Snapp%20https%3A%2F%2Fcareer.snapp.ir%2Fjob%2Fsenior-backend-software-engineer-fintech-java">
      <span class="rrssb-icon">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28">
          <path d="M24.253 8.756C24.69 17.08 18.297 24.182 9.97 24.62a15.093 15.093 0 0 1-8.86-2.32c2.702.18 5.375-.648 7.507-2.32a5.417 5.417 0 0 1-4.49-3.64c.802.13 1.62.077 2.4-.154a5.416 5.416 0 0 1-4.412-5.11 5.43 5.43 0 0 0 2.168.387A5.416 5.416 0 0 1 2.89 4.498a15.09 15.09 0 0 0 10.913 5.573 5.185 5.185 0 0 1 3.434-6.48 5.18 5.18 0 0 1 5.546 1.682 9.076 9.076 0 0 0 3.33-1.317 5.038 5.038 0 0 1-2.4 2.942 9.068 9.068 0 0 0 3.02-.85 5.05 5.05 0 0 1-2.48 2.71z"></path>
        </svg>
      </span>
                                                <span class="rrssb-text">
        Twitter
      </span>
                                            </a>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </body>
@endsection
