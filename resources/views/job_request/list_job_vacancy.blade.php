@extends('site_layout.master')
@section('body')
<body class="offers-controller index" id="index">
    <div id="components-container">
        <div class="header-component component" id="section-88535">
            <div class="header-cover-image white-center-logo" style="background-image:  url(#39;/img/background_header.jpg&#39;);">
                <div class="overlay" style="background-color: #273952; opacity: 0.6;"></div>
            </div>
            <div class="white-center-logo">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <h1 class="brand">
                            <a href="https://ramzinex.com/"> <img src="http://127.0.0.1:8000/img/logo-coin.png" class="img-responsive img-circle flip animatex mCS_img_loaded" alt="friend 8">
                            </a></h1>
                        <div class="pull-right">
                        </div>
                        <div class="pull-right hidden-xs hidden-sm">
                            <button class="navbar-toggle" data-target="#navbar-collapse" data-toggle="collapse" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="pull-left hidden-xs hidden-sm">
                            <a class="company-link btn btn-primary hidden-xs" href="https://ramzinex.com/">    وبسایت شرکت    <i class="fa fa-arrow-circle-right"></i>
                            </a>
                            <ul class="nav navbar-nav socials hidden-xs navbar-right">
                                <li>
                                    <a target="_blank" href="https://www.linkedin.com/company/ramzinex/"> <i class="fa fa-linkedin-square"></i>
                                    </a></li>
                                <li>
                                    <a target="_blank" href="https://twitter.com/Ramzinex_com"> <i class="fa fa-twitter-square"></i>
                                    </a></li>
                                <li>
                                    <a target="_blank" href="https://www.instagram.com/ramzinex_com/"> <i class="fa fa-instagram"></i>
                                    </a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="content container">
                    <div class="info-container">
                        <div class="info">
                            <div class="overlay" style="background-color: #273952; opacity: 0.6;"></div>
                            <h2>
                                <img src="http://127.0.0.1:8000/img/1619013027577.jfif" class="img-responsive flip animatex mCS_img_loaded" alt="friend 8"  style="background-color: #273952; opacity: 0.6; width: 100%">
                            </h2>
                            <h3>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="spacing"></div>
        </div>

        <div class="text-component component" id="section-88536">
            <div class="no-photo container">
                <h3 class="section-title">
                    درباره ما</h3>
                <div class="text row">
                    <div class="body col-xs-12">
                        <h3 class="text-justify text-center">
                            <span style="font-size: 16px;line-height: 5;"><span></span><span></span><span></span>ما جمعی از فارغ التحصیلانِ مالی، امنیت و ام بی ای دانشگاه صنعتی شریف در راستای حذف واسطه و تجارت آزاد ارز های دیجیتال بر آن شدیم ‌، بکوشیم بستری ایمن راحت و پیشرفته را به شما معرفی کنیم؛ در همین راستا با خرید و فروش ارز های دیجیتال پا به این عرصه گذاشته ایم</span>
                        </h3></div>
                </div>
            </div>
        </div>

        <div class="offers-component component" id="section-88537">
            <div class="container">
                <h3 class="section-title">
                    به ما ملحق شوید </h3>
                <h4 class="section-subtitle">
                     موقعیت شغلی های فعال در رمزینکس </h4>
                <div class="grouped-view">
                    <div class="jobs-container " id="jobs-list">

                        <div class="filters-container">
                            <div class="col-md-12">
                                <div class="filter" data-filter="department">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <ul class="pills">
                                                @foreach($job_types as $job_type)
                                                <li>
                                                    <a href="{{ route('search',$job_type->id)}}"  class="btn btn-primary">{{$job_type->title}}</a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @foreach($job_vacancies as $job_vacancy)
                            @if($job_vacancy->status==1)
                                <div class="row">
                                    <div class="jobs list list-container">
                                        <a class="col-md-6" data-department="{{$job_vacancy->jobtype->title}}" href="{{route('jobvacancy.detail',$job_vacancy->id)}}">
                                            <div class="job" id="job-661746">
                                                <h5 class="title">{{$job_vacancy->title}}</h5>
                                                <div class="info clearfix">
                                                    <div class="department">
                                                        {{$job_vacancy->jobtype->title}}
                                                    </div>
                                                    <ul class="meta hidden-xs">
                                                        <li class="location">
                                                            تهران ایران
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="job-data hidden">
                                                    <div class="city">
                                                        تهران
                                                    </div>
                                                    <div class="country">
                                                        ایران
                                                    </div>
                                                    <div class="language">
                                                        ["en"]
                                                    </div>
                                                    <div class="department">
                                                        {{$job_vacancy->jobtype->title}}
                                                    </div>
                                                    <div class="tag">
                                                        []
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="spacing" style="margin-bottom: 20px"></div>

        <div class="perks-component component" id="section-160324">
            <div class="perks-background-image" style="">
                <div class="overlay" style="background-color:  #273952; opacity: 0.4;"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="section-title">
                                مزایا </h3>
                            <h4 class="section-subtitle" style="color: #f0f4f7">
                                مجموعه ای از آنچه که ما ارائه می دهیم </h4>
                        </div>
                    </div>
                    <div class="entry row row-centered">
                        <div class="col-md-4">
                            <ul>
                                <li>
                                    کار از راه دور
                                </li>
                                <li>
                                    دستمزد رقابتی
                                </li>
                                <li>
                                    وام کارمندی
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <ul>
                                <li>
                                    انعطاف پذیری زمان
                                </li>
                                <li>
                                    دوره های آموزشی داخلی
                                </li>
                                <li>
                                    بیمه درمانی تکمیلی
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@endsection
