@extends('site_layout.master')
@section('body')
<body class="candidates-controller new" id="new-421904">
    <div class="header-component component" id="section-88535">
        <div class="header-cover-image white-center-logo" style="background-image: url(#39;/img/background_header.jpg&#39;);">
            <div class="overlay" style="background-color: #273952; opacity: 0.6;"></div>
        </div>
        <div class="white-center-logo">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container">
                        <h1 class="brand">
                            <a href="https://ramzinex.com/"> <img src="http://127.0.0.1:8000/img/logo-coin.png" class="img-responsive img-circle flip animatex mCS_img_loaded" alt="friend 8">
                            </a>
                        </h1>
                        <div class="pull-right">
                        </div>
                        <div class="pull-right hidden-xs hidden-sm">
                            <button class="navbar-toggle" data-target="#navbar-collapse" data-toggle="collapse" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="pull-left hidden-xs hidden-sm">
                            <a class="company-link btn btn-primary hidden-xs" href="https://ramzinex.com/">    وبسایت شرکت    <i class="fa fa-arrow-circle-right"></i>
                            </a>
                            <ul class="nav navbar-nav socials hidden-xs navbar-right">
                                <li>
                                    <a target="_blank" href="https://www.linkedin.com/company/ramzinex/"> <i class="fa fa-linkedin-square"></i>
                                    </a></li>
                                <li>
                                    <a target="_blank" href="https://twitter.com/Ramzinex_com"> <i class="fa fa-twitter-square"></i>
                                    </a></li>
                                <li>
                                    <a target="_blank" href="https://www.instagram.com/ramzinex_com/"> <i class="fa fa-instagram"></i>
                                    </a></li>
                            </ul>
                        </div>
                    </div>
            </nav>
            <div class="content container">
                <img src="http://127.0.0.1:8000/img/1619013027577.jfif" class="img-responsive flip animatex mCS_img_loaded" alt="friend 8"  style="background-color: #273952; opacity: 0.2; width: 100%">
                <div class="centered" style=""><h1>{{$jobs->title}}</h1></div>
                <div class="bottom-center"><h4>
                        <ul>
                            <i class="ion ion-ios-people"></i>  <strong>{{$jobs->jobtype->title}}</strong><br>

                            <i class="ion ion-location"></i> تهران
                        </ul>
                    </h4>
                </div>
                <div class="info-container">
                    <div class="info">


                    </div>
                </div>
            </div>
    </div>
        <div class="col-sm-12">
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
        <div class="breadcrumbs">
            <div class="container">
                <ul>
                    <li>
                        <a href="{{route('jobvacancy.list')}}">لیست موقعیت شغلی های فعال در رمزینکس</a>
                    </li>
                    <li>
                        <a href="{{route('jobvacancy.detail',$jobs->id)}}">{{$jobs->title}}</a>
                    </li>
                    <li>
                        <a class="active">فرم ارسال درخواست</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="spacing"></div>
    </div>
    <section>
        <div class="apply-form-component">
            <div class="container">
                <form method="post" action="{{route('request.store')}}" enctype="multipart/form-data">
                    @method('POST')
                    @csrf
                    <section>
                        <div class="col-md-3 description">
                            <h3>
                                مشخصات فردی
                            </h3>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group"><label class="hide" for="job_vacancy_id">آیدی موقعیت شغلی <abbr title="required">*</abbr></label><input type="hidden" type="number" class="form-control" required="required" aria-required="true" name="job_vacancy_id" value="{{$jobs->id}}"></div>
                            <div class="form-group"><label for="name">نام<abbr title="required">*</abbr></label><input class="form-control" required="required" aria-required="true" placeholder="نام" type="text" name="name" ></div>
                            <div class="form-group"><label for="last_name">نام خانوادگی<abbr title="required">*</abbr></label><input class="form-control" required="required" aria-required="true" placeholder="نام خانوادگی" type="text" name="last_name" ></div>
                            <div class="form-group"><label for="phone" >شماره موبایل<abbr title="required">*</abbr></label><input class="form-control" required="required" aria-required="true" placeholder="شماره موبایل" type="text" name="phone" ></div>
                        </div>
                    </section>
                    <section>
                        <div class="col-md-3 description">
                            <h3>
                                رزومه
                                <abbr title="required">*</abbr>
                            </h3>
                            <p>
                                فایل رزومه خود را آپلود کنید
                            </p>
                        </div>
                        <div class="col-md-7">
                          <div class="form-group">
                              <label for="file"></label>
                              <input type="file" class="form-control" name="file" required>
                          </div>
                        </div>
                    </section>
                    <section class="closing">
                        <button type="submit" name="submit" class="btn btn-primary">
                            ثبت اطلاعات
                        </button>
                    </section>
                </form>
            </div>
        </div>
    </section>
</body>

@endsection

