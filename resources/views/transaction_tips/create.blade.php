
@extends('layouts.appAdmin')
@section('title', 'ایجاد نکته معاملات')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
        @if (session()->has('success'))
            <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
        @if (session()->has('success'))
            <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <form action="{{route('transaction_tips.store')}}" method="POST" class="form-inline" enctype="multipart/form-data">

                   {{csrf_field()}}

                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="show_order">ترتیب نمایش</label>
                        <input type="number" class="form-control"
                               name="show_order"
                               placeholder="ترتیب نمایش ..." required>
                    </div>

                    <div class="form-group pa-10">
                        <label for="currency_id">نام ارز</label>
                        <select class="form-control" name="currency_id">
                            <option value=""> </option>
                            @foreach($currencies as $currency)
                                <option value="{{$currency->id}}">
                                    {{$currency->title}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group pa-10">
                        <label for="network_id">نام شبکه</label>
                        <select  class="form-control" name="network_id">
                            <option value=""> </option>
                            @foreach($networks as $network)
                                <option value="{{$network->id}}">
                                    {{$network->title}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group pa-10">
                        <label for="type_id">نام نوع</label>
                        <select class="form-control" name="type_id">
                            <option value=""> </option>
                            @foreach($types as $type)
                                <option value="{{$type->id}}">
                                    {{$type->title}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-1 p-r-20">
                        <div class="form-group">
                            <label class="control-label mb-10"
                                   for="message_body">توضیحات </label>
                            <textarea class="form-control"
                              name="description"
                              placeholder=" توضیحات ..." required>
                            </textarea>
                        </div>
                    </div>

                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button id="update-profile" type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ایجاد نکات معاملات
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
