
@extends('layouts.appAdmin')
@section('title', 'ویرایش نکته معاملات')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
        @if (session()->has('success'))
            <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
        @if (session()->has('error'))
            <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <form action="{{route('transaction_tips.update',[$transaction_tips->id])}}" method="POST" class="form-inline">

                    {{csrf_field()}}

                    @method('PUT')

                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="show_order">ترتیب نمایش</label>
                        <input type="number" class="form-control"
                               name="show_order" id="show_order"
                               value="{{$transaction_tips->show_order}}">
                    </div>

                    <div class="form-group pa-10">
                        <label for="currency_id">نام ارز</label>
                        <select  class="form-control" name="currency_id">
                            <option value=""> </option>
                            @foreach($currencies as $currency)
                                <option value="{{$currency->id}}"
                                        @if($currency->id == $transaction_tips->currency_id) selected = "selected" @endif>
                                    {{$currency->title}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group pa-10">
                        <label for="network_id">نام شبکه</label>
                        <select  class="form-control" name="network_id">
                            <option value=""> </option>
                            @foreach($networks as $network)
                                <option value="{{$network->id}}"
                                        @if($network->id == $transaction_tips->network_id) selected = "selected" @endif>
                                    {{$network->title}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group pa-10">
                        <label for="type_id">نام نوع</label>
                        <select  class="form-control" name="type_id">
                            <option value=""> </option>
                            @foreach($types as $type)
                                <option value="{{$type->id}}"
                                        @if($type->id == $transaction_tips->type_id) selected = "selected" @endif>
                                    {{$type->title}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-1 p-r-20">
                        <div class="form-group">
                            <label class="control-label mb-10"
                                   for="description">توضیحات </label>
                            <textarea class="form-control" name="description" id="description"
                                      value="{{$transaction_tips->description}}">
                                {!! $transaction_tips->description !!}
                            </textarea>
                        </div>
                    </div>

                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button id="update-profile" type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ویرایش نکات معاملات
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
