
@extends('layouts.appAdmin')
@section('title', 'نمایش لیست نکات معاملات')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    @if ( session()->has('error'))
        <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <form action="{{route('transaction_tips.show')}}" method="get" class="form-inline" enctype="multipart/form-data">

                        {{csrf_field()}}

                        <div class="form-group pa-10">
                            <label for="currency_id">نام ارز</label>
                            <select  class="form-control" name="currency_id">
                                <option value=""> </option>
                                @foreach($currencies as $currency)
                                    <option value="{{$currency->id}}">
                                        {{$currency->title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group pa-10">
                            <label for="network_id">نام شبکه</label>
                            <select  class="form-control" name="network_id">
                                <option value=""> </option>
                                @foreach($networks as $network)
                                    <option value="{{$network->id}}">
                                        {{$network->title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group pa-10">
                            <label for="type_id">نام نوع</label>
                            <select  class="form-control" name="type_id">
                                <option value=""> </option>
                                @foreach($types as $type)
                                    <option value="{{$type->id}}">
                                        {{$type->title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-12 pa-20 text-center">
                            <div class="form-actions mt-10">
                                <button id="update-profile" type="submit"
                                        class="btn btn-primary mr-10 mb-30">
                                    جستجوی نکات معاملات
                                </button>
                            </div>
                        </div>

                    </form>

                    <table id="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">
                                <thead>
                                     <tr>
                                    <th scope="col"> ترتیب نمایشی</th>
                                    <th scope="col"> نام ارز</th>
                                    <th scope="col">نام شبکه</th>
                                    <th scope="col">نام نوع</th>
                                    <th scope="col">توضیحات</th>
                                    <th scope="col">عملیات</th>
                                    <th>
                                        <a href="{{route('transaction_tips.create')}}" class="btn btn-primary">ایجاد نکات معاملات</a>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                                    @foreach($transaction_tips as $transaction_tip)
                                    <tr>
                                        <td class="text-sm">{{$transaction_tip->show_order}}</td>
                                        <td class="text-sm">{{$transaction_tip->currency->title ?? ''}}</td>
                                        <td class="text-sm">{{$transaction_tip->network->title ?? ''}}</td>
                                        <td class="text-sm">{{$transaction_tip->type->title ?? ''}}</td>
                                        <td class="text-sm">{{$transaction_tip->description}}</td>
                                        <td class="text-sm">
                                            <a href="{{route('transaction_tips.update',[$transaction_tip->id])}}"
                                               class="btn btn-primary">ویرایش</a>

                                            <a href="{{route('transaction_tips.destroy',[$transaction_tip->id])}}"
                                               onclick="return confirm('آیا مایل به حذف این فیلد هستید؟')"
                                               class="btn btn-danger">حذف</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{$transaction_tips->links()}}
@endsection



