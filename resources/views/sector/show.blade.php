@extends('layouts.appAdmin')
@section('title', 'نمایش')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <table title="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">
                                <thead>
                                <tr>
                                    <th scope="col">id</th>
                                    <th scope="col"> title_fa</th>
                                    <th scope="col"> title_en</th>

                                    <th>
                                        <a href="{{route('admin.sector.create')}}" class="btn btn-success">ایجاد </a>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sectors as $sector)
                                    <tr>
                                        <td class="text-sm">{{$sector->id}}</td>
                                        <td class="text-sm">{{$sector->title_fa}}</td>
                                        <td class="text-sm">{{$sector->title_en}}</td>
                                        <td class="text-sm">
                                            <a href="{{route('admin.sector.edit',[$sector->id])}}"
                                               class="btn btn-primary">ویرایش</a>
                                            <a href="{{route('admin.sector.edit_attached',[$sector->id])}}"
                                               class="btn btn-primary"> ویرایش ارزها</a>
                                            <a href="{{route('admin.sector.delete',[$sector->id])}}"
                                               class="btn btn-danger">حذف</a>
                                            <a href="{{route('admin.sector.attach',[$sector->id])}}"
                                               class="btn btn-success">تخصیص ارز به sector</a>
                                            <a href="{{route('admin.sector.show_attached',[$sector->id])}}"
                                               class="btn btn-success">show attached global currencies</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{$sectors->links()}}
@endsection

