@extends('layouts.appAdmin')
@section('title', 'نمایش')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <table title="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">
                                <thead>
                                <tr>
                                    <th>{{$row = 0}}</th>
                                    <th scope="col">id</th>
                                    <th scope="col"> name</th>
                                    <th scope="col"> symbol</th>
                                    <th scope="col"></th>

                                    <th>
                                        <a href="{{route('admin.sector.list')}}" class="btn btn-success">back </a>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sector->globalCurrencies as $global_currency)
                                    <tr>
                                        <td>{{$row= $row +1 }}</td>
                                        <td class="text-sm">{{$global_currency->id}}</td>
                                        <td class="text-sm">{{$global_currency->name_fa}}</td>
                                        <td class="text-sm">{{$global_currency->symbol}}</td>
                                        <td><a href="{{route('admin.sector.detach',[$sector->id,$global_currency->id])}}" class="btn btn-success">detach </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{--    {{$sector->globalCurrencies->links()}}--}}
@endsection

