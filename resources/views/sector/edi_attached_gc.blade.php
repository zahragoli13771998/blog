
<html>
<head>

    <!-- Select2 CSS -->
    <link href="/css/select2.min.css" rel="stylesheet"/>
</head>
@extends('layouts.appAdmin')

@section('title', 'ویرایش')

@section('desktop')

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <div class="alert ">
                <li class="alert alert-danger">{{ $error }}</li>

            </div>
        @endforeach
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <form action="{{route('admin.sector.edit_attached',[$sector->id])}}" method="POST"   class="form-inline"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="col-md-25 col-sm-25">
                        <label>ارزهای دیجیتال</label>
                        <select id="multiple" class="js-states form-control" multiple
                                name="global_currencies_id[]" style="width: 50%">
                            @foreach($global_currencies as $global_currency)
                                <option value="{{$global_currency->id}}" selected="selected">
                                    {{$global_currency->name_fa}}
                                </option>
                            @endforeach
                        </select>

                    </div>
                    <!-- jQuery -->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                    <!-- Select2 -->
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
                    <script>

                        $("#multiple").select2({
                            placeholder: "  ارزهای موردنظر را انتخاب کنید",
                            allowClear: true
                        });
                    </script>

                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ایجاد
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
