@extends('layouts.appAdmin')

@section('title', 'ایجاد ')

@section('desktop')

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <div class="alert ">
                <li class="alert alert-danger">{{ $error }}</li>

            </div>
        @endforeach
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <form action="{{route('admin.trending_currency.create')}}" method="POST" class="form-inline"
                      enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="form-group pa-10">
                        <label for="id">آیدی</label>
                        <input type="number" class="form-control" min="1" step="1" name="id">
                    </div>
                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ایجاد
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection



