@extends('layouts.appAdmin')
@section('title', 'نمایش')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <table id="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">
                                <thead>
                                <tr>

                                    <th scope="col"> آیدی</th>
                                    <th>
                                        <a href="{{route('admin.trending_currency.create')}}"
                                           class="btn btn-success">ایجاد </a>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($trending_currencies as $currency_trending)
                                    <tr>
                                        <td class="text-sm">{{$currency_trending->id}}</td>
                                        <td class="text-sm">
                                            <a href="{{route('admin.trending_currency.edit',[$currency_trending->id])}}"
                                               class="btn btn-primary">ویرایش</a>
                                            <a href="{{route('admin.trending_currency.delete',[$currency_trending->id])}}"
                                               class="btn btn-danger">حذف</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
        {{$trending_currencies->links()}}
@endsection

