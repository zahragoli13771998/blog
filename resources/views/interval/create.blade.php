
@extends('layouts.appAdmin')
@section('title', 'ایجاد اینتروال جدید')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    @if ( session()->has('error'))
        <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

<div class="row">
    <div class="col-xlg-12 col-lg-12">
        <div class="panelGlass minH-240">
             <form action="{{route('admin.intervals.create')}}" method="POST"  class="form-inline" >

                 {{csrf_field()}}

                 <div class="form-group pa-10">
                     <label for="title">عنوان اینتروال فارسی</label>
                     <input type="text" class="form-control"
                            name="title_fa" id="title"
                            placeholder="عنوان اینتروال ...">
                 </div>
                 <div class="form-group pa-10">
                     <label for="title">عنوان اینتروال انگلیسی</label>
                     <input type="text" class="form-control"
                            name="title_en" id="title"
                            placeholder="عنوان اینتروال ...">
                 </div>

                 <div class="col-md-12 pa-20 text-center">
                     <div class="form-actions mt-10">
                         <button id="update-profile" type="submit"
                                 class="btn btn-primary mr-10 mb-30">
                             ایجاد اینتروال
                         </button>
                     </div>
                </div>

              </form>
            </div>
        </div>
    </div>

@endsection



