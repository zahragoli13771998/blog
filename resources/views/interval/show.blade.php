
@extends('layouts.appAdmin')
@section('title', 'نمایش لیست اینتروال ها')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    @if ( session()->has('error'))
        <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <table id="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">

                                <thead>
                                    <tr>
                                        <th scope="col">عنوان اینتروال</th>
                                        <th>
                                            <a href="{{route('admin.intervals.create')}}" class="btn btn-primary">ایجاد اینتروال</a>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($intervals as $interval)
                                     <tr>
                                         <td class="text-sm">{{$interval->title_fa}}</td>
                                         <td class="text-sm">{{$interval->title_en}}</td>
                                         <td class="text-sm">
                                             <a href="{{route('admin.intervals.update',[$interval->id])}}" class="btn btn-primary">ویرایش</a>

                                             <a href="{{route('admin.intervals.destroy',[$interval->id])}}"
                                                onclick="return confirm('آیا مایل به حذف این فیلد هستید؟')" class="btn btn-danger">حذف</a>
                                         </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{$intervals->links()}}
@endsection



