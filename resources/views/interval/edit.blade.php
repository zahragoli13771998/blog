
@extends('layouts.appAdmin')
@section('title', 'ویرایش اینتروال')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    @if ( session()->has('error'))
        <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

<div class="row">
    <div class="col-xlg-15 col-lg-15">
        <div class="panelGlass minH-240">
                    <form action="{{route('admin.intervals.update',[$intervals->id])}}" method="POST" class="form-inline" >

                        {{csrf_field()}}

                        @method('PUT')

                        <div class="form-wrap">

                            <div class="form-group pa-10">
                                <label class="control-label mb-10"
                                       for="title"> عنوان اینتروال فارسی</label>
                                <input type="text" class="form-control"
                                       name="title_fa" id="title"
                                       value="{{$intervals->title_fa}}">
                            </div>
                            <div class="form-group pa-10">
                                <label class="control-label mb-10"
                                       for="title">عنوان اینتروال انگلیسی</label>
                                <input type="text" class="form-control"
                                       name="title_en" id="title"
                                       value="{{$intervals->title_en}}">
                            </div>

                            <div class="col-md-12 pa-20 text-center">
                                <div class="form-actions mt-10">
                                    <button id="update-profile" type="submit"
                                            class="btn btn-primary mr-10 mb-30">
                                     ثبت تغییرات
                                    </button>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>
@endsection
