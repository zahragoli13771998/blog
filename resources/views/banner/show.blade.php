@extends('layouts.appAdmin')
@section('title', 'نمایشن بنرها')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <form action="{{route('admin.banner.show')}}" method="get" class="form-inline"
                      enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="col-sm-4">
                        <label for="title_fa">عنوان فارسی</label>
                        <input type="text" class="form-control"
                               name="title_fa" id="title_fa"
                               placeholder="title_fa">
                    </div>

                    <div class="col-sm-4">
                        <label for="banner_type_id">نوع بنر</label>
                        <select name="banner_type_id">
                            <option>
                                نوع بنر را انتخاب کنید
                            </option>
                            @foreach($banner_types as $banner_type)
                                <option value={{$banner_type->id}}>{{$banner_type->title_fa}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-12 pa-3 text-center">
                        <div class="form-actions mt-10">
                            <button id="update-profile" type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                search
                            </button>
                        </div>
                    </div>
                </form>

                <div>
                    <table id="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">
                                <thead>
                                <tr>
                                    <th scope="col">آدرس</th>
                                    <th scope="col"> عنوان فارسی</th>
                                    <th scope="col"> عنوان انگلیسی</th>
                                    <th scope="col">توضیحات فارسی</th>
                                    <th scope="col">توضیحات انگلیسی</th>
                                    {{--                                    <th scope="col">توضیحات </th>--}}
                                    <th scope="col">نوع بنر</th>
                                    <th scope="col">تصویر</th>
                                    <th scope="col">تعداد کامنت ها</th>
                                    <th scope="col">فعال/غیر فعال</th>
                                    <th scope="col">add to sliders</th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <th>
                                        <a href="{{route('admin.banner.create')}}" class="btn btn-primary">ایجاد بنر</a>
                                        <a href="{{route('admin.banner.active')}}"
                                           class="btn btn-success">نمایش بنرهای فعال</a>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($banners as $banner)
{{--                                    <th>--}}
{{--                                        <a href="{{route('admin.banner.edit2',[$banner->id])}}"--}}
{{--                                           class="btn btn-primary">ویرایش2</a>--}}
{{--                                    </th>--}}
                                    <tr>
                                        <td class="text-sm">{{$banner->URL}}</td>
                                        <td class="text-sm">{{$banner->title_fa}}</td>
                                        <td class="text-sm">{{$banner->title_en}}</td>
                                        <td class="text-sm">{{$banner->description_fa}}</td>
                                        <td class="text-sm">{{$banner->description_en}}</td>
                                        {{--                                        <td class="text-sm">{!!$banner->html_description!!}</td>--}}
                                        <td>
                                            @if($banner->bannerType)
                                                {{$banner->bannerType->title_fa}}
                                            @endif
                                        </td>
                                        <td class="row">
                                            <img width="80px" src="{{$banner->image }}" alt="banner image">
                                        </td>
                                        <td>{{$banner->comments_count}}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <a class="btn btn-primary"
                                                       @if($banner->is_enabled == 1)href="{{route('admin.banner.toggle',[$banner->id])}}"
                                                       disabled
                                                       @endif  href="{{route('admin.banner.toggle',[$banner->id])}}">
                                                        @if($banner->is_enabled == 1) غیرفعالسازی(بنر فعال است)
                                                        @else فعالسازی(بنر غیر فعال است)
                                                        @endif
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <a class="btn btn-primary"
                                               @if($banner->is_slider == 1)href="{{route('admin.banner.toggle-sliders',[$banner->id])}}"
                                               disabled
                                               @endif  href="{{route('admin.banner.toggle-sliders',[$banner->id])}}">
                                                @if($banner->is_enabled == 1) حذف از اسلایدرها
                                                @else افزئدن به اسلایدرها
                                                @endif
                                            </a>
                                        </td>
                                        <td class="text-sm">
                                            <a href="{{route('admin.banner.edit',[$banner->id])}}"
                                               class="btn btn-primary">ویرایش</a>
                                            <a href="{{route('admin.banner.delete',[$banner->id])}}"
                                               class="btn btn-danger">حذف</a>
                                        </td>
                                        <td>
                                            <a href="{{route('admin.banner.add_global_currencies',[$banner->id])}}"
                                               class="btn btn-success">تخصیص ارز به بنر</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                {{$banners->links()}}
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection



