@extends('layouts.appAdmin')

@section('title', ',ویرایش')

@section('desktop')

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    <!DOCTYPE html>
    <html>

    <head>
        <style>
            #cke_text-editor {
                width: 1000px;
            }

            .ck.ck-content.ck-editor__editable.ck-rounded-corners.ck-editor__editable_inline.ck-blurred p {
                color: black;
            }

            .ck-editor {
                max-width: 1000px;
            }

            .ck.ck-content.ck-editor__editable.ck-rounded-corners.ck-editor__editable_inline.ck-blurred p:active {
                color: black;
            }
        </style>
    </head>
    <div class="row">
        <div class="col-xlg-15 col-lg-15">
            <div class="panelGlass minH-240">
                <form action="{{route('admin.banner.edit',[$banner->id])}}" method="POST" class="form-inline"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="URL">آدرس</label>
                        <input type="text" class="form-control"
                               name="URL" id="URL"
                               value="{{$banner->URL}}">
                    </div>
                    <div class="form-group pa-10">
                        <label for="title_fa">عنوان فارسی</label>
                        <input type="text" class="form-control"
                               name="title_fa" id="title_fa"
                               value="{{$banner->title_fa}}">
                    </div>

                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="title_en">عنوان انگلیسی</label>
                        <input type="text" class="form-control"
                               name="title_en" id="title_en"
                               value="{{$banner->title_en}}">
                    </div>

                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="description_fa">توضیحات فارسی </label>
                        <textarea rows="4" cols="50"
                                  name="description_fa"
                                  class="form-control">
                            {{$banner->description_fa}}
                            </textarea>
                    </div>

                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="description_en">توضیحات انگلیسی </label>
                        <textarea rows="4" cols="50"
                                  name="description_en"
                                  class="form-control">
                            {{$banner->description_en}}

                            </textarea>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="html_description">پاسخ کامل </label>
                            <textarea rows="4" cols="50"
                                      name="html_description" id="text-editor"
                                      class="form-control">
                       {{$banner->html_description}}
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group pa-10">
                        <label for="banner_types">نوع بنر</label>
                        <select class="form-control" name="banner_type_id">
                            @foreach($banner_types as $banner_type)
                                <option value="{{$banner_type->id}}"
                                        @if($banner_type->title_fa == $banner->bannerType->title_fa)
                                        selected="selected"
                                    @endif>
                                    {{$banner_type->title_fa}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="withdraw_min">آپلود تصویر</label>
                        <input type="file" class="form-control"
                               name="image" id="image">
                    </div>
                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button id="update-profile" type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ثبت تغییرات
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="/js/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('text-editor', {
            filebrowserUploadUrl: "{{route('admin.ckeditor.file.store',['_token' => csrf_token()])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
    <script src="/js/ckeditor/starter.js"></script>
@endsection
