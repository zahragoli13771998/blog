@extends('layouts.appAdmin')

@section('title', 'ایجاد بنر')

@section('desktop')

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <div class="alert ">
                <li class="alert alert-danger">{{ $error }}</li>

            </div>
        @endforeach
    @endif
    <head>
        <style>
            #cke_text-editor {
                width: 1000px;
            }

            .ck.ck-content.ck-editor__editable.ck-rounded-corners.ck-editor__editable_inline.ck-blurred p {
                color: black;
            }

            .ck-editor {
                max-width: 1000px;
            }

            .ck.ck-content.ck-editor__editable.ck-rounded-corners.ck-editor__editable_inline.ck-blurred p:active {
                color: black;
            }
        </style>
    </head>
    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <form action="{{route('admin.banner.create')}}" method="POST" class="form-inline"
                      enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="URL">آدرس</label>
                        <input type="url" class="form-control"
                               name="URL" id="URL"
                               placeholder="آدرس ..."
                               value="{{old('URL')}}">
                    </div>
                    <div class="form-group pa-10">
                        <label for="title_fa">عنوان فارسی</label>
                        <input type="text" class="form-control"
                               name="title_fa" id="title_fa"
                               placeholder="عنوان فارسی ..."
                               value="{{old('title_fa')}}">
                    </div>

                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="title_en">عنوان انگلیسی</label>
                        <input type="text" class="form-control"
                               name="title_en" id="title_en"
                               placeholder="عنوان انگلیسی ..."
                               value="{{old('title_en')}}">
                    </div>

                    <div class="form-group pa-10">
                        <div class="form-group" style="width: 100%">
                            <label class="control-label"
                                   for="description_fa">توضیحات فارسی </label>
                            <textarea rows="4" cols="50" placeholder="...توضیحات فارسی"
                                      name="description_fa"
                                      class="form-control">{{old('description_fa')}}</textarea>
                        </div>
                    </div>
                    <div class="form-group pa-10">
                        <div class="form-group" style="width: 100%">
                            <label class="control-label"
                                   for="description_en">توضیحات انگلیسی </label>
                            <textarea rows="4" cols="50" placeholder="توضیحات انگلیسی..."
                                      name="description_en"
                                      class="form-control">{{old('description_en')}}</textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group pa-10">
                            <label class="control-label mb-10"
                                   for="html_description">پاسخ کامل </label>
                            <textarea rows="4" cols="50"
                                      name="html_description" id="text-editor"
                                      class="form-control">
                       {{old('html_description')}}
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="image">آپلود تصویر</label>
                        <input type="file" class="form-control"
                               name="image" id="image"
                               placeholder="آپلود تصویر...">
                    </div>
                    <div class="form-group pa-10">
                        <label for="banner_types">نوع بنر</label>
                        <select class="form-control" name="banner_type_id" required>
                            @foreach($banner_types as $banner_type)
                                <option value="{{$banner_type->id}}">
                                    {{$banner_type->title_fa}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ایجاد
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="/js/ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('text-editor', {
            filebrowserUploadUrl: "{{route('admin.ckeditor.file.store',['_token' => csrf_token()])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
    <script src="/js/ckeditor/starter.js"></script>
@endsection
