
@extends('layouts.appAdmin')
@section('title', 'نمایش لیست نوع بنر ها')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    @if ( session()->has('error'))
        <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <table id="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">

                                <thead>
                                    <tr>
                                        <th scope="col"> نوع بنر</th>
                                        <th scope="col"> نوع بنر انگلیسی</th>
                                        <th>
                                            <a href="{{route('admin.banner.types.create')}}" class="btn btn-primary">ایجاد نوع بنر</a>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($banner_types as $banner_type)
                                     <tr>
                                         <td class="text-sm">{{$banner_type->title_fa}}</td>
                                         <td class="text-sm">{{$banner_type->title_en}}</td>
                                         <td class="text-sm">
                                             <a href="{{route('admin.banner.types.update',[$banner_type->id])}}" class="btn btn-primary">ویرایش</a>

                                             <a href="{{route('admin.banner.types.destroy',[$banner_type->id])}}"
                                                onclick="return confirm('آیا مایل به حذف این فیلد هستید؟')" class="btn btn-danger">حذف</a>
                                         </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{$banner_types->links()}}
@endsection



