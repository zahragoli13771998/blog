
@extends('layouts.appAdmin')
@section('title', 'ایجاد نوع بنر جدید')

@section('desktop')

    @if ( count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    @if ( session()->has('error'))
        <div class="alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

<div class="row">
    <div class="col-xlg-12 col-lg-12">
        <div class="panelGlass minH-240">
             <form action="{{route('admin.banner.types.create')}}" method="POST" class="form-inline" >

                 {{csrf_field()}}

                 <div class="form-group pa-10">
                     <label for="title_fa"> نوع بنر</label>
                     <input type="text" class="form-control"
                            name="title_fa" id="title_fa"
                            placeholder=" نوع بنر ...">
                 </div>
                 <div class="form-group pa-10">
                     <label for="title_en">نوع بنر انگلیسی</label>
                     <input type="text" class="form-control"
                            name="title_en" id="title_en"
                            placeholder=" نوع بنر ...">
                 </div>
                 <div class="col-md-12 pa-20 text-center">
                     <div class="form-actions mt-10">
                         <button id="update-profile" type="submit"
                                 class="btn btn-primary mr-10 mb-30">
                             ایجاد نوع بنر
                         </button>
                     </div>
                </div>

              </form>
            </div>
        </div>
    </div>

@endsection



