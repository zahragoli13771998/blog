@extends('layouts.marketing')

@section('title', ',ویرایش دسته بندی')

@section('desktop')

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-xlg-15 col-lg-15">
            <div class="panelGlass minH-240">
                <form action="{{route('marketing.faq.category.edit',[$category->id])}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}


                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="name_fa">نام دسته بندی</label>
                        <input type="text" class="form-control"
                               name="name_fa" id="name_fa"
                               value="{{$category->name_fa}}">
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="name_en">نام انگلیسی</label>
                        <input type="text" class="form-control"
                               name="name_en" id="name_en"
                               value="{{$category->name_en}}">
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="description">توضیحات</label>
                        <input type="text" class="form-control"
                               name="description" id="description"
                               value="{{$category->description}}">
                    </div>
                    <div class="form-group pa-10">
                        <label for="parent_id"> دسته بندی والد</label>
                        <select class="form-control" name="parent_id">
                            <option disabled {{(!$category->parent_id) ? 'selected' : ''}}>ندارد</option>
                            @foreach($categories as $parent_category)
                                <option @if($parent_category->id == $category->parent_id) selected="selected" @endif value="{{$parent_category->id}}">
                                    {{$parent_category->name_fa}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="icon">آپلود تصویر</label>
                        <input type="file"   class="form-control"
                               name="icon" id="icon">
                    </div>
                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button id="update-profile" type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ثبت تغییرات
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    </div>
@endsection
