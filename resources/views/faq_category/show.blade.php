@extends('layouts.marketing')
@section('title', 'نمایش')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="card-group">
        <div class="row">
            <div class="col-xlg-12 col-lg-12">
                <div class="panelGlass minH-240">
                    <div>
                        <table id="bonusTable" class="table mediumThead">
                            <div class="row">
                                <div class="form-wrap">
                                    <thead>
                                    <tr>
                                        <th scope="col">نام دسته بندی</th>
                                        <th scope="col">دسته بندی انگلیسی</th>
                                        <th scope="col">توضیحات</th>
                                        <th scope="col">آیکون</th>
                                        <th scope="col">والد</th>

                                        <th scope="col">
                                            <a class="btn btn-success" href="{{route('marketing.faq.category.create')}}">ایجاد</a>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $category)
                                        <tr>
                                            <td class="text-sm">{{$category->name_fa}}</td>
                                            <td class="text-sm">{{$category->name_en}}</td>
                                            <td class="text-sm">{{$category->description}}</td>
                                            <td scope="row">
                                                <img width="80px" src="{{$category->icon}}">
                                            </td>
                                            <td class="text-sm"> @if($category->parentCategory){{$category->parentCategory->name_fa}}  @endif</td>
                                            <td>
                                                <a href="{{route('marketing.faq.category.edit',[$category->id])}}"
                                                   class="btn btn-primary">ویرایش</a>
                                                <a href="{{route('marketing.faq.category.delete',[$category->id])}}"
                                                   class="btn btn-danger">حذف</a>
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </div>
                            </div>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{$categories->links()}}
@endsection



