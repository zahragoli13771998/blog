@extends('layouts.marketing')

@section('title', 'ایجاد دسته بندی')

@section('desktop')

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <div class="alert ">
                <li class="alert alert-danger">{{ $error }}</li>

            </div>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif


    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('marketing.faq.category.create') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group pa-5">
                        <label class="control-label mb-10"
                               for="parent_id">parent</label>
                        <select  class="form-control" name="parent_id">
                            <option value="{{null}}">ندارد</option>
                            @foreach ($categories as $category)
                                <option  value="{{$category->id}}">{{$category->name_fa}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>نام</label>
                        <input type="text" name="name_fa" class="form-control" value="{{ old('name_fa') }}"
                               placeholder="Category Name" required>
                    </div>
                    <div class="form-group">
                        <label>نام انگلیسی</label>
                        <input type="text" name="name_en" class="form-control" value="{{ old('name_en') }}"
                               placeholder="Category english Name" required>
                    </div>
                    <div class="form-group">
                        <label>توضیحات</label>
                        <input type="text" name="description" class="form-control" value="{{ old('description') }}"
                               placeholder="Category description" required>
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="icon">آپلود تصویر</label>
                        <input type="file"   class="form-control"
                               name="icon" id="icon">
                    </div>
                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ایجاد
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
