@extends('layouts.appAdmin')

@section('title', 'ایجاد ')

@section('desktop')

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <div class="alert ">
                <li class="alert alert-danger">{{ $error }}</li>

            </div>
        @endforeach
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <form action="{{route('admin.ohlc.edit',[$ohlc->id])}}" method="POST"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group pa-10">
                        <label for="global_currency_id">global currencies</label>
                        <div>
                            <select class="form-control" name="global_currency_id" required style="width: 50%">
                                @foreach($global_currencies as $global_currency)
                                    <option value="{{$global_currency->id}}">
                                        {{$global_currency->name_fa}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group pa-10">
                        <label for="interval_id">intervals</label>
                        <div>
                            <select class="form-control" name="interval_id" required style="width: 50%">
                                @foreach($intervals as $interval)
                                    <option value="{{$interval->id}}"
                                        @if($interval == $ohlc->interval) selected="selected"
                                        @endif>
                                        {{$interval->title_fa}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10" for="open">open </label>
                        <div>
                            <input type="number" step="0.1" class="form-control" style="width: 50%"
                                   name="open" id="open"
                                   placeholder=" ...open"
                                   value="{{$ohlc->open}}">
                        </div>
                    </div>
                    <div class="form-group pa-10">
                        <div class="form-group" style="width: 50%">
                            <label class="control-label"
                                   for="high">high </label>
                            <input type="number" step="0.1" placeholder="...high"
                                   name="high" value="{{$ohlc->high}}"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="form-group pa-10">
                        <div class="form-group" style="width: 50%">
                            <label class="control-label"
                                   for="low">low </label>
                            <input type="number" step="0.1" placeholder="low..."
                                   name="low" value="{{$ohlc->low}}"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="form-group pa-10">
                        <div class="form-group" style="width: 50%">
                            <label class="control-label"
                                   for="close">close </label>
                            <input type="number" step="0.1" placeholder="close..."
                                   name="close" value="{{$ohlc->close}}"
                                   class="form-control">
                        </div>
                    </div>

                    <div class="form-group pa-10">
                        <div class="form-group" style="width: 50%">
                            <label class="control-label"
                                   for="volume">volume </label>
                            <input type="number" step="0.1" placeholder="volume..."
                                   name="volume" value="{{$ohlc->volume}}"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="form-group pa-10">
                        <div class="form-group" style="width: 50%">
                            <label class="control-label"
                                   for="market_cap">market_cap </label>
                            <input type="number" placeholder="market_cap..."
                                   name="market_cap" value="{{$ohlc->market_cap}}"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ایجاد
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection



