@extends('layouts.appAdmin')
@section('title', 'نمایش ')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <table id="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">
                                <thead>
                                <tr>
                                    <th scope="col">open</th>
                                    <th scope="col">high</th>
                                    <th scope="col"> low</th>
                                    <th scope="col"> close</th>
                                    <th scope="col">volume</th>
                                    <th scope="col">market cap</th>
                                    <th scope="col">global currency</th>
                                    <th scope="col">interval</th>

                                    <th scope="col"></th>
                                    <th>
                                        <a href="{{route('admin.ohlc.create')}}" class="btn btn-primary">ایجاد</a>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ohlces as $ohlc)

                                    <tr>
                                        <td class="text-sm">{{$ohlc->open}}</td>
                                        <td class="text-sm">{{$ohlc->high}}</td>
                                        <td class="text-sm">{{$ohlc->low}}</td>
                                        <td class="text-sm">{{$ohlc->close}}</td>
                                        <td class="text-sm">{{$ohlc->volume}}</td>
                                        <td class="text-sm">{{$ohlc->market_cap}}</td>
                                        <td class="text-sm">
                                            @if($ohlc->currency)
                                                {{$ohlc->currency->name_fa}}
                                            @endif
                                        </td>
                                        <td class="text-sm">
                                            @if($ohlc->interval)
                                                {{$ohlc->interval->title_fa}}
                                            @endif
                                        </td>
                                        <td class="text-sm">
                                            <a href="{{route('admin.ohlc.edit',[$ohlc->id])}}"
                                               class="btn btn-primary">ویرایش</a>
                                            <a href="{{route('admin.ohlc.delete',[$ohlc->id])}}"
                                               class="btn btn-danger">حذف</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                {{$ohlces->links()}}
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection



