@extends('layouts.appAdmin')

@section('title', 'ویرایش')

@section('desktop')

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    <!DOCTYPE html>
    <html>

    <head>
        <script src="https://cdn.ckeditor.com/ckeditor5/26.0.0/classic/ckeditor.js"></script>
        <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    </head>


    <div class="row">
        <div class="col-xlg-15 col-lg-15">
            <div class="panelGlass minH-240">
                <form action="{{route('comments.edit',[$comment->id])}}" method="POST"
                      class="form-inline" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="form-group pa-10">
                        <label for="commentable_id">بنر مربوطه</label>
                        <select class="form-control" name="commentable_id">

                                <option @if($related_model->id == $comment->commentable_id) selected="selected"
                                        @endif value="{{$comment->commentable->id}}">
                                    {{$related_model->title_fa}}
                                </option>

                        </select>
                    </div>
                    <div class="form-group pa-10">
                        <label class="control-label mb-10"
                               for="body">کامنت </label>
                        <textarea rows="4" cols="50"
                                  name="body"  id="text-editor"
                                  class="form-control">
                            {{$comment->body}}

                        </textarea>
                    </div>


                    <div class="col-md-12 pa-20 text-center">
                        <div class="form-actions mt-10">
                            <button id="update-profile" type="submit"
                                    class="btn btn-primary mr-10 mb-30">
                                ثبت تغییرات
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        ClassicEditor
            .create(document.querySelector('#text-editor'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
