@extends('layouts.appAdmin')
@section('title', 'نمایش')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <head>
        <style>
            button.like {
                width: 30px;
                height: 30px;
                margin: 0 auto;
                line-heigth: 50px;
                border-radius: 50%;
                color: rgba(0, 150, 136, 1);
                background-color: rgba(38, 166, 154, 0.3);
                border-color: rgba(0, 150, 136, 1);
                border-width: 1px;
                font-size: 15px;
            }

            button.dislike {
                width: 30px;
                height: 30px;
                margin: 0 auto;
                line-heigth: 50px;
                border-radius: 50%;
                color: rgba(255, 82, 82, 1);
                background-color: rgba(255, 138, 128, 0.3);
                border-color: rgba(255, 82, 82, 1);
                border-width: 1px;
                font-size: 15px;
            }

            button.learnmore {
                width: 100%;
                padding: 10px;
                border: none;
                background: rgba(0, 151, 167, 1);
                border-radius: 5px;
                text-transform: uppercase;
                font-size: 16px;
                color: #fff;
                letter-spacing: 1px;
            }

            #image {
                height: 200px;
                width: 500px;
                padding: 5px;

            }
        </style>
    </head>
    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <table id="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">
                                <thead>
                                <tr>
                                    <th scope="col">نام کاربری</th>
                                    <th scope="col">نام مستعار</th>
                                    <th scope="col">نوع</th>
                                    <th scope="col">عنوان</th>
                                    <th scope="col">کامنت والد</th>
                                    <th scope="col"> کامنت</th>
                                    <th scope="col">عکس</th>
                                    <th scope="col">vot options</th>
                                    <th scope="col">نوع کامنت</th>
                                    <th scope="col">استاتوس</th>
                                    <th scope="col">انتخاب</th>
                                    <th>
                                </tr>
                                </thead>
                                <form action="{{ route('admin.comments.toggle.multiple') }}"
                                      enctype="multipart/form-data" method="post">
                                    @csrf
                                    <tbody>
                                    @foreach($comments as $comment)
                                        <tr>
                                            <td class="text-sm">{{$comment->user->sid}}</td>
                                            <td class="text-sm">{{$comment->nickname ?? '-'}}</td>
{{--                                            <td class="text-sm">{{$comment->comment_type['type']}}</td>--}}
                                            <td class="text-sm">
                                                @if(isset($comment->commentable->name_en))
                                                    {{$comment->commentable->name_en}}
                                                @elseif(isset($comment->commentable->title))
                                                    {{$comment->commentable->title['fa']}}
                                                @elseif(isset($comment->commentable->question))
                                                    {{$comment->commentable->question['fa']}}
                                                @endif
                                            </td>
                                            <td class="text-sm">
                                                @if($comment->comment)
                                                    {!!$comment->comment->body!!}
                                                @endif
                                            </td>
                                            <td class="text-sm"> {!! $comment->body !!} </td>
                                            <td>

                                                @if(isset($comment->file))
                                                    <div class="row">
                                                        <div class="col-sm-5">
                                                            <img width="20px" id="image" src="{{$comment->file->path}}">
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                            <td>
                                                @if(isset($comment->options))
                                                    <div class="row">
                                                        @foreach($comment->options as $option)
                                                            <span class="badge badge-light">{{$option}}</span>
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </td>
                                            <td class="text-sm">{{$comment->type}}</td>

                                            <td>
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <a class="btn btn-primary"
                                                           @if($comment->status == 1)href="{{route('admin.comments.toggle',[$comment->id])}}"
                                                           disabled
                                                           @endif  href="{{route('admin.comments.toggle',[$comment->id])}}">
                                                            @if($comment->status == 1)غیرفعال
                                                            @else فعال
                                                            @endif
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                            {{--                                            <td class="text-sm">
                                                                                            <a href="{{route('comment.edit',[$comment->id])}}"
                                                                                               class="btn btn-primary">ویرایش</a>
                                                                                            <a href="{{route('comment.delete',[$comment->id])}}"
                                                                                               class="btn btn-danger">حذف</a>
                                                                                            <a href="{{route('comment.reply',[$comment->commentable_id,$comment->id])}}"
                                                                                               class="btn btn-success">ریپلای</a>
                                                                                        </td>--}}
                                            <td>
                                                @if($comment->status != 1)
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <input type="checkbox" name="comments[]"
                                                                   value="{{$comment->id}}"/>
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <button type="submit" class="btn btn-primary">تایید کامنت های انتخاب شده</button>
                                </form>
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{$comments->links()}}
@endsection



