@extends('layouts.appAdmin')
@section('title', 'نمایش')

@section('desktop')
    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <div class="row">
        <div class="col-xlg-12 col-lg-12">
            <div class="panelGlass minH-240">
                <div>
                    <table id="bonusTable" class="table mediumThead">
                        <div class="row">
                            <div class="form-wrap">
                                <thead>
                                <tr>

                                    <th scope="col"> آیدی </th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                    <th>
                                        <a href="{{route('admin.trend.create')}}" class="btn btn-success">ایجاد </a>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($trends as $trend)
                                    <tr>
                                        <td class="text-sm">{{$trend->id}}</td>
                                        <td class="text-sm">
                                            <a href="{{route('admin.trend.edit',[$trend->id])}}"
                                               class="btn btn-primary">ویرایش</a>
                                            <a href="{{route('admin.trend.delete',[$trend->id])}}"
                                               class="btn btn-danger">حذف</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </div>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{$trends->links()}}
@endsection

