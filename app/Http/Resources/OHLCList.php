<?php

namespace App\Http\Resources;

use App\Helpers\HelperMethods;
use Illuminate\Http\Resources\Json\JsonResource;

class OHLCList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'open' => $this->formatPrices($this->open),
            'high' => $this->formatPrices($this->high),
            'low' => $this->formatPrices($this->low),
            'close' => $this->formatPrices($this->close),
            'volume' => HelperMethods::abbreviatePrices($this->volume),
            'market_cap' => HelperMethods::abbreviatePrices($this->market_cap),
            'title_fa' => $this->title_fa,
            'title_en' => $this->title_en
        ];
    }

    /**
     * Format and return prices in desired way
     * @param float $price
     * @return float
     */
    private function formatPrices(float $price): float
    {
        if ($this->currency->price_precision !== null) {
            $price = round($price, $this->currency->price_precision);
        }
        return $price;
    }
}
