<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class Comments extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $depthId = $this->comment_id != null && $this->depth_id == null ? $this->comment_id : $this->depth_id;

        return [
            'id' => $this->id,
            'nickname' => $this->nickname,
            'comment_id' => $this->comment_id,
            'depth_id' => $depthId,
            'body' => $this->body,
            $this->mergeWhen(isset($this->status), [
                'status' => $this->status,
            ]),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            $this->mergeWhen(isset($this->replies_count), [
                'replies_count' => $this->replies_count,
            ]),
//
            'depths_count' => $this->depths()->where('status', '=', '1')->count(),

            //TODO
//            $this->mergeWhen(isset($this->depths_count), [
//                'depths_count' => $this->depths_count,
//            ]),
            $this->mergeWhen(isset($this->likes_count), [
                'likes_count' => $this->likes_count,
            ]),
            $this->mergeWhen(isset($this->dislikes_count), [
                'dislikes_count' => $this->dislikes_count,
            ]),
            $this->mergeWhen(isset($this->file_path), [
                'file_path' => $this->file_path,
            ]),
            $this->mergeWhen(isset($this->type), [
                'type' => $this->type,
            ]),
            $this->mergeWhen(isset($this->options), [
                'options' => $this->options,
                'answers' => $this->getAnswersCount(),
            ]),
            $this->mergeWhen(isset($this->end_time), [
                'end_time' => Carbon::parse($this->created_at)->addHours(24)->toDateString(),
            ]),
            'comment_entity_type' => $this->commentable_type,
            'comment_entity_id' => $this->commentable_id,
            'is_liked' => false,
//                $this->whenAppended('is_liked'),
            'is_disliked' => false,
//        $this->whenAppended('is_disliked'),
            'created_at_unix' => strtotime($this->created_at),
            'depths' => Comments::collection($this->whenLoaded('depths')),
            'replies' => Comments::collection($this->whenLoaded('replies')),
            'user' => $this->whenLoaded('user')
        ];
    }
}
