<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class JobVacancyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title_fa' => $this->title_fa,
            'title_en' => $this->title_en,
            'description'=>$this->description,
            'jobtype' => [
                'id' => $this->jobtype->id,
                'title_fa' => $this->jobtype->title_fa,
                'title_en' => $this->jobtype->title_en
            ],

            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at];
    }

    public function with($request)
    {
        return [
            'status' => 0
        ];
    }
}
