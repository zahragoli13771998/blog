<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image' => $this->image,
            'URL' => $this->URL,
            'title' => [
                'fa' => $this->title_fa,
                'en' => $this->title_en
            ],
            'is_enabled'=>$this->is_enabled,
            'description' => $this->description_fa,
            'created_at_unix' =>$this->created_at,

        ];
    }

}
