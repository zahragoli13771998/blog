<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class JobTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title_fa" => $this->title_fa,
            "title_en" => $this->title_en,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "job_vaccancy" => $this->whenLoaded('jobVaccancy')

        ];
    }
 
    public function with($request)
    {
        return ['status'=>0];
    }
}
