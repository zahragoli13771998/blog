<?php

namespace App\Http\Resources;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;


class Pool extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * //     * //     * @return array
     */
    public function toArray($request)
    {
        $count = Comment::query()->where('depth_id', '=', $this->id)->where('type', '=', 'survey_answer')->count();

        $depthId = $this->comment_id != null && $this->depth_id == null ? $this->comment_id : $this->depth_id;
        return [
            'id' => $this->id,
            'nickname' => $this->nickname,
            'comment_id' => $this->comment_id,
            'depth_id' => $depthId,
            'status' => $this->status,
            'body' => $this->body,
            'options' => $this->options,
            'answers' => [

                ['answer_index' => 1,
                    'count' => Comment::query()->where('depth_id', '=', $this->id)->where('type', '=', 'survey_answer')->where('answer_index', '=', 1)->count(),
                ],
                [
                    'answer_index' => 2,
                    'count' => Comment::query()->where('depth_id', '=', $this->id)->where('type', '=', 'survey_answer')->where('answer_index', '=', 2)->count()
                ]
            ],
            'answer_index' => $this->answer_index,
            'total_answer_count' => $count,
            'created_at_unix' => strtotime($this->created_at),
            'updated_at' => $this->updated_at,
            'end_time_at_unix' => strtotime($this->end_time),
            'end_time_at' => $this->end_time
        ];
}
}
