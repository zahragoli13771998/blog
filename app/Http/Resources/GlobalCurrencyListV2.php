<?php

namespace App\Http\Resources;

use App\Helpers\HelperMethods;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class GlobalCurrencyListV2 extends JsonResource
{
    /**
     * Define btc precision multiplier
     * @var int
     */
    private int $btc_precision_multiplier = 7;

    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_fa' => $this->name_fa,
            'rank' => $this->rank,
            $this->mergeWhen(isset($this->pair_id), [
                'pair_id' => $this->pair_id
            ]),
            'currency_id'=>$this->currency_id,
            'icon' => asset($this->icon),
            'price' => $this->formatPrices(),
            'symbol' => $this->symbol,
            'webview_url' => $this->whenAppended('webview_url'),
            'webViewUrl' => $this->whenAppended('webview_url'),
            $this->mergeWhen(isset($this->symbol_id),['symbol_id' => $this->symbol_id]),
            'daily_trading_volume' => HelperMethods::abbreviatePrices($this->daily_trading_volume ?? 0),
            'daily_trading_volume_full' => $this->daily_trading_volume,
            'coin_circulation' => HelperMethods::abbreviatePrices($this->coin_circulation ?? 0),
            'coin_circulation_full' => $this->coin_circulation,
            'total_supply' => HelperMethods::abbreviatePrices($this->total_supply ?? 0),
            $this->mergeWhen(isset($this->market_dominance), ['market_dominance' => $this->market_dominance]),
            $this->mergeWhen(isset($this->total_volume), ['total_volume' => $this->total_volume]),
            'likes_count' => $this->likes_count,
            'dislikes_count' => $this->dislikes_count,
            'comments_count' => $this->comments_count,
            'views_count' => $this->views_count,
            'include_to_count' => $this->includes_count,
            'market_cap' => HelperMethods::abbreviatePrices($this->calculateMarketCap()),
            'market_cap_full' => $this->calculateMarketCap(),
            'description' => $this->whenAppended('description'),
            'change_percent' => $this->changePercent,
            'ohlc' => OHLCList::collection($this->whenLoaded('ohlc')),
            'banners' => $this->whenLoaded('banners'),
            'ramzinex_info' => $this->whenLoaded('ramzinexInfo'),
            'tags' => $this->whenLoaded('tags')
        ];
    }

    public function with($request)
    {
        return ['status' => 0];
    }

    /**
     * Format and return prices in propre format
     * @return array
     */
    private function formatPrices(): array
    {
        $btc_price = $this->btc_price;
        $usd_price = $this->price;

        if($this->price_precision !== null) {
            $precision = $this->price_precision;
            $btc_price = round($btc_price, $precision + $this->btc_precision_multiplier);
            $usd_price = round($usd_price, $precision);
        }

        return [
            'btc' => $btc_price,
            'usd' => $usd_price,
            'rial' => $this->rial_price
        ];
    }

    /**
     * Calculate and return coins market cap
     * @return string
     */
    private function calculateMarketCap(): float
    {
        return $this->total_supply * $this->price;
    }
}

