<?php

namespace App\Http\Controllers;

use App\Http\Requests\Game\show;
use App\Jobs\SmS;
use App\Models\GameUser;
use App\Repositories\Interfaces\GameScoreRepositoryInterface;
use App\Repositories\Interfaces\GameUserRepositoryInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use App\logic\Sms\KaveNegarHelper;
use Lunaweb\RecaptchaV3\Facades\RecaptchaV3;
use Symfony\Component\Console\Input\Input;


class GameController extends Controller
{
    /**
     * Game User repository container
     * @var GameUserRepositoryInterface
     */
    private GameUserRepositoryInterface $game_user_repository;

    /**
     * Game Score repository container
     * @var GameScoreRepositoryInterface $game_score_repository
     */
    private GameScoreRepositoryInterface $game_score_repository;

    /**
     * sms helper
     * @var KaveNegarHelper $sms_helper
     */
    private KaveNegarHelper $sms_helper;

    /**
     * GameUserRepository constructor.
     * @param GameUserRepositoryInterface $game_user_repository
     * @param GameScoreRepositoryInterface $game_score_repository
     */
    public function __construct(GameUserRepositoryInterface $game_user_repository, GameScoreRepositoryInterface $game_score_repository)

    {
        $this->game_user_repository = $game_user_repository;
        $this->game_score_repository = $game_score_repository;
        $this->sms_helper = KaveNegarHelper::getInstance();
    }

    public function code(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'phone' => ['required', 'regex:/09(0[1-5]|1[0-9]|3[0-9]|2[0-2]|9[0-4])-?[0-9]{3}-?[0-9]{4}/'],
            'inviter' => ['nullable', 'regex:/09(0[1-5]|1[0-9]|3[0-9]|2[0-2]|9[0-4])-?[0-9]{3}-?[0-9]{4}/', 'exists:game_users,phone_number'],
            'g-recaptcha-response' => ['required'],
        ], [
            'inviter.exists' => 'شماره تلفن معرف موجود نیست',
        ]);
        $recaptcha = new \ReCaptcha\ReCaptcha('6LfZunwcAAAAAPjnoJCZF0nrxK9BmKMQodUIpDUN');

        $resp = $recaptcha->setExpectedHostname('api-two.ramzinex.com')
            ->setScoreThreshold(0.5)
            ->verify($request->get('g-recaptcha-response'));

        if (!$resp->isSuccess()) {
            Log::info($resp->isSuccess());
            return response()->json([
                'status' => 1,
                'data' => [
                    'wait' => false,
                    'description' => [
                        'en' => 'invalid captcha',
                        'fa' => 'کپچا نا معتبر'
                    ]
                ]
            ]);
        }

        if ($validation->fails()) {
            return response()->json([
                'status' => 1,
                'data' => [
                    'wait' => false,
                    'description' => [
                        'en' => implode(',', $validation->errors()->all()),
                        'fa' => implode(',', $validation->errors()->all())
                    ]
                ]

            ]);
        }

        //send sms if it was not
        if (!Cache::has($request->get('phone'))) {
            $new_code = rand(100000, 999999);
            $code = 'code' . $new_code;
            if (Cache::has($code)) {
                $new_code = rand(100000, 999999);
                Cache::put($code, $new_code, 180);
            }
            $phone = $request->get('phone');
//            $this->dispatchSync(new SmS($phone,$code));
            $this->sms_helper->send($request->get('phone'), $new_code);
            Cache::put($request->get('phone'), ['phone' => $request->get('phone'), 'inviter' => $request->get('inviter'), 'code' => $new_code], 180);
        }
        return response()->json([
            'status' => 0,
            'data' => [
                'wait' => true,
                'description' => [
                    'en' => 'Enter the verification code sent',
                    'fa' => 'کد تایید ارسال شده را وارد کنید'
                ]
            ]
        ]);
    }

    /**
     * Prepare user for playing game
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validation = Validator::make($request->all(), [
            'phone' => ['required', 'regex:/09(0[1-5]|1[0-9]|3[0-9]|2[0-2]|9[0-4])-?[0-9]{3}-?[0-9]{4}/'],
            'verification_code' => ['required'],
        ]);

        if ($validation->fails()) {
            return response()->json([
                'status' => 1,
                'data' => [
                    'user' => null,
                    'can_play' => false,
                    'description' => [
                        'en' => implode(',', $validation->errors()->all()),
                        'fa' => implode(',', $validation->errors()->all())
                    ]
                ]

            ]);
        }

        $cache = Cache::get($request->get('phone'));
        if (is_null($cache) || $cache['code'] != $request->get('verification_code')) {

            return response()->json([
                'status' => 1,
                'data' => [
                    'user' => null,
                    'can_play' => false,
                    'description' => [
                        'en' => 'The verification code entered is incorrect',
                        'fa' => 'کد تایید وارد شده اشتباه است'
                    ]
                ]
            ]);
        }
        if (!$this->game_user_repository->doesGameUserExist($cache['phone'])) {

            if ($cache['inviter']) {
                $inviter = $this->game_user_repository->getGameUser($cache['inviter']);
            }

            $result = $this->game_user_repository->createGameUser([
                'phone_number' => $cache['phone'],
                'inviter_id' => $inviter->id ?? null
            ]);

            if (!$result) {
                return response()->json([
                    'status' => 1,
                    'data' => [
                        'user' => null,
                        'can_play' => false,
                        'description' => [
                            'en' => 'user can not create',
                        ]
                    ]
                ]);
            }
            $user = $this->game_user_repository->getGameUser($cache['phone']);
            $can_play = $this->game_user_repository->canUserPlay($user);
            return response()->json([
                'status' => 0,
                'data' => [
                    'user' => $user,
                    'can_play' => $can_play
                ]
            ]);
        }

        $user = $this->game_user_repository->getGameUser($cache['phone']);
        $can_play = $this->game_user_repository->canUserPlay($user);
        return response()->json([
            'status' => 0,
            'data' => [
                'user' => $user,
                'can_play' => $can_play
            ]
        ]);
    }

    public function show(show $request)
    {

        $validation = Validator::make($request->all(), [
            'phone' => ['required', 'exists:game_users,phone_number', 'regex:/09(0[1-5]|1[0-9]|3[0-9]|2[0-2]|9[0-4])-?[0-9]{3}-?[0-9]{4}/'],
        ]);

        if ($validation->fails()) {
            return response()->json([
                'status' => 1,
                'data' => [
                    'user' => null,
                    'invited_count' => null,
                    'scores' => null,
                    'description' => [
                        'fa' => implode(',', $validation->errors()->all())
                    ]
                ]
            ]);
        }


        $user = $this->game_user_repository->getGameUser($request->get('phone'));
        $scores = $this->game_score_repository->getUserScore($user->id, false);
        $invited = $this->game_user_repository->activeInvitedCount($user);
        $bonus = $this->game_user_repository->getUserBonus($user);
        if (!$user) {
            return response()->json([
                'status' => 0,
                'data' => [
                    'user' => null,
                    'invited_count' => null,
                    'scores' => null

                ]
            ]);
        }
        return response()->json([
            'status' => 0,
            'data' => [
                'user' => $user,
                'invited_count' => $invited,
                'bonus' => $bonus,
                'scores' => $scores
            ]
        ]);
    }

    /**
     * @param int|null $count
     * top users
     * @return JsonResponse
     */
    public function topAccounts(int $count = null): JsonResponse
    {
        $accounts = $this->game_score_repository->topAccounts($count ?? 5, true);
        //format numbers private
        foreach ($accounts as $account) {
            $account->user->phone_number = substr($account->user->phone_number, 0, 4) . "****" . substr($account->user->phone_number, 7, 4);
        }
        $accounts = json_decode($accounts);
        return response()->json([
            'status' => 0,
            'data' => [
                $accounts
            ]
        ]);
    }

    /**
     * return pacman screen play or json error
     */
    public function play(Request $request)
    {
        $user = $this->game_user_repository->getGameUser($request->get('phone'));
        $can_play = $this->game_user_repository->canUserPlay($user);
        $bonus = $this->game_user_repository->getUserBonus($user);
        if (!$can_play) {
            return view('pacman-game.error');
        }
        return view('pacman-game.index', compact('bonus'));
    }


    /**
     * @param Request $request
     * @return bool
     */
    public function storeGameData(Request $request): bool
    {
        $score = $this->doEncryptData($request->get('score'));
        $phone = $this->doEncryptData($request->get('phone'));

        if ($user = $this->game_user_repository->getGameUser($phone)) {
            $can_play = $this->game_user_repository->canUserPlay($user);
            if (!$can_play) {
                return false;
            }
            if ($score < 100000) {
                $this->game_score_repository->createScore($user, ['score' => $score]);
                return true;
            }
        }
        return false;
    }

    /**
     * return all game users
     * //     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function users(Request $request)
//    : View
    {
        $search = $request->only(['phone']);
        $users = $this->game_user_repository->paginateGameUsers(15, true, $search);

        return view('pacman-game.users.index', compact('users'));
    }

    /**
     * return all game users
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function scores(int $id): View
    {
        $scores = $this->game_score_repository->getAllUserScores($id);
        return view('pacman-game.scores.index', compact('scores'));
    }

    /**
     * destroy game scores by id
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function deleteScores(int $id): RedirectResponse
    {
        $score = $this->game_score_repository->getScoreById($id);
        $e = $this->game_score_repository->destroyScore($score);

        //create status message
        if ($e) {
            return redirect()->back()->with('success', "successfully deleted");
        }

        return redirect()->back()->with('error', "try again");
    }

    private function doEncryptData(string $encoded)
    {
        $encoded = base64_decode($encoded);
        $decoded = "";
        for ($i = 0; $i < strlen($encoded); $i++) {
            $b = ord($encoded[$i]);
            $a = $b ^ 10;
            $decoded .= chr($a);
        }
        return base64_decode(base64_decode($decoded));
    }

    public function test()
    {
        $x = array();
        $data = ['09024683109', '09107792522','0919377910789','888'];
        $data_ = GameUser::query()->pluck('phone_number')->toArray();
//        return $data_;
        foreach ($data as $d){
            if (!array_search($d,$data_))
           $x[]=$d;
        }return $x;
    }

}
