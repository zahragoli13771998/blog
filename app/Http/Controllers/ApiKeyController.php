<?php

namespace App\Http\Controllers;

use App\Models\GameUser;
use App\Services\UserAuthentication\UserAuthenticatorService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ApiKeyController extends Controller
{
    private UserAuthenticatorService $user_authenticator_service;

    public function __construct(UserAuthenticatorService $user_authenticator_service){
    $this->user_authenticator_service = $user_authenticator_service;
}
    public function pwaCodeVersion()
    {
        return [
            'status' => 0,

            'data' =>['code-version'=>'60']
        ];
    }

    public function gameUser()
    {

        GameUser::query()->select('phone_number')->take(100);
    }

    public function test1(Request $request)
    {
        DB::beginTransaction();
        $user_id = $this->user_authenticator_service->getUserId($request);
        $gameUser = GameUser::query()
            ->lockForUpdate()
            ->findOrFail(16);
        $gameUser->phone_number = $gameUser->phone_number .'------++'.$user_id;
        sleep(10);
        $gameUser->save();

        Db::commit();
        return $gameUser;
    }

//    public function test2(Request $request)
//    {
//        DB::beginTransaction();
//        $user_id = $this->user_authenticator_service->getUserId($request);
//        $gameUser = GameUser::query()->lockForUpdate()->findOrFail(16);
//        $gameUser->phone_number = $gameUser->phone_number.'***'.$user_id;
////        sleep(2);
//        $gameUser->save();
//        DB::commit();
//        return $gameUser;
//    }

public function index() {
    return DB::transaction(function() {
        if (\Auth::guard('user')->check()) {
            $model = \App\Models\User::find(1)->lockForUpdate();
            sleep(60);
            $model->point = 100000;
            $model->save();
        } else {
            $model = \App\Models\User::find(1);
            $model->point = 999;
            $model->save();
        }

        return $model;
    });
}
    public function test2(Request $request) {
        $user_id = $this->user_authenticator_service->getUserId($request);
        return DB::transaction(function() use ($user_id) {

                $model = GameUser::query()->lockForUpdate()->find(18);
//                sleep(10);
                $model->phone_number = $model->phone_number."---**".$user_id;
                $model->save();
            return $model;
        });
    }
}
