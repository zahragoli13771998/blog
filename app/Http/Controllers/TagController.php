<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tag\Create;
use App\Http\Requests\Tag\Update;
use App\Models\GlobalCurrency;
use App\Repositories\Interfaces\BannerRepositoryInterface;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Repositories\Interfaces\TagRepositoryInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;


class TagController extends Controller
{
    /**
     * tag Repository container
     * @var TagRepositoryInterface
     */
    private TagRepositoryInterface $tag_repository;

    /**
     * GlobalCurrency Repository container
     * @var GlobalCurrencyInterface
     */
    private GlobalCurrencyInterface $currency;

    /**
     * Banner repository container
     * @var BannerRepositoryInterface
     */
    private BannerRepositoryInterface $banner_repository;

    /**
     * Construct and inject necessary services to their container
     * @param GlobalCurrencyInterface $currency
     * @param TagRepositoryInterface $tag_repository
     * @param BannerRepositoryInterface $banner_repository
     */
    public function __construct(GlobalCurrencyInterface $currency, TagRepositoryInterface $tag_repository, BannerRepositoryInterface $banner_repository)
    {
        $this->currency = $currency;
        $this->tag_repository = $tag_repository;
        $this->banner_repository = $banner_repository;
    }

    /**
     * show tags
     * @return Application|Factory|View
     */
    public function showTag(): View
    {
        $tags = $this->tag_repository->paginateTag();
        return view('tag.show', compact('tags'));
    }

    /**
     * return create form of tags for global currency
     * @return Application|Factory|View
     */
    public function showCreateGCTag(): View
    {
        $global_currencies = $this->currency->getAllGlobalCurrencies();
        return view('tag.global_currency_tag_create', compact('global_currencies'));
    }

       /**
     * create a new tag for global currency
     * @param Create $request
     * @return RedirectResponse
     */
    public function addGlobalCurrencyTag(Create $request): RedirectResponse
    {
        $taggable_obj = $this->currency->getById($request->global_currency_id);
        $data = $request->only(['title']);
        $created = $this->tag_repository->create($taggable_obj, $data);

        if (!$created) {
            return redirect()->route('admin.tag.show')->with('error', 'ایجاد تگ با مشکل مواجه شد');
        }
        return redirect()->route('admin.tag.show')->with('success', 'تگ با موفقیت ایجاد شد');
    }

    /**
     * return create form of tags for banner
     * @return Application|Factory|View
     */
    public function showCreateBannerTag(): View
    {
        $banners = $this->banner_repository->getAllBanners();
        return view('tag.banner_tag_create', compact('banners'));
    }

    /**
     * create a new tag for banners
     * @param Create $request
     * @return RedirectResponse
     */
    public function addBannerTag(Create $request): RedirectResponse
    {
        $taggable_obj = $this->banner_repository->getById($request->banner_id);
        $data = $request->only(['title']);
        $created = $this->tag_repository->create($taggable_obj, $data);

        if (!$created) {
            return redirect()->route('admin.tag.show')->with('error', 'ایجاد تگ با مشکل مواجه شد');
        }
        return redirect()->route('admin.tag.show')->with('success', 'تگ با موفقیت ایجاد شد');

    }

    /**
     * return edit form of tags
     * @param int $id
     * @return Application|Factory|View
     */
    public function editTag(int $id): View
    {
        $tag = $this->tag_repository->getById($id);
        return view('tag.edit', compact('tag'));
    }

    /**
     * update an existing tag
     * @param Update $request
     * @param int $id
     * @return RedirectResponse
     */
    public function updateTag(Update $request, int $id): RedirectResponse
    {
        $global_currency = $this->tag_repository->getById($id);

        $data = $request->only(['title']);
        $updated = $this->tag_repository->update($global_currency, $data);
        if (!$updated) {
            return redirect()->route('admin.tag.show')->with('error', 'ایجاد تگ با مشکل مواجه شد');
        }
        return redirect()->route('admin.tag.show')->with('success', 'تگ با موفقیت بروزرسانی شد');
    }

    /**
     * delete tag
     * @param int $id
     * @return RedirectResponse
     */
    public function deleteTag(int $id): RedirectResponse
    {
        $tag = $this->tag_repository->getById($id);
        $deleted = $this->tag_repository->delete($tag);
        if (!$deleted) {
            return redirect()->route('admin.tag.global_currency.show')->with('error', 'حذف تگ با مشکل مواجه شد');
        }
        return redirect()->route('admin.tag.global_currency.show')->with('success', 'تگ با موفقیت حذف شد');
    }

}
