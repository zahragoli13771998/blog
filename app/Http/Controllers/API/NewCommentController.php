<?php

namespace App\Http\Controllers\API;

use App\Models\Comment;
use App\Models\Interfaces\HasComments;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

class NewCommentController
{
    private CommentRepositoryInterface $commentRepository;

    /**
     * @param CommentRepositoryInterface $commentRepository
     */
    public function __construct(CommentRepositoryInterface $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }


//    public function get(){
    public function commentFetchQueryBuilder(bool $only_root = false, bool $with_relation = false, HasComments $commentable_obj = null, bool $only_accepted = false, int $comment_id = null, int $user_id = null)
    {
//        return Comment::query()->wherein('type',['comment','survey'])->where('commentable_id','=',1)->get();
        return (($commentable_obj) ? $commentable_obj->comments() : Comment::query())->when($only_root, function (Builder $query) {
            $query->whereNull('comment_id');
        })->wherein('type',['comment','survey'])->when($with_relation, function (Builder $query) use ($user_id, $only_accepted) {
            $query->with(['replies' => function ($query) use ($only_accepted, $user_id) {
                if ($only_accepted) {
                    $query->where('status', '=', 1);
                }

                $query->withCount(['replies' => function ($query) use ($only_accepted) {
                    if ($only_accepted) {
                        $query->where('status', '=', 1);
                    }
                }, 'likes', 'dislikes']);

                $query->when($user_id, function (Builder $query) use ($user_id) {
                    $query->with([
                        'likes' => fn($query) => $query->where('user_id', '=', $user_id),
                        'dislikes' => fn($query) => $query->where('user_id', '=', $user_id)
                    ]);
                });

            }, 'replies.file', 'replies.user:id,sid', 'file', 'user:id,sid']);
//            $query->whereHas('getAnswersCount')->with('getAnswersCount');
            $query->withCount(['replies' => function ($query) use ($only_accepted) {
                if ($only_accepted) {
                    $query->where('status', '=', 1);
                }
            }, 'likes', 'dislikes']);
        })->when($only_accepted, function (Builder $query) {
            $query->where('status', '=', 1);
        })->when(isset($comment_id), function (Builder $query) use ($comment_id) {
            $query->where('comment_id', '=', $comment_id);
        })->when($user_id, function (Builder $query) use ($user_id) {
            $query->with([
                'likes' => fn($query) => $query->where('user_id', '=', $user_id),
                'dislikes' => fn($query) => $query->where('user_id', '=', $user_id)
            ]);
        }
        )->get();
    }

    public function ddd(){
        $this->commentRepository->createComment('','');
    }
}
