<?php

namespace App\Http\Controllers\API;

use App\Logic\CurlHelper;
use App\Models\GlobalCurrency;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use PHPUnit\Exception;


class RamzinexCurrenciesGlobalsController
{

    /**
     * Construct and inject necessary services to their container
     * GlobalCurrencyController constructor.
     * @param GlobalCurrencyInterface $currency
     */
    public function __construct(GlobalCurrencyInterface $currency)
    {
        $this->currency = $currency;

    }

    public function getNotIncludedCurrencies()
    {
        $pair_data = CurlHelper::getInstance()->getResponse("https://publicapi.ramzinex.com/exchange/api/v1.0/exchange/pairs/265");
        $decoded_pair_data = json_decode($pair_data, 1);
        $global = new GlobalCurrency();
        $global->name_fa = $decoded_pair_data["data"]["name"]["fa"];
        $global->name_en = "apenft";
        $global->symbol = $decoded_pair_data["data"]["base_currency_symbol"]["en"];
        $global->pair_id = $decoded_pair_data["data"]["pair_id"];
        $global->currency_id = $decoded_pair_data["data"]["base_currency_id"];
        $global->price = $decoded_pair_data["data"]["sell"] / 287758;

        try {
            $global->save();
        } catch (\Exception|QueryException $exception) {
            Log::error('error1' . $exception->getMessage());
            return "false";
        }
        return "true";


    }


    public function showGlobals()
    {
return "hii";
    $topRank = $this->currency->getTopGlobalsWithRamzinexGlobals(true,true);
        return $topRank;
//          return  GlobalCurrenciesList::collection($topRank)->additional([
//            'status' => 0,
//            'count' => $topRank->count()
//        ]);
    }




    public function hideExtraGlobals()

    {
        $currencies = GlobalCurrency::query()->wherenotIn('id', [9238, 3410])->get();
        $ramzinexCurrencies = GlobalCurrency::query()->whereNotNull('pair_id')->get();
        $all = $currencies . '**' . $ramzinexCurrencies;
        return $all;
    }

    public function testCurl()
    {
        $url = "https://api.coingecko.com/api/v3/coins";
        $currencies = CurlHelper::getInstance()->getResponse($url);
        $decoded = json_decode($currencies, 1);
        return count($decoded);
    }
}
