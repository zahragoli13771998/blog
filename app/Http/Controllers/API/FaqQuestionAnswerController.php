<?php

namespace App\Http\Controllers\API;

use App\Factories\CommentRepoFactory;
use App\Http\Requests\Api\QuestionAnswers\AddFaqComment;
use App\Http\Requests\Api\QuestionAnswers\GetFaqComment;
use App\Http\Requests\Api\QuestionAnswers\GetFaq;
use App\Http\Requests\Api\QuestionAnswers\SearchFaq;
use App\Http\Requests\Api\QuestionAnswers\ShowRepetitive;
use App\Http\Requests\Api\QuestionAnswers\UpdateFaqComment;
use App\Http\Resources\Comments;
use App\logic\ConfigManager;
use App\Models\FaqQuestionAnswer;
use App\Repositories\Interfaces\DislikeRepositoryInterface;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Repositories\Interfaces\FaqCategoryRepositoryInterface;
use App\Repositories\Interfaces\FaqQuestionAnswerRepositoryInterface;
use App\Repositories\Interfaces\FavoriteRepositoryInterface;
use App\Repositories\Interfaces\LikeRepositoryInterface;
use App\Services\Interfaces\CommentServiceInterface;
use App\Services\Interfaces\LikeDislikeServiceInterface;
use App\Services\Interfaces\ViewCounterServiceInterface;
use App\Services\UserAuthentication\UserAuthenticatorService;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;


//Todo move like/dislikes operation to service layer
class FaqQuestionAnswerController
{
    /**
     * questionAnswer repository container
     * @var FaqQuestionAnswerRepositoryInterface
     */
    private FaqQuestionAnswerRepositoryInterface $question_answer_repository;

    /**
     * like repository container
     * @var LikeRepositoryInterface
     */
    private LikeRepositoryInterface $like_repository;

    /**
     * comment repository container
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $comment_repository;

    /**
     * comment repository container
     * @var CommentServiceInterface
     */
    private CommentServiceInterface $comment_service;

    /**
     * dislike repository container
     * @var DislikeRepositoryInterface
     */
    private DislikeRepositoryInterface $dislike_repository;

    /**
     * User Authenticator Service container
     * @var UserAuthenticatorService
     */
    protected UserAuthenticatorService $user_authenticator_service;

    /**
     * @var FaqCategoryRepositoryInterface
     */
    private FaqCategoryRepositoryInterface $category_repository;

    /**
     * ViewCounter Service container
     * @var ViewCounterServiceInterface
     */
    private ViewCounterServiceInterface $view_counter_service;

    /**
     * FavoriteRepository Container
     * @var FavoriteRepositoryInterface
     */
    private FavoriteRepositoryInterface $favorite_repository;

    /**
     * like service container
     * @var LikeDislikeServiceInterface
     */
    private LikeDislikeServiceInterface $like_dislike_service;

    /**
     * FaqCategoriesAndFaqQaController constructor.
     * @param FaqQuestionAnswerRepositoryInterface $questionAnswerRepository
     * @param UserAuthenticatorService $user_authenticator_service
     * @param LikeRepositoryInterface $like_repository
     * @param DislikeRepositoryInterface $dislike_repository
     * @param CommentServiceInterface $comment_service
     * @param FaqCategoryRepositoryInterface $category_repository
     * @param ViewCounterServiceInterface $view_counter_service
     * @param FavoriteRepositoryInterface $favorite_repository
     * @param LikeDislikeServiceInterface $like_dislike_service
     */
    public function __construct(FaqQuestionAnswerRepositoryInterface $questionAnswerRepository, UserAuthenticatorService $user_authenticator_service, LikeRepositoryInterface $like_repository, DislikeRepositoryInterface $dislike_repository, CommentServiceInterface $comment_service, FaqCategoryRepositoryInterface $category_repository, ViewCounterServiceInterface $view_counter_service, FavoriteRepositoryInterface $favorite_repository, LikeDislikeServiceInterface $like_dislike_service)
    {
        $this->question_answer_repository = $questionAnswerRepository;
        $this->like_repository = $like_repository;
        $this->dislike_repository = $dislike_repository;
        $this->user_authenticator_service = $user_authenticator_service;
        $this->comment_repository = CommentRepoFactory::createRepository();
        $this->comment_service = $comment_service;
        $this->category_repository = $category_repository;
        $this->view_counter_service = $view_counter_service;
        $this->favorite_repository = $favorite_repository;
        $this->like_dislike_service = $like_dislike_service;
    }

    /**
     * Get and return question
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function showQuestion(Request $request, int $id): JsonResponse
    {
        $faq_key = 'faq_detail_for_id' . $id;
        if (Cache::has($faq_key)) {
            $question = Cache::get($faq_key);
        } else {
            $question = $this->question_answer_repository->getById($id, true, null);
            Cache::put($faq_key, $question, 300);
        }
        if ($request->hasHeader('Authorization')) {
            $user_id = $this->user_authenticator_service->getUserId($request);
        }

        $this->view_counter_service->addViews($question, $user_id ?? null);

        return response()->json([
            'status' => 0,
            'data' => $question
        ]);

    }

    /**
     * return repetitive questionAnswers
     * @param Request $request
     * @return JsonResponse
     */
    public function showQuestions(ShowRepetitive $request): JsonResponse
    {
        $only_repetitive = $request->input('only_repetitive', true);
        return response()->json([
            'status' => 0,
            'data' => [
                $this->question_answer_repository->getAllQuestionAnswer($only_repetitive, true)
            ]
        ]);
    }


    /**
     * Get And Return All question's id
     * @return JsonResponse
     */
    public function getQuestionsId(): JsonResponse
    {
        $key = 'questions_ids';
//        try {
//            $questions = FaqQuestionAnswer::query()->pluck('id');
//        }catch (\Exception|QueryException $ex){
//            Log::error('error while getting  '.$ex->getMessage());
//        }
        if (Cache::has($key)) {
            $questions = Cache::get($key);
        } else {
            $questions = $this->question_answer_repository->getAllQuestionAnswer()->pluck('id');
            Cache::put($key, $questions, 259200);
        }

//        $questions = $this->question_answer_repository->getAllQuestionAnswer();
        return response()->json([
            'status' => 0,
            'data' => $questions
        ]);
    }

    /**
     * filter questions according to given category_name and question_title
     * @param SearchFaq $request
     * @return JsonResponse
     */
    public function searchQuestionAndCategories(SearchFaq $request): JsonResponse
    {
        $title = $request->input('title');

        return response()->json([
            'status' => 0,
            'data' => [
                'categories' => $this->category_repository->getAllCategories(false, false, $title),
                'questions' => $this->question_answer_repository->getAllQuestionAnswer(false, false, $title)
            ],
        ]);
    }

    /**
     * add a question_answer to favorites
     * @param GetFaq $request
     * @return JsonResponse
     */
    public function addToFavorite(GetFaq $request): JsonResponse
    {
        $question_id = $request->question_id;
        $question = $this->question_answer_repository->getById($question_id);
        $user_id = $this->user_authenticator_service->getUserId($request);
        if ($this->favorite_repository->isAddedToFavorites($question, $user_id)) {
            $this->favorite_repository->deleteFavorite($question, $user_id, 'faq');
        } else {
            $this->favorite_repository->createFavorite($question, $user_id, 'faq');
        }
        $favored_list = 'user_' . $user_id . '_favored_faq_list';
        if (Cache::has($favored_list)) {
            $users_favorites_list = Cache::get($favored_list);
        } else {
            $users_favorites_list = $this->favorite_repository->getUserFavoredGlobalList($user_id, 'faq');
            Cache::put($favored_list, $users_favorites_list, 86400);
        }
        return response()->json([
            'status' => 0,
            'data' => [
                'favorites_count' => $this->favorite_repository->getFavoriteCount($question),
                'favored_faqs' => $users_favorites_list->toArray()
            ]
        ]);
    }

    /**
     * check if question_answer added to favorite or not
     * @param GetFaq $request
     * @return JsonResponse
     */
    public function isAddedToFavorites(GetFaq $request): JsonResponse
    {
        $question_id = $request->question_id;
        $question = $this->question_answer_repository->getById($question_id);
        $user_id = $this->user_authenticator_service->getUserId($request);
        return response()->json([
            'status' => 0,
            'data' => [
                'is_favored' => false
//                    $this->favorite_repository->isAddedToFavorites($question, $user_id)
            ]
        ]);

    }

    /**
     * get user's feedback for each question
     * @param GetFaq $request
     * @return JsonResponse
     */
    public function like(GetFaq $request): JsonResponse
    {
        $question_id = $request->question_id;
        $question = $this->question_answer_repository->getById($question_id);
        $result = $this->like_dislike_service->createlike($question, $request, 'faq');

        if (!$result) {
            return $this->generateFeedbackResponse();
        }
        $user_id = $this->user_authenticator_service->getUserId($request);
        $likes_list = 'user_' . $user_id . '_liked_faq_list';
        if (Cache::has($likes_list)) {
            $users_likes_list = Cache::get($likes_list);
        } else {
            $users_likes_list = $this->like_repository->getUserLikesList($user_id, 'faq');
            Cache::put($likes_list, $users_likes_list, 86400);

        }
        $dislikes_list = 'user_' . $user_id . '_disliked_faq_list';
        if (Cache::has($dislikes_list)) {
            $users_dislikes_list = Cache::get($dislikes_list);
        } else {
            $users_dislikes_list = $this->dislike_repository->getUserDisLikesList($user_id, 'faq');
            Cache::put($dislikes_list, $users_dislikes_list, 86400);
        }
        $new_likes_count = $this->like_repository->getLikeCount($question);
        $new_dislikes_count = $this->dislike_repository->getDisLikeCount($question);
        return $this->generateFeedbackResponse(true, $new_likes_count, $new_dislikes_count, $users_likes_list, $users_dislikes_list);
    }

    /**
     * Checks if user is liked question answer or not
     * @param GetFaq $request
     * @return JsonResponse
     */
    public function isLiked(GetFaq $request): JsonResponse
    {
        $question_id = $request->question_id;
        $question = $this->question_answer_repository->getById($question_id);
        $user_id = $this->user_authenticator_service->getUserId($request);

        return response()->json([
            'status' => 0,
            'data' => [
                'is_liked' => false
//                    $this->like_repository->isLiked($question, $user_id)
            ]
        ]);
    }

    /**
     * get user's feedback for each question
     * @param GetFaq $request
     * @return JsonResponse
     */
    public function disLike(GetFaq $request): JsonResponse
    {
        $question_id = $request->question_id;
        $question = $this->question_answer_repository->getById($question_id);
        $result = $this->like_dislike_service->createDislike($question, $request, 'faq');
        if (!$result) {
            return $this->generateFeedbackResponse();
        }
        $user_id = $this->user_authenticator_service->getUserId($request);
        $likes_list = 'user_' . $user_id . '_liked_faq_list';
        if (Cache::has($likes_list)) {
            $users_likes_list = Cache::get($likes_list);
        } else {
            $users_likes_list = $this->like_repository->getUserLikesList($user_id, 'faq');
            Cache::put($likes_list, $users_likes_list, 86400);

        }
        $dislikes_list = 'user_' . $user_id . '_disliked_faq_list';
        if (Cache::has($dislikes_list)) {
            $users_dislikes_list = Cache::get($dislikes_list);
        } else {
            $users_dislikes_list = $this->dislike_repository->getUserDisLikesList($user_id, 'faq');
            Cache::put($dislikes_list, $users_dislikes_list, 86400);
        }
        $new_dislikes_count = $this->dislike_repository->getDisLikeCount($question);
        $new_likes_count = $this->like_repository->getLikeCount($question);
        return $this->generateFeedbackResponse(true, $new_likes_count, $new_dislikes_count, $users_likes_list, $users_dislikes_list);
    }

    /**
     * Checks if user is disliked question answer or not
     * @param GetFaq $request
     * @return JsonResponse
     */
    public function isDisliked(GetFaq $request): JsonResponse
    {
        $question_id = $request->question_id;
        $question = $this->question_answer_repository->getById($question_id);
        $user_id = $this->user_authenticator_service->getUserId($request);

        return response()->json([
            'status' => 0,
            'data' => [
                'is_disliked' => false
//                    $this->dislike_repository->isDisliked($question, $user_id)
            ]
        ]);
    }

    /**
     * Add new comment for question answer
     * @param AddFaqComment $request
     * @return JsonResponse
     */
    public function addComment(AddFaqComment $request): JsonResponse
    {
        $faq_question_answer = $this->question_answer_repository->getById($request->faq_question_id);
        $comment = $this->comment_service->createComment($faq_question_answer, $request);
        if (!$comment) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on creating comment',
                    'fa' => 'ثبت کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'description' => [
                'en' => 'Comment created successfully',
                'fa' => 'کامنت شما با موفقیت ثبت شد'
            ]
        ]);
    }


    /**
     *update an existing comment
     * @param UpdateFaqComment $request
     * @return JsonResponse
     */
    public function updateComment(UpdateFaqComment $request): JsonResponse
    {
        $result = $this->comment_service->updateComment($request);

        if (!$result) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on Editing comment',
                    'fa' => 'ویرایش کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'description' => [
                'en' => 'Comment edited successfully',
                'fa' => 'ویرایش کامنت با موفقیت انجام شد'
            ]
        ]);
    }

    public function getFrequentQuestions()
    {
        return $this->question_answer_repository->getFaq();
    }

    /**
     * return question's accepted comments
     * @param GetFaqComment $request
     * @return AnonymousResourceCollection
     */
    public function getComment(GetFaqComment $request): AnonymousResourceCollection
    {
        $comments_key = 'faq_comments_for:' . $request->question_id;
        $faq_key = 'faq_detail_for_id:' . $request->question_id;
        $faq_comment_count = 'faq' . $request->question_id . 'comments_count';
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);
        if (Cache::has($faq_key)) {
            $question_answer = Cache::get($faq_key);
        } else {
            $question_answer = $this->question_answer_repository->getById($request->question_id);
            Cache::put($faq_key, $question_answer, 86400);
        }
        if (Cache::has($comments_key)) {
            $comments = Cache::get($comments_key);
        } else {
            $comments = $this->comment_repository
                ->getAllComment($question_answer, false, true, $offset, $limit, true, null, null);
            Cache::put($comments_key, $comments, 43200);
        }
        if (Cache::has($faq_comment_count)) {
            $comments_count = Cache::get($faq_comment_count);
        } else {
            $comments_count = $this->comment_repository->getCommentsCount($question_answer, true);
            Cache::put($faq_comment_count, $comments_count, 43200);
        }
        return Comments::collection($comments)->additional([
            'status' => 0,
            'count' => $comments_count,
        ]);
    }

    /**
     * it deletes feedbacks(if exist) then delete comments
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function deleteComment(Request $request, int $id): JsonResponse
    {
        $result = $this->comment_service->deleteComment($request, $id);

        if (!$result) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on deleting comment',
                    'fa' => 'حذف کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'description' => [
                'en' => 'Comment delete successfully',
                'fa' => 'کامنت شما با موفقیت حذف شد'
            ]
        ]);
    }

    /**
     * Generate feedback success/unsuccess response
     * @param bool $is_success
     * @param int $like_count
     * @param int $dislike_count
     * @param object|null $liked_list
     * @param object|null $disliked_list
     * @return JsonResponse
     */
    private function generateFeedbackResponse(bool $is_success = false, int $like_count = 0, int $dislike_count = 0, object $liked_list = null, object $disliked_list = null): JsonResponse
    {
        if ($is_success) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'You\'re like submitted successfully',
                    'fa' => 'فیدبک شما با موفقیت ثبت گردید',
                ],
                'data' => [
                    'likes_count' => $like_count,
                    'dislikes_count' => $dislike_count,
                    'liked_faqs_list' => $liked_list->toArray(),
                    'disliked_faqs_list' => $disliked_list->toArray()
                ]
            ]);
        }

        return response()->json([
            'status' => 1,
            'description' => [
                'en' => 'There was a problem in submitting your feedback',
                'fa' => 'مشکلی در ثبت فیدبک شما بوجود آمده است',
            ]
        ]);
    }

}
