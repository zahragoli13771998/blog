<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Resume\JobRequest\Search;
use App\Http\Requests\Resume\JobRequest\StoreRequest;
use App\Http\Requests\Resume\JobVacancy\Get;
use App\Http\Resources\JobTypeResource;
use App\Http\Resources\JobVacancyResource;
use App\Repositories\Interfaces\JobTypeRepositoryInterface;
use App\Repositories\Interfaces\JobVacancyRepositoryInterface;
use App\Repositories\Interfaces\JobVacancyRequestRepositoryInterface;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Filesystem\Cache;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Log;

class JobVacancyRequestController extends Controller
{
    /**
     * var coming from job vacancy request repository interface
     * @var JobVacancyRequestRepositoryInterface
     */
    private JobVacancyRequestRepositoryInterface $job_vacancy_request;

    /**
     * var coming from job type repository interface
     * @var JobTypeRepositoryInterface
     */
    private JobTypeRepositoryInterface $job_type;

    /**
     * var coming from job vacancy repository interface
     * @var JobVacancyRepositoryInterface
     */
    private JobVacancyRepositoryInterface $job_vacancy;

    /**
     * for entering job vacancy request repository in controller
     * JobVacancyRequestController constructor.
     * @param JobVacancyRequestRepositoryInterface $job_vacancy_request
     * @param JobTypeRepositoryInterface $job_type
     * @param JobVacancyRepositoryInterface $job_vacancy
     */
    public function __construct(JobVacancyRequestRepositoryInterface $job_vacancy_request, JobTypeRepositoryInterface $job_type, JobVacancyRepositoryInterface $job_vacancy)
    {
        $this->job_vacancy_request = $job_vacancy_request;
        $this->job_type = $job_type;
        $this->job_vacancy = $job_vacancy;

    }

    /**
     * for showing active job types with related vacancies
     * @return AnonymousResourceCollection
     */
    public function show(): AnonymousResourceCollection
    {
        $job_vacancies = $this->job_vacancy->getAll();
        return JobVacancyResource::collection($job_vacancies);
    }

    /**
     * for showing active job types only
     * @return JsonResponse
     */
    public function showOnlyJobTypes(): JsonResponse
    {
        $job_types = $this->job_type->getAll();
        return response()->json([
            'status' => 0,
            'data' => [
                $job_types
            ]
        ]);
    }


    /**
     * for searching between job vacancies by job type id
     * @param Search $search
     * @return AnonymousResourceCollection
     */
    public function search(Search $search): AnonymousResourceCollection
    {
        $id = $search->input('job_type_id');
        $job_vacancies = $this->job_vacancy->getAll($id);
        return JobVacancyResource::collection($job_vacancies);

    }

    /**
     * to get specific job vacancy's detail
     * @param Get $request
     * @return
     */
    public function getJobVacancyDetail(Get $request): JsonResponse
    {
        $job_type = $request->input('job_type');
        $job_vacancies = $this->job_vacancy->getAll($job_type);
        return response()->json([
            'status' => 0,
            'data' => JobVacancyResource::collection($job_vacancies)
        ]);
    }


    /**
     * to get specific job vacancy's detail
     * @param Get $request
     * @return
     */
    public function getEachJobVacancyDetail(int $id): JsonResponse
    {

        $job_vacancy = $this->job_vacancy->getJobVacancyById($id);
        return response()->json([
            'status' => 0,
            'data' => $job_vacancy
        ]);
    }

    /**
     * create a job request
     * @param StoreRequest $job_request
     * @return JsonResponse
     */
    public function store(StoreRequest $job_request): JsonResponse
    {

        $data = $job_request->only(['full_name', 'phone', 'file', 'email', 'job_vacancy_id']);
        $job_vacancy = $this->job_vacancy->getJobVacancyById($data['job_vacancy_id'] ?? 27);
        if ($job_request->has('file')) {
            try {
                $data['file'] = $job_request->file('file')->store('resume', 'public');
            } catch (QueryException | Exception $ex) {
                Log::error($ex->getMessage());
            }
        }
        $job_vacancy_request = $this->job_vacancy_request->store($data, $job_vacancy);

        if (!$job_vacancy_request) {
            return response()->json([
                'status' => 0,
                'description' => [
                  'en'=>  'error while creating request',
                  'fa'=>  ' اجرای درخواست شما با مشکل مواجه شد'
                ]
            ]);
        }
        return response()->json([
            'status' => 0,
            'description' => [
              'en'=>  'your request is sent successfully',
              'fa'=>  'درخواست شما با موفقیت انجام شد'
            ]
        ]);
    }


}
