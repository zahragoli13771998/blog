<?php

namespace App\Http\Controllers\API;

use App\Repositories\Interfaces\FaqCategoryRepositoryInterface;
use App\Services\UserAuthentication\UserAuthenticatorService;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;


class FaqCategoryController
{
    /**
     * FaqCategoryService repository container
     * @var FaqCategoryRepositoryInterface
     */
    private FaqCategoryRepositoryInterface $faqCategoryRepository;

    /**
     * User Authenticator service container
     * @var UserAuthenticatorService
     */
    private UserAuthenticatorService $user_authenticator_service;

    /**
     * FaqCategoriesAndFaqQaController constructor.
     * @param FaqCategoryRepositoryInterface $faqCategoryRepository
     * @param UserAuthenticatorService $user_authenticator_service
     */
    public function __construct(FaqCategoryRepositoryInterface $faqCategoryRepository, UserAuthenticatorService $user_authenticator_service)
    {
        $this->faqCategoryRepository = $faqCategoryRepository;
        $this->user_authenticator_service = $user_authenticator_service;
    }

    /**
     * return all categories with their children and questionAnswers in json format
     * @param Request $request
     * @param int|null $parent_id
     * @return JsonResponse
     */
    public function showCategories(Request $request, int $parent_id = null): JsonResponse
    {

        if ($parent_id) {
            $category_key = 'category_with_parent' . $parent_id;
            if (Cache::has($category_key)) {
                $category = Cache::get($category_key);
            } else {
                $category = $this->faqCategoryRepository->getById($parent_id, true, null);
                Cache::put($category_key, $category, 86400);
            }

        } else {
            $category_key = 'categories';
            if (Cache::has($category_key)) {
                $category = Cache::get($category_key);
            } else {
                $category = $this->faqCategoryRepository->getAllCategories(true);
                Cache::put($category_key, $category, 86400);
            }

        }

        return response()->json([
            'status' => 0,
            'data' => $category
        ]);
    }

    /**
     * return category with its questionAnswers according to given id
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function showCategoryAndRelatedQuestions(Request $request, int $id): JsonResponse
    {
        $key = 'category_with_question_for:' . $id;
        if (Cache::has($key)) {
            $faq_question_answers = Cache::get($key);
        } else {
            $faq_question_answers = $this->faqCategoryRepository->getById($id, true, null)->faqQuestionAnswers;
            Cache::put($key, $faq_question_answers, 86400);
        }
        return response()->json([
            'status' => 0,
            'data' => $faq_question_answers
        ]);
    }
}
