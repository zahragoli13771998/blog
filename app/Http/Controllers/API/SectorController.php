<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Sector\Get;
use App\Http\Requests\Api\Sector\Search;
use App\Http\Resources\GlobalCurrenciesList;
use App\Models\GlobalCurrency;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Repositories\Interfaces\SectorRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;

class SectorController extends Controller
{
    /**
     * Sector Repository Container
     * @var SectorRepositoryInterface
     */
    private SectorRepositoryInterface $sector_repository;
    private GlobalCurrencyInterface $globalCurrency;


    /**
     * Sector repository constructor
     * @param SectorRepositoryInterface $sector_repository
     * @param GlobalCurrencyInterface $globalCurrency
     */
    public function __construct(SectorRepositoryInterface $sector_repository, GlobalCurrencyInterface $globalCurrency)
    {
        $this->sector_repository = $sector_repository;
        $this->globalCurrency = $globalCurrency;
    }

    /**
     * return the list of sectors
     * @return JsonResponse
     */
    public function getSectorList(): JsonResponse
    {
        $key = 'sectors_list';
        if (Cache::has($key)) {
            $list = Cache::get($key);
        } else {
            $list = $this->sector_repository->getAllSectors();
            Cache::put($key, $list, 2592000);
        }
        return response()->json([
            'status' => 0,
            'data' => [$list]
        ]);
    }

    /**
     * return sectors with globalcurrencies
     * @param Get $request
     * @return JsonResponse
     */
    public function getSector(Get $request): JsonResponse
    {
        $sector = $this->sector_repository->getById($request->id);
        $from = $request->input('offset', 0);
        $take = $request->input('limit', 100);
        $key = 'sectors_' . $request->id . 'detail_limit:' . $take . 'offset:' . $from;
        if (Cache::has($key)) {
            $sectors = Cache::get($key);
        } else {
            $sectors =
                $this->sector_repository->getSectorsWithGlobals($sector, $take, $from);
            Cache::put($key, $sectors, 2592000);
        }
        return response()->json([
            'status' => 0,
            'data' => $sectors
        ]);
    }
}
