<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\GlobalCurrency;
use App\Models\Trend;
use App\Models\Views;
use App\Repositories\CurrencyRepository;
use App\Repositories\Interfaces\GainerRepositoryInterface;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Repositories\Interfaces\LooserRepositoryInterface;
use App\Repositories\Interfaces\TrendingCurrencyRepositoryInterface;
use App\Repositories\Interfaces\TrendRepositoryInterfac;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class StateCurrenciesController extends Controller
{
    /**
     * Gainers Repository container
     * @var GainerRepositoryInterface
     */
    protected GainerRepositoryInterface $gainer_repository;

    /**
     * Looser Repository container
     * @var LooserRepositoryInterface
     */
    protected LooserRepositoryInterface $looser_repository;

    /**
     * Trends Repository container
     * @var TrendRepositoryInterfac
     */
    protected TrendRepositoryInterfac $trend_repository;

    /**
     * Trending currency repository container
     * @var TrendingCurrencyRepositoryInterface
     */
    protected TrendingCurrencyRepositoryInterface $trending_currency_repository;
    private GlobalCurrencyInterface $currency_repository;

    /**
     * Construct and inject necessary services to their container
     * StateCurrenciesController constructor.
     * @param GlobalCurrencyInterface $currency_repository
     * @param GainerRepositoryInterface $gainer_repository
     * @param LooserRepositoryInterface $looser_repository
     * @param TrendRepositoryInterfac $trend_repository
     * @param TrendingCurrencyRepositoryInterface $trending_currency_repository
     */
    public function __construct(GlobalCurrencyInterface $currency_repository, GainerRepositoryInterface $gainer_repository, LooserRepositoryInterface $looser_repository, TrendRepositoryInterfac $trend_repository, TrendingCurrencyRepositoryInterface $trending_currency_repository)
    {
        $this->gainer_repository = $gainer_repository;
        $this->looser_repository = $looser_repository;
        $this->trend_repository = $trend_repository;
        $this->currency_repository = $currency_repository;
        $this->trending_currency_repository = $trending_currency_repository;
    }

    /**
     * Get and generate Gainer List
     * @return JsonResponse
     */
    public function gainer(): JsonResponse
    {
        $key = 'gainers';
        if (Cache::has($key)) {
            $gainers = Cache::get($key);
        } else {
            $gainers = $this->gainer_repository->getAllGainers(true)->pluck('id');
            Cache::put($key, $gainers, 43200);
        }
        return response()->json([
            'status' => 0,
            'data' => $gainers
        ]);
    }

    /**
     * Get and generate Gainer List
     * @return JsonResponse
     */
    public function globalGainer(): JsonResponse
    {
        $key = 'gainers';
        if (Cache::has($key)) {
            $gainers = Cache::get($key);
        } else {
            $gainers = $this->gainer_repository->getAllGainers(true)->pluck('id');
            Cache::put($key, $gainers, 43200);
        }
        return response()->json([
            'status' => 0,
            'data' => $gainers
        ]);
    }

    /**
     * Generate and get Looser list
     * @return JsonResponse
     */
    public function looser(): JsonResponse
    {
        $key = 'loosers';
        if (Cache::has($key)) {
            $looser = Cache::get($key);
        } else {

            $looser = $this->looser_repository->getAllLoosers(true)->pluck('id');
            Cache::put($key, $looser, 43200);
        }
        return response()->json([
            'status' => 0,
            'data' => $looser
        ]);
    }


    /**
     * Generate and get Looser list
     * @return JsonResponse
     */
    public function globalLooser(): JsonResponse
    {
        $key = 'loosers';
        if (Cache::has($key)) {
            $looser = Cache::get($key);
        } else {

            $looser = $this->looser_repository->getAllLoosers(true)->pluck('id');
            Cache::put($key, $looser, 43200);
        }
        return response()->json([
            'status' => 0,
            'data' => $looser
        ]);
    }

    /**
     * Generate and get trends list
//     * @return JsonResponse
     */
    public function globalTrending()
//    : JsonResponse
    {
        $key = 'short_time_cache_global_daily_trending';
        $key_long = 'long_time_cache_global_daily_trending';
        $key_lock = 'lock_time_cache_global_daily_trending';
//        if (Cache::has($key)) {
//            $trends = Cache::get($key);
//        } else {
//            if (Cache::has($key_long)) {
//                if (Cache::has($key_lock)) {
//                    $trends = Cache::get($key_long);
//                    return response()->json([
//                        'status' => 0,
//                        'data' => $trends
//                    ]);
//                }
//            }
//            Cache::put($key_lock, 1, 86400);
        $d = array();
        $stop = Carbon::now()->toDateString();
        $start = Carbon::now()->subHours(24)->toDateString();

        $currency_trends = Views::query()->where('viewable_type', '=', 'App\Models\GlobalCurrency')
            ->whereBetween('created_at', [$start, $stop])->get();
        foreach ($currency_trends as $currency_trend) {
            $d[] = $currency_trend->viewable_id;
        }
//        return $d;
        $e =array_count_values($d);
        asort($e);
        var_dump($e);
//        dd($a);
//            $trends = collect(array_keys(array_count_values($d)))->take(4)->toArray();
//        $trends = sort($a);

//        Cache::put($key, $trends, 86400);
//        Cache::put($key_long, $trends, 8640000000);
//        }
//        return response()->json([
//            'status' => 0,
//            'data' => '****' . $a
//                $currency_trends
//                $trends
//        ]);
    }

    /**
     * Generate and get trends list
     * @return JsonResponse
     */
    public function trending(): JsonResponse
    {
        $key = 'short_time_cache_currency_trending';
        $key_long = 'long_time_cache_currency_trending';
        $key_lock = 'lock_time_cache_currency_trending';
        if (Cache::has('trends')) {
            $trending_pairs = Cache::get('trends');
        } else {
            if (Cache::has($key_lock)) {
                if (Cache::has($key_long)) {
                    $trending_pairs = Cache::get($key_long);
                    return response()->json([
                        'status' => 0,
                        'data' => $trending_pairs
                    ]);
                } else {
                    abort(500);
                }
            }
            Cache::put($key_lock, 1, 86400);
            $d = array();
            $stop = Carbon::now()->toDateString();
            $start = Carbon::now()->subHours(24)->toDateString();
            $currency_trends = $this->trend_repository->dailyTrends($start, $stop);
            foreach ($currency_trends as $currency_trend) {
                $d[] = $currency_trend->viewable_id;
            }
            $trends = collect(array_keys(array_count_values($d)))->take(4)->toArray();
            $trending_pairs = GlobalCurrency::query()->whereIn('id', $trends)->pluck('pair_id');
            Cache::put($key, $trending_pairs, 86400);
            Cache::put($key_long, $trending_pairs, 259200);
        }
        return response()->json([
            'status' => 0,
            'data' => $trending_pairs
        ]);
    }

    public function lastAddedCurrencies()
    {
        $key = 'short__time_cache_last__trending';
        $key_long = 'long__time_cache_last__trending';
        $key_lock = 'lock__time_cache_last__trending';
        srand(time());
        $r = rand(1, 100);
        $mod = $r % 10;
        if ($mod != 0 && Cache::has($key_long)) {
            $globals = Cache::get($key_long);
        } else {
            if (Cache::has($key)) {
                $globals = Cache::get($key_long);
            } else {
                $last_trends = Currency::query()->orderByDesc('created_at')->take(4)->pluck('id')->toArray();
                $globals = GlobalCurrency::query()->whereIn('currency_id', $last_trends)->pluck('pair_id');
                Cache::put($key, $globals, 600);
                Cache::put($key_long, $globals, 60000);
            }
        }

        return response()->json([
            'status' => 0,
            'data' => [
                274,256,246
            ]
        ]);

    }

    /**
     * Generate and get currency trends list
     * @return JsonResponse
     */
    public
    function currencyTrends(): JsonResponse
    {
        $key = 'short_time_cache_currency_trends';
        $key_long = 'long_time_cache_currency_trends';
//        $lock = 'lock_time_for_currency_trends';

        if (Cache::has($key)) {
            $currency_trends = Cache::get($key);
        } else {
//            if (Cache::has($lock)) {
//                if (Cache::has($key_long)) {
//                    $currency_trends = Cache::get($key_long);
//                    return response()->json([
//                        'status' => 0,
//                        'data' => $currency_trends
//                    ]);
//                } else {
//                    abort(500);
//                }
//            }
//            Cache::put($lock, 1, 43200);
            $currency_trends = $this->trending_currency_repository->getAllTrendingCurrency()->pluck('id');
            Cache::put($key, $currency_trends, 259200);
//            Cache::put($key_long, $currency_trends, 4320000000);
        }


        return response()->json([
            'status' => 0,
            'data' =>[2,9,46]
        ]);
    }

    /**
     * return random global currencies
     * @param Request $request
     * @return JsonResponse
     */
    public function getGlobalTrends2(int $id): JsonResponse
    {
        $key_short = 'short_time_global_random_trends_for_global:' . $id;
        $key_long = 'long_time_global_random_trends_for_global:' . $id;
        $lock = 'lock_time_global_random_trends';
        if (Cache::has($key_short)) {
            $rand = Cache::get($key_short);
        } else {
            if (Cache::has($lock)) {
                if (Cache::has($key_long)) {
                    $rand = Cache::get($key_long);
                    return response()->json([
                        'status' => 0,
                        'data' => $rand
                    ]);
                }
            }
            Cache::put($lock, 1, 18000);
            $currencies = $this->currency_repository->getAllGlobalCurrencies()->whereBetween('rank', [1, 100])->where('id', '!=', 3)->pluck('id')->toArray();
            $rand = array_rand($currencies, 5);
            Cache::put($key_short, $rand, 18000);
            Cache::put($key_long, $rand, 259200);
        }
        return response()->json([
            'status' => 0,
            'data' => $rand
        ]);
    }

    /**
     * return random global currencies
     * @return JsonResponse
     */
    public
    function getGlobalTrends(): JsonResponse
    {
        $key_short = 'short_time_global_random_trends';
        $key_long = 'long_time_global_random_trends';
        $lock = 'lock_time_global_random_trends';
        if (Cache::has($key_short)) {
            $rand = Cache::get($key_short);
        } else {
            if (Cache::has($lock)) {
                if (Cache::has($key_long)) {
                    $rand = Cache::get($key_long);
                    return response()->json([
                        'status' => 0,
                        'data' => $rand
                    ]);
                } else {
                    abort(500);
                }
            }
            Cache::put($lock, 1, 18000);

            $currencies = $this->currency_repository->getAllGlobalCurrencies()->whereBetween('rank', [1, 100])->where('id', '!=', 3)->pluck('id')->toArray();
            $rand = array_rand($currencies, 5);
            Cache::put($key_short, $rand, 18000);
            Cache::put($key_long, $rand, 259200);
        }

        return response()->json([
            'status' => 0,
            'data' => $rand
        ]);
    }
}

