<?php

namespace App\Http\Controllers\API;

use App\Factories\CommentRepoFactory;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\GlobalCurrency\AddComment;
use App\Http\Requests\Api\GlobalCurrency\GetComment;
use App\Http\Requests\Api\GlobalCurrency\GetGlobalCurrency;
use App\Http\Requests\Api\GlobalCurrency\SearchGC;
use App\Http\Requests\Api\GlobalCurrency\UpdateComment;
use App\Http\Resources\BannerResource;
use App\Http\Resources\Comments;
use App\Http\Resources\GlobalCurrenciesList;
use App\Http\Resources\Pool;
use App\Models\Banner;
use App\Models\Comment;
use App\Models\GlobalCurrency;
use App\Repositories\Interfaces\BannerRepositoryInterface;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Repositories\Interfaces\DislikeRepositoryInterface;
use App\Repositories\Interfaces\FavoriteRepositoryInterface;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Repositories\Interfaces\IncludeToRazminexRepositoryInterface;
use App\Repositories\Interfaces\LikeRepositoryInterface;
use App\Repositories\Interfaces\PairsRepositoryInterface;
use App\Services\Interfaces\UserAuthenticatorServiceInterface;
use App\Services\Interfaces\CommentServiceInterface;
use App\Services\Interfaces\LikeDislikeServiceInterface;
use App\Services\Interfaces\ViewCounterServiceInterface;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;
use Kavenegar\KavenegarApi;


class GlobalCurrencyController extends Controller
{
    /**
     * Global Currency repository container
     * @var GlobalCurrencyInterface
     */
    private GlobalCurrencyInterface $currency;

    /**
     * User authenticator service container
     * @var UserAuthenticatorServiceInterface
     */
    protected UserAuthenticatorServiceInterface $user_authenticator_service;

    /**
     * Comment Repository container
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $comment_repository;

    /**
     * Comment service container
     * @var CommentServiceInterface
     */
    private CommentServiceInterface $comment_service;

    /**
     * Like repository container
     * @var LikeRepositoryInterface
     */
    protected LikeRepositoryInterface $like_repository;

    /**
     * Dislike Repository container
     * @var DislikeRepositoryInterface
     */
    protected DislikeRepositoryInterface $dislike_repository;

    /**
     * View counter service container
     * @var ViewCounterServiceInterface
     */
    protected ViewCounterServiceInterface $view_counter_service;

    /**
     * like service container
     * @var LikeDislikeServiceInterface
     */
    private LikeDislikeServiceInterface $like_dislike_service;

    /**
     * FavoriteRepository Container
     * @var FavoriteRepositoryInterface
     */
    private FavoriteRepositoryInterface $favorite_repository;

    /**
     * IncludeToRazminexRepository Container
     * @var IncludeToRazminexRepositoryInterface
     */
    private IncludeToRazminexRepositoryInterface $include_to_razminex_repository;

    /**
     * Construct and inject necessary services to their container
     * GlobalCurrencyController constructor.
     * @param GlobalCurrencyInterface $currency
     * @param UserAuthenticatorServiceInterface $user_authenticator_service
     * @param BannerRepositoryInterface $banner_repository
     * @param CommentServiceInterface $comment_service
     * @param LikeRepositoryInterface $like_repository
     * @param DislikeRepositoryInterface $dislike_repository
     * @param ViewCounterServiceInterface $view_counter_service
     * @param FavoriteRepositoryInterface $favorite_repository
     * @param IncludeToRazminexRepositoryInterface $include_to_razminex_repository
     * @param LikeDislikeServiceInterface $like_dislike_service
     */
    public function __construct(GlobalCurrencyInterface $currency, UserAuthenticatorServiceInterface $user_authenticator_service, CommentServiceInterface $comment_service, LikeRepositoryInterface $like_repository, DislikeRepositoryInterface $dislike_repository, ViewCounterServiceInterface $view_counter_service, FavoriteRepositoryInterface $favorite_repository, IncludeToRazminexRepositoryInterface $include_to_razminex_repository, LikeDislikeServiceInterface $like_dislike_service, PairsRepositoryInterface $pairsRepository)
    {
        $this->currency = $currency;
        $this->user_authenticator_service = $user_authenticator_service;
        $this->comment_repository = CommentRepoFactory::createRepository();
        $this->comment_service = $comment_service;
        $this->dislike_repository = $dislike_repository;
        $this->like_repository = $like_repository;
        $this->view_counter_service = $view_counter_service;
        $this->favorite_repository = $favorite_repository;
        $this->include_to_razminex_repository = $include_to_razminex_repository;
        $this->like_dislike_service = $like_dislike_service;
        $this->pairsRepository = $pairsRepository;
    }

    /**
     * return trading view's chart according to given symbol
     * @param string $symbol
     * @param Request $request
     * @return Application|Factory|View
     */
    public function tradingViewBySymbol(string $symbol, Request $request)
    {
        return view('global-currency.trading_view_global2', ['symbol' => $symbol, 'theme' => $request->input('theme', 'lite')]);
    }

    /**
     *  return trading view's chart according to given pairId
     * @param Request $request
     * @param int $pair_id
     * @return Application|Factory|View
     */
    public function tradingViewByPairId(Request $request, int $pair_id)
    {
        $key = 'symbol_for_pair:' . $pair_id;
        if (Cache::has($key)) {
            $symbol = Cache::get($key);
        } else {
            $symbol = GlobalCurrency::query()->where('pair_id', '=', $pair_id)->pluck('symbol')[0];
            Cache::put($key, $symbol, 86400);
        }
        return view('global-currency.trading_view_global2', ['symbol' => $symbol, 'theme' => $request->input('theme', 'lite')]);
    }

    public function getTest()
    {
        GlobalCurrency::query()->where('rank','>',6700)->update(['is_hidden' => 1]);
    }

    /**
     * Get and return List of global currencies
     * @param SearchGC $request
     * @return AnonymousResourceCollection
     */
    public function index(SearchGC $request): AnonymousResourceCollection
    {
        $from = $request->input('offset', 0);
        $take = $request->input('limit', 100);
        $sort = $request->input('sort_by');
        $search['ramzinex_currencies'] = $request->input('ramzinex_currencies');
        $key = 'short_time_cache_global--currencies_list_limit_for_all';
        $keyLong = 'long_time_cache:global-currencies_list_limit_for_all';
        srand(time());
        $global_currencies_count = $this->currency->globalCurrenciesCount();
        $r = rand(1, 100);
        $mod = $r % 10;
        if ($mod != 0 && Cache::has($keyLong)) {
            $global_currencies = Cache::get($keyLong);
        } else {
            if (Cache::has($key)) {
                $global_currencies = Cache::get($key);
            } else {
                $global_currencies = $this->currency
                    ->getAllGlobalCurrencies(true, true, 0, 1000, null, true, $sort, $search, true);;
                Cache::put($key, $global_currencies, 300);
                Cache::put($keyLong, $global_currencies, 259200);
            }
        }
        return GlobalCurrenciesList::collection($global_currencies)->additional([
            'status' => 0,
            'count' => $global_currencies_count
        ]);
    }

    /**
     * Get and return specific Global Currency detail
     * @param Request $request
     * @param int $id
     * @return GlobalCurrenciesList
     */
    public function show(Request $request, int $id): GlobalCurrenciesList
    {
        $key = 'short_time_cache:global_detail_for_id:' . $id;
        $key_long = 'long_time_cache:global_detail_for_id:' . $id;
        $lock = 'lock_time_cache:global_detail_for_id:' . $id;
        if ($request->hasHeader('Authorization')) {
            $user_id = $this->user_authenticator_service->getUserId($request);
        }
        srand(time());
        $r = rand(1, 100);
        $mod = $r % 10;
        if ($mod != 0 && Cache::has($key_long)) {
            $global_currency = Cache::get($key_long);
        } else {
            if (Cache::has($key)) {
                $global_currency = Cache::get($key);
            } else {

                $global_currency = $this->currency->getById($id, true, true, null, null, true);
                Cache::put($key, $global_currency, 300);
                Cache::put($key_long, $global_currency, 259200);
            }
        }

        $global_currency->append(['description', 'webview_url']);
        $this->view_counter_service->addViews($global_currency, $user_id ?? null);
        return new GlobalCurrenciesList($global_currency);

    }


    /**
     * show global currency's related banners
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function showRelatedBanners(Request $request, int $id): JsonResponse
    {
        $from = $request->input('offset', 0);
        $take = $request->input('limit', 10);
        $key = 'short_time__cache:global__' . $id . 'related_banners_limit:' . $take . 'offset:' . $from;
        $key_long = 'long_time__cache:global__' . $id . 'related_banners_limit:' . $take . 'offset:' . $from;
        $key_lock = 'lock_time__cache:global_' . $id . 'related_banners_limit:' . $take . 'offset:' . $from;
        srand(time());
        $r = rand(1, 1000);
        $mod = $r % 10;
        if ($mod != 0 && Cache::has($key_long)) {
            $banners = Cache::get($key_long);
        } else {
            if (Cache::has($key)) {
                $banners = Cache::get($key);
            } else {
                $banners = Banner::query()->WhereHas('globalCurrencies', function (Builder $query) use ($id) {
                    $query->where('global_currency_id', '=', $id);
                })->when($from, function (Builder $query) use ($from) {
                    $query->skip($from);
                })->when($take, function (Builder $query) use ($take) {
                    $query->take($take);
                })->orderByDesc('updated_at')->get();
                Cache::put($key, $banners, 84600);
                Cache::put($key_long, $banners, 259200);
            }

        }
        return response()->json([
            'status' => 0,
            'data' => BannerResource::collection($banners),
            'count' => Count($banners)
        ]);
    }

    /**
     * return global's accepted comments
     * @param GetComment $request
     * @return AnonymousResourceCollection
     */
    public function getComments(GetComment $request): AnonymousResourceCollection
    {
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);
        $global_currency_key = 'global--currency:' . $request->global_currency_id;
        $comments_key = 'short_time_cache_global_comment_for:' . $request->global_currency_id . 'limit:' . $limit . 'offset:' . $offset;
        $comments_key2 = 'long_time_cache:global_comment_for:' . $request->global_currency_id . 'limit:' . $limit . 'offset:' . $offset;
        $comments_lock = 'long_time_cache:global_comment_for:' . $request->global_currency_id;
        $gc_comment_count = 'short_time_cache:global_' . $request->global_currency_id . 'comments_count';
        $gc_comment_count2 = 'long_time_cache:global_' . $request->global_currency_id . 'comments_count';
        $gc_comment_count_lock = 'long_time_cache:global_' . $request->global_currency_id . 'comments_count';
        srand(time());
        $r = rand(1, 100);
        $mod = $r % 30;
        if ($mod != 0 && Cache::has($comments_key2)) {
            $comments = Cache::get($comments_key2);
            $comments_count = Cache::get($gc_comment_count2);
        } else {
            if (Cache::has($comments_key)) {
                $comments = Cache::get($comments_key);
            } else {
                $global_currency = $this->currency->getById($request->global_currency_id);
                Cache::put($global_currency_key, $global_currency, 86400);
                $comments = $this->comment_repository
                    ->getAllComment($global_currency, false, true, $offset, $limit, true, null, $user_id ?? null);
                Cache::put($comments_key, $comments, 43200);
                Cache::put($comments_key2, $comments, 259200);
            }
            if (Cache::has($gc_comment_count)) {
                $comments_count = Cache::get($gc_comment_count);
            } else {
                $global_currency = Cache::get($global_currency_key);
                $comments_count = $this->comment_repository->getCommentsCount($global_currency, true, null);
                Cache::put($gc_comment_count, $comments_count, 43200);
                Cache::put($gc_comment_count2, $comments_count, 259200);
            }
        }

        return Comments::collection($comments)->additional([
            'status' => 0,
            'count' => $comments_count
        ]);
    }

    /**
     * add a currency to favorites
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public function addToFavorite(GetGlobalCurrency $request): JsonResponse
    {
        $global_currency_id = $request->global_currency_id;
        $global_currency = $this->currency->getById($global_currency_id);
        $user_id = $this->user_authenticator_service->getUserId($request);
        if ($this->favorite_repository->isAddedToFavorites($global_currency, $user_id)) {
            $this->favorite_repository->deleteFavorite($global_currency, $user_id, 'global');
        } else {
            $this->favorite_repository->createFavorite($global_currency, $user_id, 'global');
        }
        $favored_list = 'user_' . $user_id . '_favored_faq_list';
        if (Cache::has($favored_list)) {
            $users_favorites_list = Cache::get($favored_list);
        } else {
            $users_favorites_list = $this->favorite_repository->getUserFavoredGlobalList($user_id, 'faq');
            Cache::put($favored_list, $users_favorites_list, 86300);
        }
        return response()->json([
            'status' => 0,
            'data' => [
                'favorites_count' => $this->favorite_repository->getFavoriteCount($global_currency),
                'favored_globals' => $users_favorites_list->toArray()
            ]
        ]);
    }

    /**
     * check if global_currency added to favorite or not
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public function isAddedToFavorites(GetGlobalCurrency $request): JsonResponse
    {
        return response()->json([
            'status' => 0,
            'data' => [
                'is_favored' => false
            ]
        ]);
    }

    /**
     * get user's feedback for each global currency
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public function like(GetGlobalCurrency $request): JsonResponse
    {
        $global_currency_id = $request->global_currency_id;
        $global_currency = $this->currency->getById($global_currency_id);
        $user_id = $this->user_authenticator_service->getUserId($request);
        try {
            $testKey = 'global_new_like_2_' . $request->global_currency_id;
            $r = Redis::connection('like_dislike');
            if ($user_id) {
                $r->sadd($testKey, $user_id);
            }
        } catch (\Exception $e) {
            Cache::increment('count_all_glob_new_error_2:' . $request->global_currency_id);
        }
        $result = $this->like_dislike_service->createLike($global_currency, $request, 'global');

        if (!$result) {
            return $this->generateFeedbackResponse();
        }
        $likes_list = 'user_' . $user_id . '_liked_global_list';
        if (Cache::has($likes_list)) {
            $users_likes_list = Cache::get($likes_list);
        } else {
            $users_likes_list = $this->like_repository->getUserLikesList($user_id, 'global');
            Cache::put($likes_list, $users_likes_list, 86200);

        }
        $dislikes_list = 'user_' . $user_id . '_disliked_global_list';
        if (Cache::has($dislikes_list)) {
            $users_dislikes_list = Cache::get($dislikes_list);
        } else {
            $users_dislikes_list = $this->dislike_repository->getUserDisLikesList($user_id, 'global');
            Cache::put($dislikes_list, $users_dislikes_list, 86200);
        }
        $new_like_count = $this->like_repository->getLikeCount($global_currency);
        $new_dislike_count = $this->dislike_repository->getDisLikeCount($global_currency);
        return $this->generateFeedbackResponse(true, $new_like_count, $new_dislike_count, $users_likes_list, $users_dislikes_list);
    }

    /**
     * Check if user liked global or not
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public function isLiked(GetGlobalCurrency $request): JsonResponse
    {
        return response()->json([
            'status' => 0,
            'data' => [
                'is_liked' => 'false'
            ]
        ]);
    }

    /**
     * get user's feedback for each question
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public function disLike(GetGlobalCurrency $request): JsonResponse
    {
        $global_currency_id = $request->global_currency_id;
        $global_currency = $this->currency->getById($global_currency_id);
        $user_id = $this->user_authenticator_service->getUserId($request);
        $result = $this->like_dislike_service->createDislike($global_currency, $request, 'global');

        if (!$result) {
            return $this->generateFeedbackResponse();
        }
        $likes_list = 'short_time_cache:user_' . $user_id . '_liked_global_list';
        $likes_list2 = 'long_time_cache:user_' . $user_id . '_liked_global_list';
        if (Cache::has($likes_list)) {
            $users_likes_list = Cache::get($likes_list);
        } else {
            $users_likes_list = $this->like_repository->getUserLikesList($user_id, 'global');
            Cache::put($likes_list2, $users_likes_list, 8640000);
            $lock_like = Cache::lock($likes_list, 86400);
            if ($lock_like->get()) {
                Cache::get($likes_list2);
            } else {
                Cache::put($likes_list, $users_likes_list, 86400);
            }

        }
        $dislikes_list = 'user_' . $user_id . '_disliked_global_list';
        if (Cache::has($dislikes_list)) {
            $users_dislikes_list = Cache::get($dislikes_list);
        } else {
            $users_dislikes_list = $this->dislike_repository->getUserDisLikesList($user_id, 'global');
            Cache::put($dislikes_list, $users_dislikes_list, 86400);
        }
        $new_dislike_count = $this->dislike_repository->getDisLikeCount($global_currency);
        $new_like_count = $this->like_repository->getLikeCount($global_currency);
        return $this->generateFeedbackResponse(true, $new_like_count, $new_dislike_count, $users_likes_list, $users_dislikes_list);
    }

    /**
     * Check if user disliked global or not
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public
    function isDisliked(GetGlobalCurrency $request): JsonResponse
    {
        return response()->json([
            'status' => 0,
            'data' => [
                'is_disliked' => 'false'
            ]
        ]);
    }

    /**
     * add selected global currency to includeToRamzinex 's list
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public function includeToRamzinex(GetGlobalCurrency $request): JsonResponse
    {
        $global_currency_id = $request->global_currency_id;
        $global_currency = $this->currency->getById($global_currency_id);
        $user_id = $this->user_authenticator_service->getUserId($request);
        $is_included = $this->include_to_razminex_repository->checkIfIncludeToRamzinexExists($global_currency, $user_id);

        if ($is_included) {
            $result = $this->include_to_razminex_repository->deleteIncludeToRamzinex($global_currency);
        } else {
            $result = $this->include_to_razminex_repository->createIncludeToRamzinex($global_currency, $user_id);
        }

        if (!$result) {
            return $this->generateIncludeToRamzinexResponce();
        }

        $included_count = $this->include_to_razminex_repository->getIncludeToRamzinexCount($global_currency);
        return $this->generateIncludeToRamzinexResponce(true, $included_count);
    }


    /**
     * store created comment
     * @param AddComment $request
     * @return JsonResponse
     */
    public
    function addComment(AddComment $request): JsonResponse
    {
        $global_currency = $this->currency->getById($request->global_currency_id);
        $comment = $this->comment_service->createComment($global_currency, $request);
        $user_id = $this->user_authenticator_service->getUserId($request);
        try {
            $testKey = 'global_comment_' . $request->global_currency_id;
            $r = Redis::connection('like_dislike');
            $r->sadd($testKey, $user_id);
        } catch (\Exception $e) {
            Cache::increment('count_all_comment_glob_error_dadbd71:' . $request->global_currency_id);
        }
        if (!$comment) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on creating comment',
                    'fa' => 'ثبت کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'description' => [
                'en' => 'Comment created successfully',
                'fa' => 'کامنت شما با موفقیت ثبت شد'
            ]
        ]);
    }

    public function answerPoll(AddComment $request)
    {
        $global_currency = $this->currency->getById($request->global_currency_id);
        $data = $request->only(['comment_id', 'depth_id', 'answer_index', 'global_currency_id']);
        $Poll_comment = Comment::query()->findOrFail($data['depth_id']);
        $data['user_id'] = $this->user_authenticator_service->getUserId($request);
        $answer_exists = Comment::query()->where('type', '=', 'survey_answer')->where('depth_id', '=', $data['depth_id'])->where('user_id', '=', $data['user_id'])->exists();
        try {
            $answered = Comment::updateOrCreate(['comment_id' => $data['comment_id'], 'commentable_id' => $data['global_currency_id'], 'commentable_type' => 'App\Models\GlobalCurrency', 'depth_id' => $data['depth_id'], 'user_id' => $data['user_id'], 'type' => 'survey_answer', 'status' => 1], ['answer_index' => $data['answer_index']]);

        } catch (\Exception $exception) {
            Log::error('answer_poll' . $exception->getMessage());
        }
        if (!$answered) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on creating comment',
                    'fa' => 'ثبت کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'description' => [
                'en' => 'Comment created successfully',
                'fa' => 'کامنت شما با موفقیت ثبت شد'
            ],
            'data' => new Pool($Poll_comment)
        ]);
    }

    /**
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function showPoll(Request $request): AnonymousResourceCollection
    {
        $global_currency_id = $request->id;
        $limit = $request->input('limit', 10);
        $offset = $request->input('offset', 0);
        $comments = Comment::query()
            ->where('commentable_type', '=', 'App\Models\GlobalCurrency')
            ->where('commentable_id', '=', $global_currency_id)
            ->where('status', '=', 1)
            ->where('type', '=', 'survey')
            ->limit($limit)->offset($offset)->get();
        return Pool::collection($comments);
    }

    /**
     * update an existing comment
     * @param UpdateComment $request
     * @return JsonResponse
     */
    public
    function updateComment(UpdateComment $request): JsonResponse
    {
        $result = $this->comment_service->updateComment($request);

        if (!$result) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on Editing comment',
                    'fa' => 'ویرایش کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'description' => [
                'en' => 'Comment edited successfully',
                'fa' => 'ویرایش کامنت با موفقیت انجام شد'
            ]
        ]);
    }

    public function ramzinexCurrencies(int $id)
    {
        $global_key_short = 'short_time_cache_ramzinex_global:' . $id;

        if (Cache::has($global_key_short)) {
            $global = Cache::get($global_key_short);
        } else {
            $global = GlobalCurrency::query()->where('pair_id', "=", $id)->get();
            Cache::put($global_key_short, $global, 300);
        }
        return response()->json(['status' => 0,
            'data' => [$global]
        ]);
    }

    public
    function ramzinexCurrenciesWithGlobals()
    {
        $key = 'ramzinex__currencies_id_with_globals_id';
        if (Cache::has($key)) {
            $data = Cache::get($key);
        } else {
            $data = ["1" => 1, "4" => 2, "5" => 10, "6" => 4, "7" => 5, "9" => 6, "10" => 3, "11" => 14,
                "12" => 32, "13" => 9, "14" => 16, "15" => 12, "17" => 8, "19" => 7, "20" => 19, "21" => 33, "22" => 17,
                "23" => 45, "25" => 26, "26" => 15, "27" => 11, "28" => 124, "30" => 86, "33" => 36, "37" => 41, "46" => 6689,
                "51" => 135, "56" => 73, "61" => 20, "66" => 155, "71" => 1660, "76" => 189,
                "81" => 69, "86" => 6704, "91" => 6948, "96" => 25, "101" => 1656, "106" => 40,
                "111" => 138, "116" => 24, "121" => 65, "126" => 35, "131" => 52, "134" => 97, "135" => 90, "136" => 34,
                "137" => 28, "138" => 1657, "139" => 9275, "140" => 7604, "141" => 50, "142" => 150,
                "143" => 8339, "144" => 6963, "145" => 9244, "146" => 6944, "147" => 55, "148" => 38, "149" => 102, "150" => 7523, "151" => 47, "152" => 101
            ];
            Cache::put($key, $data, 31536000);
        }
        return response()->json([
            'status' => 0,
            'data' => $data
        ]);
    }

    public
    function ramzinexPairsWithGlobals()
    {
        $key = 'ramzinex_pairs_id_with_globals_id';
        if (Cache::has($key)) {
            $data = Cache::get($key);
        } else {
            $data = ["1" => 2, "2" => 3, "3" => 17, "4" => 4,
                "5" => 10, "6" => 11, "7" => 33, "8" => 29, "9" => 23, "10" => 5, "11" => 46
                , "12" => 26, "14" => 19, "15" => 44, "16" => 25, "17" => 39, "19" => 34,
                "20" => 76, "24" => 131, "25" => 111, "26" => 42, "28" => 241,
                "32" => 21, "33" => 36, "34" => 239, "35" => 141, "36" => 51,
                "38" => 254, "40" => 121, "41" => 52, "45" => 40, "47" => 257,
                "50" => 247, "52" => 146, "54" => 270, "55" => 253, "65" => 136, "69" => 96,
                "73" => 71, "86" => 50, "89" => 267, "90" => 237,
                "94" => 271, "97" => 152, "101" => 258,
                "102" => 255, "114" => 275, "124" => 48, "135" => 66,
                "138" => 126, "150" => 248, "155" => 81, "181" => 269,
                "189" => 91, "208" => 276, "210" => 264, "211" => 268,
                "258" => 266, "1656" => 116, "1657" => 243, "1660" => 86,
                "6407" => 274, "6689" => 61, "6691" => 262, "6704" => 101,
                "6944" => 252, "6948" => 106, "6963" => 250, "7347" => 259,
                "7367" => 263, "7523" => 256, "7604" => 246, "7886" => 261,
                "8339" => 249, "8353" => 260, "8950" => 272, "9244" => 251, "9275" => 244

            ];
            Cache::put($key, $data, 31536000);
        }
        return response()->json([
            'status' => 0,
            'data' => $data
        ]);
    }

    /**
     * it deletes feedbacks(if exist) then delete comments
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public
    function deleteComment(Request $request, int $id): JsonResponse
    {
        $result = $this->comment_service->deleteComment($request, $id);

        if (!$result) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on deleting comment',
                    'fa' => 'حذف کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'description' => [
                'en' => 'Comment delete successfully',
                'fa' => 'کامنت شما با موفقیت حذف شد'
            ]
        ]);
    }


    /**
     * Generate favorite response
     * @param bool $is_success
     * @param int $favorite_count
     * @return JsonResponse
     */
    private
    function generateFavoriteResponce(bool $is_success = false, int $favorite_count = 0)
    {
        if ($is_success) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'There was a problem in adding global to favorites',
                    'fa' => 'مشکلی در افزودن global به لیست منتخبین رخ داده است',
                ],
                'data' => [
                    'favorite_counter' => $favorite_count
                ]
            ]);
        }
        return response()->json([
            'status' => 1,
            'description' => [
                'en' => 'There was a problem in adding global to favorites',
                'fa' => 'مشکلی در افزودن global به لیست منتخبین رخ داده است',
            ]
        ]);
    }

    /**
     * Generate feedback success/unsuccess response
     * @param bool $is_success
     * @param int $like_count
     * @param int $dislike_count
     * @param object|null $liked_list
     * @param null $disliked_list
     * @return JsonResponse
     */
    private
    function generateFeedbackResponse(bool $is_success = false, int $like_count = 0, int $dislike_count = 0, object $liked_list = null, $disliked_list = null): JsonResponse
    {
        if ($is_success) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'You\'re like submitted successfully',
                    'fa' => 'فیدبک شما با موفقیت ثبت گردید',
                ],
                'data' => [
                    'likes_count' => $like_count,
                    'dislikes_count' => $dislike_count,
                    'liked_globals_list' => $liked_list->toArray(),
                    'disliked_globals_list' => $disliked_list->toArray()
                ]
            ]);
        }

        return response()->json([
            'status' => 1,
            'description' => [
                'en' => 'There was a problem in submitting your feedback',
                'fa' => 'مشکلی در ثبت فیدبک شما بوجود آمده است',
            ]
        ]);
    }

    /**
     * Generate feedback success/unsuccess response
     * @param bool $is_success
     * @param int $includes_count
     * @return JsonResponse
     */
    private
    function generateIncludeToRamzinexResponce(bool $is_success = false, int $includes_count = 0): JsonResponse
    {
        if ($is_success) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'You\'re request submitted successfully',
                    'fa' => 'درخواست شما با موفقیت ثبت گردید',
                ],
                'data' => [
                    'includes_count' => $includes_count

                ]
            ]);
        }

        return response()->json([
            'status' => 1,
            'description' => [
                'en' => 'There was a problem in submitting your request',
                'fa' => 'مشکلی در ثبت درخواست شما بوجود آمده است',
            ]
        ]);
    }

//    public function test()
//    {
//        return "hi";
//        try {
//            $global = $this->currency->getGlobalPairByPairId(2, true, false, true);
//
//        } catch (\Exception|QueryException $ex) {
//            Log::error('pair error' . $ex->getMessage());
//            return '**';
//        }
//        return $global;

//    }

}
