<?php

namespace App\Http\Controllers\API;

use App\Factories\CommentRepoFactory;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Comment\GetComment;
use App\Http\Requests\Api\Comment\CommentReplies;
use App\Http\Resources\Comments;
use App\logic\ConfigManager;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Repositories\Interfaces\DislikeRepositoryInterface;
use App\Repositories\Interfaces\LikeRepositoryInterface;
use App\Services\Interfaces\LikeDislikeServiceInterface;
use App\Services\UserAuthentication\UserAuthenticatorService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Cache;

class CommentController extends Controller
{
    /**
     * comment repository container
     * @var CommentRepositoryInterface
     */
    protected CommentRepositoryInterface $comment_repository;

    /**
     * Like/Dislike service container
     * @var LikeDislikeServiceInterface
     */
    private LikeDislikeServiceInterface $like_dislike_service;

    /**
     * Like repository container
     * @var LikeRepositoryInterface
     */
    private LikeRepositoryInterface $like_Repository;

    /**
     * Dislike repository container
     * @var DislikeRepositoryInterface
     */
    private DislikeRepositoryInterface $dislike_Repository;

    /**
     * User Authenticator Service container
     * @var UserAuthenticatorService
     */
    protected UserAuthenticatorService $user_authenticator_service;

    /**
     * Construct and inject necessary services to their container
     * @param LikeDislikeServiceInterface $like_dislike_service
     * @param LikeRepositoryInterface $like_Repository
     * @param DislikeRepositoryInterface $dislike_Repository
     * @param UserAuthenticatorService $user_authenticator_service
     */
    public function __construct(LikeDislikeServiceInterface $like_dislike_service, LikeRepositoryInterface $like_Repository, DislikeRepositoryInterface $dislike_Repository, UserAuthenticatorService $user_authenticator_service)
    {
        $this->comment_repository = CommentRepoFactory::createRepository();
        $this->like_dislike_service = $like_dislike_service;
        $this->like_Repository = $like_Repository;
        $this->dislike_Repository = $dislike_Repository;
        $this->user_authenticator_service = $user_authenticator_service;
    }

    /**
     * Get and return comment replies
     * @param CommentReplies $request
     * @return AnonymousResourceCollection
     */
    public function getCommentReplies(CommentReplies $request): AnonymousResourceCollection
    {
        $comment_id = $request->input('comment_id');
        $from = $request->input('offset', 0);
        $take = $request->input('limit', 10);

        if ($request->hasHeader('Authorization')) {
            $user_id = $this->user_authenticator_service->getUserId($request);
        }

        $comments = $this->comment_repository->getAllComment(null, false, true, $from, $take, true, $comment_id, $user_id ?? null);
        $comment_counts = $this->comment_repository->getCommentsCount(null, true, $comment_id);
        return Comments::collection($comments)->additional([
            'status' => 0,
            'count' => $comment_counts
        ]);
    }

    /**
     * create comment's like and delete comment's dislike if exists
     * @param GetComment $request
     * @return JsonResponse
     */
    public function likeComments(GetComment $request): JsonResponse
    {
        $comment = $this->comment_repository->getById($request->comment_id);
        $result = $this->like_dislike_service->createLike($comment, $request, 'comment');
        $user_id = $this->user_authenticator_service->getUserId($request);
        if (!$result) {
            return $this->generateCommentFeedbackResponse();
        }
        $likes_list = 'user_' . $user_id . '_liked_comment_list';
        if (Cache::has($likes_list)) {
            $users_likes_list = Cache::get($likes_list);
        } else {
            $users_likes_list = $this->like_Repository->getUserLikesList($user_id, 'comment');
            Cache::put($likes_list, $users_likes_list, 86400);

        }
        $dislikes_list = 'user_' . $user_id . '_disliked_comment_list';
        if (Cache::has($dislikes_list)) {
            $users_dislikes_list = Cache::get($dislikes_list);
        } else {
            $users_dislikes_list = $this->dislike_Repository->getUserDisLikesList($user_id, 'comment');
            Cache::put($dislikes_list, $users_dislikes_list, 86400);

        }
        $like_counter = $this->like_Repository->getLikeCount($comment);
        $dislike_count = $this->dislike_Repository->getDisLikeCount($comment);
        return $this->generateCommentFeedbackResponse(true, $like_counter, $dislike_count,$users_likes_list->toArray(),$users_dislikes_list->toArray());
    }

    /**
     * create comment's dislike and delete comment's like if exists
     * @param GetComment $request
     * @return JsonResponse
     */
    public function dislikeComments(GetComment $request): JsonResponse
    {
        $comment = $this->comment_repository->getById($request->comment_id);
        $result = $this->like_dislike_service->createDislike($comment, $request, 'comment');
        if (!$result) {
            return $this->generateCommentFeedbackResponse();
        }
        $user_id = $this->user_authenticator_service->getUserId($request);
        if (!$result) {
            return $this->generateCommentFeedbackResponse();
        }
        $likes_list = 'user_' . $user_id . '_liked_comment_list';
        if (Cache::has($likes_list)) {
            $users_likes_list = Cache::get($likes_list);
        } else {
            $users_likes_list = $this->like_Repository->getUserLikesList($user_id, 'comment');
            Cache::put($likes_list, $users_likes_list, 86400);

        }
        $dislikes_list = 'user_' . $user_id . '_disliked_comment_list';
        if (Cache::has($dislikes_list)) {
            $users_dislikes_list = Cache::get($dislikes_list);
        } else {
            $users_dislikes_list = $this->dislike_Repository->getUserDisLikesList($user_id, 'comment');
            Cache::put($dislikes_list, $users_dislikes_list, 86400);
        }
        $lik_count = $this->like_Repository->getLikeCount($comment);
        $dislike_count = $this->dislike_Repository->getDisLikeCount($comment);
        return $this->generateCommentFeedbackResponse(true, $lik_count, $dislike_count,$users_likes_list->toArray(),$users_dislikes_list->toArray());

    }

    /**
     * Generate comment's feedback success/unsuccess response
     * @param bool $is_success
     * @param int $like_count
     * @param int $dislike_count
     * @return JsonResponse
     */
    private function generateCommentFeedbackResponse(bool $is_success = false, int $like_count = 0, int $dislike_count = 0, array $like_list = [], array $dislike_list = []): JsonResponse
    {
        if ($is_success) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Your like submitted successfully',
                    'fa' => 'فیدبک شما با موفقیت ثبت گردید',
                ],
                'data' => [
                    'likes_count' => $like_count,
                    'dislikes_count' => $dislike_count,
                    'liked_comments' => $like_list,
                    'disliked_comments' => $dislike_list
                ]
            ]);
        }

        return response()->json([
            'status' => 1,
            'description' => [
                'en' => 'There was a problem in submitting your feedback',
                'fa' => 'مشکلی در ثبت فیدبک شما بوجود آمده است',
            ]
        ]);
    }

}
