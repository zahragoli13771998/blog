<?php

namespace App\Http\Controllers\API;

use App\Factories\CommentRepoFactory;
use App\Http\Requests\Api\Banner\CreateBannerComment;
use App\Http\Requests\Api\Banner\GetBanner;
use App\Http\Requests\Api\Banner\BannerComments;
use App\Http\Requests\Api\Banner\ActiveBanners;
use App\Http\Requests\Api\Banner\UpdateBannerComment;
use App\Http\Resources\BannerResource;
use App\Http\Resources\Comments;
use App\logic\ConfigManager;
use App\Models\Banner;
use App\Repositories\Interfaces\BannerRepositoryInterface;
use App\Repositories\Interfaces\BannerTypeRepositoryInterface;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Repositories\Interfaces\DislikeRepositoryInterface;
use App\Repositories\Interfaces\FavoriteRepositoryInterface;
use App\Repositories\Interfaces\LikeRepositoryInterface;
use App\Services\Interfaces\LikeDislikeServiceInterface;
use App\Services\Interfaces\ViewCounterServiceInterface;
use App\Services\UserAuthentication\UserAuthenticatorService;
use App\Services\Interfaces\CommentServiceInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

//Todo move like/dislikes operation to service layer
class BannerController
{

    /**
     * Banner repository container
     * @var BannerRepositoryInterface
     */
    private BannerRepositoryInterface $banner_repository;

    /**
     * Banner Type Repository container
     * @var BannerTypeRepositoryInterface
     */
    private BannerTypeRepositoryInterface $banner_type_repository;

    /**
     * Comment Repository container
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $comment_repository;

    /**
     * Comment service container
     * @var CommentServiceInterface
     */
    private CommentServiceInterface $comment_service;

    /**
     * Like repository container
     * @var LikeRepositoryInterface
     */
    protected LikeRepositoryInterface $like_repository;

    /**
     * Dislike Repository container
     * @var DislikeRepositoryInterface
     */
    protected DislikeRepositoryInterface $dislike_repository;

    /**
     * User Authenticator Service container
     * @var UserAuthenticatorService
     */
    protected UserAuthenticatorService $user_authenticator_service;

    /**
     * FavoriteRepository Container
     * @var FavoriteRepositoryInterface
     */
    private FavoriteRepositoryInterface $favorite_repository;

    /**
     * like service Container
     * @var LikeDislikeServiceInterface
     */
    private LikeDislikeServiceInterface $like_dislike_service;

    /**
     * ViewCountService Container
     * @var ViewCounterServiceInterface
     */
    private ViewCounterServiceInterface $view_counter_service;


    /**
     * Construct and inject necessary services to container
     * BannerController constructor.
     * @param BannerRepositoryInterface $bannerRepository
     * @param BannerTypeRepositoryInterface $banner_type_repository
     * @param ViewCounterServiceInterface $view_counter_service
     * @param LikeRepositoryInterface $like_repository
     * @param DislikeRepositoryInterface $dislike_repository
     * @param UserAuthenticatorService $user_authenticator_service
     * @param CommentServiceInterface $comment_service
     * @param FavoriteRepositoryInterface $favorite_repository
     * @param LikeDislikeServiceInterface $like_dislike_service
     */
    public function __construct(BannerRepositoryInterface $bannerRepository, BannerTypeRepositoryInterface $banner_type_repository, ViewCounterServiceInterface $view_counter_service, LikeRepositoryInterface $like_repository, DislikeRepositoryInterface $dislike_repository, UserAuthenticatorService $user_authenticator_service, CommentServiceInterface $comment_service, FavoriteRepositoryInterface $favorite_repository, LikeDislikeServiceInterface $like_dislike_service)
    {
        $this->banner_repository = $bannerRepository;
        $this->banner_type_repository = $banner_type_repository;
        $this->comment_repository = CommentRepoFactory::createRepository();
        $this->comment_service = $comment_service;
        $this->dislike_repository = $dislike_repository;
        $this->like_repository = $like_repository;
        $this->view_counter_service = $view_counter_service;
        $this->user_authenticator_service = $user_authenticator_service;
        $this->favorite_repository = $favorite_repository;
        $this->like_dislike_service = $like_dislike_service;
    }

    /**
     *  it returns all banners in json format
     * @param ActiveBanners $request
     * @return JsonResponse
     */
    public function showActiveBanners2(ActiveBanners $request): JsonResponse
    {
        $type_id = $request->input('type_id');
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);
        $key = 'short_time_cache:banners?limit=' . $limit . '&offset=' . $offset . '&type_id=' . $type_id;
        $key2 = 'long_time_cache:banners?limit=' . $limit . '&offset=' . $offset . '&type_id=' . $type_id;
        $count = 'banner_counts_type:' . $type_id;
        $count2 = 'long_time_cache:banner_counts_type:' . $type_id;
        $lock_banner = 'lock_time_cache:banners';
        $lock_banner_count = 'lock_time_cache:banners_count';
        if (Cache::has($key)) {
            $banners = Cache::get($key2);
        } else {
            if (Cache::has($lock_banner)) {
                if (Cache::has($key2)) {
                    $banners = Cache::get($key2);
                }
            }
            Cache::put($lock_banner, 1, 600);
            $banners = $this->banner_repository->getAllBanners($type_id, $offset, $limit, true, true, true, null, true);
            Cache::put($key, $banners, 600);
            Cache::put($key2, $banners, 259200);
        }
        if (Cache::has($count)) {
            $banners_count = Cache::get($count);
        } else {
            if (Cache::has($lock_banner_count)) {
                if (Cache::has($count2)) {
                    $banners_count = Cache::get($count2);
                    $banners = Cache::get($key2);
                    return response()->json([
                        'status' => 0,
                        'count' => $banners_count,
                        'data' => $this->generateJsonResponse($banners)
                    ]);
                }
            }
            $banners_count = $this->banner_repository->getBannersCount($type_id, true);
            Cache::put($count, $banners_count, 600);
            Cache::put($count2, $banners_count, 259200);
        }

        return response()->json([
            'status' => 0,
            'count' => $banners_count,
            'data' => $this->generateJsonResponse($banners)
        ]);
    }

    /**
     *  it returns all banners in json format
     * @param ActiveBanners $request
     * @return JsonResponse
     */
    public function showActiveBanners(ActiveBanners $request): JsonResponse
    {
        $type_id = $request->input('type_id');
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);
        $key = 'short_time_cache_banners?limit=' . $limit . '&offset=' . $offset . '&type_id=' . $type_id;
        $key2 = 'long_time_cache_banners?limit=' . $limit . '&offset=' . $offset . '&type_id=' . $type_id;
        $count = 'banner_counts_type:' . $type_id;
        $count2 = 'long_time_cache:banner_counts_type:' . $type_id;
        $lock_banner = 'lock_time_cache:banners';
        $lock_banner_count = 'lock_time_cache:banners_count';
        srand(time());
        $r = rand(1, 100);
        $mod = $r % 10;
        if (Cache::has($key)) {
            $banners = Cache::get($key2);
        } else {
            if (Cache::has($lock_banner)) {
                if (Cache::has($key2)) {
                    $banners = Cache::get($key2);
                }
            }
            Cache::put($lock_banner, 1, 600);
            if ($mod != 0 && Cache::has($key2)) {
                $banners = Cache::get($key2);
            } else {
                $banners = $this->banner_repository->getAllBanners($type_id, $offset, $limit, true, true, true, null, true);
                Cache::put($key, $banners, 600);
                Cache::put($key2, $banners, 259200);
            }

        }
        if (Cache::has($count)) {
            $banners_count = Cache::get($count);
        } else {
            if (Cache::has($lock_banner_count)) {
                if (Cache::has($count2)) {
                    $banners_count = Cache::get($count2);
                    $banners = Cache::get($key2);
                    return response()->json([
                        'status' => 0,
                        'count' => $banners_count,
                        'data' => $this->generateJsonResponse($banners)
                    ]);
                }
            }
            $banners_count = $this->banner_repository->getBannersCount($type_id, true);
            Cache::put($count, $banners_count, 600);
            Cache::put($count2, $banners_count, 259200);
        }

        return response()->json([
            'status' => 0,
            'count' => $banners_count,
            'data' => $this->generateJsonResponse($banners)
        ]);
    }


    /**
     * Get and return single banner
     * @param GetBanner $request
     * @return JsonResponse
     */
    public
    function getBanner2(GetBanner $request): JsonResponse
    {
        if ($request->hasHeader('Authorization')) {
            $user_id = $this->user_authenticator_service->getUserId($request);
        }
        $key = 'short_time_cache:banner_detail_for:' . $request->banner_id;
        $key2 = 'long_time_cache:banner_detail_for:' . $request->banner_id;
        $lock = 'lock_time_cache:banner_detail_for:' . $request->banner_id;
        srand(time());
        $r = rand(1, 100);
        $mod = $r % 10;

        if (Cache::has($key)) {
            $banner = Cache::get($key);
        } else {
            if (Cache::has($lock)) {
                if (Cache::has($key2)) {
                    $banner = Cache::get($key2);
                    return response()->json([
                        'status' => 0,
                        'data' => $this->generateBannerJsonResponse($banner)
                    ]);
                }
            }
            Cache::put($lock, 1, 600);
            if ($mod != 0 && Cache::has($key2)) {
                $banner = Cache::get($key2);
            } else {
                $banner = $this->banner_repository->getById($request->banner_id, true, true, null);
                Cache::put($key, $banner, 600);
                Cache::put($key2, $banner, 60000);
            }
        }

        $this->view_counter_service->addViews($banner, $user_id ?? null);


        return response()->json([
            'status' => 0,
            'data' => $this->generateBannerJsonResponse($banner)
        ]);
    }

    public
    function getBanner(GetBanner $request): JsonResponse
    {
        if ($request->hasHeader('Authorization')) {
            $user_id = $this->user_authenticator_service->getUserId($request);
        }
        $key = 'short_time_cache:banner_detail_for:' . $request->banner_id;
        $key2 = 'long_time_cache:banner_detail_for:' . $request->banner_id;
        $lock = 'lock_time_cache:banner_detail_for:' . $request->banner_id;
        srand(time());
        $r = rand(1, 100);
        $mod = $r % 10;
        if ($mod != 0 && Cache::has($key2)) {
            $banner = Cache::get($key2);
        } else {
            if (Cache::has($key)) {
                $banner = Cache::get($key2);
            } else {
                $banner = $this->banner_repository->getById($request->banner_id, true, true, null);
                Cache::put($key, $banner, 600);
                Cache::put($key2, $banner, 60000);
            }
        }
        $this->view_counter_service->addViews($banner, $user_id ?? null);


        return response()->json([
            'status' => 0,
            'data' => $this->generateBannerJsonResponse($banner)
        ]);
    }

    /**
     * @return
     */
    public function getSliders()
    {
        $key1 = 'sliders';
        $key2 = 'ramzinex_banners';
        if (Cache::has($key1)) {
            $banners_of_ramzinex = Cache::get($key1);
        } else {
            $banners_of_ramzinex = $this->banner_repository->getAllBanners(3, 0, 5, true, false, false, null, true);

            Cache::put($key1,$banners_of_ramzinex,300);
        }
        if (Cache::has($key2)) {
            $banners = Cache::get($key2);
        } else {
            $banners = $this->banner_repository->getSliders();
            Cache::put($key2,$banners,300);
        }


        return response()->json([
            'status' => 0,
            'data' => [
                'sliders' => BannerResource::collection($banners),
                'ramzinex_banners' => BannerResource::collection($banners_of_ramzinex)
            ]
        ]);
//        return BannerResource::collection($banners)->additional([ 'status' => 0]);
    }

    /**
     * return banner's accepted comments
     * @param BannerComments $request
     * @return AnonymousResourceCollection
     */
    public
    function getComment(BannerComments $request): AnonymousResourceCollection
    {
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);
        $banner_key = 'banner_:' . $request->banner_id;
        $comments_key = 'short_time_cache:v1_banner_comment_for:' . $request->banner_id;
        $comments_key2 = 'long_time_cache:v1_banner_comment_for:' . $request->banner_id . 'limit:' . $limit . 'offset:' . $offset;
        $comments_lock = 'long_time_cache:v1_banner_comment_for:' . $request->banner_id . 'limit:' . $limit . 'offset:' . $offset;
        $banner_comment_count = 'short_time_cache:v1_banner_' . $request->banner_id . 'comments_count';
        $banner_comment_count2 = 'long_time_cache:v1_banner_' . $request->banner_id . 'comments_count';
        $banner_comment_count_lock = 'long_time_cache:v1_banner_' . $request->banner_id . 'comments_count';
        srand(time());
        $r = rand(1, 100);
        $mod = $r % 10;

        if (Cache::has($comments_key)) {
            $comments = Cache::get($comments_key);
        } else {
            if (Cache::has($comments_lock)) {
                if (Cache::has($comments_key2)) {
                    $comments2 = Cache::get($comments_key2);
                }
            }
            Cache::put($comments_lock, 1, 43200);
            if ($mod != 0 && Cache::has($comments_key2)) {
                $comments = Cache::get($comments_key2);
            } else {
                $banner = $this->banner_repository->getById($request->banner_id);
                Cache::put($banner_key, $banner, 86400);
                $comments = $this->comment_repository
                    ->getAllComment($banner, false, true, $offset, $limit, true, null, $user_id ?? null);
                Cache::put($comments_key, $comments, 43200);
                Cache::put($comments_key2, $comments, 259200);

            }

        }
        if (Cache::has($banner_comment_count)) {
            $comments_count = Cache::get($banner_comment_count);
        } else {
            if (Cache::has($banner_comment_count_lock)) {
                if (Cache::has($banner_comment_count2)) {
                    $comments_count = Cache::get($banner_comment_count2);
                    $comments2 = Cache::get($comments_key2);
                    return Comments::collection($comments2)->additional([
                        'status' => 0,
                        'count' => $comments_count
                    ]);
                }
            }
            Cache::put($banner_comment_count_lock, 1, 43200);
            $banner = Cache::get($banner_key);
            $comments_count = $this->comment_repository->getCommentsCount($banner, true, null);
            Cache::put($banner_comment_count, $comments_count, 43200);
            Cache::put($banner_comment_count2, $comments_count, 259200);
        }

        return Comments::collection($comments)->additional([
            'status' => 0,
            'count' => $comments_count
        ]);


    }

    /**
     * Get and return banner types
     * @return JsonResponse
     */
    public
    function showBannerTypes(): JsonResponse
    {

        $key = 'short_time_cache:banners';
        $key2 = 'long_time_cache:banners';
        if (Cache::has($key2)) {
            $banner_types = Cache::get($key);
        } else {
            $banner_types = $this->banner_type_repository->getAll();
            Cache::put($key, $banner_types, 259200);
        }
        return response()->json([
            'status' => 0,
            'data' => $banner_types
        ]);
    }

    /**
     * add a banner to favorites
     * @param GetBanner $request
     * @return JsonResponse
     */
    public
    function addToFavorite(GetBanner $request): JsonResponse
    {

        $banner_id = $request->banner_id;
        $banner = $this->banner_repository->getById($banner_id);
        $user_id = $this->user_authenticator_service->getUserId($request);

        if ($this->favorite_repository->isAddedToFavorites($banner, $user_id)) {
            $this->favorite_repository->deleteFavorite($banner, $user_id, 'banner');
        } else {
            $this->favorite_repository->createFavorite($banner, $user_id, 'banner');
        }
        $favored_list = 'user_' . $user_id . '_favored_banner_list';
        if (Cache::has($favored_list)) {
            $users_favorites_list = Cache::get($favored_list);
        } else {
            $users_favorites_list = $this->favorite_repository->getUserFavoredGlobalList($user_id, 'banner');
            Cache::put($favored_list, $users_favorites_list, 86400);
        }
        return response()->json([
            'status' => 0,
            'data' => [
                'favorites_count' => $this->favorite_repository->getFavoriteCount($banner),
                'favored_banners_list' => $users_favorites_list->toArray()
            ]
        ]);

    }

    /**
     * check if banner added to favorite or not
     * @param GetBanner $request
     * @return JsonResponse
     */
    public
    function isAddedToFavorites(GetBanner $request): JsonResponse
    {
//        $banner_id = $request->banner_id;
//        $banner = $this->banner_repository->getById($banner_id);
//        $user_id = $this->user_authenticator_service->getUserId($request);
        return response()->json([
            'status' => 0,
            'data' => [
                'is_favored' => false
//                    $this->favorite_repository->isAddedToFavorites($banner, $user_id),

            ]
        ]);

    }

    /**
     * get user's feedback for each question
     * @param GetBanner $request
     * @return JsonResponse
     */
    public
    function like(GetBanner $request): JsonResponse
    {
        $banner_id = $request->banner_id;
        $banner = $this->banner_repository->getById($banner_id);
        $result = $this->like_dislike_service->createLike($banner, $request, 'banner');
        $user_id = $this->user_authenticator_service->getUserId($request);
        try {
            $testKey = 'banner_new_like_2_' . $request->banner_id;
            $r = Redis::connection('like_dislike');
            if ($user_id) {
                $r->sadd($testKey, $user_id);
            }
        } catch (\Exception $e) {
            Cache::increment('count_all_banner_like_error_2:' . $request->banner_id);
        }
        if (!$result) {
            return $this->generateFeedbackResponse();
        }
        $likes_list = 'user_' . $user_id . '_liked_banner_list';
        if (Cache::has($likes_list)) {
            $users_likes_list = Cache::get($likes_list);
        } else {
            $users_likes_list = $this->like_repository->getUserLikesList($user_id, 'banner');
            Cache::put($likes_list, $users_likes_list, 86400);

        }
        $dislikes_list = 'user_' . $user_id . '_disliked_banner_list';
        if (Cache::has($dislikes_list)) {
            $users_dislikes_list = Cache::get($dislikes_list);
        } else {
            $users_dislikes_list = $this->dislike_repository->getUserDisLikesList($user_id, 'banner');
            Cache::put($dislikes_list, $users_dislikes_list, 86400);
        }
        $new_like_count = $this->like_repository->getLikeCount($banner);
        $new_dislike_count = $this->dislike_repository->getDisLikeCount($banner);
        return $this->generateFeedbackResponse(true, $new_like_count, $new_dislike_count, $users_likes_list, $users_dislikes_list);

    }

    /**
     * Check if user liked banner or not
     * @param GetBanner $request
     * @return JsonResponse
     */
    public
    function isLiked(GetBanner $request): JsonResponse
    {
//        $banner_id = $request->banner_id;

//        $banner = $this->banner_repository->getById($banner_id);
//        $user_id = $this->user_authenticator_service->getUserId($request);

        return response()->json([
            'status' => 0,
            'data' => [
                'is_liked' => false
//                    $this->like_repository->isLiked($banner, $user_id)
            ]
        ]);
    }

    /**
     * get user's feedback for each question
     * @param GetBanner $request
     * @return JsonResponse
     */
    public
    function disLike(GetBanner $request): JsonResponse
    {
        $banner_id = $request->banner_id;
        $banner = $this->banner_repository->getById($banner_id);
        $result = $this->like_dislike_service->createDislike($banner, $request, 'banner');
        $user_id = $this->user_authenticator_service->getUserId($request);
        try {
            $testKey = 'banner_new_dis_like_2_' . $request->banner_id;
            $r = Redis::connection('like_dislike');
            if ($user_id) {
                $r->sadd($testKey, $user_id);
            }
        } catch (\Exception $e) {
            Cache::increment('count_all_banner_dislike_error_2:' . $request->banner_id);
        }
        if (!$result) {
            return $this->generateFeedbackResponse();
        }
        $likes_list = 'user_' . $user_id . '_liked_banner_list';
        if (Cache::has($likes_list)) {
            $users_likes_list = Cache::get($likes_list);
        } else {
            $users_likes_list = $this->like_repository->getUserLikesList($user_id, 'banner');
            Cache::put($likes_list, $users_likes_list, 86400);

        }
        $dislikes_list = 'user_' . $user_id . '_disliked_banner_list';
        if (Cache::has($dislikes_list)) {
            $users_dislikes_list = Cache::get($dislikes_list);
        } else {
            $users_dislikes_list = $this->dislike_repository->getUserDisLikesList($user_id, 'banner');
            Cache::put($dislikes_list, $users_dislikes_list, 86400);
        }
        $new_dislike_count = $this->dislike_repository->getDisLikeCount($banner);
        $new_like_count = $this->like_repository->getLikeCount($banner);
        return $this->generateFeedbackResponse(true, $new_like_count, $new_dislike_count, $users_likes_list, $users_dislikes_list);

    }

    /**
     * Check if user disliked banner or not
     * @param GetBanner $request
     * @return JsonResponse
     */
    public
    function isDisliked(GetBanner $request): JsonResponse
    {
//        $banner_id = $request->banner_id;
//        $banner = $this->banner_repository->getById($banner_id);
//        $user_id = $this->user_authenticator_service->getUserId($request);
        return response()->json([
            'status' => 0,
            'data' => [
                'is_disliked' => false
//                    $this->dislike_repository->isDisliked($banner, $user_id)
            ]
        ]);
    }

    /**
     * store created comment
     * @param CreateBannerComment $request
     * @return JsonResponse
     */
    public
    function addComment(CreateBannerComment $request): JsonResponse
    {
        $banner = $this->banner_repository->getById($request->banner_id);
        $user_id = $this->user_authenticator_service->getUserId($request);
        $comment = $this->comment_service->createComment($banner, $request);
        try {
            $testKey = 'banner_comment_' . $request->banner_id;
            $r = Redis::connection('like_dislike');
            $r->sadd($testKey, $user_id);
        } catch (\Exception $e) {
            Cache::increment('count_all_comment_banner_error_dadbd71:' . $request->banner_id);
        }
        if (!$comment) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on creating comment',
                    'fa' => 'ثبت کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'description' => [
                'en' => 'Comment created successfully',
                'fa' => 'کامنت شما با موفقیت ثبت شد'
            ]
        ]);
    }

    /**
     * update an existing comment
     * @param UpdateBannerComment $request
     * @return JsonResponse
     */
    public
    function updateComment(UpdateBannerComment $request): JsonResponse
    {
        $result = $this->comment_service->updateComment($request);

        if (!$result) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on Editing comment',
                    'fa' => 'ویرایش کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'description' => [
                'en' => 'Comment edited successfully',
                'fa' => 'ویرایش کامنت با موفقیت انجام شد'
            ]
        ]);
    }

    /**
     * it deletes feedbacks(if exist) then delete comments
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public
    function deleteComment(Request $request, int $id): JsonResponse
    {
        $result = $this->comment_service->deleteComment($request, $id);

        if (!$result) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on deleting comment',
                    'fa' => 'حذف کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'description' => [
                'en' => 'Comment delete successfully',
                'fa' => 'کامنت شما با موفقیت حذف شد'
            ]
        ]);
    }

    /**
     * Delete extra info from collection
     * @param Collection $collection
     * @return Collection
     */
    private
    function generateJsonResponse(Collection $collection): Collection
    {
        return $collection->map(function ($collection) {

            $collected = (object)$collection;

            $collected->makeHidden(['description']);

            $collected->comments_count = $collected->comments()->where('status', '=', 1)->count();
            return $collected;
        });
    }

    /**
     * Delete extra info from collection
     * @param Banner $banner
     * @return Banner
     */
    private
    function generateBannerJsonResponse(Banner $banner): Banner
    {
//        $key = 'banners_comment_count'.$banner->id;
//        if (Cache::has($key)){
//            $banner->comments_count = Cache::get($key);
//        }else{
        $banner->comments_count = $banner->comments()->where('status', '=', 1)->count();
//            Cache::put($key,$banner->comments_count,43200);
//        }
        return $banner;
    }

    /**
     * Generate feedback success/unsuccess response
     * @param bool $is_success
     * @param int $like_count
     * @param int $dislike_count
     * @param object|null $like_list
     * @param object|null $dislike_list
     * @return JsonResponse
     */
    private
    function generateFeedbackResponse(bool $is_success = false, int $like_count = 0, int $dislike_count = 0, object $like_list = null, object $dislike_list = null): JsonResponse
    {
        if ($is_success) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'You\'re like submitted successfully',
                    'fa' => 'فیدبک شما با موفقیت ثبت گردید',
                ],
                'data' => [
                    'likes_count' => $like_count,
                    'dislikes_count' => $dislike_count,
                    'liked_banners_list' => $like_list->toArray(),
                    'disliked_banners_list' => $dislike_list->toArray()
                ]
            ]);
        }

        return response()->json([
            'status' => 1,
            'description' => [
                'en' => 'There was a problem in submitting your feedback',
                'fa' => 'مشکلی در ثبت فیدبک شما بوجود آمده است',
            ]
        ]);
    }
}
