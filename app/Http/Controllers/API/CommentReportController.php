<?php

namespace App\Http\Controllers\API;

use App\logic\CommentReportManager;
use App\Services\UserAuthentication\UserAuthenticatorService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CommentReportController
{
    /**
     * @var UserAuthenticatorService
     */
    private UserAuthenticatorService $userAuthenticatorService;

    /**
     * @param UserAuthenticatorService $userAuthenticatorService
     */
    public function __construct(UserAuthenticatorService $userAuthenticatorService)
    {
        $this->userAuthenticatorService = $userAuthenticatorService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $data = $request->only(['comment_id']);
        $user_id = $this->userAuthenticatorService->getUserId($request);
        $data['user_id'] = $user_id;
        $exist = CommentReportManager::getInstance()->exists($data);
        if ($exist==true){
            return response()->json([
                'status' => 1,
                'description' => [
                    'fa'=>"گزارش خطای شما قبلا ثبت شده است",
                    'en'=>"your report submitted before"
                ]
            ]);
        }
        $reported = CommentReportManager::getInstance()->createReport($data);

        if (!$reported) {
            return response()->json([
                'status' => 1,
                'data' => [
                    "ثبت ریپورت با مشکل مواجه شد"
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'data' => [
                "ریپورت ثبت شد"
            ]
        ]);
    }
}
