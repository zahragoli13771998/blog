<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CurrencyConversion\CurrencyConversion;
use App\logic\ConfigManager;
use App\Services\Interfaces\CurrenciesConversionServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;

class CurrencyConversionController extends Controller
{


    /**
     * currencies conversion service container
     * @var CurrenciesConversionServiceInterface
     */
    private CurrenciesConversionServiceInterface $conversion_service;

    /**
     * inject conversion service
     * CurrencyConversionController constructor.
     * @param CurrenciesConversionServiceInterface $conversion_service
     */
    public function __construct(CurrenciesConversionServiceInterface $conversion_service)
    {
        $this->conversion_service = $conversion_service;
    }

    /**
     * get conversion number with currency_id and conversion_id
     * @param CurrencyConversion $request
     * @return JsonResponse
     */
    public function convertCurrencies(CurrencyConversion $request): JsonResponse
    {
        $base_currency_id = $request->input('base_currency_id');
        $converting_currency_id = $request->input('converting_currency_id');
        $converting_currency_count = $request->input('converting_currency_count', 1);
        $conversion = $this->conversion_service->convertCurrencies($base_currency_id, $converting_currency_id, $converting_currency_count);


        return response()->json([
            'status' => 0,
            'data' => ['price' => $conversion],
        ]);
    }
}
