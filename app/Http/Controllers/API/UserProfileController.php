<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\UserProfile\UserComments;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\Interfaces\UserAuthenticatorServiceInterface;
use Illuminate\Http\Client\HttpClientException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\KavenegarApi;

class UserProfileController extends Controller
{
    /**
     * User repository container
     * @var UserRepositoryInterface
     */
    protected UserRepositoryInterface $user_repository;

    /**
     * User auth service container
     * @var UserAuthenticatorServiceInterface
     */
    protected UserAuthenticatorServiceInterface $user_auth_service;

    /**
     * Construct and inject necessary services to their container
     * UserProfileController constructor.
     * @param UserRepositoryInterface $user_repository
     * @param UserAuthenticatorServiceInterface $user_auth_service
     */
    public function __construct(UserRepositoryInterface $user_repository, UserAuthenticatorServiceInterface $user_auth_service)
    {
        $this->user_repository = $user_repository;
        $this->user_auth_service = $user_auth_service;
    }

    /**
     * Get and generate user comments
     * @param UserComments $request
     * @return JsonResponse
     */
    public function getUserProfile(UserComments $request): JsonResponse
    {
        $from = $request->input('offset', 0);
        $take = $request->input('limit', 10);
        $user_id = $request->input('user_id');

        $user = $this->user_repository->getUser(null, $user_id);
        $comments = $this->user_repository->getUserComments($user, $take, $from, true, true, true, true);
        $user_counts = $this->user_repository->loadCountsOnUser($user)->makeHidden(['id', 'sid', 'name', 'created_at', 'updated_at']);

        return response()->json([
            'status' => 0,
            'data' => [
                'comments' => $comments->append(['replied_to', 'comment_type']),
                'counts' => $user_counts,
//                'joined_at'=>Jalalian::forge($user->created_at)
            ]
        ]);
    }

    /**
     * Get user favorite items
     * @param Request $request
     * @return JsonResponse
     */
    public function getUserFavorites(Request $request): JsonResponse
    {
        $user_id = $this->user_auth_service->getUserId($request);
        $key_banner = 'short_cache_for_favorite_banners_belongs_to' . $user_id;
        $key_banner_long = 'long_cache_for_favorite_banners_belongs_to' . $user_id;
        $lock_banner = 'lock_cache_for_favorite_banners_belongs_to' . $user_id;
        $key_global = 'short_cache_for_favorite_globals_belongs_to' . $user_id;
        $key_global_long = 'long_time_for_favorite_globals_belongs_to' . $user_id;
        $lock_global = 'lock_time_for_favorite_globals_belongs_to' . $user_id;
        $key_question = 'short_cache_for_favorite_questions_belongs_to' . $user_id;
        $key_question_long = 'short_cache_for_favorite_questions_belongs_to' . $user_id;
        $lock_question = 'lock_time_for_favorite_questions_belongs_to' . $user_id;
        srand(time());
        $r = rand(1, 100);
        $mod = $r % 10;
        $user = $this->user_repository->getUser(null, $user_id);
        if (Cache::has($key_banner)) {
            $banners = Cache::get($key_banner);
            $global_currencies = Cache::get($key_global);
            $questions = Cache::get($key_question);
        } else {
            if (Cache::has($lock_banner)) {
                if (Cache::has($key_banner_long)) {
                    $banners = Cache::get($key_banner_long);
                    $global_currencies = Cache::get($key_global_long);
                    $questions = Cache::get($key_global_long);
                    return response()->json([
                        'status' => 0,
                        'data' => [
                            'banners' => $banners,
                            'questions' => $questions,
                            'global_currencies' => $global_currencies
                        ]
                    ]);
                } else {
                    abort(500);
                }
            }
            Cache::put($lock_banner, 1, 43220);
            if ($mod != 0 && Cache::has($key_banner_long) && Cache::has($key_global_long) && Cache::has($key_question_long)) {

                $banners = Cache::get($key_banner_long);
                $questions = Cache::get($key_question_long);
                $global_currencies = Cache::get($key_global_long);
            } else {
                $user_favorites = $this->user_repository->getUserFavorites($user)->makeHidden(['id', 'sid', 'name', 'created_at', 'updated_at']);
                $banners = $user_favorites->banners->pluck('id');
                Cache::put($key_banner, $banners, 43200);
                Cache::put($key_banner_long, $banners, 43200000);
                $global_currencies = $user_favorites->globalCurrencies->pluck('id');
                Cache::put($key_global, $global_currencies, 43210);
                Cache::put($key_global_long, $global_currencies, 432000010);
                $questions = $user_favorites->questions->pluck('id');
                Cache::put($key_question, $questions, 43220);
                Cache::put($key_question_long, $questions, 432200000);
            }

        }
        return response()->json([
            'status' => 0,
            'data' => [
                'banners' => $banners,
                'questions' => $questions,
                'global_currencies' => $global_currencies
            ]
        ]);
    }

    public function call(Request $request)
    {
        $code = random_int(100000, 999999);
//        $b = "232";

//     return $a;
        $new_code = str_split($code, 2);
//        $x = str_replace(",", "*", (string)$new_code);
//        var_dump($x);
//        return "**";
        var_dump($new_code);

//        return $new_code[1];
//        $a =   (int)$new_code[0];
//$a =array();
////        $i =0;
//        for ($i=0; $i<2; $i++){
//           $a[$i] = (int)$new_code[$i];
//           $i = $i+1;
//        }

        var_dump($code);
        var_dump($new_code);
//        return "**0";
//        var_dump($a[0]);
//        var_dump($a[1]);
//        var_dump($a[2]);
//       var_dump($a);
//        $a =array();
//        array_push($a,(int)$new_code[0],(int)$new_code[1],(int)$new_code[2]);
//       return $a;
//        try {
            $api      = new KavenegarApi( '6A304A786D6E71587249376747306A385A584456314F466155547A737949684E36516E7439544F785041593D' );
            $receptor = "09024683109";
//            $message  =$a;
            $result   = $api->CallMakeTTS( $receptor , ''.$code);
            if ($result) {
                var_dump($result);
            }
//        } catch (ApiException $e) {
//            echo $e->errorMessage();

//        }
    }
}
