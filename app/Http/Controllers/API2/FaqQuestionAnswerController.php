<?php

namespace App\Http\Controllers\API2;

use App\Factories\CommentRepoFactory;
use App\Http\Requests\Api2\QuestionAnswers\AddFaqComment;
use App\Http\Requests\Api\QuestionAnswers\GetFaqComment;
use App\Http\Resources\Comments;
use App\logic\ConfigManager;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Repositories\Interfaces\FaqQuestionAnswerRepositoryInterface;
use App\Services\Interfaces\CommentServiceInterface;
use App\Services\UserAuthentication\UserAuthenticatorService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Cache;


class FaqQuestionAnswerController
{
    /**
     * questionAnswer repository container
     * @var FaqQuestionAnswerRepositoryInterface
     */
    private FaqQuestionAnswerRepositoryInterface $question_answer_repository;

    /**
     * comment repository container
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $comment_repository;

    /**
     * User Authenticator Service container
     * @var UserAuthenticatorService
     */
    protected UserAuthenticatorService $user_authenticator_service;

    /**
     * CommentService Container
     * @var CommentServiceInterface
     */
    private CommentServiceInterface $comment_service;

    /**
     * FaqCategoriesAndFaqQaController constructor.
     * @param FaqQuestionAnswerRepositoryInterface $questionAnswerRepository
     * @param UserAuthenticatorService $user_authenticator_service
     * @param CommentServiceInterface $comment_service
     */
    public function __construct(FaqQuestionAnswerRepositoryInterface $questionAnswerRepository, UserAuthenticatorService $user_authenticator_service, CommentServiceInterface $comment_service)
    {
        $this->question_answer_repository = $questionAnswerRepository;
        $this->user_authenticator_service = $user_authenticator_service;
        $this->comment_repository = CommentRepoFactory::createRepository('comment_version_two');
        $this->comment_service = $comment_service;
    }

    /**
     * return question's accepted comments
     * @param GetFaqComment $request
     * @return AnonymousResourceCollection
     */
    public function getComment(GetFaqComment $request): AnonymousResourceCollection
    {
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);
        $key = 'short_time_cache_for_comments_of_faq:'.$request->question_id;
        $key_count = 'short_time_cache_for_comments_count_of_faq'.$request->question_id;
        if (Cache::has($key)){
            $comments = Cache::get($key);
            $comments_count = Cache::get($key_count);
        }else{
            $question_answer = $this->question_answer_repository->getById($request->question_id);
            $comments = $this->comment_repository
                ->getAllComment($question_answer, true, true, $offset, $limit, true, null, $user_id ?? null);
            $comments_count = $this->comment_repository->getCommentsCount($question_answer, true);

        }

        return Comments::collection($comments)->additional([
            'status' => 0,
            'count' => $comments_count,
        ]);
    }

    /**
     * Add new comment for question answer
     * @param AddFaqComment $request
     * @return JsonResponse
     */
    public function addComment(AddFaqComment $request): JsonResponse
    {
        $faq_question_answer = $this->question_answer_repository->getById($request->faq_question_id);
        $comment = $this->comment_service->createComment($faq_question_answer, $request);

        if (!$comment) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on creating comment',
                    'fa' => 'ثبت کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'data' => new Comments($comment),
            'description' => [
                'en' => 'Comment created successfully',
                'fa' => 'کامنت شما با موفقیت ثبت شد'
            ]
        ]);
    }
}
