<?php

namespace App\Http\Controllers\API2;

use App\Factories\CommentRepoFactory;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api2\Comment\CommentReplies;
use App\Http\Resources\Comments;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Services\UserAuthentication\UserAuthenticatorService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CommentController extends Controller
{

    /**
     * User Authenticator Service container
     * @var UserAuthenticatorService
     */
    protected UserAuthenticatorService $user_authenticator_service;
    /**
     * CommentRepository Container
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $comment_repository;


    /**
     * Construct and inject necessary services to their container
     * @param UserAuthenticatorService $user_authenticator_service
     */
    public function __construct(UserAuthenticatorService $user_authenticator_service)
    {
        $this->comment_repository= CommentRepoFactory::createRepository('comment_version_two');
        $this->user_authenticator_service = $user_authenticator_service;
    }

    /**
     * Get and return comment replies
     * @param CommentReplies $request
     * @return AnonymousResourceCollection
     */
    public function getCommentReplies(CommentReplies $request): AnonymousResourceCollection
    {
        $depth_id = $request->input('depth_id');
        $from = $request->input('offset', 0);
        $take = $request->input('limit', 10);
        $type = $request->input('type','comment');
        if ($request->hasHeader('Authorization')) {
            $user_id = $this->user_authenticator_service->getUserId($request);
        }
        $comments = $this->comment_repository->getAllComment(null, false, true, $from, $take, true, $depth_id, $user_id ?? null,$type);
        $comment_counts = $this->comment_repository->getCommentsCount(null, true, $depth_id);
        return Comments::collection($comments)->additional([
            'status' => 0,
            'count' => $comment_counts
        ]);
    }

}
