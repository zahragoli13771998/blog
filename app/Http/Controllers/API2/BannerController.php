<?php

namespace App\Http\Controllers\API2;

use App\Factories\CommentRepoFactory;
use App\Http\Requests\Api\Banner\BannerComments;
use App\Http\Requests\Api2\Banner\CreateBannerComment;
use App\Http\Resources\Comments;
use App\logic\ConfigManager;
use App\Repositories\Interfaces\BannerRepositoryInterface;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Services\Interfaces\CommentServiceInterface;
use App\Services\UserAuthentication\UserAuthenticatorService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Cache;

class BannerController
{
    /**
     * Banner repository container
     * @var BannerRepositoryInterface
     */
    private BannerRepositoryInterface $banner_repository;

    /**
     * Comment Repository container
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $comment_repository;

    /**
     * User Authenticator Service container
     * @var UserAuthenticatorService
     */
    protected UserAuthenticatorService $user_authenticator_service;

    /**
     * CommentService Container
     * @var CommentServiceInterface
     */
    private CommentServiceInterface $comment_service;

    /**
     * Construct and inject necessary services to container
     * BannerController constructor.
     * @param BannerRepositoryInterface $bannerRepository
     * @param UserAuthenticatorService $user_authenticator_service
     * @param CommentServiceInterface $comment_service
     */
    public function __construct(BannerRepositoryInterface $bannerRepository, UserAuthenticatorService $user_authenticator_service, CommentServiceInterface $comment_service)
    {
        $this->banner_repository = $bannerRepository;
        $this->comment_repository = CommentRepoFactory::createRepository('comment_version_two');
        $this->user_authenticator_service = $user_authenticator_service;
        $this->comment_service = $comment_service;
    }

    /**
     * return banner's accepted comments
     * @param BannerComments $request
     * @return AnonymousResourceCollection
     */
    public function getComment(BannerComments $request): AnonymousResourceCollection
    {
        $banner_key = 'short_time_cache:banners_for_get_comments_for_id:' . $request->banner_id;
        $banner_comment_count = 'short_time_cache:banners_' . $request->banner_id . 'comments_count';
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);
        $comment_banner = 'short_time_cache:banner_comments_for_banners=' . $request->banner_id . 'limit:' . $limit . 'offset:' . $offset;;
        if (Cache::has($banner_comment_count)) {
            $comments_count = Cache::get($banner_comment_count);
        } else {
            $banner = $this->banner_repository->getById($request->banner_id);
            $comments_count = $this->comment_repository->getCommentsCount($banner, true);
            Cache::put($banner_comment_count, $comments_count, 43300);
        }
        if (Cache::has($comment_banner)) {
            $banner = Cache::get($banner_key);
            $comments = Cache::get($comment_banner);
        } else {
            $banner = $this->banner_repository->getById($request->banner_id);
            Cache::put($banner_key, $banner, 43300);
            $comments = $this->comment_repository
                ->getAllComment($banner, true, true, $offset, $limit, true, null, $user_id ?? null);
            Cache::put($comment_banner, $comments, 43300);
        }


        return Comments::collection($comments)->additional([
            'count' => $comments_count,
            'status' => 0
        ]);
    }

    /**
     * store created comment
     * @param CreateBannerComment $request
     * @return JsonResponse
     */
    public function addComment(CreateBannerComment $request): JsonResponse
    {
        $banner = $this->banner_repository->getById($request->banner_id);
        $comment = $this->comment_service->createComment($banner, $request);

        if (!$comment) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on creating comment',
                    'fa' => 'ثبت کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'data' => new Comments($comment),
            'description' => [
                'en' => 'Comment created successfully',
                'fa' => 'کامنت شما با موفقیت ثبت شد'
            ]
        ]);
    }
}
