<?php

namespace App\Http\Controllers\API2;

use App\Factories\CommentRepoFactory;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api2\GlobalCurrency\AddComment;
use App\Http\Requests\Api\GlobalCurrency\GetComment;
use App\Http\Resources\Comments;
use App\logic\ConfigManager;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Services\Interfaces\CommentServiceInterface;
use App\Services\Interfaces\UserAuthenticatorServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Cache;

class GlobalCurrencyController extends Controller
{
    /**
     * Global Currency repository container
     * @var GlobalCurrencyInterface
     */
    private GlobalCurrencyInterface $currency;

    /**
     * User authenticator service container
     * @var UserAuthenticatorServiceInterface
     */
    protected UserAuthenticatorServiceInterface $user_authenticator_service;

    /**
     * Comment Repository container
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $comment_repository;

    /**
     * CommentService Container
     * @var CommentServiceInterface
     */
    private CommentServiceInterface $comment_service;

    /**
     * Construct and inject necessary services to their container
     * GlobalCurrencyController constructor.
     * @param GlobalCurrencyInterface $currency
     * @param UserAuthenticatorServiceInterface $user_authenticator_service
     * @param CommentServiceInterface $comment_service
     */
    public function __construct(GlobalCurrencyInterface $currency, UserAuthenticatorServiceInterface $user_authenticator_service, CommentServiceInterface $comment_service)
    {
        $this->currency = $currency;
        $this->user_authenticator_service = $user_authenticator_service;
        $this->comment_repository = CommentRepoFactory::createRepository('comment_version_two');
        $this->comment_service = $comment_service;
    }

    /**
     * return banner's accepted comments
     * @param GetComment $request
     * @return AnonymousResourceCollection
     */
    public function getComments(GetComment $request): AnonymousResourceCollection
    {
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);
        $global_currency_key = 'global--currency:' . $request->global_currency_id;
        $comments_key = 'short_time_cache:v2_global_comment_for:' . $request->global_currency_id . 'limit:' . $limit . 'offset:' . $offset;;
        $comments_key2 = 'long_time_cache:v2_global_comment_for:' . $request->global_currency_id . 'limit:' . $limit . 'offset:' . $offset;;
        $comments_lock = 'long_time_cache:v2_global_comment_for:' . $request->global_currency_id;
        $gc_comment_count = 'short_time_cache:v2_global_' . $request->global_currency_id . 'comments_count';
        $gc_comment_count2 = 'long_time_cache:v2_global_' . $request->global_currency_id . 'comments_count';
        $gc_comment_count_lock = 'long_time_cache:v2_global_' . $request->global_currency_id . 'comments_count';
        srand(time());
        $r = rand(1, 100);
        $mod = $r % 10;
        if (Cache::has($comments_key)) {
            $comments = Cache::get($comments_key);
        } else {
            if (Cache::has($comments_lock)) {
                if (Cache::has($comments_key2)) {
                    $comments2 = Cache::get($comments_key2);
                }
            }
            Cache::put($comments_lock, 1, 43200);
            if ($mod != 0 && Cache::has($comments_key2)) {
                $comments = Cache::get($comments_key2);
            } else {
                $global_currency = $this->currency->getById($request->global_currency_id);
                Cache::put($global_currency_key, $global_currency, 86400);
                $comments = $this->comment_repository
                    ->getAllComment($global_currency, false, true, $offset, $limit, true, null, $user_id ?? null);
                Cache::put($comments_key, $comments, 43200);
                Cache::put($comments_key2, $comments, 43200000000);

            }

        }
        if (Cache::has($gc_comment_count)) {
            $comments_count = Cache::get($gc_comment_count);
        } else {
            if (Cache::has($gc_comment_count_lock)) {
                if (Cache::has($gc_comment_count2)) {
                    $comments_count = Cache::get($gc_comment_count2);
                    $comments2 = Cache::get($comments_key2);
                    return Comments::collection($comments2)->additional([
                        'status' => 0,
                        'count' => $comments_count
                    ]);
                }
            }
            Cache::put($gc_comment_count_lock, 1, 43200);
            $global_currency = Cache::get($global_currency_key);
            $comments_count = $this->comment_repository->getCommentsCount($global_currency, true, null);
            Cache::put($gc_comment_count, $comments_count, 43200);
            Cache::put($gc_comment_count2, $comments_count, 43200000000);
        }

        return Comments::collection($comments)->additional([
            'status' => 0,
            'count' => $comments_count
        ]);
    }

    /**
     * store created comment
     * @param AddComment $request
     * @return JsonResponse
     */
    public function addComment(AddComment $request): JsonResponse
    {
        $global_currency = $this->currency->getById($request->global_currency_id);
        $comment = $this->comment_service->createComment($global_currency, $request);
        if (!$comment) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on creating comment',
                    'fa' => 'ثبت کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'data' => new Comments($comment),
            'description' => [
                'en' => 'Comment created successfully',
                'fa' => 'کامنت شما با موفقیت ثبت شد'
            ]
        ]);
    }
}
