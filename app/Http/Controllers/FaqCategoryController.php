<?php

namespace App\Http\Controllers;

use App\Http\Requests\FaqCategory\Create;
use App\Http\Requests\FaqCategory\Update;
use App\Models\FaqQuestionAnswer;
use App\Repositories\FaqQuestionAnswerRepository;
use App\Repositories\Interfaces\FaqCategoryRepositoryInterface;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class FaqCategoryController extends Controller
{
    /**
     * FaqCategory repository container
     * @var FaqCategoryRepositoryInterface
     */
    private FaqCategoryRepositoryInterface $faq_category_repository;
    private FaqQuestionAnswerRepository $answerRepository;

    /**
     * FaqCategoryController constructor.
     * @param FaqCategoryRepositoryInterface $faq_category_repository
     */
    public function __construct(FaqCategoryRepositoryInterface $faq_category_repository,FaqQuestionAnswerRepository $answerRepository)
    {
        $this->faq_category_repository = $faq_category_repository;
        $this->answerRepository=$answerRepository;
    }

    /**
     * show list of categories and their parent
     * @return Factory|Application|View
     */
    public function show(): view
    {
        $categories = $this->faq_category_repository->paginateCategory();
        return view('faq_category.show', compact('categories'));
    }

    /**
     * return create form
     * @return View
     */
    public function create(): view
    {
        $categories = $this->faq_category_repository->getAllCategories();

        return view('faq_category.create', compact('categories'));
    }

    /**
     * create a new FaqCategory
     * @param Create $category
     * @return RedirectResponse
     */
    public function add(Create $category): RedirectResponse
    {
        $data = $category->only(['parent_id', 'name_fa', 'name_en', 'icon','description']);

        if ($category->has('icon')) {
            try {
                $data['icon'] = $category->file('icon')->store('faqCategories/icon', 'public');
            } catch (QueryException | \Exception $ex) {
                Log::error($ex->getMessage());
            }
        }

        $check_if_created = $this->faq_category_repository->addCategory($data);

        if (!$check_if_created) {
            return redirect()->back()->with('error', 'ایجاد دسته بندی با مشکل مواجه شد');
        }

        return redirect()->route('marketing.faq.category.show_categories')->with('success', 'دسته بندی با موفقیت ایجاد شد');
    }

    /**
     * show edit form
     * @param int $id
     * @return Factory|Application|View
     */
    public function edit(int $id): view
    {
        $categories = $this->faq_category_repository->getAllCategories();
        $category = $this->faq_category_repository->getById($id);

        return view('faq_category.edit', compact('category', 'categories'));
    }

    /**
     * update an existing FaqCategory
     * @param int $id
     * @param Update $request
     * @return RedirectResponse
     */
    public function update(int $id, Update $request): RedirectResponse
    {
        $category = $this->faq_category_repository->getById($id);
        $categories = $this->faq_category_repository->getAllCategories();

        $data = $request->only(['parent_id', 'name_fa', 'name_en', 'icon','description']);

        if ($request->has('icon')) {
            try {
                Storage::disk('public')->delete($category->icon);
                $data['icon'] = $request->file('icon')->store('faqCategories/icon', 'public');
            } catch (QueryException | \Exception $ex) {
                Log::error($ex->getMessage());
            }
        }

        $check_if_updated = $this->faq_category_repository->editCategory($data, $category);

        if (!$check_if_updated) {
            return redirect()->back()->with('error', 'ویرایش دسته بندی با مشکل مواجه شد');
        }
        return redirect()->route('marketing.faq.category.show_categories', compact('categories'))->with('success', 'ویرایش دسته بندی با موفقیت انجام شد');
    }

    /**
     * delete an existing FaqCategory
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        $category = $this->faq_category_repository->getById($id);
        $faqs = $this->answerRepository->getByFaqCategoryId($id);
        foreach ($faqs as $faq ){

            $this->answerRepository->deleteQuestionAnswer($faq);
        }
        try {
            Storage::disk('public')->delete($category->icon);
        } catch (QueryException | \Exception $ex) {
            Log::error($ex->getMessage());
        }

        $check_if_deleted = $this->faq_category_repository->deleteCategory($category);

        if (!$check_if_deleted) {
            return redirect()->back()->with('error', 'حذف دسته بندی با مشکل مواجه شد');
        }

        return redirect()->back()->with('success', 'حذف دسته بندی با موفقیت انجام شد');
    }
}
