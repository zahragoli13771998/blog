<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class VueController extends Controller
{

    public function pwaIndex(Request $request)
    {
        if (
            strpos($request->fullUrl(),'pt/images') !== false
            || strpos($request->fullUrl(),'pt/static') !== false
        ) {
            abort(404);
        }
        return view('pwa_index');
    }


}
