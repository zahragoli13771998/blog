<?php

namespace App\Http\Controllers;

use App\Models\GlobalCurrency;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Repositories\Interfaces\SectorRepositoryInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SectorInfoController extends Controller
{
    /**
     * Sector repository container
     * @var SectorRepositoryInterface
     */
    private SectorRepositoryInterface $sector_repository;

    /**
     * global currency repository container
     * @var GlobalCurrencyInterface
     */
    private GlobalCurrencyInterface $global_currency;

    /**
     * Sector repository constructor
     * @param SectorRepositoryInterface $sector_repository
     * @param GlobalCurrencyInterface $global_currency
     */
    public function __construct(SectorRepositoryInterface $sector_repository, GlobalCurrencyInterface $global_currency)
    {
        $this->sector_repository = $sector_repository;
        $this->global_currency = $global_currency;
    }

    /**
     * @return
     */
    public function show()
    {
       return $this->global_currency->getAllGlobalCurrencies(true,true);
    }

//    /**
//     * @return View
//     */
//    public function create(): View
//    {
//        return view('sector.create');
//    }
//
//    /**
//     * @param Create $request
//     * @return RedirectResponse
//     */
//    public function store(Create $request): RedirectResponse
//    {
//        $sector = $request->only(['title_fa', 'title_en']);
////        $sector = $request->only(['title_fa', 'title_en', 'description_fa', 'description_en', 'market_cap', 'trading_volume']);
//        $created = $this->sector_repository->createSector($sector);
//        if (!$created) {
//            return redirect()->route('admin.sector.list')->with('error', 'sector could not be created');
//        }
//        return redirect()->route('admin.sector.list')->with('success', 'created successfully');
//
//    }
//
//    /**
//     * @param int $id
//     * @return Application|Factory|\Illuminate\Contracts\View\View
//     */
//    public function selectGlobalCurrencies(int $id): View
//    {
//        $sector = $this->sector_repository->getById($id);
//        $global_currencies = $this->global_currency->getAllGlobalCurrencies();
//        return view('sector.attach_gc_to_sector', compact('sector', 'global_currencies'));
//    }
//
//
//
//    /**
//     * @param int $id
//     * @return View
//     */
//    public function edit(int $id): View
//    {
//        $sector = $this->sector_repository->getById($id);
//        return view('sector.edit', compact('sector'));
//    }
//
//    /**
//     * @param int $id
//     * @param Update $request
//     * @return RedirectResponse
//     */
//    public function update(int $id, Update $request): RedirectResponse
//    {
//        $data = $request->only(['title_fa', 'title_en']);
////        $data = $request->only(['title_fa', 'title_en', 'description_fa', 'description_en']);
//        $sector = $this->sector_repository->getById($id);
//        $created = $this->sector_repository->updateSector($sector, $data);
//        if (!$created) {
//            return redirect()->route('admin.sector.list')->with('error', 'sector could not be updated');
//        }
//        return redirect()->route('admin.sector.list')->with('success', 'updated successfully');
//
//    }
//
//    /**
//     * @param int $id
//     * @return RedirectResponse
//     */
//    public function destroy(int $id): RedirectResponse
//    {
//        $sector = $this->sector_repository->getById($id);
//        $is_deleted = $this->sector_repository->deleteSector($sector);
//        if (!$is_deleted) {
//            return redirect()->route('admin.sector.list')->with('error', 'sector could not be deleted');
//        }
//        return redirect()->route('admin.sector.list')->with('success', 'deleted successfully');
//
//    }
}
