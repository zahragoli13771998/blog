<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Looser\Create;
use App\Http\Requests\Looser\Update;
use App\Repositories\Interfaces\LooserRepositoryInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class LooserController extends Controller
{
    /**
     * LooserRepository Container
     * @var \App\Repositories\Interfaces\LooserRepositoryInterface
     */
    private LooserRepositoryInterface $looser_repository;

    /**
     * LooserController constructor.
     * @param LooserRepositoryInterface $looser_repository
     */
    public function __construct(LooserRepositoryInterface $looser_repository)
    {
        $this->looser_repository = $looser_repository;
    }

    /**
     * show loosers
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show():View
    {
        $loosers = $this->looser_repository->paginateLooser();
        return view('looser.show',compact('loosers'));
    }
    /**
     * return create form of Looser
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(): view
    {
        return view('looser.create');
    }

    /**
     * store created Looser
     * @param Create $request
     * @return RedirectResponse
     */
    public function add(Create $request): RedirectResponse
    {
        $data = $request->only(['id','value']);
        $created = $this->looser_repository->createLooser($data);
        if (!$created) {
            return redirect()->back()->with('error', 'ایجاد Looser با مشکل مواجه شد');
        }
        return redirect()->route('admin.looser.show')->with('success', ' Looser
        با موفقیت ایجاد شد'
        );
    }

    /**
     * return edit form of Looser
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id): View
    {
        $looser = $this->looser_repository->getById($id);
        return view('looser.edit', compact('looser'));
    }

    /**
     * save edited Looser
     * @param Update $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Update $request, int $id): RedirectResponse
    {
        $looser = $this->looser_repository->getById($id);
        $data = $request->only(['id','value']);
        $updated = $this->looser_repository->updateLooser($looser, $data);

        if (!$updated) {
            return redirect()->back()->with('error', 'ویرایش looser با مشکل مواجه شد');
        }
        return redirect()->route('admin.looser.show')->with('success', 'looser با موفقیت ویرایش شد');
    }

    /**
     * delete a Looser
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        $Looser = $this->looser_repository->getById($id);
        $deleted = $this->looser_repository->deleteLooser($Looser);
        if (!$deleted) {
            return redirect()->back()->with('error', 'حذف Looser با موفقیت انجام شد');
        }
        return redirect()->route('admin.looser.show')->with('success', 'Looser
        با موفقیت حذف شد');
    }
}
