<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GCurrencyRamzinexInfo\Store;
use App\Http\Requests\GCurrencyRamzinexInfo\Update;
use App\Repositories\Interfaces\GCurrencyRamzinexInfoInterface;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class GCurrencyInfoController extends Controller
{
    /**
     * The global currency ramzinex info repository instance.
     *
     * @var GCurrencyRamzinexInfoInterface
     */
    private GCurrencyRamzinexInfoInterface $g_currency_ramzinex_info_repository;

    /**
     * The global currency repository instance.
     *
     * @var GlobalCurrencyInterface
     */
    private GlobalCurrencyInterface $global_currency_repository;

    /**
     * global currency ramzinex info repository instance
     * @param GCurrencyRamzinexInfoInterface $g_currency_ramzinex_info_repository
     * @param GlobalCurrencyInterface $global_currency_repository
     */
    public function __construct(GCurrencyRamzinexInfoInterface $g_currency_ramzinex_info_repository, GlobalCurrencyInterface $global_currency_repository)
    {
        $this->g_currency_ramzinex_info_repository = $g_currency_ramzinex_info_repository;

        $this->global_currency_repository = $global_currency_repository;
    }

    /**
     * Display a listing of the resource.
     * @return View
     */
    public function show(): View
    {
        $g_currencies_ramzinex_info = $this->g_currency_ramzinex_info_repository->paginate(10, true);
        return view('gcurrency_ramzinex_info.show', compact('g_currencies_ramzinex_info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $currencies = $this->global_currency_repository->getPairGlobalCurrencies(false);
        return view('gcurrency_ramzinex_info.create', compact('currencies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id): view
    {
        $g_currency_ramzinex_info = $this->g_currency_ramzinex_info_repository->findById($id);
        return view('gcurrency_ramzinex_info.edit', compact('g_currency_ramzinex_info'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $store
     * @return RedirectResponse
     */
    public function store(Store $store): RedirectResponse
    {
        $input = $store->only('buy','sell','open', 'close','high','low','quote_volume','base_volume','change_percent','global_currency_id');
        $global_currency = $this->global_currency_repository->getById($input['global_currency_id']);
        $result = $this->g_currency_ramzinex_info_repository->store($input, $global_currency);
        if (!$result) {
            return redirect()->back()->with('error', 'عملیات ذخیره سازی با شکست مواجه شد.');
        }
        return redirect()->back()
            ->with('success', 'عملیات ذخیره سازی با موفقیت انجام شد.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param Update $update
     * @return RedirectResponse
     */
    public function update(int $id, Update $update): RedirectResponse
    {
        $data = $this->g_currency_ramzinex_info_repository->findById($id);
        $input = $update->only('buy','sell','open', 'close', 'high', 'low', 'quote_volume', 'base_volume', 'change_percent');
        $result = $this->g_currency_ramzinex_info_repository->update($input, $data);
        if (!$result) {
            return redirect()->back()->with('error', 'عملیات بروزرسانی با شکست مواجه شد.');
        }
        return redirect()->back()
            ->with('success', 'عملیات بروزرسانی با موفقیت انجام شد.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id): RedirectResponse
    {
        $currency_info = $this->g_currency_ramzinex_info_repository->findById($id);
        $result = $this->g_currency_ramzinex_info_repository->delete($currency_info);

        if (!$result) {
            return redirect()->back()->with('error', 'عملیات حذف با شکست مواجه شد.');
        }

        return redirect()->back()->with('success', 'عملیات حذف با موفقیت انجام شد.');
    }
}
