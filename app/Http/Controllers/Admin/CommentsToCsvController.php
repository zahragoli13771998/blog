<?php

namespace App\Http\Controllers\Admin;

use App\Models\Comment;

class CommentsToCsvController
{

    public function getComments(){
        $name = 'faqComments.xls';
        $headers = [
            'Content-Disposition' => 'attachment; filename=' . $name,
        ];
        $colom = \Illuminate\Support\Facades\Schema::getColumnListing("comments");
        $temp_colom = array_flip($colom);
        unset($temp_colom['id']);
        $colom = array_flip($temp_colom);
        return response()->stream(function () use ($colom) {
            $file = fopen('php://output', 'w+');
            fputcsv($file, $colom);
            $data = Comment::query()->findOrFail(31335);

            foreach ($data as $key => $value) {
                $data = $value->toArray();

                unset($data['id']);

                fputcsv($file, $data);
            }
            $blanks = array("\t", "\t", "\t", "\t");
            fputcsv($file, $blanks);
            $blanks = array("\t", "\t", "\t", "\t");
            fputcsv($file, $blanks);
            $blanks = array("\t", "\t", "\t", "\t");
            fputcsv($file, $blanks);

            fclose($file);
        }, 200, $headers);

    }
}
