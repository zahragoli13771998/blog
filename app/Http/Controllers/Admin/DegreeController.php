<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Resume\Degree\StoreDegreeRequest;
use App\Http\Requests\Resume\Degree\UpdateDegreeRequest;
use App\Repositories\Interfaces\DegreeRepositoryInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class DegreeController extends Controller
{
    /**
     * var coming from degree repository interface
     * @var DegreeRepositoryInterface
     */
    private DegreeRepositoryInterface $degree;

    /**
     * for entering degree repository in controller
     * DegreeController constructor.
     * @param DegreeRepositoryInterface $degree
     */
    public function __construct(DegreeRepositoryInterface $degree)
    {
        $this->degree = $degree;
    }

    /**
     *  for showing degree
//     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show()
    {
        $degree = $this->degree->paginate();
        return view('admin.degree.list_degree', compact('degree'));
    }

    /**
     * for creating degree
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('admin.degree.create_degree');
    }

    /**
     * for storing degree
     * @param StoreDegreeRequest $request
     * @return Application|RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $data = $request->only(['title_en','title_fa']);
        $result = $this->degree->store($data);
        if (!$result) {
            return redirect()->route('hr_panel.degree.list')->with('error', 'مشکلی پیش آمده است');
        }
        return redirect()->route('hr_panel.degree.list')->with('success', 'مدرک با موفقیت ذخیره شد');
    }

    /**
     * for editing an existing degree
     * @param int $id
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id)
    {
        $degree = $this->degree->getDegreeById($id);
        return view('admin.degree.edit_degree', compact('degree'));
    }

    /**
     * for updating an existing degree
     * @param UpdateDegreeRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UpdateDegreeRequest $request, int $id): RedirectResponse
    {
        $degree = $this->degree->getDegreeById($id);
        $data = $request->only(['title_en','title_fa']);
        $result = $this->degree->update($degree, $data);
        if (!$result) {
            return redirect()->route('hr_panel.degree.list')->with('error', 'مشکلی پیش آمده است');
        }
        return redirect()->route('hr_panel.degree.list')->with('success', 'مدرک با موفقیت بروزرسانی شد');
    }

    /**
     * @param int $id
     * @return RedirectResponse
     * for deleting an existing degree
     */
    public function destroy(int $id): RedirectResponse
    {
        $degree = $this->degree->getDegreeById($id);
        $result = $this->degree->delete($degree);
        if (!$result) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است');
        }
        return redirect()->back()->with('success', 'مدرک با موفقیت حذف شد');
    }
}
