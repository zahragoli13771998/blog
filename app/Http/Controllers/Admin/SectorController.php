<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Sector\Create;
use App\Http\Requests\Sector\Update;
use App\Repositories\Interfaces\SectorRepositoryInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class SectorController extends Controller
{
    /**
     * Sector repository container
     * @var SectorRepositoryInterface
     */
    private SectorRepositoryInterface $sector_repository;


    /**
     * Sector repository constructor
     * @param SectorRepositoryInterface $sector_repository
     */
    public function __construct(SectorRepositoryInterface $sector_repository)
    {
        $this->sector_repository = $sector_repository;
    }

    /**
     * @return View
     */
    public function show(): View
    {
        $sectors = $this->sector_repository->paginateSectors();
        return view('sector.show', compact('sectors'));
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return view('sector.create');
    }

    /**
     * @param Create $request
     * @return RedirectResponse
     */
    public function store(Create $request): RedirectResponse
    {
        $sector = $request->only(['title_fa', 'title_en']);
        $created = $this->sector_repository->createSector($sector);
        if (!$created) {
            return redirect()->route('admin.sector.list')->with('error', 'sector could not be created');
        }
        return redirect()->route('admin.sector.list')->with('success', 'created successfully');

    }

    /**
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        $sector = $this->sector_repository->getById($id);
        return view('sector.edit', compact('sector'));
    }

    /**
     * @param int $id
     * @param Update $request
     * @return RedirectResponse
     */
    public function update(int $id, Update $request): RedirectResponse
    {
        $data = $request->only(['title_fa', 'title_en']);
        $sector = $this->sector_repository->getById($id);
        $created = $this->sector_repository->updateSector($sector, $data);
        if (!$created) {
            return redirect()->route('admin.sector.list')->with('error', 'sector could not be updated');
        }
        return redirect()->route('admin.sector.list')->with('success', 'updated successfully');
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $sector = $this->sector_repository->getById($id);
        $is_deleted = $this->sector_repository->deleteSector($sector);
        if (!$is_deleted) {
            return redirect()->route('admin.sector.list')->with('error', 'sector could not be deleted');
        }
        return redirect()->route('admin.sector.list')->with('success', 'deleted successfully');

    }

}
