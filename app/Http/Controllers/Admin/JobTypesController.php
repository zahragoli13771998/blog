<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Resume\JobType\StoreJobTypeRequest;
use App\Http\Requests\Resume\JobType\UpdateJobTypeRequest;
use App\Repositories\Interfaces\JobTypeRepositoryInterface;

class JobTypesController extends Controller
{
    /**
     * var coming from job type repository interface
     * @var JobTypeRepositoryInterface
     */
    private JobTypeRepositoryInterface $job_type;

    /**
     * for entering job type repository in controller
     * JobTypesController constructor.
     * @param JobTypeRepositoryInterface $job_type
     */
    public function __construct(JobTypeRepositoryInterface $job_type)
    {
        $this->job_type = $job_type;
    }

    /**
     * for showing job type
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show()
    {
        $jobtype = $this->job_type->paginate();
        return view('admin.job_type.list_job_type', compact('jobtype'));
    }

    /**
     * for creating job type
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('admin.job_type.create_job_type');
    }

    /**
     * for storing job type
     * @param StoreJobTypeRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreJobTypeRequest $request)
    {
        $data = $request->only(['title_en','title_fa']);
        $result = $this->job_type->store($data);
        if (!$result) {
            return redirect()->route('hr_panel.jobtype.list')->with('error', 'مشکلی پیش آمده است');
        }
        return redirect()->route('hr_panel.jobtype.list')->with('success', 'نوع کار با موفقیت ذخیره شد');
    }

    /**
     * for editing an existing job type
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id)
    {
        $jobtype = $this->job_type->getJobTypeById($id);
        return view('admin.job_type.edit_job_type', compact('jobtype'));
    }

    /**
     * for updating an existing job type
     * @param UpdateJobTypeRequest $request
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UpdateJobTypeRequest $request, int $id)
    {
        $data = $request->only(['title_en','title_fa']);
        $job_type = $this->job_type->getJobTypeById($id);
        $result = $this->job_type->update($job_type, $data);
        if (!$result) {
            return redirect()->route('hr_panel.jobtype.list')->with('error', 'مشکلی پیش آمده است');
        }
        return redirect()->route('hr_panel.jobtype.list')->with('success', 'نوع کار با موفقیت بروزرسانی شد');
    }

    /**
     * for deleting an existing job type
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(int $id)
    {
        $job_type = $this->job_type->getJobTypeById($id);
        $result = $this->job_type->delete($job_type);
        if (!$result) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است');
        }
        return redirect()->back()->with('success', 'نوع کار با موفقیت حذف شد');
    }
}
