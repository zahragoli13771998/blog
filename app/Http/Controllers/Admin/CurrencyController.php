<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\CurrencyRepositoryInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    /**
     * @var CurrencyRepositoryInterface
     */
    private CurrencyRepositoryInterface $currency_repository;

    /**
     * @param CurrencyRepositoryInterface $currency_repository
     */
    public function __construct(CurrencyRepositoryInterface $currency_repository)
    {
        $this->currency_repository = $currency_repository;

    }

    /**
     * @return Application|Factory|View
     */
    public function show():View
    {
        $currencies = $this->currency_repository->paginateCurrencies(50);
        return view('currency.show', compact('currencies'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create():View
    {
        return view('currency.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $currency = $request->only(['title','id']);
        $created = $this->currency_repository->createCurrency($currency);
        if (!$created) {
            return redirect()->route('admin.currency.show')->with('error', 'currency could not be created');
        }
        return redirect()->route('admin.currency.show')->with('success', 'created successfully');

    }

    /**
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit(int $id):View
    {
        $currency = $this->currency_repository->getById($id);
        return view('currency.edit', compact('currency'));
    }

    /**
     * @param int $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(int $id, Request $request): RedirectResponse
    {
        $data = $request->only(['title','id']);
        $currency = $this->currency_repository->getById($id);
        $created = $this->currency_repository->updateCurrency($currency, $data);
        if (!$created) {
            return redirect()->route('admin.currency.show')->with('error', 'currency could not be updated');
        }
        return redirect()->route('admin.currency.show')->with('success', 'updated successfully');

    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $currency = $this->currency_repository->getById($id);
        $is_deleted = $this->currency_repository->deleteCurrency($currency);
        if (!$is_deleted) {
            return redirect()->route('admin.currency.show')->with('error', 'currency could not be deleted');
        }
        return redirect()->route('admin.currency.show')->with('success', 'deleted successfully');

    }

}
