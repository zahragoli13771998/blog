<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GlobalCurrency\CreateCurrencyRequest;
use App\Http\Requests\GlobalCurrency\Search;
use App\Http\Requests\GlobalCurrency\UpdateGlobalCurrencyRequest;
use App\Jobs\UpdateWholeGlobalCurrencies;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Services\Interfaces\TickerServiceInterface;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;


/**
 * global currencies
 * Class CurrencyController
 * @package App\Http\Controllers
 */
class GlobalCurrencyController extends Controller
{

    /**
     * Define global currencies store path
     */
    const GLOBAL_CURRENCIES_IMG_PATH = 'global_currencies';

    /**
     * Global Currency Repository container
     * @var GlobalCurrencyInterface
     */
    private GlobalCurrencyInterface $global_currency_repository;
    private TickerServiceInterface $tickerService;

    /**
     * CurrencyController constructor.
     * @param TickerServiceInterface $tickerService
     * @param GlobalCurrencyInterface $global_currency_repository
     */
    public function __construct(TickerServiceInterface $tickerService, GlobalCurrencyInterface $global_currency_repository)
    {
        $this->global_currency_repository = $global_currency_repository;
        $this->tickerService = $tickerService;
    }


    /**
     * retrieve global currencies list
     * @return Factory|Application|\Illuminate\View\View
     */
    public function index(Search $request): View
    {
        $sort_by = $request->sort_by;
        $search = $request->only(['request_name_en', 'request_name_fa', 'request_symbol', 'ramzinex_currencies']);
        $global_currencies = $this->global_currency_repository->paginate(100, true, $sort_by, $search);
        return view('global-currency.index', compact('global_currencies'));
    }


    /**
     * return view to store new global currency
     * @return Factory|Application|\Illuminate\View\View
     */
    public function create(): View
    {
        return view('global-currency.create');
    }


    /**
     * store new global currency
     * @param CreateCurrencyRequest $request
     * @return RedirectResponse
     */
    public function store(CreateCurrencyRequest $request): RedirectResponse
    {
        $data = $request->only(['rank', 'name_en', 'name_fa', 'description_en', 'description_fa', 'symbol', 'price', 'btc_price', 'price_precision', 'total_supply', 'pair_id', 'currency_id', 'total_volume', 'market_dominance', 'daily_trading_volume', 'coin_circulation']);

        if ($request->has('icon')) {
            $data['icon'] = $request->file('icon')
                ->store(static::GLOBAL_CURRENCIES_IMG_PATH, 'public');
        }

        $result = $this->global_currency_repository->createCurrency($data);

        if ($result) {
            return redirect()->back()->with('success', "successfully created");
        }

        return redirect()->back()->with('error', "try again");
    }


    /**
     * return one specific global currency by id
     * @param int $id
     * @return Factory|Application|\Illuminate\View\View
     */
    public function show(int $id): View
    {
        $global_currency = $this->global_currency_repository->getById($id, true);
        return view('global-currency.show', ['global_currency' => $global_currency]);
    }


    /**
     * return view to edit specific global currency
     * @param int $id
     * @return Factory|Application|\Illuminate\View\View
     */
    public function edit(int $id): View
    {
        $global_currency = $this->global_currency_repository->getById($id, true);
        return view('global-currency.edit', compact('global_currency'));
    }


    /**
     * update global currency information
     * @param UpdateGlobalCurrencyRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UpdateGlobalCurrencyRequest $request, int $id): RedirectResponse
    {
        $data = $request->only(['rank', 'name_en', 'name_fa', 'description_en', 'description_fa', 'symbol', 'price', 'price_precision', 'total_supply', 'pair_id', 'currency_id', 'total_volume', 'market_dominance', 'daily_trading_volume', 'coin_circulation']);

        if ($request->has('icon')) {
            $data['icon'] = $request->file('icon')->store(static::GLOBAL_CURRENCIES_IMG_PATH, 'public');
        }

        $currency = $this->global_currency_repository->getById($id);

        $e = $this->global_currency_repository->update($data, $currency);

        //create status message
        if (!$e) {
            return redirect()->route('admin.global-currencies.index')->with('error', "try again");
        }
        return redirect()->route('admin.global-currencies.index')->with('success', "updated");

    }


    /**
     * delete one specific global currency
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $currency = $this->global_currency_repository->getById($id);
        $e = $this->global_currency_repository->delete($currency);

        //create status message
        if ($e) {
            return redirect()->back()->with('success', "successfully deleted");
        }

        return redirect()->back()->with('error', "try again");
    }

    /**
     * toggle global currencies
     * @param int $id
     * @return RedirectResponse
     */
    public function makeGlobalCurrencyHidden(int $id): RedirectResponse
    {
        $global_currency = $this->global_currency_repository->getById($id);
        $data = ['is_hidden' => !$global_currency->is_hidden];
        $result = $this->global_currency_repository->update($data, $global_currency);
        if (!$result) {
            return redirect()->route('admin.global-currencies.index')->with('error', 'مشکلی پیش آمده است');
        }
        return redirect()->route('admin.global-currencies.index')->with('success', 'درخواست شما با موفقیت اجرا شد');

    }

    public function updateglobalsprice()
    {

        try {
//            Artisan::call('global_currency:update_all_in_one');
            $this->dispatch($this->updateglobalsprice());
//            $this->tickerService->updateWholeGlobalCurrencies();
        } catch (\Exception | QueryException $ex) {
            Log::error($ex->getMessage());
            return false;
        }
        return true;
    }

}
