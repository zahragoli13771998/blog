<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Sector\Attach;
use App\Http\Requests\Sector\Detach;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Repositories\Interfaces\SectorRepositoryInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;


class SectorGCController extends Controller
{
    /**
     * Sector repository container
     * @var SectorRepositoryInterface
     */
    private SectorRepositoryInterface $sector_repository;

    /**
     * global currency repository container
     * @var GlobalCurrencyInterface
     */
    private GlobalCurrencyInterface $global_currency;

    /**
     * Sector repository constructor
     * @param SectorRepositoryInterface $sector_repository
     * @param GlobalCurrencyInterface $global_currency
     */
    public function __construct(SectorRepositoryInterface $sector_repository, GlobalCurrencyInterface $global_currency)
    {
        $this->sector_repository = $sector_repository;
        $this->global_currency = $global_currency;
    }

    /**
     * @param int $id
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function selectGlobalCurrencies(int $id): View
    {
        $sector = $this->sector_repository->getById($id);
        $global_currencies = $this->global_currency->getAllGlobalCurrencies();
        return view('sector.attach_gc_to_sector', compact('sector', 'global_currencies'));
    }


    /**
     * @param Attach $attach
     * @param int $id
     * @return RedirectResponse
     */
    public function attachGlobalCurrencies(Attach $attach, int $id): RedirectResponse
    {
        $sector = $this->sector_repository->getById($id);
        $data = $attach->global_currencies_id;
        $is_attached = $this->sector_repository->attachGlobalCurrenciesToSectors($sector, $data);
        if (!$is_attached) {
            return redirect()->route('admin.sector.list')->with('error', 'error while attaching');
        }
        return redirect()->route('admin.sector.list')->with('success', 'attached successfully');
    }

    /**
     * @param int $id
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function showAttached(int $id)
    {
        $sector = $this->sector_repository->getById($id, true);

        return view('sector.show_attached', compact('sector'));
    }

    /**
     * @param int $id
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id){
        $sector = $this->sector_repository->getById($id);
        $global_currencies = $this->global_currency->getAllGlobalCurrencies();
        return view('sector.edit_attached_gc',compact('sector','global_currencies'));
    }

    /**
     * @param int $id
     * @param Attach $request
     * @return RedirectResponse
     */
    public function update(int $id,Attach $request){
        $sector = $this->sector_repository->getById($id);
        $data = $request->global_currencies_id;
        $is_attached = $this->sector_repository->editAttachGlobalCurrenciesToSectors($sector, $data);

        if (!$is_attached) {
            return redirect()->route('admin.sector.list')->with('error', 'error while attaching');
        }
        return redirect()->route('admin.sector.list')->with('success', 'attached successfully');
    }

    /**
     * @param Detach $request
     * @param int $id
     * @return RedirectResponse
     */
    public function delete( int $id , int $gc_id): RedirectResponse
    {
        $sector = $this->sector_repository->getById($id);
        $global_currency_id = $gc_id;
        $is_detached = $this->sector_repository->detach($sector,$global_currency_id);
        if (!$is_detached) {
            return redirect()->route('admin.sector.list')->with('error', 'error while attaching');
        }
        return redirect()->route('admin.sector.list')->with('success', 'attached successfully');
    }
}
