<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Banner\Create;
use App\Http\Requests\Banner\Index;
use App\Http\Requests\Banner\Update;
use App\Repositories\Interfaces\BannerRepositoryInterface;
use App\Repositories\Interfaces\BannerTypeRepositoryInterface;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class BannerController extends Controller
{
    /**
     * Banner repository container
     * @var BannerRepositoryInterface
     */
    private BannerRepositoryInterface $bannerRepository;

    /**
     * Banner type repository container
     * @var BannerTypeRepositoryInterface
     */
    private BannerTypeRepositoryInterface $banner_type_repository;

    /**
     * comment repository container
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $comment_repository;

    /**
     * BannerController constructor.
     * @param BannerRepositoryInterface $bannerRepository
     * @param BannerTypeRepositoryInterface $banner_type_repository
     * @param CommentRepositoryInterface $comment_repository
     */
    public function __construct(BannerRepositoryInterface $bannerRepository, BannerTypeRepositoryInterface $banner_type_repository, CommentRepositoryInterface $comment_repository)
    {
        $this->bannerRepository = $bannerRepository;
        $this->banner_type_repository = $banner_type_repository;
        $this->comment_repository = $comment_repository;
    }

    /**
     * it shows all banners
     * @param Index $request
     * @return View
     */
    public function show(Index $request): view
    {
        $input = $request->only('title_fa','banner_type_id');
        $banner_types = $this->banner_type_repository->getAll();
        $banners = $this->bannerRepository->paginateBanner(true, 50, $input);
        return view('banner.show', compact('banners', 'banner_types', 'input'));
    }

    /**
     * it shows the create form
     * @return Factory|Application|View
     */
    public function create(): view
    {
        $banner_types = $this->banner_type_repository->getAll();
        return view('banner.create', compact('banner_types'));
    }


    /**
     * it saves the created banner
     * @param Create $banner
     * @return RedirectResponse
     */
    public function add(Create $banner): RedirectResponse
    {
        $new_banner = $banner->only(['image', 'URL', 'title_fa','html_description', 'description_fa', 'description_en', 'title_en', 'banner_type_id']);
        if ($banner->has('image')) {
            try {
                $new_banner['image'] = $banner->file('image')->store('banner', 'public');
            } catch (QueryException | Exception $ex) {
                Log::error($ex->getMessage());
            }
        }
        $result = $this->bannerRepository->createBanner($new_banner);

        if (!$result) {
            return redirect()->back()->with('error', 'ایجاد بنر با مشکل مواجه شد');
        }

        return redirect()->route('admin.banner.show')->with('success', 'بنر جدید با موفقیت ایجاد شد');
    }

    /**
     * it shows the edit form
     * @param int $banner_id
     * @return Factory|Application|View
     */
    public function edit(int $banner_id): view
    {
        $banner_types = $this->banner_type_repository->getAll();
        $banner = $this->bannerRepository->getById($banner_id);

        return view('banner.edit', compact('banner', 'banner_types'));
    }


    /**
     * it saves changed banner
     * @param Update $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Update $request, int $id): RedirectResponse
    {
        $banner = $this->bannerRepository->getById($id);
        $edited_banner = $request->only(['image', 'URL', 'title_fa', 'title_en', 'html_description','description_fa', 'description_en', 'banner_type_id']);
        if ($request->has('image')) {
            try {
                Storage::disk('public')->delete($banner->image);
                $edited_banner['image'] = $request->file('image')->store('banner', 'public');
            } catch (QueryException | Exception $ex) {
                Log::error($ex->getMessage());
            }
        }

        $check_if_updated = $this->bannerRepository->updateBanner($edited_banner, $banner);

        if (!$check_if_updated) {
            return redirect()->back()->with('error', 'ویرایش بنر با مشکل مواجه شد');
        }

        return redirect()->route('admin.banner.show')->with('success', 'ویرایش بنر موفقیت انجام شد');
    }
    /**
     * it saves changed banner
     * @param Update $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update2(Update $request, int $id): RedirectResponse
    {
        $banner = $this->bannerRepository->getById($id);
        $edited_banner = $request->only(['banner_type_id']);
        $check_if_updated = $this->bannerRepository->updateBanner($edited_banner, $banner);
        if (!$check_if_updated) {
            return redirect()->back()->with('error', 'ویرایش بنر با مشکل مواجه شد');
        }
        return redirect()->route('admin.banner.show')->with('success', 'ویرایش بنر موفقیت انجام شد');
    }

    /**
     * it delete banner
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        $banner = $this->bannerRepository->getById($id);
        try {
            Storage::disk('public')->delete($banner->image);
        } catch (QueryException | Exception $ex) {
            Log::error($ex->getMessage());
        }
        $comment_deleted = $this->comment_repository->delete(null, $banner);
        $check_if_deleted = $this->bannerRepository->destroyBanners($banner);

        if (!$comment_deleted) {
            return redirect()->back()->with('error', 'حذف کامنت های بنر با مشکل مواجه شد.');
        }
        if (!$check_if_deleted) {
            return redirect()->back()->with('error', 'حذف بنر با مشکل مواجه شد.');
        }

        return redirect()->back()->with('success', 'حذف با موفقیت انجام شد .');
    }

    /**
     * Enable/Disable banner
     * @param int $id
     * @return RedirectResponse
     */
    public function toggleBanners(int $id): RedirectResponse
    {
        $banner = $this->bannerRepository->getById($id);

        $data = ['is_enabled' => !$banner->is_enabled];

        $check_if_enabled = $this->bannerRepository->updateBanner($data, $banner);

        if (!$check_if_enabled) {
            return redirect()->back()->with('error', 'فعالسازی با مشکل مواجه شد');
        }

        return redirect()->back()->with('success', 'فعال شد');
    }

    /**
     * Enable/Disable banner
     * @param int $id
     * @return RedirectResponse
     */
    public function toggleSliderBanners(int $id): RedirectResponse
    {
        $banner = $this->bannerRepository->getById($id);

        $data = ['is_slider' => !$banner->is_slider];

        $check_if_enabled = $this->bannerRepository->updateBanner($data, $banner);

        if (!$check_if_enabled) {
            return redirect()->back()->with('error', 'افزودن به اسلایدرها با مشکل مواجه شد');
        }

        return redirect()->back()->with('success', 'بنر به اسلایدر ها اضافه شد');
    }
}
