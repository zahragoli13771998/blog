<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\Update;
use App\Http\Resources\Comments;
use App\Models\Comment;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class CommentController extends Controller
{
    /**
     * comment repository container
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $comment_repository;

    /**
     * CommentController constructor.
     * @param CommentRepositoryInterface $commentRepository
     */
    public function __construct(CommentRepositoryInterface $commentRepository)
    {
        $this->comment_repository = $commentRepository;

    }

    /**
     *show comments
     * @return \Illuminate\Contracts\Foundation\Application|Factory|\Illuminate\Contracts\View\View|View
     */
    public function showComments(): view
    {
        $comments = $this->comment_repository->paginateComment(false, true, 100, true);
        return view('comment.show', compact('comments'));
    }

    /**
     *show comments
     * //     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Pagination\LengthAwarePaginator|Factory|\Illuminate\Contracts\View\View
     */
    public function showSurvey()
    {
        $comments = Comment::query()->where('type', '=', 'survey')->orderByDesc('created_at')->paginate(100);
        return view('comment.survey', compact('comments'));
    }

    /**
     * show create form of comment
     * @param int $commentable_id
     * @param int|null $comment_id
     * @return Factory|Application|View
     */
    public function create(int $commentable_id, int $comment_id = null): view
    {
        return view('comment.reply', compact('commentable_id', 'comment_id'));
    }


    /**
     *show edit form of comment
     * @param int $id
     * @return Factory|Application|RedirectResponse|View
     */
    public function edit(int $id): view
    {
        $comment = $this->comment_repository->getById($id);
        $related_model = $comment->commentable;
        return view('comment.edit', compact('comment', 'related_model'));
    }

    /**
     *update an existing comment
     * @param int $id
     * @param Update $update
     * @return RedirectResponse
     */
    public function update(int $id, Update $update): RedirectResponse
    {
        $data = $update->only(['body', 'nickname']);
        $comment = $this->comment_repository->getById($id);
        $is_updated = $this->comment_repository->updateComment($comment, $data);
        if (!$is_updated) {
            return redirect()->back()->with('error', 'ویرایش کامنت با مشکل مواجه شد .');
        }
        return redirect()->route('comment.show', [$comment->commentable_id])->with('success', 'ویرایش کامنت با موفقیت انجام شد ');
    }

    /**
     * it deletes feedbacks(if exist) then delete comments
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        $comment = $this->comment_repository->getById($id);
        $deleted = $this->comment_repository->delete($comment);
        if (!$deleted) {
            return redirect()->back()->with('error', 'حذف کامنت با مشکل مواجه شد');
        }
        return redirect()->route('comment.show', [$comment->commentable_id])->with('success', 'کامنت با موفقیت حذف شد');
    }


    /**
     * enable or disable comments
     * @param int $id
     * @return RedirectResponse
     */
    public function toggleComment(int $id): RedirectResponse
    {
        $comment = $this->comment_repository->getById($id);
        $data = ['status' => !$comment->status];
        $check_if_enabled = $this->comment_repository->updateComment($comment, $data);
        if (!$check_if_enabled) {
            return redirect()->back()->with('error', 'فعالسازی کامنت با مشکل مواجه شد');
        }
        return redirect()->back()->with('success', 'کامنت فعال شد');
    }

    /**
     * enable or disable comments
     * @param int $id
//     * @return RedirectResponse
     */
    public function toggleSurvey(int $id)
//    : RedirectResponse
    {
        $comment = $this->comment_repository->getById($id);
        $data = ['status' => !$comment->status];
        $check_if_enabled = $this->comment_repository->updateComment($comment, $data);
        if (!$check_if_enabled) {
            return 'false';
//            return redirect()->back()->with('error', 'فعالسازی کامنت با مشکل مواجه شد');
        }
        return 'true';
//        return redirect()->back()->with('success', 'کامنت فعال شد');
    }

    /**
     * enable or disable multiple comments
     * @param Request $request
     * @return RedirectResponse
     */
    public function toggleMultipleComment(Request $request): RedirectResponse
    {
        foreach ($request->get('comments') as $id) {
            $comment = $this->comment_repository->getById($id);
            $data = ['status' => !$comment->status];
            $this->comment_repository->updateComment($comment, $data);
        }
        return redirect()->back()->with('success', 'کامنت ها فعال شد');
    }

    public function verifySurvey(int $id)
    {
        try {
            Comment::query()->findOrFail($id)->update(['status' => 1]);
            return 'true';
        } catch (\Exception | QueryException $exception) {
            Log::error($exception->getMessage());
            return 'false';
        }


    }
}
