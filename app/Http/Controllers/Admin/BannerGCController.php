<?php


namespace App\Http\Controllers\Admin;


use App\Http\Requests\BannerGlobalCurrency\Assign;
use App\Repositories\Interfaces\BannerRepositoryInterface;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class BannerGCController
{
    /**
     * GlobalCurrency Repository Container
     * @var GlobalCurrencyInterface
     */
    private GlobalCurrencyInterface $global_currency;

    /**
     * Banner Repository Container
     * @var BannerRepositoryInterface
     */
    private BannerRepositoryInterface $banner_repository;

    /**
     * BannerGCController constructor.
     * @param BannerRepositoryInterface $banner_repository
     * @param GlobalCurrencyInterface $global_currency
     */
    public function __construct(BannerRepositoryInterface $banner_repository, GlobalCurrencyInterface $global_currency)
    {
        $this->banner_repository = $banner_repository;
        $this->global_currency = $global_currency;
    }

    public function showGCWithBanners(int $id)
    {
        $global_currency = $this->global_currency->getById($id);
        return view('global-currency.showGCBanners',compact('global_currency'));
    }

    /**
     * select global_currencies to add to banners
     * @param int $id
     * @return View
     */
    public function selectGlobalCurrencies(int $id): View
    {
        $banner = $this->banner_repository->getById($id);
        $global_currencies = $this->global_currency->getAllGlobalCurrencies();
        return view('banner.add_global_currencies', compact('global_currencies', 'banner'));
    }

    /**
     * update banners by syncing selected global_currencies with banners
     * @param Assign $request
     * @param int $id
     * @return RedirectResponse
     */
    public function addGlobalCurrencyToBanner(Assign $request, int $id): RedirectResponse
    {
        $currencies = $request->global_currencies_id;
        $banner = $this->banner_repository->getById($id);
        $added = $this->banner_repository->attachGlobalCurrencies($banner, $currencies);
        if (!$added) {
            return redirect()->route('admin.banner.show')->with('error', 'تخصیص ارز به بنر با مشکل مواجه شد');
        }
        return redirect()->route('admin.banner.show')->with('success', 'تخصیص ارز به بنر با موفقیت انجام شد');


    }

    /**
     * select banners to add to global_currencies
     * @param int $id
     * @return View
     */
    public function selectBanners(int $id): View
    {
        $global_currency = $this->global_currency->getById($id);
        $banners = $this->banner_repository->getAllBanners();
        return view('global-currency.add_banners', compact('banners', 'global_currency'));
    }

    /**
     *  update global_currencies by syncing selected banners with global_currencies
     * @param Assign $request
     * @param int $id
     * @return RedirectResponse
     */
    public function addBannerToGlobalCurrency(Assign $request, int $id): RedirectResponse
    {
        $banners = $request->banners_id;
        $global_currency = $this->global_currency->getById($id);
        $added = $this->global_currency->attachBanners($global_currency, $banners);
        if (!$added) {
            return redirect()->route('admin.global-currencies.index')->with('error', 'تخصیص ینر به ارز با مشکل مواجه شد');
        }
        return redirect()->route('admin.global-currencies.index')->with('success', 'تخصیص بنر به ارز با موفقیت انجام شد');
    }


}
