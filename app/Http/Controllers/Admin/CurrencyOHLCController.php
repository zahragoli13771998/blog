<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Ohlc\Create;
use App\Http\Requests\Ohlc\Update;
use App\Repositories\Interfaces\IntervalRepositoryInterface;
use App\Repositories\Interfaces\CurrencyOHLCRepositoryInterface;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;


class CurrencyOHLCController extends Controller
{
    /**
     * CurrencyOHLCRepository container
     * @var CurrencyOHLCRepositoryInterface
     */
    private CurrencyOHLCRepositoryInterface $currency_ohlc_Repository;

    /**
     * GlobalCurrency Repository Container
     * @var GlobalCurrencyInterface
     */
    private GlobalCurrencyInterface $global_currency;

    /**
     * BannerRepository Container
     * @var IntervalRepositoryInterface
     */
    private IntervalRepositoryInterface $interval_repository;

    /**
     * CurrencyOHLCController constructor.
     * @param CurrencyOHLCRepositoryInterface $currency_ohlc_Repository
     * @param GlobalCurrencyInterface $global_currency
     * @param IntervalRepositoryInterface $interval_repository
     */
    public function __construct(CurrencyOHLCRepositoryInterface $currency_ohlc_Repository, GlobalCurrencyInterface $global_currency, IntervalRepositoryInterface $interval_repository)
    {
        $this->currency_ohlc_Repository = $currency_ohlc_Repository;
        $this->global_currency = $global_currency;
        $this->interval_repository = $interval_repository;
    }

    /**
     * show the list of ohlces
     * @return Application|Factory|View
     */
    public function show(): View
    {
        $ohlces = $this->currency_ohlc_Repository->paginate();
        return view('ohlc.show', compact('ohlces'));
    }

    /**
     * retun the create form of ohlc
     * @return Application|Factory|View
     */
    public function create(): View
    {
        $global_currencies = $this->global_currency->getAllGlobalCurrencies();
        $intervals = $this->interval_repository->getAll();
        return view('ohlc.create', compact('global_currencies', 'intervals'));
    }

    /**
     * create a new ohlc
     * @param Create $request
     * @return RedirectResponse
     */
    public function store(Create $request): RedirectResponse
    {
        $data = $request->only(['global_currency_id', 'interval_id', 'open', 'high', 'low', 'close', 'volume', 'market_cap']);
        $created = $this->currency_ohlc_Repository->store($data);
        if (!$created) {
            return redirect()->route('admin.ohlc.show')->with('error', 'مشکلی رخ داده است');
        }
        return redirect()->route('admin.ohlc.show')->with('success', 'باموفقیت ایجاد شد');
    }

    /**
     * return the edit form of ohlc
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit(int $id): View
    {
        $ohlc = $this->currency_ohlc_Repository->findById($id);
        $global_currencies = $this->global_currency->getAllGlobalCurrencies();
        $intervals = $this->interval_repository->getAll();
        return view('ohlc.edit', compact('ohlc', 'global_currencies', 'intervals'));
    }

    /**
     * edit an existing ohlc
     * @param Update $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Update $request, int $id): RedirectResponse
    {
        $data = $request->only(['global_currency_id', 'interval_id', 'open', 'high', 'low', 'close', 'volume', 'market_cap']);
        $ohlc = $this->currency_ohlc_Repository->findById($id);
        $edited = $this->currency_ohlc_Repository->update($data, $ohlc);
        if (!$edited) {
            return redirect()->route('admin.ohlc.show')->with('error', 'بروزرسانی با مشکل مواجه شد');
        }
        return redirect()->route('admin.ohlc.show')->with('success', 'بروزرسانی با موفقیت انجام شد');
    }

    /**
     * delete ohlc
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        $ohlc = $this->currency_ohlc_Repository->findById($id);
        $deleted = $this->currency_ohlc_Repository->delete($ohlc);
        if (!$deleted) {
            return redirect()->route('admin.ohlc.show')->with('error', 'حذف با مشکل مواجه شد');
        }
        return redirect()->route('admin.ohlc.show')->with('success', 'حذف با موفقیت انجام شد');
    }
}
