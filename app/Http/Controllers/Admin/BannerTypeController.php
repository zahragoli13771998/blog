<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\BannerTypeRepositoryInterface;
use App\Http\Requests\BannerType\Store;
use App\Http\Requests\BannerType\Update;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class BannerTypeController extends Controller
{
    /**
     * The banner types repository instance.
     *
     * @var BannerTypeRepositoryInterface
     */
    private BannerTypeRepositoryInterface $banner_type_repository;

    /**
     * Instantiate a new banner types instance.
     *
     * @param BannerTypeRepositoryInterface $banner_type_repository
     */
    public function __construct(BannerTypeRepositoryInterface $banner_type_repository)
    {
        $this->banner_type_repository = $banner_type_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function show(): View
    {
        $banner_types = $this->banner_type_repository->paginate();

        return view('banner_type.show', compact('banner_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('banner_type.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        $banner_types = $this->banner_type_repository->findById($id);

        return view('banner_type.edit', compact('banner_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $store
     * @return RedirectResponse
     */
    public function store(Store $store): RedirectResponse
    {
        $input = $store->only(['title_fa', 'title_en']);

        $result = $this->banner_type_repository->store($input);

        if (!$result) {
            return redirect()->back()->with('error', 'عملیات ذخیره سازی با شکست مواجه شد.');
        }

        return redirect()->route('admin.banner.types.show')->with('success', 'عملیات ذخیره سازی با موفقیت انجام شد.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $update
     * @param int $id
     * @return RedirectResponse
     */
    public function update(int $id, Update $update): RedirectResponse
    {
        $banner_types = $this->banner_type_repository->findById($id);

        $input = $update->only(['title_fa', 'title_en']);

        $result = $this->banner_type_repository->update($input, $banner_types);

        if (!$result) {
            return redirect()->back()->with('error', 'عملیات بروزرسانی با شکست مواجه شد.');
        }

        return redirect()->route('admin.banner.types.show')->with('success', 'عملیات بروزرسانی با موفقیت انجام شد.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $banner_types = $this->banner_type_repository->findById($id);

        $result = $this->banner_type_repository->delete($banner_types);

        if (!$result) {
            return redirect()->back()->with('error', 'عملیات حذف با شکست مواجه شد.');
        }

        return redirect()->back()->with('success', 'عملیات حذف با موفقیت شد.');
    }
}
