<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class marketingCkeditorController extends Controller
{
    /**
     * Upload and store file
     * @param Request $request
     */
    public function storeFile(Request $request)
    {
        if ($request->hasFile('upload')) {
            $file = $request->file('upload')->store('editor', 'public');
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('storage/'.$file);
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url')</script>";
            echo $response;
        }
    }
}
