<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\TrendingCurrency\Create;
use App\Http\Requests\TrendingCurrency\Update;
use App\Repositories\Interfaces\CurrencyRepositoryInterface;
use App\Repositories\Interfaces\TrendingCurrencyRepositoryInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class TrendingCurrencyController extends Controller
{

    /**
     * TrendingCurrencyRepository container
     * @var TrendingCurrencyRepositoryInterface
     */
    private TrendingCurrencyRepositoryInterface $currency_trending_repository;

    /**
     * CurrencyTrendingController constructor.
     * @param TrendingCurrencyRepositoryInterface $currency_trending_repository
     */
    public function __construct(TrendingCurrencyRepositoryInterface $currency_trending_repository)
    {
        $this->currency_trending_repository = $currency_trending_repository;
    }

    /**
     * show trending_currencies
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(): View
    {
        $trending_currencies = $this->currency_trending_repository->paginateTrendingCurrency();
        return view('trending_currency.show', compact('trending_currencies'));
    }

    /**
     * return create form of trending_currency
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add():View
    {
        return view('trending_currency.create');
    }

    /**
     * store created trending_currency
     * @param Create $request
     * @return RedirectResponse
     */
    public function create(Create $request): RedirectResponse
    {

        $data = $request->only(['id']);
        $created = $this->currency_trending_repository->createTrendingCurrency($data);
        if (!$created) {
            return redirect()->route('admin.trending.show')->with('error', 'ایجاد trending_currency با مشکل مواجه شد .');
        }
        return redirect()->route('admin.trending_currency.show')->with('success', 'currency_trending با موفقیت ایجاد شد');

    }

    /**
     * return edit form of trending_currency
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id):View
    {
        $trending_currency = $this->currency_trending_repository->getById($id);
        return view('trending_currency.edit', compact('trending_currency'));
    }

    /**
     * store edited trending_currency
     * @param Update $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Update $request, int $id): RedirectResponse
    {
        $data = $request->only(['id']);
        $currency_trending = $this->currency_trending_repository->getById($id);
        $updated = $this->currency_trending_repository->updateTrendingCurrency($currency_trending, $data);
        if (!$updated) {
            return redirect()->route('admin.trending_currency.show')->with('error', 'ویرایش trending_currency با مشکل مواجه شد.');
        }
        return redirect()->route('admin.trending_currency.show')->with('success', 'trending_currency با موفقیت ایجاد شد');
    }

    /**
     * delete trending_currency
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        $currency_trending = $this->currency_trending_repository->getById($id);
        $deleted = $this->currency_trending_repository->deleteTrendingCurrency($currency_trending);
        if (!$deleted) {
            return redirect()->route('admin.trending_currency.show')->with('error', 'حذف trending_currency با مشکل مواجه شد.');
        }
        return redirect()->route('admin.trending_currency.show')->with('success', 'trending_currency با موفقیت حذف شد');
    }

}
