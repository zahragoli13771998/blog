<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Trend\Create;
use App\Http\Requests\Trend\Update;
use App\Repositories\Interfaces\TrendRepositoryInterfac;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class TrendController extends Controller
{

    /**
     * TrendRepository Container
     * @var \App\Repositories\Interfaces\TrendRepositoryInterfac
     */
    private TrendRepositoryInterfac $trend_repository;

    /**
     * TrendController constructor.
     * @param TrendRepositoryInterfac $trend_repository
     */
    public function __construct(TrendRepositoryInterfac $trend_repository)
    {
        $this->trend_repository = $trend_repository;
    }

    /**
     * show trends
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show():View
    {
        $trends = $this->trend_repository->paginateTrends();
        return view('trend.show',compact('trends'));
    }
    /**
     * return create form of Trend
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(): view
    {
        return view('trend.create');
    }

    /**
     * store created Trend
     * @param Create $request
     * @return RedirectResponse
     */
    public function add(Create $request): RedirectResponse
    {
        $data = $request->only(['id']);

        $created = $this->trend_repository->createTrend($data);
        if (!$created) {
            return redirect()->back()->with('error', 'ایجاد trend با مشکل مواجه شد');
        }
        return redirect()->route('admin.trend.show')->with('success', ' trend
        با موفقیت ایجاد شد'
        );
    }

    /**
     * return edit form of Trend
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id): View
    {
        $trend = $this->trend_repository->getById($id);
        return view('trend.edit', compact('trend'));
    }

    /**
     * save edited Trend
     * @param Update $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Update $request, int $id): RedirectResponse
    {
        $trend = $this->trend_repository->getById($id);
        $data = $request->only(['id']);
        $updated = $this->trend_repository->updateTrend($trend, $data);

        if (!$updated) {
            return redirect()->back()->with('error', 'ویرایش trend با مشکل مواجه شد');
        }
        return redirect()->route('admin.trend.show')->with('success', 'trend با موفقیت ویرایش شد');
    }

    /**
     * delete a Trend
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        $trend = $this->trend_repository->getById($id);
        $deleted = $this->trend_repository->deleteTrend($trend);
        if (!$deleted) {
            return redirect()->back()->with('error', 'حذف trend با موفقیت انجام شد');
        }
        return redirect()->route('admin.trend.show')->with('success', 'trend
        با موفقیت حذف شد');
    }
}
