<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Resume\JobVacancy\StoreJobVacancyRequest;
use App\Http\Requests\Resume\JobVacancy\UpdateJobVacancyRequest;
use App\Models\JobTypes;
use App\Models\JobVacancies;
use App\Repositories\Interfaces\JobVacancyRepositoryInterface;
use App\Repositories\Interfaces\JobTypeRepositoryInterface;
use App\Repositories\Interfaces\DegreeRepositoryInterface;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\QueryException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class JobVacancyController extends Controller
{
    /**
     * var coming from job vacancy repository interface
     * @var JobVacancyRepositoryInterface
     */
    private JobVacancyRepositoryInterface $jobVacancyRepository;

    /**
     * var coming from job type repository interface
     * @var JobTypeRepositoryInterface
     */
    private JobTypeRepositoryInterface $job_type;

    /**
     * var coming from degree repository interface
     * @var DegreeRepositoryInterface
     */
    private DegreeRepositoryInterface $degree;

    /**
     * for entering job vacancy and job type and degree repository in controller
     * JobVacancyController constructor.
     * @param JobVacancyRepositoryInterface $jobVacancyRepository
     * @param JobTypeRepositoryInterface $job_type
     * @param DegreeRepositoryInterface $degree
     */
    public function __construct(JobVacancyRepositoryInterface $jobVacancyRepository, JobTypeRepositoryInterface $job_type, DegreeRepositoryInterface $degree)
    {
        $this->jobVacancyRepository = $jobVacancyRepository;
        $this->job_type = $job_type;
        $this->degree = $degree;
    }

    /**
     * for showing job vacancy
//     * @return Application|Factory|View
     */
    public function show(int $id)
    {
        $job = $this->jobVacancyRepository->paginate($id);
        return view('admin.job_vacancy.list_job_vacancy', compact('job'));
    }

    /**
     * for creating job vacancy
     * //     * @return Application|Factory|View
     */
    public function create()
    {
        $job_types = $this->job_type->getAll(false);
        return view('admin.job_vacancy.create_job_vacancy', compact('job_types'));
    }

    public function add(StoreJobVacancyRequest $request)
    {
        $data = $request->only(['title_en', 'title_fa', 'description', 'job_type_id', 'min_experience', 'salary', 'min_degree_id', 'expire_time']);
        return $data;

    }

    /**
     * for storing job vacancy
     * @param StoreJobVacancyRequest $request
     * @return RedirectResponse
     */
    public function store(StoreJobVacancyRequest $request): RedirectResponse
    {
        $data = $request->only(['title_en', 'title_fa', 'description', 'job_type_id', 'expire_time']);

        $result = $this->jobVacancyRepository->store($data);

        if (!$result) {
            return redirect()->route('hr_panel.jobvacancy_list',$data['job_type_id'])->with('error', 'مشکلی پیش آمده است');
        }
        return redirect()->route('hr_panel.jobvacancy_list',$data['job_type_id'])->with('success', 'موقعیت شغلی با موفقیت ذخیره شد');
    }

    /**
     * for editing an existing job vacancy
     * @param int $id
     * @return Application|Factory|View
     */
    public function edit(int $id)
    {
        $job_types = $this->job_type->getAll(false);
        $job = $this->jobVacancyRepository->getJobVacancyById($id);
        return view('admin.job_vacancy.edit_job_vacancy', compact('job', 'job_types'));
    }

    /**
     * for updating an existing job vacancy
     * @param UpdateJobVacancyRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UpdateJobVacancyRequest $request, int $id): RedirectResponse
    {
        $data = $request->only(['title_en', 'title_fa', 'description', 'job_type_id', 'expire_time']);
        $job_vacancy = $this->jobVacancyRepository->getJobVacancyById($id);
        $result = $this->jobVacancyRepository->update($job_vacancy, $data);
        if (!$result) {
            return redirect()->route('hr_panel.jobvacancy_list')->with('error', 'مشکلی پیش آمده است');
        }
        return redirect()->route('hr_panel.jobvacancy_list')->with('success', 'موقعیت شغلی با موفقیت بروزرسانی شد');
    }

    /**
     * for deleting an existing job vacancy
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $job_vacancy = $this->jobVacancyRepository->getJobVacancyById($id);
        $result = $this->jobVacancyRepository->delete($job_vacancy);
        if (!$result) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است');
        }
        return redirect()->back()->with('success', 'موقعیت شغلی با موفقیت حذف شد');
    }

    /**
     * for updating job vacancy status by admin
     * @param int $id
     * @return RedirectResponse
     */
    public function updateStatus(int $id): RedirectResponse
    {
        $job_vacancy = $this->jobVacancyRepository->getJobVacancyById($id);
        $data = ['status' => !$job_vacancy->status];
        $result = $this->jobVacancyRepository->update($job_vacancy, $data);
        if (!$result) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است');
        }
        return redirect()->back()->with('success', 'وضعیت با موفقیت بروزرسانی شد');
    }
}
