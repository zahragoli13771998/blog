<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\HRLoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.auth.login');
    }

    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function createMarketingAuth()
    {
        return view('admin.auth.marketingLogin');
    }

    public function createHR()
    {
        return view('admin.auth.hrLogin');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param HRLoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(HRLoginRequest $request)
    {
        $request->authenticate();

        $request->session()->regenerate();

        return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param HRLoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeMarketingAuth(HRLoginRequest $request)
    {
        $request->authenticate();

        $request->session()->regenerate();

        return redirect()->intended('marketing/panel/faq/category');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param HRLoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeHR(HRLoginRequest $request)
    {
        Log::info('******************************');
        Log::info($request);
        $request->session()->regenerate();

        return redirect()->intended('hr_panel/panel/jobtype/list');
    }


    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('admin.login');
    }
    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyMarketing(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('login');
    }

    public function destroyHR(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('hr.login');
    }
}
