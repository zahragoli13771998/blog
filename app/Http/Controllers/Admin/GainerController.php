<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Gainer\Create;
use App\Http\Requests\Gainer\Update;
use App\Repositories\Interfaces\GainerRepositoryInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class GainerController extends Controller
{
    /**
     * Gainer Repository Container
     * @var \App\Repositories\Interfaces\GainerRepositoryInterface
     */
    private GainerRepositoryInterface $gainer_repository;

    /**
     * GainerController constructor.
     * @param GainerRepositoryInterface $gainer_repository
     */
    public function __construct(GainerRepositoryInterface $gainer_repository)
    {
        $this->gainer_repository = $gainer_repository;
    }

    /**
     * show gainers
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show():View
    {
        $gainers = $this->gainer_repository->paginateGainers();
        return view('gainer.show',compact('gainers'));
    }
    /**
     * return create form of gainer
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(): view
    {
        return view('gainer.create');
    }

    /**
     * store created gainer
     * @param Create $request
     * @return RedirectResponse
     */
    public function add(Create $request): RedirectResponse
    {
        $data = $request->only(['id','value']);
        $created = $this->gainer_repository->createGainer($data);
        if (!$created) {
            return redirect()->back()->with('error', 'ایجاد gainer با مشکل مواجه شد');
        }
        return redirect()->route('admin.gainer.show')->with('success', ' gainer
        با موفقیت ایجاد شد'
        );
    }

    /**
     * return edit form of gainer
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id): View
    {
        $gainer = $this->gainer_repository->getById($id);
        return view('gainer.edit', compact('gainer'));
    }

    /**
     * save edited gainer
     * @param Update $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Update $request, int $id): RedirectResponse
    {
        $gainer = $this->gainer_repository->getById($id);
        $data = $request->only(['id','value']);
        $updated = $this->gainer_repository->updateGainer($gainer, $data);

        if (!$updated) {
            return redirect()->back()->with('error', 'ویرایش gainer با مشکل مواجه شد');
        }
        return redirect()->route('admin.gainer.show')->with('success', 'gainer با موفقیت ویرایش شد');
    }

    /**
     * delete a gainer
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        $gainer = $this->gainer_repository->getById($id);
        $deleted = $this->gainer_repository->deleteGainer($gainer);
        if (!$deleted) {
            return redirect()->back()->with('error', 'حذف gainer با موفقیت انجام شد');
        }
        return redirect()->route('admin.gainer.show')->with('success', 'gainer
        با موفقیت حذف شد');
    }
}
