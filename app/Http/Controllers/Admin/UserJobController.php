<?php

namespace App\Http\Controllers\Admin;

use App\Models\JobVacancyRequest;
use App\Repositories\Interfaces\JobVacancyRequestRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class  UserJobController extends Controller
{
    /**
     * var coming from job vacancy request repository interface
     * @var JobVacancyRequestRepositoryInterface
     */
    private JobVacancyRequestRepositoryInterface $user_job_request;

    /**
     * for entering users job repository in controller
     * UserJobController constructor.
     * @param JobVacancyRequestRepositoryInterface $user_job_request
     */
    public function __construct(JobVacancyRequestRepositoryInterface $user_job_request)
    {
        $this->user_job_request = $user_job_request;
    }

    /**
     * for showing job vacancies request
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(int $id)
    {
        $job_request = $this->user_job_request->showById($id);
        return view('admin.job_vacancy.list_request', compact('job_request'));
    }

    /**
     * for downloading uploaded resumes
     * @param int $id
     * //     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getFile(int $id)
    {
        $user_job = $this->user_job_request->getJobVacancyRequestById($id);
        $path = $user_job->resume_path;
        Log::info('----' . $path);
        return response()->file(storage_path("app/public/$path"));
//        return response()->file($path);

    }

    /**
     * for deleting job vacancy request by admin
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id): \Illuminate\Http\RedirectResponse
    {
        $user_job = $this->user_job_request->getJobVacancyRequestById($id);
        $result = $this->user_job_request->delete($user_job);
        if (!$result) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است');
        }
        return redirect()->back()->with('success', 'درخواست مورد نظر با موفقیت حذف شد');
    }

    /**
     * for updating status by admin
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateStatus(int $id): \Illuminate\Http\RedirectResponse
    {
        $user_job = $this->user_job_request->getJobVacancyRequestById($id);

        if ($user_job->status == 2) {
            $data = ['status' => $user_job->status - 1];
        }

        elseif ($user_job->status == 1) {
            $data = ['status' => $user_job->status + 1];
        }
        else {
            $data = ['status' => $user_job->status + 1];
        }

        $result = $this->user_job_request->update($user_job, $data);
        if (!$result) {
            return redirect()->back()->with('error', 'مشکلی پیش آمده است');
        }
        return redirect()->back()->with('success', 'وضعیت با موفقیت بروزرسانی شد');
    }

    /**
     * for showing job vacancy request using job types
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function resumeShow(int $id)
    {
        $user_job = $this->user_job_request->getJobVacancyRequestByType($id);
        return view('hr_panel.job_type.list_request', compact('user_job'));
    }


}
