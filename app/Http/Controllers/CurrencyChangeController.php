<?php

namespace App\Http\Controllers;

use App\Http\Requests\GlobalChange\GetAll;
use App\Http\Requests\GlobalChange\Store;
use App\Http\Requests\GlobalChange\Update;
use App\Models\GlobalCurrency;
use App\Repositories\Interfaces\CurrencyChangesPercentRepositoryInterface;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Repositories\Interfaces\IntervalRepositoryInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class CurrencyChangeController extends Controller
{
    /**
     * The profit and damage repository instance.
     *
     * @var CurrencyChangesPercentRepositoryInterface
     * @var IntervalRepositoryInterface
     */
    private CurrencyChangesPercentRepositoryInterface $currencyChangesPercentRepository;

    /**
     * @var IntervalRepositoryInterface
     */
    private IntervalRepositoryInterface $interval_repository;

    /**
     * Global Currency Repository container
     * @var GlobalCurrencyInterface
     */
    private GlobalCurrencyInterface $global_currency_repository;

    /**
     * Instantiate a new profit and damage instance.
     *
     * @param CurrencyChangesPercentRepositoryInterface $currencyChangesPercentRepository
     * @param IntervalRepositoryInterface $interval_repository
     * @param GlobalCurrencyInterface $global_currency_repository
     */
    public function __construct(CurrencyChangesPercentRepositoryInterface $currencyChangesPercentRepository,
                                IntervalRepositoryInterface $interval_repository, GlobalCurrencyInterface $global_currency_repository)
    {
        $this->currencyChangesPercentRepository = $currencyChangesPercentRepository;
        $this->interval_repository = $interval_repository;
        $this->global_currency_repository = $global_currency_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param GetAll $getAll
     * @return View
     */
    public function show(GetAll $getAll): View
    {
        $currencies = $this->global_currency_repository->getAllGlobalCurrencies(false, true);
        $intervals = $this->interval_repository->getAll();
        $input = $getAll->only(['global_currency_id', 'interval_id']);
        $currencyChangePercent = $this->currencyChangesPercentRepository->paginate($input);
        return view('currency_change_percent.show',
            compact('currencyChangePercent', 'currencies', 'intervals'));
    }

    /**
     * create the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $currencies = $this->global_currency_repository->getAllGlobalCurrencies(false, true);
        $intervals = $this->interval_repository->getAll();
        return view('currency_change_percent.create',
            compact('currencies', 'intervals'));
    }

    /**
     * edit the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id): view
    {
        $currencies = $this->global_currency_repository->getAllGlobalCurrencies(false, true);
        $intervals = $this->interval_repository->getAll();
        $currencyChange = $this->currencyChangesPercentRepository->findById($id);
        return view('currency_change_percent.edit',
            compact('currencyChange', 'currencies', 'intervals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $store
     * @return RedirectResponse
     */
    public function store(Store $store): RedirectResponse
    {
        $input = $store->only(['value', 'global_currency_id', 'interval_id']);
        $result = $this->currencyChangesPercentRepository->store($input);
        if (!$result) {
            return redirect()->back()->with('error', 'عملیات ذخیره سازی با شکست مواجه شد.');
        }
        return redirect()->back()->with('success', 'عملیات ذخیره سازی با موفقیت انجام شد.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param Update $update
     * @return RedirectResponse
     */
    public function update(int $id, Update $update): RedirectResponse
    {
        $change_percent = $this->currencyChangesPercentRepository->findById($id);
        $input = $update->only(['value', 'global_currency_id', 'interval_id']);
        $result = $this->currencyChangesPercentRepository->update($input, $change_percent);
        if (!$result) {
            return redirect()->back()->with('error', 'عملیات بروزرسانی با شکست مواجه شد.');
        }
        return redirect()->back()->with('success', 'عملیات بروزرسانی با موفقیت انجام شد.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $change_percent = $this->currencyChangesPercentRepository->findById($id);
        $result = $this->currencyChangesPercentRepository->delete($change_percent);
        if (!$result) {
            return redirect()->back()->with('error', 'عملیات حذف با شکست مواجه شد.');
        }
        return redirect()->back()->with('success', 'عملیات حذف با موفقیت انجام شد.');
    }
}
