<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Services\UserAuthentication\UserAuthenticatorService;
use Illuminate\Http\Request;

class ExcelController
{
    /**
     * Construct and inject necessary services to container
     * @param UserAuthenticatorService $user_authenticator_service
e
     */
    public function __construct( UserAuthenticatorService $user_authenticator_service)
    {
        $this->user_authenticator_service = $user_authenticator_service;
    }
    public function exportExcelView()
    {
        return view('export');
    }

    public function exportExcel()
    {
        $name = 'currencies2.csv';
        $headers = [
            'Content-Disposition' => 'attachment; filename=' . $name,
        ];
        $colom = \Illuminate\Support\Facades\Schema::getColumnListing("currency");
        $temp_colom = array_flip($colom);
        unset($temp_colom['id']);
        $colom = array_flip($temp_colom);
        return response()->stream(function () use ($colom) {
            $file = fopen('php://output', 'w+');
            fputcsv($file, $colom);
            $data = Currency::Select('title')->get();

            foreach ($data as $key => $value) {
                $data = $value->toArray();

                unset($data['id']);

                fputcsv($file, $data);
            }
            $blanks = array("\t", "\t", "\t", "\t");
            fputcsv($file, $blanks);
            $blanks = array("\t", "\t", "\t", "\t");
            fputcsv($file, $blanks);
            $blanks = array("\t", "\t", "\t", "\t");
            fputcsv($file, $blanks);

            fclose($file);
        }, 200, $headers);
    }

    public function testUrl(Request $request){
        $code = uniqid();
        $user_id = $this->user_authenticator_service->getUserId($request);
        $url1 = "https://api-two.ramzinex.com/register_with_invitation=".$code."&user_id=".$user_id;
       return redirect($url1);
    }

    public function test(Request $request)
    {
        $url = $request->url();
        $str = substr($url,strpos($url,'=')+1);
        $exploded_str = explode('&',$str);
        $user_id = str_replace('user_id=','',$exploded_str[1]);
        return "user_id:".$user_id."------------"."invitation:".$exploded_str[0];
//       $user_id= substr($invite_link,strpos($invite_link,'&user_id',true)+1);
//       return "invite:".$invite_link."------"."user_id:".$user_id."***";
//      echo $str;
//        return "str:".$str."----".$exploded_str[0];

    }
}
