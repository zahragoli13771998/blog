<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
class StorageManagerController extends Controller
{
    /**
     * Get and return file from path
     * @param string $path
     * @return StreamedResponse
     * @throws FileNotFoundException
     */
    public function showFile(string $path)
    {
        $path = storage_path('app/public/'.$path);

        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }
}
