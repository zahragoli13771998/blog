<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\IntervalRepositoryInterface;
use App\Http\Requests\Interval\Store;
use App\Http\Requests\Interval\Update;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class IntervalController extends Controller
{
    /**
     * The interval repository instance.
     *
     * @var IntervalRepositoryInterface
     */
    private IntervalRepositoryInterface $interval_repository;

    /**
     * Instantiate a new interval instance.
     *
     * @param IntervalRepositoryInterface $interval_repository
     */
    public function __construct(IntervalRepositoryInterface $interval_repository)
    {
        $this->interval_repository = $interval_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function show(): View
    {
        $intervals = $this->interval_repository->paginate();
        return view('interval.show', compact('intervals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('interval.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id): view
    {
        $intervals = $this->interval_repository->findById($id);
        return view('interval.edit', compact('intervals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $store
     * @return RedirectResponse
     */
    public function store(Store $store): RedirectResponse
    {
        $input = $store->only('title_fa', 'title_en');
        $result = $this->interval_repository->store($input);
        if (!$result) {
            return redirect()->back()->with('error', 'عملیات ذخیره سازی با شکست مواجه شد.');
        }
        return redirect()->route('admin.intervals.show')
            ->with('success', 'عملیات ذخیره سازی با موفقیت انجام شد.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param Update $update
     * @return RedirectResponse
     */
    public function update(int $id, Update $update): RedirectResponse
    {
        $intervals = $this->interval_repository->findById($id);
        $input = $update->only('title_fa', 'title_en');
        $result = $this->interval_repository->update($input, $intervals);
        if (!$result) {
            return redirect()->back()->with('error', 'عملیات بروزرسانی با شکست مواجه شد.');
        }
        return redirect()->route('admin.intervals.show')
            ->with('success', 'عملیات بروزرسانی با موفقیت انجام شد.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $intervals = $this->interval_repository->findById($id);
        $result = $this->interval_repository->delete($intervals);

        if (!$result) {
            return redirect()->back()->with('error', 'عملیات حذف با شکست مواجه شد.');
        }

        return redirect()->back()->with('success', 'عملیات حذف با موفقیت انجام شد.');
    }
}

