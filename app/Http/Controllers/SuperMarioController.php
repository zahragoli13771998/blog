<?php

namespace App\Http\Controllers;

use App\Http\Requests\SuperMario\login;
use App\Repositories\Interfaces\GameScoreRepositoryInterface;
use App\Repositories\Interfaces\GameUserRepositoryInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;


class SuperMarioController extends Controller
{
    /**
     * Game User repository container
     * @var GameUserRepositoryInterface
     */
    private GameUserRepositoryInterface $game_user_repository;

    /**
     * Game Score repository container
     * @var GameScoreRepositoryInterface $game_score_repository
     */
    private GameScoreRepositoryInterface $game_score_repository;

    /**
     * GameUserRepository constructor.
     * @param GameUserRepositoryInterface $game_user_repository
     * @param GameScoreRepositoryInterface $game_score_repository
     */
    public function __construct(GameUserRepositoryInterface $game_user_repository, GameScoreRepositoryInterface $game_score_repository)
    {
        $this->game_user_repository = $game_user_repository;
        $this->game_score_repository = $game_score_repository;
    }

    /**
     * return login view
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function create(): view
    {
        return view('fullScreenMario-master.login');
    }

    /**
     * Prepare user for playing game
     * @param login $request
     * @return RedirectResponse
     */
    public function store(login $request): RedirectResponse
    {
        if (!$this->game_user_repository->doesGameUserExist($request->phone)) {

            if ($request->filled('inviter')) {
                $inviter = $this->game_user_repository->getGameUser($request->inviter);
            }

            $result = $this->game_user_repository->createGameUser([
                'phone_number' => $request->phone,
                'inviter_id' => $inviter->id ?? null
            ]);

            if (!$result) {
               return back()->with('error' , 'Something wrong in user registration');
            }
        }
        $request->session()->put('phone', $request->phone);

        return redirect()->route('game.mario.play');
    }

    /**
     * return super mario screen play
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function play(): view
    {
        return view('fullScreenMario-master.mario');
    }


    /**
     * @param Request $request
     * @return bool
     */
    public function storeData(Request $request): bool
    {
        $data = $request->only(['coin', 'score']);
        $user = $this->game_user_repository->getGameUser($request->session()->get('phone'));
        return $this->game_score_repository->createScore($user, $data);
    }

    /**
     * return all game users
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function users():View
    {
        $users = $this->game_user_repository->paginateGameUsers(15, true);
        return view('fullScreenMario-master.users.index', compact('users'));
    }

    /**
     * return all game users
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function scores(int $id):View
    {
        $scores = $this->game_score_repository->getUserScore($id);
        return view('fullScreenMario-master.scores.index', compact('scores'));
    }
}
