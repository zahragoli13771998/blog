<?php

namespace App\Http\Controllers\API3;

use App\Factories\CommentRepoFactory;
use App\Http\Requests\Api\Banner\GetBanner;
use App\Http\Requests\Api\Banner\BannerComments;
use App\Http\Requests\Api\Banner\ActiveBanners;
use App\Http\Resources\Comments;
use App\Models\Banner;
use App\Repositories\Interfaces\BannerRepositoryInterface;
use App\Repositories\Interfaces\BannerTypeRepositoryInterface;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Repositories\Interfaces\DislikeRepositoryInterface;
use App\Repositories\Interfaces\FavoriteRepositoryInterface;
use App\Repositories\Interfaces\LikeRepositoryInterface;
use App\Services\Interfaces\LikeDislikeServiceInterface;
use App\Services\Interfaces\ViewCounterServiceInterface;
use App\Services\UserAuthentication\UserAuthenticatorService;
use App\Services\Interfaces\CommentServiceInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Cache;

//Todo move like/dislikes operation to service layer
class BannerController
{
    /**
     * Banner repository container
     * @var BannerRepositoryInterface
     */
    private BannerRepositoryInterface $banner_repository;

    /**
     * Banner Type Repository container
     * @var BannerTypeRepositoryInterface
     */
    private BannerTypeRepositoryInterface $banner_type_repository;

    /**
     * Comment Repository container
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $comment_repository;

    /**
     * Like repository container
     * @var LikeRepositoryInterface
     */
    protected LikeRepositoryInterface $like_repository;

    /**
     * Dislike Repository container
     * @var DislikeRepositoryInterface
     */
    protected DislikeRepositoryInterface $dislike_repository;

    /**
     * User Authenticator Service container
     * @var UserAuthenticatorService
     */
    protected UserAuthenticatorService $user_authenticator_service;

    /**
     * ViewCountService Container
     * @var ViewCounterServiceInterface
     */
    private ViewCounterServiceInterface $view_counter_service;


    /**
     * Construct and inject necessary services to container
     * BannerController constructor.
     * @param BannerRepositoryInterface $bannerRepository
     * @param BannerTypeRepositoryInterface $banner_type_repository
     * @param ViewCounterServiceInterface $view_counter_service
     * @param LikeRepositoryInterface $like_repository
     * @param DislikeRepositoryInterface $dislike_repository
     * @param UserAuthenticatorService $user_authenticator_service
     * @param CommentServiceInterface $comment_service
     * @param FavoriteRepositoryInterface $favorite_repository
     * @param LikeDislikeServiceInterface $like_dislike_service
     */
    public function __construct(BannerRepositoryInterface $bannerRepository, BannerTypeRepositoryInterface $banner_type_repository, ViewCounterServiceInterface $view_counter_service, LikeRepositoryInterface $like_repository, DislikeRepositoryInterface $dislike_repository, UserAuthenticatorService $user_authenticator_service, CommentServiceInterface $comment_service, FavoriteRepositoryInterface $favorite_repository, LikeDislikeServiceInterface $like_dislike_service)
    {
        $this->banner_repository = $bannerRepository;
        $this->banner_type_repository = $banner_type_repository;
        $this->comment_repository = CommentRepoFactory::createRepository();
        $this->dislike_repository = $dislike_repository;
        $this->like_repository = $like_repository;
        $this->view_counter_service = $view_counter_service;
        $this->user_authenticator_service = $user_authenticator_service;
    }

    /**
     *  it returns all banners in json format
     * @param ActiveBanners $request
     * @return JsonResponse
     */
    public function showActiveBanners(ActiveBanners $request): JsonResponse
    {
        $type_id = $request->input('type_id');
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);
        $cache_key_list_banner = 'banner_type:' . $type_id;
        $cache_key_count_banners = 'banner_count';

        if (Cache::has($cache_key_list_banner)) {
            $banners = Cache::get($cache_key_list_banner);
            $banners_count = Cache::get($cache_key_count_banners);
        } else {
            $banners = $this->banner_repository->getAllBanners($type_id, $offset, $limit, true, true, true, null, true);
            $banners_count = $this->banner_repository->getBannersCount($type_id, true);
            Cache::put($cache_key_list_banner, $banners, 300);
            Cache::put($cache_key_count_banners, $banners_count, 300);
        }
        return response()->json([
            'status' => 0,
            'count' => $banners_count,
            'data' => $this->generateJsonResponse($banners)
        ]);
    }

//    public function showBannerWithType()
//    {
//        $types = $this->banner_type_repository->getAll()->pluck('id', 'title_fa')->toArray();
//        $data[] = array();
//        foreach ($types as $key => $val) {
//            $data[$key] = $this->banner_repository->getBannersWithType($val);
//        }
//        return response()->json([
//            'status' => 0,
//            'data' => $data
//        ]);
//    }

    /**
     * Get and return single banner
     * @param GetBanner $request
     * @return JsonResponse
     */
    public function getBanner(GetBanner $request): JsonResponse
    {
        $key = 'banner_detail' . $request->banner_id;
        if (Cache::has($key)) {
            $banner = Cache::get($key);
        } else {
            $banner = $this->banner_repository->getById($request->banner_id, true, true, $user_id ?? null);
            Cache::put($key, $banner, 300);
        }
        $this->view_counter_service->addViews($banner, $user_id ?? null);

        return response()->json([
            'status' => 0,
            'data' => $this->generateBannerJsonResponse($banner)
        ]);
    }

    /**
     * return banner's accepted comments
     * @param BannerComments $request
     * @return AnonymousResourceCollection
     */
    public function getComment(BannerComments $request): AnonymousResourceCollection
    {
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);
        $cache_key_list_banner_comments = 'banner_' . $request->banner_id . '_comments';
        if (Cache::has($cache_key_list_banner_comments)) {
            $comments = Cache::get($cache_key_list_banner_comments);
        } else {
            $banner = $this->banner_repository->getById($request->banner_id);
            $comments = $this->comment_repository
                ->getAllComment($banner, false, true, $offset, $limit, true, null, null);
            Cache::put($cache_key_list_banner_comments, $comments, 300);
        }
        $comments_count = $this->comment_repository->getCommentsCount($banner, true);

        return Comments::collection($comments)->additional([
            'count' => $comments_count,
            'status' => 0
        ]);
    }

    /**
     * Get and return banner types
     * @return JsonResponse
     */
    public function showBannerTypes(): JsonResponse
    {
        return response()->json([
            'status' => 0,
            'data' => $this->banner_type_repository->getAll()
        ]);
    }

    /**
     * Delete extra info from collection
     * @param Collection $collection
     * @return Collection
     */
    private function generateJsonResponse(Collection $collection): Collection
    {
        return $collection->map(function ($collection) {

            $collected = (object)$collection;

            $collected->makeHidden(['description']);
            $collected->comments_count = $collected->comments()->where('status', '=', 1)->count();
            return $collected;
        });
    }

    /**
     * Delete extra info from collection
     * @param Banner $banner
     * @return Banner
     */
    private function generateBannerJsonResponse(Banner $banner): Banner
    {
        $banner->comments_count = $banner->comments()->where('status', '=', 1)->count();
        return $banner;
    }

    /**
     * Generate feedback success/unsuccess response
     * @param bool $is_success
     * @param int $like_count
     * @param int $dislike_count
     * @return JsonResponse
     */
    private function generateFeedbackResponse(bool $is_success = false, int $like_count = 0, int $dislike_count = 0): JsonResponse
    {
        if ($is_success) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'You\'re like submitted successfully',
                    'fa' => 'فیدبک شما با موفقیت ثبت گردید',
                ],
                'data' => [
                    'likes_count' => $like_count,
                    'dislikes_count' => $dislike_count
                ]
            ]);
        }

        return response()->json([
            'status' => 1,
            'description' => [
                'en' => 'There was a problem in submitting your feedback',
                'fa' => 'مشکلی در ثبت فیدبک شما بوجود آمده است',
            ]
        ]);
    }

}
