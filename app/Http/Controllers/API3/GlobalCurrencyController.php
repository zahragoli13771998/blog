<?php

namespace App\Http\Controllers\API3;

use App\Factories\CommentRepoFactory;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\GlobalCurrency\AddComment;
use App\Http\Requests\Api\GlobalCurrency\GetComment;
use App\Http\Requests\Api\GlobalCurrency\GetGlobalCurrency;
use App\Http\Requests\Api\GlobalCurrency\SearchGC;
use App\Http\Requests\Api\GlobalCurrency\UpdateComment;
use App\Http\Requests\Api\Sector\Get;
use App\Http\Requests\UserActionList\Like;
use App\Http\Resources\Comments;
use App\Http\Resources\GlobalCurrenciesList;
use App\Http\Resources\GlobalCurrencyListV2;
use App\Repositories\Interfaces\BannerRepositoryInterface;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Repositories\Interfaces\DislikeRepositoryInterface;
use App\Repositories\Interfaces\FavoriteRepositoryInterface;
use App\Repositories\Interfaces\GlobalCurrencyV2Interface;
use App\Repositories\Interfaces\IncludeToRazminexRepositoryInterface;
use App\Repositories\Interfaces\LikeRepositoryInterface;
use App\Services\Interfaces\UserAuthenticatorServiceInterface;
use App\Services\Interfaces\CommentServiceInterface;
use App\Services\Interfaces\LikeDislikeServiceInterface;
use App\Services\Interfaces\ViewCounterServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;


class GlobalCurrencyController extends Controller
{
    /**
     * Global Currency repository container
     * @var GlobalCurrencyV2Interface
     */
    private GlobalCurrencyV2Interface $currency;

    /**
     * User authenticator service container
     * @var UserAuthenticatorServiceInterface
     */
    protected UserAuthenticatorServiceInterface $user_authenticator_service;

    /**
     * Comment Repository container
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $comment_repository;

    /**
     * Comment service container
     * @var CommentServiceInterface
     */
    private CommentServiceInterface $comment_service;

    /**
     * Like repository container
     * @var LikeRepositoryInterface
     */
    protected LikeRepositoryInterface $like_repository;

    /**
     * Dislike Repository container
     * @var DislikeRepositoryInterface
     */
    protected DislikeRepositoryInterface $dislike_repository;

    /**
     * View counter service container
     * @var ViewCounterServiceInterface
     */
    protected ViewCounterServiceInterface $view_counter_service;

    /**
     * like service container
     * @var LikeDislikeServiceInterface
     */
    private LikeDislikeServiceInterface $like_dislike_service;

    /**
     * FavoriteRepository Container
     * @var FavoriteRepositoryInterface
     */
    private FavoriteRepositoryInterface $favorite_repository;

    /**
     * IncludeToRazminexRepository Container
     * @var IncludeToRazminexRepositoryInterface
     */
    private IncludeToRazminexRepositoryInterface $include_to_razminex_repository;

    /**
     * Construct and inject necessary services to their container
     * GlobalCurrencyController constructor.
     * @param GlobalCurrencyV2Interface $currency
     * @param UserAuthenticatorServiceInterface $user_authenticator_service
     * @param BannerRepositoryInterface $banner_repository
     * @param CommentServiceInterface $comment_service
     * @param LikeRepositoryInterface $like_repository
     * @param DislikeRepositoryInterface $dislike_repository
     * @param ViewCounterServiceInterface $view_counter_service
     * @param FavoriteRepositoryInterface $favorite_repository
     * @param IncludeToRazminexRepositoryInterface $include_to_razminex_repository
     * @param LikeDislikeServiceInterface $like_dislike_service
     */
    public function __construct(GlobalCurrencyV2Interface $currency, UserAuthenticatorServiceInterface $user_authenticator_service, BannerRepositoryInterface $banner_repository, CommentServiceInterface $comment_service, LikeRepositoryInterface $like_repository, DislikeRepositoryInterface $dislike_repository, ViewCounterServiceInterface $view_counter_service, FavoriteRepositoryInterface $favorite_repository, IncludeToRazminexRepositoryInterface $include_to_razminex_repository, LikeDislikeServiceInterface $like_dislike_service)
    {
        $this->currency = $currency;
        $this->user_authenticator_service = $user_authenticator_service;
        $this->comment_repository = CommentRepoFactory::createRepository();
        $this->comment_service = $comment_service;
        $this->dislike_repository = $dislike_repository;
        $this->like_repository = $like_repository;
        $this->view_counter_service = $view_counter_service;
        $this->favorite_repository = $favorite_repository;
        $this->include_to_razminex_repository = $include_to_razminex_repository;
        $this->like_dislike_service = $like_dislike_service;
    }

    /**
     * Get and return List of global currencies
     * @param SearchGC $request
     * @return AnonymousResourceCollection
     */
    public function index(SearchGC $request): AnonymousResourceCollection
    {
        $from = $request->input('offset', 0);
        $take = $request->input('limit', 100);
        $sort = $request->input('sort_by');
        $search['ramzinex_currencies'] = $request->input('ramzinex_currencies');
        $count_key = 'v2_global_list_count';
        $key = 'v2_global-currencies_list_limit' . $take . 'from' . $from;
        if (Cache::has($key)) {
            $global_currencies = Cache::get($key);
            $global_currencies_count = Cache::get($count_key);
        } else {
            $global_currencies = $this->currency
                ->getAllGlobalCurrencies(true, true, $from, $take, false, $sort, $search, true);
            $global_currencies_count = $this->currency->globalCurrenciesCount();
            Cache::put($key, $global_currencies, 600);
            Cache::put($count_key, $global_currencies_count, 86400);
        }
        return GlobalCurrencyListV2::collection($global_currencies)->additional([
            'status' => 0,
            'count' => $global_currencies_count
        ]);
    }

    public function getGlobalLikedList(Like $request)
    {
        $type = $request->type;
        $user_id = $this->user_authenticator_service->getUserId($request);
        $global_likes = 'user_' . $user_id . '_liked_globals_list';
        if (Cache::has($global_likes)) {
            $users_likes_list = Cache::get($global_likes);
        } else {
            $users_likes_list = $this->like_repository->getUserLikesList($user_id, $type);
            Cache::put($global_likes, $users_likes_list, 600);

        }
        return $users_likes_list;
    }

    public function GetGlobalDislikeList(Like $request)
    {
        $type = $request->type;
        $user_id = $this->user_authenticator_service->getUserId($request);
        $global_dislikes = 'user_' . $user_id . '_disliked_globals_list';
        if (Cache::has($global_dislikes)) {
            $users_likes_list = Cache::get($global_dislikes);
        } else {
            $users_likes_list = $this->dislike_repository->getUserDisLikesList($user_id, $type);
            Cache::put($global_dislikes, $users_likes_list, 600);
        }
        return $users_likes_list;
    }

    public function getGlobalFavoredList(Like $request)
    {
        $type = $request->type;
        $user_id = $this->user_authenticator_service->getUserId($request);
        $favored_globals = 'user_' . $user_id . '_favored_globals_list';
        if (Cache::has($favored_globals)) {
            $users_favorites_list = Cache::get($favored_globals);
        } else {
            $users_favorites_list = $this->favorite_repository->getUserFavoredGlobalList($user_id, $type);
            Cache::put($favored_globals, $users_favorites_list, 600);

        }
        return $users_favorites_list;
    }

    /**
     * Get and return specific Global Currency detail
     * @param Request $request
     * @param int $id
     * @return GlobalCurrencyListV2
     */
    public function show(Request $request, int $id): GlobalCurrencyListV2
    {
        $key = 'v2-global_detail_for_id:' . $id;
        if (Cache::has($key)) {
            $global_currency = Cache::get($key);
        } else {
            $global_currency = $this->currency->getById($id, true, true, null, true);
            Cache::put($key, $global_currency, 300);
        }
        if ($global_currency->is_hidden) {
            abort(404);
        }
//            $this->view_counter_service->addViews($global_currency, $user_id ?? null);
        return new GlobalCurrencyListV2($global_currency);
//        } catch (\Throwable $e) {
//            Cache::increment('count_all_glob_new_error_dsabd71');
//            throw $e;
//        }
    }


    /**
     * show global currency's related banners
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function showRelatedBanners(Request $request, int $id): JsonResponse
    {
        return response()->json([
            'status' => 0,
            'data' => [
                'banners' => $this->currency->getById($id, true, true, $user_id ?? null)
            ]
        ]);
    }

    /**
     * return banner's accepted comments
     * @param GetComment $request
     * @return AnonymousResourceCollection
     */
    public function getComments(GetComment $request): AnonymousResourceCollection
    {
        $gc_key = 'global_detail_for_id:' . $request->global_currency_id;
        $comments_key = 'global_comments_for:' . $request->global_currency_id;
        $gc_comment_count = 'global_' . $request->global_currency_id . 'comments_count';
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);

//        if ($request->hasHeader('Authorization')) {
//            $user_id = $this->user_authenticator_service->getUserId($request);
//        }
        if (Cache::has($comments_key) && Cache::has($gc_comment_count)) {
            $global_currency = Cache::get($gc_key);
            $comments = Cache::get($comments_key);
            $comments_count = Cache::get($gc_comment_count);
        } else {
            $global_currency = $this->currency->getById($request->global_currency_id);
            $comments = $this->comment_repository
                ->getAllComment($global_currency, false, true, $offset, $limit, true, null, $user_id ?? null);
            $comments_count = $this->comment_repository->getCommentsCount($global_currency, true, null);
            Cache::put($gc_comment_count, $comments_count, 60);
            Cache::put($comments_key, $comments, 60);
        }
        return Comments::collection($comments)->additional([
            'status' => 0,
            'count' => $comments_count
        ]);
    }

    /**
     * add a currency to favorites
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public function addToFavorite(GetGlobalCurrency $request): JsonResponse
    {
        $global_currency_id = $request->global_currency_id;
        $global_currency = $this->currency->getById($global_currency_id);
        $user_id = $this->user_authenticator_service->getUserId($request);
        if ($this->favorite_repository->isAddedToFavorites($global_currency, $user_id)) {
            $this->favorite_repository->deleteFavorite($global_currency, $user_id);
        } else {
            $this->favorite_repository->createFavorite($global_currency, $user_id);
        }
        return response()->json([
            'status' => 0,
            'data' => [
                'favorites_count' => $this->favorite_repository->getFavoriteCount($global_currency)
            ]
        ]);
    }

    /**
     * check if global_currency added to favorite or not
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public function isAddedToFavorites(GetGlobalCurrency $request): JsonResponse
    {
        $global_currency_id = $request->global_currency_id;
        $global_currency = $this->currency->getById($global_currency_id);
        $user_id = $this->user_authenticator_service->getUserId($request);
        return response()->json([
            'status' => 0,
            'data' => [
                'is_favored' => $this->favorite_repository->isAddedToFavorites($global_currency, $user_id)
            ]
        ]);
    }

    /**
     * get user's feedback for each global currency
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public function like(GetGlobalCurrency $request): JsonResponse
    {
        $global_currency_id = $request->global_currency_id;
        $user_id = $this->user_authenticator_service->getUserId($request);
        $global_currency = $this->currency->getById($global_currency_id);
        $result = $this->like_dislike_service->createLike($global_currency, $request, 'global_likes');
        if (!$result) {
            return $this->generateFeedbackResponse();
        }
        $new_like_count = $this->like_repository->getLikeCount($global_currency);
        $new_dislike_count = $this->dislike_repository->getDisLikeCount($global_currency);
        return $this->generateFeedbackResponse(true, $new_like_count, $new_dislike_count);
    }

    /**
     * Check if user liked banner or not
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public function isLiked(GetGlobalCurrency $request): JsonResponse
    {
        $global_currency_id = $request->global_currency_id;
        $user_id = $this->user_authenticator_service->getUserId($request);
        $key = 'global_' . $global_currency_id . 'liked_by' . $user_id;
        $global_currency = $this->currency->getById($global_currency_id);
        return response()->json([
            'status' => 0,
            'data' => [
                'is_liked' => $this->like_repository->isLiked($global_currency, $user_id)
            ]
        ]);
    }

    /**
     * get user's feedback for each question
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public function disLike(GetGlobalCurrency $request): JsonResponse
    {
        $global_currency_id = $request->global_currency_id;
        $global_currency = $this->currency->getById($global_currency_id);
        $result = $this->like_dislike_service->createDislike($global_currency, $request, 'global_dislikes');
        if (!$result) {
            return $this->generateFeedbackResponse();
        }

        $new_dislike_count = $this->dislike_repository->getDisLikeCount($global_currency);
        $new_like_count = $this->like_repository->getLikeCount($global_currency);
        return $this->generateFeedbackResponse(true, $new_like_count, $new_dislike_count);
    }

    /**
     * Check if user disliked banner or not
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public function isDisliked(GetGlobalCurrency $request): JsonResponse
    {
        $global_currency_id = $request->global_currency_id;
        $global_currency = $this->currency->getById($global_currency_id);
        $user_id = $this->user_authenticator_service->getUserId($request);

        return response()->json([
            'status' => 0,
            'data' => [
                'is_disliked' => $this->dislike_repository->isDisliked($global_currency, $user_id)
            ]
        ]);
    }

    /**
     * add selected global currency to includeToRamzinex 's list
     * @param GetGlobalCurrency $request
     * @return JsonResponse
     */
    public function includeToRamzinex(GetGlobalCurrency $request): JsonResponse
    {
        $global_currency_id = $request->global_currency_id;
        $global_currency = $this->currency->getById($global_currency_id);
        $user_id = $this->user_authenticator_service->getUserId($request);
        $is_included = $this->include_to_razminex_repository->checkIfIncludeToRamzinexExists($global_currency, $user_id);

        if ($is_included) {
            $result = $this->include_to_razminex_repository->deleteIncludeToRamzinex($global_currency);
        } else {
            $result = $this->include_to_razminex_repository->createIncludeToRamzinex($global_currency, $user_id);
        }

        if (!$result) {
            return $this->generateIncludeToRamzinexResponce();
        }

        $included_count = $this->include_to_razminex_repository->getIncludeToRamzinexCount($global_currency);
        return $this->generateIncludeToRamzinexResponce(true, $included_count);
    }


    /**
     * store created comment
     * @param AddComment $request
     * @return JsonResponse
     */
    public function addComment(AddComment $request): JsonResponse
    {
        $global_currency = $this->currency->getById($request->global_currency_id);
        $comment = $this->comment_service->createComment($global_currency, $request);
        $user_id = $this->user_authenticator_service->getUserId($request);
        try {
            $testKey = 'global_comment_' . $request->global_currency_id;
            $r = Redis::connection('like_dislike');
            $r->sadd($testKey, $user_id);
        } catch (\Exception $e) {
            Cache::increment('count_all_comment_glob_error_dadbd71:' . $request->global_currency_id);
        }
        if (!$comment) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on creating comment',
                    'fa' => 'ثبت کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'description' => [
                'en' => 'Comment created successfully',
                'fa' => 'کامنت شما با موفقیت ثبت شد'
            ]
        ]);
    }

    /**
     * update an existing comment
     * @param UpdateComment $request
     * @return JsonResponse
     */
    public function updateComment(UpdateComment $request): JsonResponse
    {
        $result = $this->comment_service->updateComment($request);

        if (!$result) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on Editing comment',
                    'fa' => 'ویرایش کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'description' => [
                'en' => 'Comment edited successfully',
                'fa' => 'ویرایش کامنت با موفقیت انجام شد'
            ]
        ]);
    }

    /**
     * it deletes feedbacks(if exist) then delete comments
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function deleteComment(Request $request, int $id): JsonResponse
    {
        $result = $this->comment_service->deleteComment($request, $id);

        if (!$result) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'Error on deleting comment',
                    'fa' => 'حذف کامنت با مشکل مواجه شد'
                ]
            ]);
        }

        return response()->json([
            'status' => 0,
            'description' => [
                'en' => 'Comment delete successfully',
                'fa' => 'کامنت شما با موفقیت حذف شد'
            ]
        ]);
    }


    /**
     * Generate favorite response
     * @param bool $is_success
     * @param int $favorite_count
     * @return JsonResponse
     */
    private function generateFavoriteResponce(bool $is_success = false, int $favorite_count = 0)
    {
        if ($is_success) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'There was a problem in adding banner to favorites',
                    'fa' => 'مشکلی در افزودن banner به لیست منتخبین رخ داده است',
                ],
                'data' => [
                    'favorite_counter' => $favorite_count
                ]
            ]);
        }
        return response()->json([
            'status' => 1,
            'description' => [
                'en' => 'There was a problem in adding banner to favorites',
                'fa' => 'مشکلی در افزودن banner به لیست منتخبین رخ داده است',
            ]
        ]);
    }

    /**
     * Generate feedback success/unsuccess response
     * @param bool $is_success
     * @param int $like_count
     * @param int $dislike_count
     * @return JsonResponse
     */
    private function generateFeedbackResponse(bool $is_success = false, int $like_count = 0, int $dislike_count = 0): JsonResponse
    {
        if ($is_success) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'You\'re like submitted successfully',
                    'fa' => 'فیدبک شما با موفقیت ثبت گردید',
                ],
                'data' => [
                    'likes_count' => $like_count,
                    'dislikes_count' => $dislike_count
                ]
            ]);
        }

        return response()->json([
            'status' => 1,
            'description' => [
                'en' => 'There was a problem in submitting your feedback',
                'fa' => 'مشکلی در ثبت فیدبک شما بوجود آمده است',
            ]
        ]);
    }

    /**
     * Generate feedback success/unsuccess response
     * @param bool $is_success
     * @param int $includes_count
     * @return JsonResponse
     */
    private function generateIncludeToRamzinexResponce(bool $is_success = false, int $includes_count = 0): JsonResponse
    {
        if ($is_success) {
            return response()->json([
                'status' => 0,
                'description' => [
                    'en' => 'You\'re request submitted successfully',
                    'fa' => 'درخواست شما با موفقیت ثبت گردید',
                ],
                'data' => [
                    'includes_count' => $includes_count

                ]
            ]);
        }

        return response()->json([
            'status' => 1,
            'description' => [
                'en' => 'There was a problem in submitting your request',
                'fa' => 'مشکلی در ثبت درخواست شما بوجود آمده است',
            ]
        ]);
    }

    public function checkShaba(Request $request)
    {
        $address = '360560611828005140238001';
        $address3 = substr($address, 0, 2);
        $address2 = substr($address, 2);
        $new_address = $address2 . '1827' . $address3;
        $mod = $this->my_bcmod($new_address, 97);
        if ($mod != 1) {
            return 'fty-rtyturtutyuyrta3ls';
        }
        return 'trt*ryryutyutyutyut3ue';
    }

    function my_bcmod($x, $y)
    {
        $take = 5;
        $mod = '';
        do {
            $a = (int)$mod . substr($x, 0, $take);
            $x = substr($x, $take);
            $mod = fmod($a,$y);
        } while (strlen($x));

        return (int)$mod;
    }
}

