<?php

namespace App\Http\Controllers\API3;

use App\Factories\CommentRepoFactory;
use App\Http\Requests\UserActionList\Like;
use App\Models\Comment;
use App\Repositories\Interfaces\BannerRepositoryInterface;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Repositories\Interfaces\DislikeRepositoryInterface;
use App\Repositories\Interfaces\FavoriteRepositoryInterface;
use App\Repositories\Interfaces\GlobalCurrencyV2Interface;
use App\Repositories\Interfaces\IncludeToRazminexRepositoryInterface;
use App\Repositories\Interfaces\LikeRepositoryInterface;
use App\Services\Interfaces\CommentServiceInterface;
use App\Services\Interfaces\LikeDislikeServiceInterface;
use App\Services\Interfaces\UserAuthenticatorServiceInterface;
use App\Services\Interfaces\ViewCounterServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class UserListsController
{
    /**
     * Global Currency repository container
     * @var GlobalCurrencyV2Interface
     */
    private GlobalCurrencyV2Interface $currency;

    /**
     * User authenticator service container
     * @var UserAuthenticatorServiceInterface
     */
    protected UserAuthenticatorServiceInterface $user_authenticator_service;

    /**
     * Comment Repository container
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $comment_repository;

    /**
     * Comment service container
     * @var CommentServiceInterface
     */
    private CommentServiceInterface $comment_service;

    /**
     * Like repository container
     * @var LikeRepositoryInterface
     */
    protected LikeRepositoryInterface $like_repository;

    /**
     * Dislike Repository container
     * @var DislikeRepositoryInterface
     */
    protected DislikeRepositoryInterface $dislike_repository;

    /**
     * View counter service container
     * @var ViewCounterServiceInterface
     */
    protected ViewCounterServiceInterface $view_counter_service;

    /**
     * like service container
     * @var LikeDislikeServiceInterface
     */
    private LikeDislikeServiceInterface $like_dislike_service;

    /**
     * FavoriteRepository Container
     * @var FavoriteRepositoryInterface
     */
    private FavoriteRepositoryInterface $favorite_repository;

    /**
     * IncludeToRazminexRepository Container
     * @var IncludeToRazminexRepositoryInterface
     */
    private IncludeToRazminexRepositoryInterface $include_to_razminex_repository;

    /**
     * Construct and inject necessary services to their container
     * GlobalCurrencyController constructor.
     * @param GlobalCurrencyV2Interface $currency
     * @param UserAuthenticatorServiceInterface $user_authenticator_service
     * @param BannerRepositoryInterface $banner_repository
     * @param CommentServiceInterface $comment_service
     * @param LikeRepositoryInterface $like_repository
     * @param DislikeRepositoryInterface $dislike_repository
     * @param ViewCounterServiceInterface $view_counter_service
     * @param FavoriteRepositoryInterface $favorite_repository
     * @param IncludeToRazminexRepositoryInterface $include_to_razminex_repository
     * @param LikeDislikeServiceInterface $like_dislike_service
     */
    public function __construct(GlobalCurrencyV2Interface $currency, UserAuthenticatorServiceInterface $user_authenticator_service, BannerRepositoryInterface $banner_repository, CommentServiceInterface $comment_service, LikeRepositoryInterface $like_repository, DislikeRepositoryInterface $dislike_repository, ViewCounterServiceInterface $view_counter_service, FavoriteRepositoryInterface $favorite_repository, IncludeToRazminexRepositoryInterface $include_to_razminex_repository, LikeDislikeServiceInterface $like_dislike_service)
    {
        $this->currency = $currency;
        $this->user_authenticator_service = $user_authenticator_service;
        $this->comment_repository = CommentRepoFactory::createRepository();
        $this->comment_service = $comment_service;
        $this->dislike_repository = $dislike_repository;
        $this->like_repository = $like_repository;
        $this->view_counter_service = $view_counter_service;
        $this->favorite_repository = $favorite_repository;
        $this->include_to_razminex_repository = $include_to_razminex_repository;
        $this->like_dislike_service = $like_dislike_service;
    }

    /**
     * @param Like $request
     * @return JsonResponse
     */
    public function getUsersLikesList(Like $request): JsonResponse
    {
        $user_id = $this->user_authenticator_service->getUserId($request);
        $likes_list = 'user_' . $user_id . '_liked_' . $request->type . '_list';
        $likes_list_long = 'long_time_user_' . $user_id . '_liked_' . $request->type . '_list';
        $likes_list_lock = 'lock_time:user_' . $user_id . '_liked_' . $request->type . '_list';
        srand(time());
        $r = rand(1, 100);
        $mod = $r % 10;
        if (Cache::has($likes_list)) {
            $users_likes_list = Cache::get($likes_list);
        } else {
            if (Cache::has($likes_list_lock)) {
                if (Cache::has($likes_list_long)) {
                    $users_likes_list = Cache::get($likes_list_long);
                    return response()->json([
                        'status' => 0,
                        'data' =>
                            $users_likes_list->toArray()

                    ]);
                }
            }
            Cache::put($likes_list_lock, 1, 86400);
            if ($mod != 0 && Cache::has($likes_list_long)) {
                $users_likes_list = Cache::get($likes_list_long);
            } else {
                $users_likes_list = $this->like_repository->getUserLikesList($user_id, $request->type);
                Cache::put($likes_list, $users_likes_list, 86400);
                Cache::put($likes_list_long, $users_likes_list, 86400000000);
            }
        }
        return response()->json([
            'status' => 0,
            'data' =>
                $users_likes_list->toArray()

        ]);
    }

    /**
     * @param Like $request
     * @return JsonResponse
     */
    public function getUsersDisLikesList(Like $request): JsonResponse
    {
        $user_id = $this->user_authenticator_service->getUserId($request);
        $dislikes_list = 'user_' . $user_id . '_disliked_' . $request->type . '_list';
        $dislikes_list_long = 'long_time:user_' . $user_id . '_disliked_' . $request->type . '_list';
        $dislikes_list_lock = 'lock_time:user_' . $user_id . '_disliked_' . $request->type . '_list';
        srand(time());
        $r = rand(1, 100);
        $mod = $r % 10;
        if (Cache::has($dislikes_list)) {
            $users_dislikes_list = Cache::get($dislikes_list);
        } else {
            if (Cache::has($dislikes_list_lock)) {
                if (Cache::has($dislikes_list_long)) {
                    $users_dislikes_list = Cache::get($dislikes_list_long);
                    return response()->json([
                        'status' => 0,
                        'data' => $users_dislikes_list->toArray()

                    ]);
                }
            }
            Cache::put($dislikes_list_lock, 1, 86400);
            if ($mod != 0 && Cache::has($dislikes_list_long)) {
                $users_dislikes_list = Cache::get($dislikes_list_long);
            } else {
                $users_dislikes_list = $this->dislike_repository->getUserDisLikesList($user_id, $request->type);
                Cache::put($dislikes_list, $users_dislikes_list, 86400);
                Cache::put($dislikes_list_long, $users_dislikes_list, 86400000);
            }

        }
        return response()->json([
            'status' => 0,
            'data' => $users_dislikes_list->toArray()

        ]);
    }

    /**
     * @param Like $request
     * @return JsonResponse
     */
    public function getUsersFavoritesList(Like $request): JsonResponse
    {
        $user_id = $this->user_authenticator_service->getUserId($request);
        $favored_list = 'user_' . $user_id . '_favored_' . $request->type . '_list';
        $favored_list_long = 'long_time_cache_user_' . $user_id . '_favored_' . $request->type . '_list';
        $favored_list_lock = 'lock_time_user_' . $user_id . '_favored_' . $request->type . '_list';
        srand(time());
        $r = rand(1, 100);
        $mod = $r % 10;
        if (Cache::has($favored_list)) {
            $users_favorites_list = Cache::get($favored_list);
        } else {
            if (Cache::has($favored_list_lock)) {
                if (Cache::has($favored_list_long)) {
                    $users_favorites_list = Cache::get($favored_list_long);
                    return response()->json([
                        'status' => 0,
                        'data' => $users_favorites_list->toArray()

                    ]);
                }
            }
            Cache::put($favored_list_lock, 1, 86400);
            if ($mod != 0 && Cache::has($favored_list_long)) {
                $users_favorites_list = Cache::get($favored_list_long);
            } else {
                $users_favorites_list = $this->favorite_repository->getUserFavoredGlobalList($user_id, $request->type);
                Cache::put($favored_list, $users_favorites_list, 86400);
                Cache::put($favored_list_long, $users_favorites_list, 864000000);
            }
        }
        return response()->json([
            'status' => 0,
            'data' => $users_favorites_list->toArray()

        ]);
    }

    public function getUserAnsweredPool(Request $request): JsonResponse
    {

        $user_id = $this->user_authenticator_service->getUserId($request);
        $answered_pools_list_short = 'short_user_' . $user_id . 'answered_pools_list';
        $answered_pools_list_long = 'user_' . $user_id . 'answered_pool_list';
        if (Cache::has($answered_pools_list_short)) {
            $user_answered_pool_list = Cache::get($answered_pools_list_short);
        } else {
            $user_answered_pool_list =
                Comment::query()->where('type', '=', 'survey_answer')->where('user_id', '=', $user_id)->where('status','=',1)->get();
            Cache::put($answered_pools_list_short, $user_answered_pool_list, 600);
        }
        return response()->json([
            'status' => 0,
            'data' => $user_answered_pool_list->toArray()

        ]);
    }
}
