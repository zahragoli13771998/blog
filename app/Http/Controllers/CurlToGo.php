<?php

namespace App\Http\Controllers;

use App\Logic\CurlHelper;
use App\Services\Curl\GuzzleCurl;

class CurlToGo
{

    public function test(){
        $url = "http://localhost:8090/add-event";
        $test = CurlHelper::getInstance()->postHttpsResponse($url,json_encode(['data'=>[
            'price'=>100,
            'amount'=>0.2,
            'time'=>16534658,
            'type'=>"tt",
            'pairId'=>89,
            'hash'=>'wkeur*645lrkj$ertweg'
        ]]));
    return $test;
    }


}
