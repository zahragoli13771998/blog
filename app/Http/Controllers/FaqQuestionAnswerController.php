<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionAnswers\Create;
use App\Http\Requests\QuestionAnswers\CreateDeeplink;
use App\Http\Requests\QuestionAnswers\Search;
use App\Http\Requests\QuestionAnswers\Update;
use App\Repositories\Interfaces\DeepLinkRepositoryInterface;
use App\Repositories\Interfaces\FaqCategoryRepositoryInterface;
use App\Repositories\Interfaces\FaqQuestionAnswerRepositoryInterface;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class FaqQuestionAnswerController extends Controller
{
    /**
     * questionAnswer repository container
     * @var FaqQuestionAnswerRepositoryInterface
     */
    private FaqQuestionAnswerRepositoryInterface $question_answer_repository;


    /**
     * FaqCategoryService repository container
     * @var FaqCategoryRepositoryInterface
     */
    private FaqCategoryRepositoryInterface $faq_category_repository;

    /**
     * Deeplink repository container
     * @var DeepLinkRepositoryInterface
     */
    private DeepLinkRepositoryInterface $deeplink_repository;

    /**
     * FaqQuestionAnswerController constructor.
     * @param FaqQuestionAnswerRepositoryInterface $questionAnswer_repository
     * @param FaqCategoryRepositoryInterface $faq_category_repository
     * @param DeepLinkRepositoryInterface $deeplink_repository
     */
    public function __construct(FaqQuestionAnswerRepositoryInterface $questionAnswer_repository, FaqCategoryRepositoryInterface $faq_category_repository, DeepLinkRepositoryInterface $deeplink_repository)
    {
        $this->question_answer_repository = $questionAnswer_repository;
        $this->faq_category_repository = $faq_category_repository;
        $this->deeplink_repository = $deeplink_repository;
    }

    /**
     * show questions
     * @param Search $request
     * @return Factory|Application|View
     */
    public function showQuestions(Search $request): view
    {
        $data = $request->only(['question_title', 'faq_category_id']);
        $categories = $this->faq_category_repository->getAllCategories();
        $question_answers = $this->question_answer_repository->paginateQuestionAnswer($data, 10, true);
        return view('question_answer.show', compact('categories', 'question_answers', 'request'));
    }


    /**
     * show create form
     * @return Factory|Application|View
     */
    public function create(): view
    {
        $categories = $this->faq_category_repository->getAllCategories();
        return view('question_answer.create', compact('categories'));
    }

    /**
     * create a questionAnswer
     * @param Create $request
     * @return RedirectResponse
     */
    public function add(Create $request): RedirectResponse
    {
        $data = $request->only(['image', 'video', 'external_video', 'faq_category_id', 'show_order', 'is_repetitive', 'question_fa', 'question_en', 'short_answer_fa', 'short_answer_en', 'complete_answer_fa', 'complete_answer_en']);

        $faq_category = $this->faq_category_repository->getById($data['faq_category_id']);

        if ($request->has('image')) {
            try {
                $data['image'] = $request->file('image')->store('faq_question_answer/image', 'public');
            } catch (QueryException | Exception $ex) {
                Log::error($ex->getMessage());
            }
        }

        if ($request->has('video')) {
            try {
                $data['video'] = $request->file('video')->store('faq_question_answer/video', 'public');
            } catch (QueryException | Exception $ex) {
                Log::error($ex->getMessage());
            }
        }

        $created = $this->question_answer_repository->addQuestionAnswer($data, $faq_category);

        if (!$created) {
            return redirect()->back()->with('error', 'ایجاد پرسش با مشکل مواجه شد');
        }
        return redirect()->route('marketing.faq.question_answer.show_questions')->with('success', 'پرسش با موفقیت ایجاد شد');
    }


    /**
     * show edit form
     * @param int $id
     * @return Factory|Application|View
     */
    public function edit(int $id): view
    {
        $question_answer = $this->question_answer_repository->getById($id);
        $categories = $this->faq_category_repository->getAllCategories();

        return view('question_answer.edit', compact('question_answer', 'categories'));
    }


    /**
     * update an existing questionAnswer
     * @param int $id
     * @param Update $request
     * @return RedirectResponse
     */
    public function update(int $id, Update $request): RedirectResponse
    {
        $question_answer = $this->question_answer_repository->getById($id);

        $data = $request->only(['faq_category_id', 'show_order', 'is_repetitive', 'question_fa', 'question_en', 'short_answer_fa', 'short_answer_en', 'complete_answer_fa', 'complete_answer_en', 'image', 'video', 'external_video']);

        if ($request->has('image')) {
            try {
                Storage::disk('public')->delete($question_answer->image);
                $data['image'] = $request->file('image')->store('faq_question_answer/image', 'public');
            } catch (QueryException | Exception $ex) {
                Log::error($ex->getMessage());
            }
        }

        if ($request->has('video')) {
            try {
                Storage::disk('public')->delete($question_answer->video);
                $data['video'] = $request->file('video')->store('faq_question_answer/video', 'public');
            } catch (QueryException | Exception $ex) {
                Log::error($ex->getMessage());
            }
        }

        $updated = $this->question_answer_repository
            ->updateQuestionAnswer($data, $question_answer);

        if (!$updated) {
            return redirect()->back()->with('error', 'ویرایش پرسش با مشکل موجه شد');
        }

        return redirect()->route('marketing.faq.question_answer.show_questions')->with('success', 'ویرایش پرسش با موفقیت انجام شد');

    }


    /**
     * delete  an existing questionAnswer by deleting it's feedback's.
     * @param int $id
     * @return RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        //Todo move all response messages to language files

        $question_answer = $this->question_answer_repository->getById($id);

        try {
            Storage::disk('public')->delete($question_answer->image);
            Storage::disk('public')->delete($question_answer->video);
        } catch (QueryException | Exception $ex) {
            Log::error($ex->getMessage());
        }

        $deleted = $this->question_answer_repository->deleteQuestionAnswer($question_answer);

        if (!$deleted) {
            return redirect()->back()->with('error', 'حذف پرسش با مشکل مواجه شد'); //Todo move all response messages to language files
        }
        return redirect()->back()->with('success', 'حذف پرسش با موفقی انجام شد');
    }

    /**
     * show selected question and create user's view
     * @param int $id
     * @param Request $request
     * @return Factory|Application|RedirectResponse|View
     */
    public function showSelectedQuestion(int $id, Request $request): view
    {
        $question_answer = $this->question_answer_repository->getById($id);
        $categories = $this->faq_category_repository->getAllCategories();
        return view('question_answer.show_question', compact('question_answer', 'categories'))->with('success', 'پرسش با موفقیت نمایش داده شد');
    }

    /**
     * return view for creating deeplink for a faq
     * @param int $id
     * @return View
     */
    public function createDeeplink(int $id): view
    {
        return view('question_answer.create_deeplink', compact('id'));
    }

    /**
     * check if a deeplink exists for a faq
     * if not creates one
     * if yes, update with new values
     * @param CreateDeeplink $request
     * @return RedirectResponse
     */
    public function storeDeeplink(CreateDeeplink $request): RedirectResponse
    {
        $question_answer = $this->question_answer_repository->getById($request->question_answer_id);
        $data = $request->only('link', 'title_fa', 'title_en', 'status');
        $deep_link = $this->deeplink_repository->getDeeplink($question_answer);
        if ($deep_link) {
            $result = $this->deeplink_repository->updateDeeplink($deep_link, $data);
            if ($result) {
                return redirect()->route('admin.faq.question_answer.show_questions')->with('success', 'لینک با موفقیت ایجاد شد');
            }

            return redirect()->route('admin.faq.question_answer.show_questions')->with('error', 'ایجاد لینک با مشکل مواجه شد');
        }

        $result = $this->deeplink_repository->createDeeplink($question_answer, $data);
        if ($result) {
            return redirect()->route('marketing.faq.question_answer.show_questions')->with('success', 'لینک با موفقیت ایجاد شد');
        }

        return redirect()->route('marketing.faq.question_answer.show_questions')->with('error', 'ایجاد لینک با مشکل مواجه شد');
    }

    /**
     * delete deeplink for a question answer
     * @param int $deeplink_id
     * @return RedirectResponse
     */
    public function deleteDeeplink(int $deeplink_id):RedirectResponse
    {
        $deeplink = $this->deeplink_repository->getSingleDeeplink($deeplink_id);
        $result = $this->deeplink_repository->deleteDeeplink($deeplink);
        if ($result) {
            return redirect()->route('marketing.faq.question_answer.show_questions')->with('success', 'لینک با موفقیت حذف شد');
        }

        return redirect()->route('marketing.faq.question_answer.show_questions')->with('error', 'حذف لینک با مشکل مواجه شد');
    }


}
