<?php


namespace App\Helpers;


class HelperMethods
{
    /**
     * Convert and return prices in proper format
     * @param float $n
     * @param int $precision
     * @return false|string
     */
    static function abbreviatePrices(float $n, int $precision = 3)
    {
        // first strip any formatting;
        $n = (0 + str_replace(",", "", $n));

        // is this a number?
        if (!is_numeric($n)) {
            return false;
        }

        switch ($n)
        {
            case $n > 1000000000000:
                return round(($n / 1000000000000), $precision) . ' T';
            case $n > 1000000000:
                return round(($n / 1000000000), $precision) . ' B';
            case  $n >1000000:
                return round(($n / 1000000), $precision) . ' M';
            case $n > 100:
                return round(($n / 1000), $precision) . ' K';
        }

        return number_format($n);
    }

}
