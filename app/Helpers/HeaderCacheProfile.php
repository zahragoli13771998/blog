<?php


namespace App\Helpers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\ResponseCache\CacheProfiles\BaseCacheProfile;
use Symfony\Component\HttpFoundation\Response;

class HeaderCacheProfile extends BaseCacheProfile
{
    /**
     * Checks to cache request or not
     * @param Request $request
     * @return bool
     */
    public function shouldCacheRequest(Request $request): bool
    {
        if ($this->isRunningInConsole()) {
            return false;
        }

        return $request->isMethod('get') || $request->isMethod('post') || $request->ajax();
    }

    public function shouldCacheResponse(Response $response): bool
    {
        return $response->isSuccessful() || $response->isRedirection();
    }

    public function useCacheNameSuffix(Request $request): string
    {
        $suffix='';

        if($request->hasHeader('Authorization')){
            $suffix = md5($request->header('Authorization'));
        }

        if(count($request->json()->all()))
        {
            $suffix .= 'json-xDaJ-'.json_encode($request->json()->all());
        }

        return $suffix;
    }
}
