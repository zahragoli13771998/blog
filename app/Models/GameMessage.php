<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Game Message
 * @package App\Models
 * @property string phone
 * @property string code
 * @property string status
 * @property string plain_data
 * @property string message_id
 * @property string status_text
 */
class GameMessage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = ['phone', 'code', 'status', 'plain_data', 'message_id', 'status_text'];

    /**
     * The database connection that should be used by the migration.
     * @var string
     */
    protected $table = 'game_messages';

}
