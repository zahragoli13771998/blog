<?php

namespace App\Models;


use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * model for job vacancies and defining relations with other models
 * Class JobVacancies
 * @package App\Models
 * @property integer id
 * @property string title_fa
 * @property string title_en
 * @property string description
 * @property integer job_type_id
 * @property boolean status
 * @property DateTime expire_time
 */
class JobVacancies extends Model
{
    /**
     *
     * @var string
     */
    public $table = 'job_vacancies';

    /**
     * for casting property in this model
     * @var string[]
     */
    protected $casts = [
        'id' => 'integer',
        'title_fa' => 'string',
        'title_en' => 'string',
        'description' => 'string',
        'job_type_id' => 'integer',
        'status' => 'boolean',
        'expire_time' => 'datetime',
    ];

    /**
     * for defining fields of related table that can be filled using mass-assignment
     * @var string[]
     */
    protected $fillable = [
        'title_fa',
        'title_en',
        'description',
        'job_type_id',
        'status',
        'expire_time',
    ];

    /**
     * defining relation between JobTypes model and JobVacancies model(one to many)
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jobtype(): BelongsTo
    {
        return $this->belongsTo(JobTypes::class, 'job_type_id', 'id');
    }


    /**
     * defining relation between JobVacancyRequest model and JobVacancies model(one to many)
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobRequest(): HasMany
    {
        return $this->hasMany(JobVacancyRequest::class, 'job_vacancy_id');
    }

}
