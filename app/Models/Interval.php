<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string title_en
 * @property string title_fa
 */
class Interval extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = ['title_fa','title_en'];

    /**
     * The database connection that should be used by the migration.
     *
     * @var string
     */
    protected $table = 'intervals';

    /**
     * Disable timestamps in model
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get all of the currency_change_percent for the interval.
     *
     * @return HasMany
     */
    public function currencyChange():HasMany
    {
        return $this->hasMany(CurrencyChangesPercent::class, 'interval_id', 'id');
    }

    /**
     * return daily interval id
     * @return int
     */
    public static function getDailyId(): int //Todo Make getting Id's more dynamic
    {
        return 1;
    }

    /**
     * return weekly interval id
     * @return int
     */
    public static function getWeeklyId(): int //Todo Make getting Id's more dynamic
    {
        return 2;
    }

    /**
     * return monthly interval id
     * @return int
     */
    public static function getMonthlyId(): int //Todo Make getting Id's more dynamic
    {
        return 3;
    }

    /**
     * return yearly interval id
     * @return int
     */
    public static function getYearlyId(): int //Todo Make getting Id's more dynamic
    {
        return 4;
    }
}
