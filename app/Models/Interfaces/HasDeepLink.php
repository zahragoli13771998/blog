<?php


namespace App\Models\Interfaces;

use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * Interface HasDeepLink
 * @package App\Models\Interfaces
 */
interface HasDeepLink
{
    /**
     * define polymorphic relation
     * @return MorphOne
     */
    public function deeplink():MorphOne;
}
