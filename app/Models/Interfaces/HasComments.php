<?php


namespace App\Models\Interfaces;

use Illuminate\Database\Eloquent\Relations\MorphMany;

interface HasComments
{
    /**
     *  define polymorphic relation
     * @return MorphMany
     */
    public function comments(): MorphMany;
}
