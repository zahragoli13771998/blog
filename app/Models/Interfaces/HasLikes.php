<?php


namespace App\Models\Interfaces;


use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

interface HasLikes
{
    /**
     * define polymorphic relation
     * @return MorphToMany
     */
    public function likes(): MorphToMany;

//    /**
//     * Checks if user liked banner or not
//     * @return bool
//     */
//    public function getIsLikedAttribute(): bool;
}
