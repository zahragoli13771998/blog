<?php


namespace App\Models\Interfaces;


use Illuminate\Database\Eloquent\Relations\MorphToMany;

interface HasDisLike
{
    /**
     * define polymorphic relation
     * @return MorphToMany
     */
    public function dislikes(): MorphToMany;

//    /**
//     * Checks if banner is disliked or not by user
//     * @return bool
//     */
//    public function getIsDislikedAttribute(): bool;
}
