<?php


namespace App\Models\Interfaces;
use Illuminate\Database\Eloquent\Relations\MorphMany;

interface HasViews
{
    /**
     * define polymorphic relation
     * @return MorphMany
     */
    public function views(): MorphMany;
}
