<?php


namespace App\Models\Interfaces;


use Illuminate\Database\Eloquent\Relations\MorphToMany;

interface HasTag
{
    /**
     * define polymorphic relation
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function tags(): MorphToMany;
}
