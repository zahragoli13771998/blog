<?php


namespace App\Models\Interfaces;

use Illuminate\Database\Eloquent\Relations\MorphToMany;

interface HasIncludes
{
    /**
     * define polymorphic relation with IncludeToRazminex
     * @return MorphToMany
     */
    public function includes(): MorphToMany;
}
