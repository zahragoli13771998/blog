<?php


namespace App\Models\Interfaces;

use Illuminate\Database\Eloquent\Relations\MorphToMany;

interface HasFavorite
{
    /**
     * define polymorphic relation with favorites table
     * @return MorphToMany
     */
    public function favorites(): MorphToMany;

//    /**
//     * Check if banner is favored by user or not
//     * @return bool
//     */
//    public function getIsFavoredAttribute(): bool;
}
