<?php

namespace App\Models;

use App\Helpers\HelperMethods;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class CurrencyOHLC
 * @package App\Models
 * @property int global_currency_id
 * @property int interval_id
 * @property double open
 * @property double high
 * @property double low
 * @property double close
 * @property int volume
 * @property int market_cap
 */
class CurrencyOHLC extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = ['global_currency_id', 'interval_id', 'open', 'high', 'low', 'close', 'volume', 'market_cap', 'created_at', 'updated_at'];

    /**
     * The database connection that should be used by the migration.
     *
     * @var string
     */
    protected $table = 'currencies_ohlc';

    /**
     * Define automatic appending values to model
     * @var array
     */
    protected $appends = [
        'title_fa',
        'title_en'
    ];

    /**
     * Make some model's elements hidden
     * @var string[]
     */
    protected $hidden = [
        'created_at', 'updated_at', 'global_currency_id', 'interval_id'
    ];

    /**
     * Define auto eager loadings for model
     * @var string[]
     */
    protected $with = ['interval:id,title_fa,title_en', 'currency:id,price_precision,name_fa'];

    /**
     * Define models attributes types for dynamical type casting
     * @var string[]
     */
    protected $casts = [
        'open' => 'double',
        'high' => 'double',
        'close' => 'double',
        'low' => 'double',
        'market_cap' => 'int'
    ];

    /**
     * Get the currency that owns the currency_change_percent.
     *
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(GlobalCurrency::class,'global_currency_id','id');
    }

    /**
     * Get the interval that owns the currency_change_percent.
     *
     * @return BelongsTo
     */
    public function interval(): BelongsTo
    {
        return $this->belongsTo(Interval::class, 'interval_id', 'id');
    }

    /**
     * Get and return attributes fa title
     * @return mixed
     */
    public function getTitleFaAttribute()
    {
        if(!$this->relationLoaded('interval')){
            $this->load(['interval']);
        }
        return $this->interval->title_fa;
    }

    /**
     * Get and return attributes en title
     * @return mixed
     */
    public function getTitleEnAttribute()
    {
        if(!$this->relationLoaded('interval')){
            $this->load(['interval']);
        }

        $title = $this->interval->title_en;

        if($this->interval){
            unset($this->interval);
        }

        return $title;
    }
}
