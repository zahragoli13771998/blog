<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class GameUser
 * @property string inviter_id
 * @property string phone_number
 */
class GameUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = ['inviter_id','phone_number'];

    /**
     * The database connection that should be used by the migration.
     * @var string
     */
    protected $table = 'game_users';

    /**
     * Get all of the user scores.
     * @return HasMany
     */
    public function scores():HasMany
    {
        return $this->hasMany(GameScore::class, 'user_id', 'id');
    }
    /**
     * relation with game user.
     * @return HasMany
     */
    public function invited():HasMany
    {
        return $this->hasMany(GameUser::class, 'inviter_id', 'id');
    }
}
