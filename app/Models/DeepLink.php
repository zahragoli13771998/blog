<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class DeepLink
 * @property string link
 * @property int deeplinkable_id
 * @property string deeplinkable_type
 * @property string title_fa
 * @property string title_en
 * @property boolean status
 * @package App\Models
 */
class DeepLink extends Model
{
    /**
     * define Deeplink's table
     * @var string
     */
    protected $table = 'deep_links';

    /**
     * define Deeplink's fillable fields
     * @var string[]
     */
    protected $fillable = ['link', 'deeplinkable_id', 'deeplinkable_type', 'title_fa', 'title_en', 'status'];

    /**
     * define Deeplink's fillable casts
     * @var string[]
     */
    protected $casts = [
        'id' => 'integer',
        'link' => 'string',
        'deeplinkable_id' => 'int',
        'deeplinkable_type' => 'string',
        'title_fa' => 'string',
        'title_en' => 'string',
        'status' => 'boolean'
    ];

    /**
     * define polymorphic relation
     * @return MorphTo
     */
    public function deeplinkable():MorphTo
    {
        return $this->morphTo('deeplinkable', 'deeplinkable_type', 'deeplinkable_id', 'id');
    }
}
