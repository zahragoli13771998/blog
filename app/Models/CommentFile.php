<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Comment file
 * @property string path
 * @property string type
 * @package App\Models
 */
class CommentFile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = ['path','type'];

    /**
     * define Comment file's casts
     * @var string[]
     */
    protected $casts = [
        'comment_id' => 'integer',
        'path' => 'string',
        'type' => 'string',
    ];

    /**
     * The database connection that should be used by the migration.
     *
     * @var string
     */
    protected $table = 'comment_files';

    /**
     * Define auto hidden attributes of model
     * @var string[]
     */
    protected $hidden = ['comment_id', 'type', 'id'];

    /**
     * Disable timestamps in model
     * @var bool
     */
    public $timestamps = false;

    /**
     *relation with comment
     *
     * @return BelongsTo
     */
    public function comment():BelongsTo
    {
        return $this->belongsTo(Comment::class, 'comment_id', 'id');
    }

    /**
     * Get and return
     * @return string
     */
    public function getPathAttribute(): string
    {
        if ($this->attributes['path']) {
            return asset('storage/' . $this->attributes['path']);
        }
    }
}
