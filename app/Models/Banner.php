<?php

namespace App\Models;

use App\Models\Interfaces\HasComments;
use App\Models\Interfaces\HasDisLike;
use App\Models\Interfaces\HasFavorite;
use App\Models\Interfaces\HasLikes;
use App\Models\Interfaces\HasTag;
use App\Models\Traits\HasDislikeTrait;
use App\Models\Traits\HasLikeTrait;
use App\Models\Traits\HasTagTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use App\Models\Interfaces\HasViews;

/**
 * Class Banner
 * @property int id
 * @property string image
 * @property string URL
 * @property integer banner_type_id
 * @property boolean is_enabled
 * @property boolean is_slider
 * @property string title_fa
 * @property string title_en
 * @property string description_fa
 * @property string description_en
 * @property string html_description
 * @package App\Models
 */
class Banner extends Model implements HasDisLike, HasLikes, HasComments, HasViews, HasFavorite ,HasTag
{
    /**
     * Use Has likes and Has Dislike traits for desired model
     */
    use HasDislikeTrait, HasLikeTrait, HasTagTrait;

    /**
     * define Banner's table
     * @var string
     */
    protected $table = 'banners';

    /**
     * define Banner's fillable fields
     * @var string[]
     */
    protected $fillable = ['id', 'image','is_slider', 'banner_type_id', 'URL', 'title_fa', 'title_en','html_description', 'description_fa', 'description_en', 'is_enabled', 'created_at', 'updated_at'];

    /**
     * define Banner's casts
     * @var string[]
     */
    protected $casts = [
        'image' => 'string',
        'banner_type_id' => 'integer',
        'URL' => 'string',
        'title_fa' => 'string',
        'title_en' => 'string',
        'description_fa' => 'string',
        'description_en' => 'string',
        'html_description'=>'string',
        'is_enabled' => 'boolean',
        'is_slider' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    /**
     * Define model's attributes to get hidden automatically
     * @var string[]
     */
    protected $hidden = [
        'title_fa',
        'title_en',
        'pivot',
        'description_fa',
        'description_en',
        'is_enabled',
        'is_slider',
        'banner_type_id',
        'created_at',
//        'updated_at'
    ];

    /**
     * append title
     * @var string[]
     */
    protected $appends = [
        'title',
        'description',
        'is_liked',
        'is_disliked',
        'is_favored'
    ];


    /**
     * title 's accessor
     * @return array
     */
    public function getTitleAttribute(): array
    {
        return [
            'fa' => $this->title_fa,
            'en' => $this->title_en
        ];
    }

    /**
     * Append new attribute that containers description fa & en
     * @return array
     */
    public function getDescriptionAttribute(): array
    {
        return [
            'fa' => $this->description_fa,
            'en' => $this->description_en
        ];
    }


    /**
     *  define each Banner has many comments in polymorphic relation
     * @return MorphMany
     */
    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable', 'commentable_type', 'commentable_id', 'id');
    }

    /**
     * Get the banner types that owns the banner.
     *
     * @return BelongsTo
     */
    public function bannerType(): BelongsTo
    {
        return $this->belongsTo(BannerType::class, 'banner_type_id', 'id');
    }

    /**
     * Define polymorphic relation
     * @return MorphMany
     */
    public function views(): MorphMany
    {
        return $this->morphMany(Views::class, 'viewable', 'viewable_type', 'viewable_id', 'id');
    }

    /**
     * define polymorphic relation with users
     * @return MorphToMany
     */
    public function favorites(): MorphToMany
    {
        return $this->morphToMany(User::class, 'favorable', 'favorites', 'favorable_id', 'user_id', 'id', 'id');
    }

    /**
     * Get and return Banner's image attribute
     * @return string|null
     */
    public function getImageAttribute(): ?string
    {
        if (isset($this->attributes['image'])) {
            return asset('storage/' . $this->attributes['image']);
        }

        return null;
    }

    /**
     * Check if banner is favored by user or not
     * @return bool
     */
    public function getIsFavoredAttribute(): bool
    {
        $favorites_relation = $this->relations;
        unset($this->relations['favorites']);
        if (isset($favorites_relation['favorites']) && count($favorites_relation['favorites'])) {
            return true;
        }
        return false;
    }

    /**
     * define many to many relation with GlobalCurrency
     * @return BelongsToMany
     */
    public function globalCurrencies(): BelongsToMany
    {
        return $this->belongsToMany(GlobalCurrency::class, 'banner_global_currencies', 'banner_id', 'global_currency_id', 'id', 'id');
    }
}
