<?php

namespace App\Models;

use App\Events\GlobalCurrenciesUpdatedEvent;
use App\Models\Interfaces\HasComments;
use App\Models\Interfaces\HasDisLike;
use App\Models\Interfaces\HasFavorite;
use App\Models\Interfaces\HasIncludes;
use App\Models\Interfaces\HasLikes;
use App\Models\Interfaces\HasTag;
use App\Models\Interfaces\HasViews;
use App\Models\Traits\HasDislikeTrait;
use App\Models\Traits\HasLikeTrait;
use App\Models\Traits\HasTagTrait;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class GlobalCurrency
 * @package App\Models
 *
 * @property integer rank
 * @property integer currency_id
 * @property integer pair_id
 * @property string name_en
 * @property string name_fa
 * @property string icon
 * @property string symbol_id
 * @property string symbol
 * @property string description_en
 * @property string description_fa
 * @property integer price_precision
 * @property double price
 * @property double rial_price
 * @property double btc_price
 * @property double total_volume
 * @property double total_supply
 * @property double market_dominance
 * @property double daily_trading_volume
 * @property double coin_circulation
 * @property boolean is_hidden
 * @property DateTime created_at
 * @property DateTime updated_at
 */
class
GlobalCurrency extends Model implements HasLikes, HasDisLike, HasComments, HasViews, HasTag, HasFavorite, HasIncludes
{
    /**
     * Use Has likes and Has Dislike traits for desired model
     */
    use HasDislikeTrait, HasLikeTrait, HasTagTrait;

//    use softDeletes;

    /**
     * Define model's mass assign allowed attributes
     * @var string[]
     */
    protected $fillable = ['rank', 'symbol_id', 'pair_id', 'price_precision', 'symbol', 'price', 'rial_price', 'btc_price', 'name_fa', 'name_en', 'description_en', 'description_fa', 'icon', 'currency_id', 'total_supply', 'market_dominance', 'total_volume', 'daily_trading_volume', 'coin_circulation', 'is_hidden', 'created_at', 'updated_at'];

    /**
     * Define model's desired table
     * @var string
     */
    protected $table = 'global_currencies';

    /**
     * Define model's attributes type casting
     * @var string[]
     */
    protected $casts = [
        'rank' => 'integer',
        'currency_id' => 'integer',
        'pair_id' => 'integer',
        'name_en' => 'string',
        'name_fa' => 'string',
        'description_en' => 'string',
        'description_fa' => 'string',
        'symbol_id' => 'string',
        'symbol' => 'string',
        'icon' => 'string',
        'price_precision' => 'integer',
        'price' => 'double',
        'rial_price' => 'double',
        'btc_price' => 'double',
        'total_supply' => 'double',
        'market_dominance' => 'double',
        'total_volume' => 'double',
        'daily_trading_volume' => 'double',
        'coin_circulation' => 'double',
        'is_hidden' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Make some model's elements hidden
     * @var string[]
     */
    protected $hidden = [
        'btc_price', 'rial_price', 'currency_id', 'description_fa', 'description_en', 'created_at', 'updated_at', 'price_precision', 'pivot'
    ];

//    /**
//     * Add attributes to model object dynamically
//     * @var string[]
//     */
    protected $appends = [
        'is_liked', 'is_disliked', 'is_favored'
    ];

    /**
     * Define model observers on different events that gets happened on model
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => GlobalCurrenciesUpdatedEvent::class
    ];

    /**
     * Define Each Global Currency may belong to a currency
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }

    /**
     * Get and return market dominance attribute
     */
    public function getMarketDominanceAttribute()
    {
        return $this->attributes['market_dominance'] ?? 0;
    }

    /**
     * Define each global currency has many changes percent
     * @return HasMany
     */
    public function changePercent(): hasMany
    {
        return $this->hasMany(CurrencyChangesPercent::class, 'global_currency_id', 'id');
    }

    /**
     * Define each global Currency has many ohlc
     * @return HasMany
     */
    public function ohlc(): hasMany
    {
        return $this->hasMany(CurrencyOHLC::class, 'global_currency_id', 'id');
    }

    /**
     * description's accessor
     * @return array
     */
    public function getDescriptionAttribute(): array
    {
        return [
            'fa' => $this->attributes['description_fa'],
            'en' => $this->attributes['description_en']
        ];
    }

    /**
     * Define each Global currency has many comments in polymorphic way
     * @return MorphMany
     */
    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable', 'commentable_type', 'commentable_id', 'id');
    }

    /**
     * Define each global currency has many views in polymorphic way
     * @return MorphMany
     */
    public function views(): MorphMany
    {
        return $this->morphMany(Views::class, 'viewable', 'viewable_type', 'viewable_id');
    }

    /**
     * define polymorphic relation with favorites table
     * @return MorphToMany
     */
    public function favorites(): MorphToMany
    {
        return $this->morphToMany(User::class, 'favorable', 'favorites', 'favorable_id', 'user_id', 'id', 'id');
    }

    /**
     * Define each global Currency has one ramzinex info
     * @return HasOne
     */
    public function ramzinexInfo(): HasOne
    {
        return $this->hasOne(GCurrencyRamzinexInfo::class, 'global_currency_id', 'id');
    }

    /**
     * Check if banner is favored by user or not
     * @return bool
     */
    public function getIsFavoredAttribute(): bool
    {
//        $favorites_relation = $this->relations;
//        unset($this->relations['favorites']);
//        if (isset($favorites_relation['favorites']) && count($favorites_relation['favorites'])) {
//            return true;
//        }
        return false;
    }

    /**
     * Make and return web view url attribute based on global currency symbol
     * @return string
     */
    public function getWebviewUrlAttribute(): string
    {
        return 'https://ramzinex.com/exchange/tv_market_global2/' . $this->attributes['symbol'];
    }

    /**
     * define polymorphic relation with IncludeToRazminex
     * @return MorphToMany
     */
    public function includes(): MorphToMany
    {
        return $this->morphToMany(User::class, 'includable', 'include_to_razminexes', 'includable_id', 'user_id', 'id', 'id');
    }

    /**
     * define many to many relation with banner
     * @return BelongsToMany
     */
    public function banners(): BelongsToMany
    {
        return $this->belongsToMany(Banner::class, 'banner_global_currencies', 'global_currency_id', 'banner_id', 'id', 'id');
    }

    /**
     * define many-to-many relation with sector
     * @return BelongsToMany
     */
    public function sectors(){
        return $this->belongsToMany(Sector::class,'global_currency_sector','sector_id','global_currency_id','id','id');
    }
}
