<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class GameScore
 * @property int score
 * @property int coin
 */
class GameScore extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var string[]
     */
    protected $fillable = ['score','coin'];

    /**
     * The database connection that should be used by the migration.
     * @var string
     */
    protected $table = 'game_scores';

    /**
     * Define each score belongs to a user
     * @return BelongsTo
     */
    public function user():BelongsTo
    {
        return $this->belongsTo(GameUser::class, 'user_id', 'id');
    }
}
