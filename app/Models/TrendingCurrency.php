<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class TrendingCurrency
 * @property int id
 * @package App\Models
 */
class TrendingCurrency extends Model
{
    /**
     * define TrendingCurrency's table
     * @var string
     */
    protected $table = 'trending_currencies';

    /**
     * define TrendingCurrency's fillable fields
     * @var string[]
     */
    protected $fillable = ['id'];

    /**
     * define TrendingCurrency's casts
     * @var string[]
     */
    protected $casts = [
        'id' => 'int',
    ];

    /**
     * Define that model's automatic timestamp is disabled
     * @var bool
     */
    public $timestamps = false;

    /**
     * define TrendingCurrency relation with Currency
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }
}
