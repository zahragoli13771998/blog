<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Admin
 * @package App\Models
 * @property string name
 * @property string email
 * @property string password
 */
class HrAdmin extends Authenticatable
{
    /**
     * Define Models Desired table
     * @var string
     */
    protected $table = 'hr_admins';

    /**
     * Define model's attributes casts
     * @var string[]
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Define Model's allowed mass assign attributes
     * @var string[]
     */
    protected $fillable = ['name', 'email', 'password'];
}
