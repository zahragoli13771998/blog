<?php

namespace App\Models;

use App\Models\Interfaces\HasDisLike;
use App\Models\Interfaces\HasLikes;
use App\Models\Traits\HasDislikeTrait;
use App\Models\Traits\HasLikeTrait;
use DateTime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Facades\DB;
use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;


/**
 * Class comment
 * @property int id
 * @property int user_id
 * @property int commentable_id
 * @property int comment_id
 * @property int answer_index
 * @property int total_answes_count
 * @property array options
 * @property DateTime end_time
 * @property int depth_id
 * @property string commentable_type
 * @property string body
 * @property string nickname
 * @property boolean status
 * @property DateTime created_at
 * @property DateTime updated_at
 * @package App\Models
 */
class Comment extends Model implements HasLikes, HasDisLike
{
    /**
     * Define eager loading limit Number
     */
    const DEFAULT_EAGER_LIMIT = 5;

    /**
     * Package for limiting eager loaded objects per parent Object.
     */
    use HasEagerLimit;

    /**
     * Use Has likes and Has Dislike traits for desired model
     */
    use HasDislikeTrait, HasLikeTrait;

    /**
     * define Comment's table
     * @var string
     */
    protected $table = 'comments';

    /**
     * define Comment's fillable fields
     * @var string[]
     */
    protected $fillable = ['id', 'user_id', 'comment_id', 'depth_id', 'commentable_type','total_answes_count', 'nickname', 'commentable_id', 'body', 'status', 'created_at', 'updated_at','type','options','answer_index','end_time'];

    /**
     * define Comment's casts
     * @var string[]
     */
    protected $casts = [
        'user_id' => 'integer',
        'comment_id' => 'integer',
        'depth_id' => 'integer',
        'answer_index' => 'integer',
        'commentable_type' => 'string',
        'commentable_id' => 'integer',
        'total_answes_count'=>'int',
        'body' => 'string',
        'type'=>'string',
        'nickname' => 'string',
        'status' => 'boolean',
        'options' => 'json',
        'end_time' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * Define Auto Hidden fields by Laravel
     * @var array
     */
    protected $hidden = [
        'user_id', 'commentable_type', 'commentable_id', 'file'
    ];

    /**
     * Define models dynamic attributes creation
     * @var array
     */
    protected $appends = [
        'file_path', 'is_liked', 'is_disliked'
    ];

    /**
     * Define each comment belongs to a user
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * define comments relation
     * @return BelongsTo
     */
    public function comment(): BelongsTo
    {
        return $this->belongsTo(self::class, 'comment_id', 'id');
    }

    /**
     * define comments Self relation
     * @return HasMany
     */
    public function replies(): HasMany
    {
        return $this->hasMany(self::class, 'comment_id', 'id');
    }

    /**
     * define comments Self relation
     * @return HasMany
     */
    public function depths(): HasMany
    {
        return $this->hasMany(self::class, 'depth_id', 'id')->limit(static::DEFAULT_EAGER_LIMIT);
    }
    /**
     * define polymorphic relation
     * @return MorphTo
     */
    public function commentable(): MorphTo
    {
        return $this->morphTo('commentable', 'commentable_type', 'commentable_id', 'id');
    }

    /**
     * define comments relation with comment files
     * @return HasOne
     */
    public function file(): HasOne
    {
        return $this->hasOne(CommentFile::class, 'comment_id', 'id');
    }

    /**
     * Get and return file path attribute
     * @return string
     */
    public function getFilePathAttribute(): ?string
    {
        return $this->file->path ?? null;
    }

    /**
     * Get and return comment type
     * @return array
     */
    public function getCommentTypeAttribute(): array
    {
        $this->makeHidden(['commentable']);
        switch ($this->attributes['commentable_type']) {
            case 'App\Models\GlobalCurrency':
                $type = 'global_currency';
                $title = $this->relations['commentable']['name_en'];
                break;
            case 'App\Models\FaqQuestionAnswer':
                $type = 'faq';
                $title = $this->relations['commentable']['question_en'];
                break;
            case 'App\Models\Banner':
                $type = 'banner';
                $title = $this->relations['commentable']['title_fa'];
                break;
        }

        return [
            'id' => $this->attributes['commentable_id'],
            'title' => $title ?? null,
            'type' => $type ?? null,
        ];
    }

    /**
     * Format created at in unix timestamps
     * @return int
     */
    public function getCreatedAtUnixAttribute(): int//Todo add in resource collection
    {
        return strtotime($this->attributes['created_at']);
    }

    /**
     * Create and return replied to object
     * @return array
     */
    public function getRepliedToAttribute(): array
    {
        $this->makeHidden('comment');
        $replied_id = $this->relations['comment']['user']['id'] ?? null;
        $replied_name = $this->relations['comment']['nickname'] ?? null;
        return [
            'user_id' => $replied_id,
            'name' => $replied_name
        ];
    }
    /**
     * return survey options count
     * @return Collection
     */
    public function getAnswersCount(): Collection
    {
        return $this->hasMany(self::class, 'depth_id', 'id')->where('type','=','survey_answer')->select(array('answer_index', DB::raw('COUNT(*) AS count')))->groupBy('answer_index')->get();
    }
    

}
