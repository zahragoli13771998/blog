<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int id
 * @property string title_fa
 * @property string title_en
 * Class BannerType
 * @package App\Models
 */
class BannerType extends Model
{
    /**
     * Define model's allowed mass assign attributes
     * @var string[]
     */
    protected $fillable = ['id', 'title_fa', 'title_en'];

    /**
     * Define model's desired table
     * @var string
     */
    protected $table = 'banner_types';

    /**
     * Define models attributes types
     * @var string[]
     */
    protected $casts = [
        'title_fa' => 'string',
        'title_en' => 'string'
    ];

    /**
     * Define model's auto append
     * @var string[]
     */
    protected $appends = [
        'title'
    ];

    /**
     * Define automatic hidden attributes of model
     * @var string[]
     */
    protected $hidden = [
        'title_fa', 'title_en'
    ];

    /**
     * Disable Model's timestamps
     * @var bool
     */
    public $timestamps = false;

    /**
     * Define each type has many banners
     * @return HasMany
     */
    public function banner(): HasMany
    {
        return $this->hasMany(Banner::class, 'banner_type_id', 'id');
    }

    /**
     * Get Title en/fa attributes
     * @return array
     */
    public function getTitleAttribute(): array
    {
        return [
            'fa' => $this->attributes['title_fa'],
            'en' => $this->attributes['title_en'],
        ];
    }
}
