<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property int user_id
 * @property int comment_id
 */
class CommentReport extends Model
{
    /**
     * @var string
     */
    protected $table ="comment_reports";

    /**
     * @var string[]
     */
    protected $fillable =["id","user_id","comment_id"];
}

