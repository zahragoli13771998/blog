<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * model for degrees and defining relations with other models
 * Class Degrees
 * @package App\Models
 * @property integer id
 * @property string title_fa
 * @property string title_en
 */

class Degrees extends Model
{
    /**
     * related table to this model
     * @var string
     */
    public $table='degrees';

    /**
     * for casting property in this model
     * @var string[]
     */
    protected $casts=[
        'id'=>'integer',
        'title_fa'=>'string',
        'title_en'=>'string',
    ];

    /**
     * for defining fields of related table that can be filled using mass-assignment
     * @var string[]
     */
    protected $fillable=[
        'title_fa',
        'title_en',
    ];

    /**
     * defining relation between Degrees model and JobVacancies model(one to many)
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobVaccancy(): HasMany
    {
        return $this->hasMany(JobVacancies::class,'min_degree_id');

    }
}
