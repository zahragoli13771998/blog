<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string title
 * @property int id
 */
class Currency extends Model
{
    protected $guarded = [];
    /**
     * define related table
     * @var string
     */
    protected $table = 'currencies';
    /**
     * define fillable fields
     * @var string[]
     */
    protected $fillable = ['id', 'title'];

    /**
     * define casts
     * @var string[]
     */
    protected $casts = ['string' => 'title'];

    /**
     * define relation
     * @return HasMany
     */
    public function globalCurrency(): HasMany
    {
        return $this->hasMany(GlobalCurrency::class);
    }
}
