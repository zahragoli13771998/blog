<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Looser
 * @property int id
 * @property double value
 * @package App\Models
 */
class Looser extends Model
{
    /**
     * define Looser table
     * @var string
     */
    protected $table = 'loosers';

    /**
     * define Looser fillable fields
     * @var string[]
     */
    protected $fillable = ['id','value'];

    /**
     * define Looser casts
     * @var string[]
     */
    protected $casts = [
        'id' => 'int',
        'value'=>'double'
    ];

    /**
     * Disable model's default timestamps
     * @var bool
     */
    public $timestamps = false;
}
