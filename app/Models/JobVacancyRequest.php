<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * for users request to upload resume for job vacancies
 * Class JobVacancyRequest
 * @package App\Models
 * @property integer id
 * @property integer job_vacancy_id
 * @property string full_name
 * @property string phone
 * @property string resume_path
 * @property string email
 * @property int status
 * @property int job_type_id
 */
class JobVacancyRequest extends Model
{
    /**
     * related table to this model
     * @var string
     */
    public $table = 'job_vacancy_requests';

    /**
     * for casting property in this model
     * @var string[]
     */
    protected $casts = [
        'id' => 'integer',
        'job_type_id'=>'integer',
        'job_vacancy_id' => 'integer',
        'full_name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'resume_path' => 'string',
        'status' => 'int',
    ];

    /**
     * for defining fields of related table that can be filled using mass-assignment
     * @var string[]
     */
    protected $fillable = [
        'job_vacancy_id',
        'full_name',
        'email',
        'phone',
        'resume_path',
        'status',
        'job_type_id'
    ];

    /**
     * defining relation between JobVacancyRequest model and JobVacancies model(one to many)
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jobVacancies(): BelongsTo
    {
        return $this->belongsTo(JobVacancies::class, 'job_vacancy_id', 'id');
    }

    /**
     * define comments relation with comment files
     * @return HasOne
     */
    public function file(): HasOne
    {
        return $this->hasOne(ResumeFile::class, 'comment_id', 'id');
    }
}
