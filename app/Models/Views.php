<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Views
 * @property int id
 * @property int user_id
 * @property int viewable_id
 * @property string viewable_type
 * @property DateTime created_at
 * @property DateTime updated_at
 * @package App\Models
 */
class Views extends Model
{
    use HasFactory;

    /**
     * define views table
     * @var string[] define View's related table
     */
    protected $table = 'views';

    /**
     * define View 's fillable fields
     * @var string[]
     */
    protected $fillable = ['id','viewable_id','viewable_type','user_id', 'created_at', 'updated_at'];

    /**
     * define View 's casts
     * @var string[]
     */
    protected $casts = [
        'user_id'=>'int',
        'viewable_id'=>'int',
        'viewable_type'=>'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];


    /**
     * define polymorphic relation
     * @return MorphTo
     */
    public function viewable(): MorphTo
    {
        return $this->morphTo('viewable','viewable_type','viewable_id', 'id');
    }

}
