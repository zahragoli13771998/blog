<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class GCurrencyRamzinexInfo
 * @package App\Models
 * @property double open
 * @property double close
 * @property double buy
 * @property double sell
 * @property double high
 * @property double low
 * @property double quote_volume
 * @property double base_volume
 * @property double change_percent
 */
class GCurrencyRamzinexInfo extends Model
{
    /**
     * define gcurrencies_ramzinex_info table
     * @var string
     */
    protected $table = 'gcurrencies_ramzinex_info';

    /**
     * define TrendingCurrency's fillable fields
     * @var string[]
     */
    protected $fillable = ['open','close','high','low','quote_volume','base_volume','change_percent', 'buy', 'buy', 'created_at', 'updated_at'];

    /**
     * define gcurrencies_ramzinex_info casts
     * @var string[]
     */
    protected $casts = [
        'open' => 'double',
        'close' => 'double',
        'buy' => 'double',
        'sell' => 'double',
        'high' => 'double',
        'low' => 'double',
        'quote_volume' => 'double',
        'base_volume' => 'double',
        'change_percent' => 'double',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Get the global currency that owns the ramzinex info.
     * @return BelongsTo
     */
    public function globalCurrency(): BelongsTo
    {
        return $this->belongsTo(GlobalCurrency::class, 'global_currency_id', 'id');
    }
}
