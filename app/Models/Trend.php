<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Trend
 * @property int id
 * @package App\Models
 */
class Trend extends Model
{
    /**
     * define Trend's table
     * @var string
     */
    protected $table = 'trends';

    /**
     * define Trend's fillable fields
     * @var string[]
     */
    protected $fillable = ['id'];

    /**
     * define Trend's casts
     * @var string[]
     */
    protected $casts = [
        'id' => 'int'
    ];

    /**
     * make timestamps ,false
     * @var string
     */
    public $timestamps = false;
}
