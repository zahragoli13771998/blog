<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int id
 * @property int sector_id
 * @property string description_fa
 * @property string description_en
 * @property double market_cap
 * @property double trading_volume
 * @package App\Models
 */
class SectorInfo extends Model
{
    /**
     * define related table
     * @var string
     */
    protected $table = 'sectors';

    /**
     * define fillable fields
     * @var string[]
     */
    protected $fillable = ['id','description_fa','sector_id', 'description_en', 'market_cap', 'trading_volume'];

    /**
     * define casts
     * @var string[]
     */
    protected $casts = [
        'id' => 'int',
        'sector_id' => 'int',
        'description_fa' => 'string',
        'description_en' => 'string',
        'market_cap' => 'double',
        'trading_volume' => 'double'
    ];

    /**
     * define relation with sectors
     * @return BelongsTo
     */
    public function Sector(): BelongsTo
    {
        return $this->belongsTo(Sector::class,'sector_id');
    }
}
