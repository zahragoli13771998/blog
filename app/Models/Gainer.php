<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Gainer
 * @property int $id
 * @property double $value
 * @package App\Models
 */
class Gainer extends Model
{
    /**
     * define Gainer's table
     * @var string
     */
    protected $table = 'gainers';

    /**
     * define Gainer's fillable fields
     * @var string[]
     */
    protected $fillable = ['id','value'];

    /**
     * define Gainer's casts
     * @var string[]
     */
    protected $casts = [
        'id' => 'int',
        'value'=>'double'
    ];

    /**
     * Define that model doesnt have default timestamps
     * @var bool
     */
    public $timestamps = false;
}
