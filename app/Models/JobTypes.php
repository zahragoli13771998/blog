<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * model for job types and defining relations with other models
 * Class JobTypes
 * @package App\Models
 * @property integer id
 * @property string title
 */

class JobTypes extends Model
{
    /**
     * related table to this model
     * @var string
     */
    public $table='job_types';

    /**
     * for casting property in this model
     * @var string[]
     */
    protected $casts=[
        'id'=>'integer',
        'title'=>'string',
    ];

    /**
     * for defining fields of related table that can be filled using mass-assignment
     * @var string[]
     */
    protected $fillable=[
        'title',
    ];

    /**
     * defining relation between JobTypes model and JobVacancies model(one to many)
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobVacancy(): HasMany
    {
        return $this->hasMany(JobVacancies::class,'job_type_id');
    }

    /**
     * for defining relation between three model(job vacancy & job type $ job vacancy request)
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function jobVacancyRequest(): HasManyThrough
    {
        return $this->hasManyThrough(
            JobVacancyRequest::class,
            JobVacancies::class,
            'job_type_id',
            'job_vacancy_id',
            'id',
            'id'
        );
    }
}
