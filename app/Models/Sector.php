<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int id
 * @property string title_fa
 * @property string title_en
 * @package App\Models
 */
class Sector extends Model
{
    /**
     * define related table
     * @var string
     */
    protected $table = 'sectors';

    /**
     * define fillable fields
     * @var string[]
     */
    protected $fillable = ['id', 'title_fa', 'title_en'];

    /**
     * define casts
     * @var string[]
     */
    protected $casts = [
        'id' => 'int',
        'title_fa' => 'string',
        'title_en' => 'string',
    ];

    /**
     * define relation with sector
     * @return HasOne
     */
    public function sectorInfo(): HasOne
    {
        return $this->hasOne(SectorInfo::class,'sector_id');
    }
    /**
     * define many-to-many relation between global currencies and sectors
     * @return BelongsToMany
     */
    public function globalCurrencies(): BelongsToMany
    {
        return $this->belongsToMany(GlobalCurrency::class, 'global_currency_sector', 'sector_id', 'global_currency_id', 'id', 'id');
    }
}
