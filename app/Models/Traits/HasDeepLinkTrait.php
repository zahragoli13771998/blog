<?php

namespace App\Models\Traits;

use App\Models\DeepLink;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * Trait HasDeepLinkTrait
 * @package App\Models\Traits
 */
trait HasDeepLinkTrait
{
    /**
     * define polymorphic relation
     * @return MorphOne
     */
    public function deeplink():MorphOne
    {
        return $this->morphOne(DeepLink::class, 'deeplinkable', 'deeplinkable_type', 'deeplinkable_id');
    }
}
