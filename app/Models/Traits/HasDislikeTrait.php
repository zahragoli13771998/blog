<?php


namespace App\Models\Traits;


use App\Models\User;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait HasDislikeTrait
{
    /**
     * Define  polymorphic relation
     * @return MorphToMany
     */
    public function dislikes(): MorphToMany
    {
        return $this->morphToMany(User::class, 'dislikeable', 'user_dislikes', 'dislikeable_id', 'user_id', 'id', 'id');
    }
    /**
     * Checks if user is disliked or not
     * @return bool
     */
    public function getIsDislikedAttribute(): bool
    {
//        $dislikes_relation = $this->relations;
//        unset($this->relations['dislikes']);
//        if (isset($dislikes_relation['dislikes']) && count($dislikes_relation['dislikes'])) {
//            return true;
//        }
        return false;
    }
}
