<?php


namespace App\Models\Traits;


use App\Models\User;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait HasLikeTrait
{
    /**
     * Define polymorphic relation
     * @return MorphToMany
     */
    public function likes(): MorphToMany
    {
        return $this->morphToMany(User::class, 'likeable', 'user_likes', 'likeable_id', 'user_id', 'id', 'id');
    }
    /**
     * Checks if user liked or not
     * @return bool
     */
    public function getIsLikedAttribute(): bool
    {
//        $likes_relation = $this->relations;
//        unset($this->relations['likes']);
//        if (isset($likes_relation['likes']) && count($likes_relation['likes'])) {
//            return true;
//        }
        return false;
    }
}
