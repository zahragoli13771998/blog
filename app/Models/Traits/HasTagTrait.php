<?php


namespace App\Models\Traits;


use App\Models\Tag;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait HasTagTrait
{

    /**
     * define polymorphic relation
     * @return MorphToMany
     */
    public function tags(): MorphToMany
    {
        return $this->morphToMany(Tag::class, 'taggable', 'taggables', 'taggable_id', 'tag_id', 'id', 'id');
    }
}
