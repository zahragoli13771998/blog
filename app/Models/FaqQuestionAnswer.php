<?php

namespace App\Models;

use App\Models\Interfaces\HasDeepLink;
use App\Models\Interfaces\HasDisLike;
use App\Models\Interfaces\HasFavorite;
use App\Models\Interfaces\HasLikes;
use App\Models\Interfaces\HasComments;
use App\Models\Traits\HasDeepLinkTrait;
use App\Models\Traits\HasDislikeTrait;
use App\Models\Traits\HasLikeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Interfaces\HasViews;

/**
 * Class FaqQuestionAnswer
 * @property int id
 * @property int faq_category_id
 * @property int show_order
 * @property string question_fa
 * @property string question_en
 * @property boolean is_repetitive
 * @property string short_answer_fa
 * @property string short_answer_en
 * @property string complete_answer_fa
 * @property string complete_answer_en
 * @property string image
 * @property string video
 * @property string external_video
 * @package App\Models
 */
class FaqQuestionAnswer extends Model implements HasLikes, HasDisLike, HasComments, HasViews, HasFavorite, HasDeepLink
{
    /**
     * Use Has likes, Has Dislike and Has Deeplink traits for desired model
     */
    use HasDislikeTrait, HasLikeTrait, HasDeepLinkTrait;

    use softDeletes;

    /**
     * define FaqQuestionAnswer 's table
     * @var string
     */
    protected $table = 'faq_question_answers';

    /**
     * define FaqQuestionAnswer 's fillable fields
     * @var string[]
     */
    protected $fillable = ['faq_category_id', 'show_order', 'is_repetitive', 'question_fa', 'question_en', 'short_answer_fa', 'short_answer_en', 'complete_answer_fa', 'complete_answer_en', 'image', 'video', 'external_video', 'created_at', 'updated_at'];

    /**
     * Define Question answer soft delete date field
     * @var string[]
     */
    protected $dates = ['deleted_at'];

    /**
     * define  FaqQuestionAnswer 's casts
     * @var string[]
     */
    protected $casts = [
        'id' => 'integer',
        'faq_category_id' => 'integer',
        'show_order' => 'integer',
        'question_en' => 'string',
        'question_fa' => 'string',
        'is_repetitive' => 'boolean',
        'short_answer_fa' => 'string',
        'short_answer_en' => 'string',
        'complete_answer_fa' => 'string',
        'complete_answer_en' => 'string',
        'image' => 'string',
        'video' => 'string',
        'external_video' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * Make fields auto hidden
     * @var string[]
     */
    protected $hidden = [
        'deleted_at', 'updated_at', 'created_at', 'complete_answer_fa',
        'complete_answer_en', 'short_answer_fa', 'short_answer_en',
        'question_fa', 'question_en'
    ];

    /**
     * Automatically append attributes
     * @var string[]
     */
    protected $appends = [
        'is_liked',
        'is_disliked',
        'plain_answer',
        'short_answer',
        'question',
        'complete_answer',
        'is_favored',
    ];

    /**
     * define relationship between faqCategory and FaqQuestionAnswer
     * @return BelongsTo
     */
    public function faqCategory(): BelongsTo
    {
        return $this->belongsTo(FaqCategory::class, 'faq_category_id');

    }

    /**
     *  define polymorphic relation
     * @return MorphMany
     */
    public function views(): MorphMany
    {
        return $this->morphMany(Views::class, 'viewable', 'viewable_type', 'viewable_id');
    }

    /**
     * Get and return Banner's image attribute
     * @return string|null
     */
    public function getImageAttribute(): ?string
    {
        if (isset($this->attributes['image'])) {
            return asset('storage/' . $this->attributes['image']);
        }

        return null;
    }

    /**
     * Get and return Banner's Video attribute
     * @return MorphMany
     */
    public function getVideoAttribute(): ?string
    {
        if (isset($this->attributes['video'])) {
            return asset('storage/' . $this->attributes['video']);
        }

        return null;
    }

    /**
     *  define each faq question has many comments in polymorphic relation
     * @return MorphMany
     */
    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable', 'commentable_type', 'commentable_id', 'id');
    }

    /**
     * Get Question Answer fa/en attributes
     */
    public function getQuestionAttribute(): array
    {
        return [
            'fa' => $this->attributes['question_fa'] ?? null,
            'en' => $this->attributes['question_en'] ?? null
        ];
    }

    /**
     * Get Short Answer fa/en attributes
     * @return array
     */
    public function getShortAnswerAttribute(): array
    {
        return [
            'en' => $this->attributes['short_answer_en'] ?? null,
            'fa' => $this->attributes['short_answer_fa'] ?? null
        ];
    }

    /**
     * Get complete answer fa/en attributes
     * @return array
     */
    public function getCompleteAnswerAttribute(): array
    {
        return [
            'fa' => $this->attributes['complete_answer_fa'] ?? null,
            'en' => $this->attributes['complete_answer_en'] ?? null,
        ];
    }

    /**
     * Get plain answer attribute on model
     * @return array
     */
    public function getPlainAnswerAttribute(): array //Todo improve later
    {
        $text_fa = str_ireplace('&nbsp;', '', strip_tags(str_ireplace(['</p>', '</li>', '</ul>'], [' ', ' ', ' '], $this->attributes['complete_answer_fa'] ?? null)));
        $text_en = str_ireplace('&nbsp;', '', strip_tags(str_ireplace(['</p>', '</li>', '</ul>'], [' ', ' ', ' '], $this->attributes['complete_answer_en'] ?? null)));
        return [
            'fa' => $text_fa,
            'en' => $text_en,
        ];
    }

    /**
     * define poly morphic relation with users
     * @return MorphToMany
     */
    public function favorites(): MorphToMany
    {
        return $this->morphToMany(User::class, 'favorable', 'favorites', 'favorable_id', 'user_id', 'id', 'id');
    }

    /**
     * Check if banner is favored by user or not
     * @return bool
     */
    public function getIsFavoredAttribute(): bool
    {
        $favorites_relation = $this->relations;
        unset($this->relations['favorites']);
        if (isset($favorites_relation['favorites']) && count($favorites_relation['favorites'])) {
            return true;
        }
        return false;
    }

}
