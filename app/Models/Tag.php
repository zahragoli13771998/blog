<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class tag
 * @package App\Models
 *
 * @property string title
 */
class Tag extends Model
{
    use softDeletes;

    /**
     * define Comment's fillable fields
     * @var string[]
     */
    protected $fillable = ['title'];

    /**
     * define Comment's table
     * @var string
     */
    protected $table = 'tags';

    /**
     * define Comment's casts
     * @var array
     */
    protected $casts = [
        'title' => 'string',
    ];

    /**
     * Make some model's elements hidden
     * @var string[]
     */
    protected $hidden = ['pivot', 'deleted_at'];

    /**
     * make timestamps ,false
     * @var bool
     */
    public $timestamps = false;

    /**
     * define polymorphic relation
     * @return MorphToMany
     */
    public function globalCurrencies(): MorphToMany
    {
        return $this->morphedByMany(GlobalCurrency::class, 'taggable', 'taggables', 'tag_id', 'taggable_id', 'id', 'id')->withPivot('taggable_type');
    }

    /**
     * @return MorphToMany
     */
    public function banners(): MorphToMany
    {
        return $this->morphedByMany(Banner::class, 'taggable', 'taggables', 'tag_id', 'taggable_id', 'id', 'id')->withPivot('taggable_type');
    }

    /**
     * Get and return comment type
     * @return string
     */
    public function getTagTypeAttribute(): string
    {
        if (count($this->banners) > 0) {
            return 'banners';
        }
        if (count($this->globalCurrencies) > 0){
            return 'global currency';
        }
    }


}
