<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * Class User
 * @package App\Models
 * @property string sid
 * @property string name
 */
class User extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'sid',
        'created_at',
        'updated_at',
    ];

    /**
     * define polymorphic relation with banners
     * @return MorphToMany
     */
    public function banners(): MorphToMany
    {
        return $this->morphedByMany(Banner::class,'favorable','favorites','user_id', 'favorable_id','id','id');
    }

    /**
     * define polymorphic relation with questions
     * @return MorphToMany
     */
    public function questions(): MorphToMany
    {
        return $this->morphedByMany(FaqQuestionAnswer::class,'favorable','favorites', 'user_id', 'favorable_id', 'id','id');
    }

    /**
     * define polymorphic relation with global_currencies
     * @return MorphToMany
     */
    public function globalCurrencies(): MorphToMany
    {
        return $this->morphedByMany(GlobalCurrency::class,'favorable','favorites','user_id', 'favorable_id','id','id');
    }

    /**
     * Define each user has many comments
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class, 'user_id', 'id');
    }

    /**
     * Define each User can like many global currencies
     * @return MorphToMany
     */
    public function globalCurrencyLikes(): MorphToMany
    {
        return $this->morphedByMany(GlobalCurrency::class,'likeable','user_likes','user_id', 'likeable_id','id','id');
    }

    /**
     * Define each User can like many banners
     * @return MorphToMany
     */
    public function bannersLikes(): MorphToMany
    {
        return $this->morphedByMany(Banner::class,'likeable','user_likes','user_id', 'likeable_id','id','id');
    }

    /**
     * Define each User can like many global currencies
     * @return MorphToMany
     */
    public function questionsLikes(): MorphToMany
    {
        return $this->morphedByMany(FaqQuestionAnswer::class,'likeable','user_likes','user_id', 'likeable_id','id','id');
    }

    /**
     * Define polymorphic relation
     * @return MorphMany
     */
    public function bannerViews(): MorphMany
    {
        return $this->morphMany(Views::class, 'viewable', 'viewable_type', 'viewable_id', 'id');
    }
    /**
     * Define each user can dislike many global currencies
     * @return MorphToMany
     */
    public function globalCurrencyDislikes(): MorphToMany
    {
        return $this->morphedByMany(GlobalCurrency::class,'dislikeable','user_dislikes','dislikeable_id','user_id','id','id');
    }

    /**
     * Define each user can dislike many global currencies
     * @return MorphToMany
     */
    public function bannersDislikes(): MorphToMany
    {
        return $this->morphedByMany(Banner::class,'dislikeable','user_dislikes','dislikeable_id','user_id','id','id');
    }

    /**
     * Define each user can dislike many global currencies
     * @return MorphToMany
     */
    public function questionsDislikes(): MorphToMany
    {
        return $this->morphedByMany(FaqQuestionAnswer::class,'dislikeable','user_dislikes','dislikeable_id','user_id','id','id');
    }
}
