<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FaqCategoryService
 * @property int id
 * @property int parent_id
 * @property string name_fa
 * @property string name_en
 * @property string icon
 * @property string description
 * @package App\Models
 */
class FaqCategory extends Model
{
    /**
     * Include soft delete scopes by using it's related trait
     */
//    use softDeletes;

    /**
     * define  FaqCategoryService 's table
     * @var string
     */
    protected $table = 'faq_categories';

    /**
     * Define auto eager-load on model
     * @var string[]
     */
    protected $with = ['subCategories'];

    /**
     * define  FaqCategoryService 's allowed mass assign fields
     * @var string[]
     */
    protected $fillable = ['id', 'parent_id', 'name_fa', 'name_en', 'icon','description'];

    /**
     * Define Soft delete attribute
     * @var string[]
     */
    protected $dates = ['deleted_at'];

    /**
     * Define default hidden attributes of model
     * @var string[]
     */
    protected $hidden = ['deleted_at', 'name_fa', 'name_en'];

    /**
     * make timestamps ,false
     * @var bool
     */
    public $timestamps = false;

    /**
     * define  FaqCategoryService 's casts
     * @var string[]
     */
    protected $casts = [
        'parent_id' => 'integer',
        'name_fa' => 'string',
        'name_en' => 'string',
        'icon'=>'string',
        'description'=>'string'
    ];

    /**
     * Define model auto appends attributes
     * @var string[]
     */
    protected $appends = [
        'name'
    ];

    /**
     * define  FaqCategoryService 's self relation : parent
     * @return BelongsTo
     */
    public function parentCategory(): BelongsTo
    {
        return $this->belongsTo(FaqCategory::class, 'parent_id', 'id');
    }


    /**
     * define  FaqCategoryService 's self relation :children
     * @return HasMany
     */
    public function subCategories(): HasMany
    {
        return $this->hasMany(FaqCategory::class, 'parent_id', 'id');
    }

    /**
     * define relationship between faqCategory and FaqQuestionAnswer
     * @return HasMany
     */
    public function faqQuestionAnswers(): HasMany
    {
        return $this->hasMany(FaqQuestionAnswer::class, 'faq_category_id', 'id');
    }

    /**
     * Get and regenerate Icon attribute URL
     * @return string
     */
    public function getIconAttribute(): ?string
    {
        if(isset($this->attributes['icon'])){
            return asset('storage/'.$this->attributes['icon']);
        }

        return null;
    }

    /**
     * Get name attributes in en/fa
     * @return array
     */
    public function getNameAttribute(): array //Todo remove-improve later
    {
        return [
            'fa' => $this->attributes['name_fa'],
            'en' => $this->attributes['name_en'],
        ];
    }
}
