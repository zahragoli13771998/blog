<?php

namespace App\Console;
use App\Console\Commands\CreateGlobalCurrenciesListCommand;
use App\Console\Commands\UpdateAllGlobalCurrenciesCommand;
use App\Console\Commands\UpdateGainerLosers;
use App\Console\Commands\UpdateGCurrencyRamzinexInfoCommand;
use App\Console\Commands\UpdateGlobalCurrenciesAllInOne;
use App\Console\Commands\UpdateGlobalCurrenciesOHLCCommand;
use App\Console\Commands\UpdateTetherPriceCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CreateGlobalCurrenciesListCommand::class,
        UpdateAllGlobalCurrenciesCommand::class,
        UpdateGlobalCurrenciesOHLCCommand::class,
        UpdateGlobalCurrenciesAllInOne::class,
        UpdateGainerLosers::class,
        UpdateGCurrencyRamzinexInfoCommand::class,
        UpdateTetherPriceCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('global_currency:update_all_in_one')->everyfiveMinutes();
        $schedule->command('global_currency:update_ohlc')->everyTwoHours();
        $schedule->command('gainer_loser:update')->dailyAt('00:00');
        $schedule->command('global_currency:update_ramzinex_info')->everyFiveMinutes();
        $schedule->command('global_currency:update_left_over_global_currencies')->everyFiveMinutes();
        $schedule->command('global_currency:update_left_over_global_currencies_ohlc')->everyFiveMinutes();
        $schedule->command('tether_price:update')->everyThreeMinutes();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
