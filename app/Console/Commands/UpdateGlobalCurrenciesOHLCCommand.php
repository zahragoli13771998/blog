<?php

namespace App\Console\Commands;

use App\Jobs\UpdateGlobalCurrenciesOHLCJob;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class UpdateGlobalCurrenciesOHLCCommand extends Command
{
    /**
     * Include dispatch jobs interface in order to run jobs in background
     */
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'global_currency:update_ohlc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'dispatch job to run ticker service to update global currency information';

    /**
     * Global currency repository container.
     */
    private GlobalCurrencyInterface $global_currency;

    /**
     * Construct and inject necessary services to their container
     * UpdateAllGlobalCurrenciesCommand constructor.
     * @param GlobalCurrencyInterface $global_currency
     */
    public function __construct(GlobalCurrencyInterface $global_currency)
    {
        parent::__construct();
        $this->global_currency = $global_currency;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $all_global_currencies = $this->global_currency->globalCurrenciesCount();

        for ($i = 0; $i < $all_global_currencies; $i += 100) {
            $this->dispatch(new UpdateGlobalCurrenciesOHLCJob($i, 100));
        }
    }
}
