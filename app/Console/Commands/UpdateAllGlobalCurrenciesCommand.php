<?php

namespace App\Console\Commands;

use App\Jobs\UpdateGlobalCurrenciesJob;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class UpdateAllGlobalCurrenciesCommand extends Command
{
    /**
     * Define Getting currencies info Job dispatcher
     * @var int
     */
    private int $step = 50;

    /**
     * Include dispatch jobs interface in order to run jobs in background
     */
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'global_currency:update {--from=} {--to=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'dispatch job to run ticker service to update global currency information';

    /**
     * Global currency repository container.
     */
    private GlobalCurrencyInterface $global_currency;

    /**
     * Construct and inject necessary services to their container
     * UpdateAllGlobalCurrenciesCommand constructor.
     * @param GlobalCurrencyInterface $global_currency
     */
    public function __construct(GlobalCurrencyInterface $global_currency)
    {
        parent::__construct();
        $this->global_currency = $global_currency;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $from = (int)$this->option('from');
        $to = (int)($this->option('to') ?? $this->global_currency->globalCurrenciesCount());

        for ($i = $from; $i < $to; $i += $this->step) {
            $this->dispatch(new UpdateGlobalCurrenciesJob($i, $this->step));
            $this->info('update from '.$i.' to '.($i+$this->step).' number of '.$to.' global currency');
        }
    }
}
