<?php

namespace App\Console\Commands;

use App\Jobs\CreateGlobalCurrenciesListJob;
use Illuminate\Console\Command;

class CreateGlobalCurrenciesListCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'global_currency:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'dispatch job to run ticker service to create all global currencies list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        CreateGlobalCurrenciesListJob::dispatch();
    }
}
