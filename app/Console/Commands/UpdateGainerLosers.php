<?php

namespace App\Console\Commands;

use App\Models\GlobalCurrency;
use App\Models\Views;
use App\Repositories\Interfaces\GainerRepositoryInterface;
use App\Repositories\Interfaces\LooserRepositoryInterface;
use App\Repositories\Interfaces\PairsRepositoryInterface;
use App\Repositories\Interfaces\TrendRepositoryInterfac;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateGainerLosers extends Command
{
    /**
     * Pairs that should get excluded from trends currencies
     * @var array
     */
    private array $exclude_trending_pairs = [
        2, 11, 10, 33
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gainer_loser:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update gainers and losers data';

    /**
     * Gainer repository container
     * @var GainerRepositoryInterface
     */
    protected GainerRepositoryInterface $gainer_repository;

    /**
     * Losers repository container
     * @var LooserRepositoryInterface
     */
    protected LooserRepositoryInterface $looser_repository;

    /**
     * Pairs repository container
     * @var PairsRepositoryInterface
     */
    protected PairsRepositoryInterface $pairs_repository;

    /**
     * Trending repository container
     * @var TrendRepositoryInterfac
     */
    protected TrendRepositoryInterfac $trending_repository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(GainerRepositoryInterface $gainer_repository, LooserRepositoryInterface $looser_repository, PairsRepositoryInterface $pairs_repository, TrendRepositoryInterfac $trending_repository)
    {
        $this->pairs_repository = $pairs_repository;
        $this->gainer_repository = $gainer_repository;
        $this->looser_repository = $looser_repository;
        $this->trending_repository = $trending_repository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return false|int
     */
    public function handle()
    {
        $pairs = $this->pairs_repository->getAllPairsInfo();

        if (!is_array($pairs) && is_null($pairs)) {
            return false;
        }

        $pairs = collect($pairs);
        $pairs = $pairs->sortByDesc('financial.last24h.change_percent');

        $result = $this->gainer_repository->deleteGainer();
        if ($result) {
            $gainers = $pairs->take(10)->pluck('financial.last24h.change_percent', 'pair_id');
            foreach ($gainers as $key => $value) {
                $this->gainer_repository->createGainer([
                    'id' => $key,
                    'value' => $value
                ]);
            }
        }

        $result = $this->looser_repository->deleteLooser();
        if ($result) {
            $losoers = $pairs->skip($pairs->count() - 10)->take(10)->pluck('financial.last24h.change_percent', 'pair_id');
            foreach ($losoers as $key => $value) {
                $this->looser_repository->createLooser([
                    'id' => $key,
                    'value' => $value
                ]);
            }
        }

//        $result = $this->trending_repository->deleteTrend();
//        if ($result) {
//            $ids = array();
//            $stop = Carbon::now()->toDateString();
//            $start = Carbon::now()->subHours(24)->toDateString();
//            $currency_trends = $this->trending_repository->dailyTrends($start, $stop);
//            foreach ($currency_trends as $currency_trend) {
//                $ids[] = $currency_trend->viewable_id;
//            }
//            $trends = collect(array_keys(array_count_values($ids)))->take(4);
//            foreach ($trends as $trend) {
//
//                $this->trending_repository->createTrend(['id'=>GlobalCurrency::query()->where('id', '=', $trend)->pluck('pair_id')[0]]);
//            }

//            $trends = $pairs->sortByDesc('financial.last24h.quote_volume')->filter(function ($value, $key) {
//                return !in_array($value['pair_id'], $this->exclude_trending_pairs);
//            })->take(10)->pluck('pair_id');
//
//            foreach ($trends as $trend) {
//                $this->trending_repository->createTrend(['id' => $trend]);
//            }
//        }

        return 0;
    }
}
