<?php

namespace App\Console\Commands;

use App\Jobs\UpdateGCurrencyRamzinexInfoJob;
use Illuminate\Console\Command;

class UpdateGCurrencyRamzinexInfoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'global_currency:update_ramzinex_info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        UpdateGCurrencyRamzinexInfoJob::dispatch();
    }
}
