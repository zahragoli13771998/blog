<?php

namespace App\Console\Commands;

use App\Jobs\UpdateLeftOverGlobalCurrenciesJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class UpdateLeftOverOHLCCommand extends Command
{
    /**
     * Include dispatch jobs interface in order to run jobs in background
     */
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'global_currency:update_left_over_global_currencies_ohlc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->dispatch(new \App\Jobs\UpdateLeftOverOHLCCommand());
    }
}

