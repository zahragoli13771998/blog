<?php

namespace App\Console\Commands;

use App\Jobs\UpdateWholeGlobalCurrencies;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Log;

class UpdateGlobalCurrenciesAllInOne extends Command
{
    /**
     * Include dispatch jobs interface in order to run jobs in background
     */
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'global_currency:update_all_in_one';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
//        try {
            $this->dispatchSync(new UpdateWholeGlobalCurrencies());
//            $this->dispatch(new UpdateWholeGlobalCurrencies());
//        }catch (\Exception|QueryException $ex){
//            Log::error($ex->getMessage());
//        }

    }
}
