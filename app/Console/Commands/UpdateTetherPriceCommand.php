<?php

namespace App\Console\Commands;

use App\Repositories\Interfaces\RxtTetherRepoInterface;
use Illuminate\Console\Command;

class UpdateTetherPriceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tether_price:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var RxtTetherRepoInterface
     */
    private RxtTetherRepoInterface $tether_repository;

    /**
     * UpdateTetherPriceCommand constructor.
     * @param RxtTetherRepoInterface $tether_repository
     * @return void
     */
    public function __construct(RxtTetherRepoInterface $tether_repository)
    {
        parent::__construct();
        $this->tether_repository = $tether_repository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->tether_repository->cacheTetherPrice();
        return 0;
    }
}
