<?php

namespace App\logic;

use App\Models\CommentReport;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class CommentReportManager
{
    private static $instance;

    /**
     * @return static
     */
    public static function getInstance()
    {

        if (!static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * @param int $comment_id
     * @return int
     */
    public function countReports(int $comment_id): int
    {
        return CommentReport::query()->where('comment_id', '=', $comment_id)->count();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function exists(array $data): bool
    {
        return CommentReport::query()->where('user_id', '=', $data['user_id'])
            ->where('comment_id', '=', $data['comment_id'])->exists();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function createReport(array $data): bool
    {
        $count = $this->countReports($data['comment_id']);

        if ($count < 10) {

            $report = new CommentReport();
            $report->user_id = $data['user_id'];
            $report->comment_id = $data['comment_id'];
            try {
                $report->save();
            } catch (QueryException | \Exception $ex) {
                Log::error('error while creating comment report' . $ex->getMessage());
                return false;
            }
            return true;
        }
        return true;


    }

}
