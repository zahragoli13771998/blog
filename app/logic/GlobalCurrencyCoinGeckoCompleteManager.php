<?php

namespace App\logic;



use Codenixsv\CoinGeckoApi\CoinGeckoClient;
use Illuminate\Support\Facades\Log;

class GlobalCurrencyCoinGeckoCompleteManager
{
    private static $instance;

    /**
     * @return static
     */
    public static function getInstance()
    {

        if (!static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    public function createGlobals(){
        Log::info('test1');
        $client = new CoinGeckoClient();
        $result = $client->coins()->getTickers('bitcoin');
        return $result;
    }
}
