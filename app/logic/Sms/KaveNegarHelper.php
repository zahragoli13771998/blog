<?php

namespace App\Logic\Sms;


use App\Models\GameMessage;
use Illuminate\Support\Facades\Log;
use Kavenegar\KavenegarApi;

class KaveNegarHelper
{
    private static $instance;
    private static $api;
    private static $sender;

    /**
     * @return static
     */
    public static function getInstance()
    {

        if (!static::$instance) {
            static::$instance = new static();
            static::$api = new KavenegarApi(config('kavenegar.apikey'));
//            static::$sender = config('sms.from_number',10002000100240);
            static::$sender =10004004004001;
        }
        return static::$instance;
    }

    public function status($messageIds)
    {
        return static::$api->Status($messageIds);
    }

    public function send($receptor, $message)
    {
        $res = static::$api->Send(10004004004001, $receptor, " کد تایید شما :$message");
        try {
            foreach ($res as $eachResult) {

                $gameMessage = new GameMessage();
                $gameMessage->code = $eachResult->message;
                $gameMessage->phone = $eachResult->receptor;
                $gameMessage->status = $eachResult->status;
                $gameMessage->plain_data = json_encode($eachResult);
                $gameMessage->message_id = $eachResult->messageid;
                $gameMessage->status_text = $eachResult->statustext;
                $gameMessage->save();
            }
        } catch (\Exception $e) {
            Log::error('error saving text messages logs');
            Log::error($e);
        }
    }
}
