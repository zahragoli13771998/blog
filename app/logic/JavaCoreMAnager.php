<?php

namespace App\Logic;


use App\Http\Controllers\ApiKeyController;
use App\Logic\Config\ConfigManager;
use App\Models\Invitation;
use App\Models\LoggedError;
use App\Models\Order;
use App\Models\Pair;
use App\Models\Withdraw;
use App\Models\WithdrawState;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class JavaCoreManager
{
    private static $instance;


    public static function getInstance()
    {

        if (!static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    public static string $baseUrl = 'http://172.22.17.4:8082/topics/requests';

    public static array $headers = [
        'Content-Type' => 'application/vnd.kafka.json.v2+json',
        'Accept' => 'application/vnd.kafka.v2+json'
    ];


    public function addUser(User $user): array
    {
        $postParams = ["records" => [["key" => 'add_user', "value" => ['userId' => $user->id]]]];
        try {
            $this->postHttpsResponseWithHeadersUsingProxy(static::$baseUrl,$postParams,static::$headers);
        }catch (\Exception $e){
            LoggedError::createSimpleLoggedError('javaCoreAddUser789456123',$e->getMessage().'__'.$e->getTraceAsString());
        }

    }


    public function submitOrder($orderId)
    {
        /** @var Order $order */
        $order = Order::query()->find($orderId);
        $userId = $order->user_id;
        $price = $order->price_calc;
        $orderType = 'GTC';
        $symbol = $order->pair_id;
        if ($order->amount1 > 0){//sell
            $side = 'BID';
        }else{//buy
            $side = 'ASK';
        }
        $postParams = ["records" => [["key" => 'submit_order', "value" => ['userId' => $userId , 'orderId' => $order->id, 'symbol' => $symbol, 'price' => intval($price * $order->pair->currency2->precision) , 'orderType' => $orderType,'size' => $order->amount2, 'side' => $side]]]];
        try {
            $this->postHttpsResponseWithHeadersUsingProxy(static::$baseUrl,$postParams,static::$headers);
        }catch (\Exception $e){
            LoggedError::createSimpleLoggedError('javaCoreSubmitOrder789456123',$e->getMessage().'__'.$e->getTraceAsString());
        }


    }

    public function createMarket(Pair $pair)
    {
        $postParams = ["records" => [["key" => 'create_market', "value" => ['symbolId' => $pair->id ,'baseCurrency' => $pair->currency2->id , 'quoteCurrency' => $pair->currency1->id , 'baseScaleK' => $pair->currency2->precision, 'quoteScaleK' => $pair->currency1->precision]]]];
        try {
            $this->postHttpsResponseWithHeadersUsingProxy(static::$baseUrl,json_encode($postParams),static::$headers);
        }catch (\Exception $e){
            LoggedError::createSimpleLoggedErrorForce('javaCoreCreateMarket789456123',$e->getMessage().'__'.$e->getTraceAsString());
        }

    }

    public function cancelOrder(Order $order)
    {
        $postParams = ["records" => [["key" => 'cancel_order', "value" => ['orderId' => $order->id , 'userId' => $order->user_id , 'symbol' => $order->pair_id]]]];
        try {
            $this->postHttpsResponseWithHeadersUsingProxy(static::$baseUrl,$postParams,static::$headers);
        }catch (\Exception $e){
            LoggedError::createSimpleLoggedError('javaCoreCancelOrder789456123',$e->getMessage().'__'.$e->getTraceAsString());
        }

    }

    public function postHttpsResponseWithHeadersUsingProxy($url, $postfields, $headers)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_PROXY, '94.182.157.73:31129');
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);


        $result = curl_exec($ch);
        return $result;
    }



}
