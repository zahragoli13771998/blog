<?php

namespace App\logic;

use Illuminate\Support\Facades\Cache;

class ConfigManager
{
    private static $instance;

    /**
     * @return static
     */
    public static function getInstance()
    {

        if (!static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * the number of likes for each banner in 1 min
     * @return int
     */
    public function like_dislike_banner_cache(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function currency_conversion(): int
    {
        return 2;
    }

    /**
     * the number of comments that can be added in 1 min
     * @return int
     */
    public function add_comment_cache(): int
    {
        return 2;
    }
    /**
     * the number of comments that can be added in 1 min
     * @return int
     */
    public function add_view_cache(): int
    {
        return 50;
    }
    public function disableAddToFavoriteGc()
    {
        return true;
    }

    public function enableLike()
    {
        return false;
    }
    public function disableAddToFavoriteBanner()
    {
        return true;
    }

    public function disableAddToFavoriteFaq()
    {
        return true;
    }
    public function numberOfAnyActionInAMinute()
    {
        return 5;
    }
}
