<?php


namespace App\Factories;


use App\Repositories\CoinGeckoRepository;
use App\Repositories\CoinPaprikaRepository;
use App\Repositories\Interfaces\GlobalCurrencyApiInterface;

class CoinApiRepositoryFactory
{
    /**
     * global currency api repositories factory
     * @param string $name
     * @return \Illuminate\Foundation\Application|mixed
     */
    public static function createService(string $name = ''): GlobalCurrencyApiInterface
    {
        switch ($name) {
            case 'coin_gecko':
                return resolve(CoinGeckoRepository::class);
            default:
                return resolve(CoinPaprikaRepository::class);
        }
    }
}
