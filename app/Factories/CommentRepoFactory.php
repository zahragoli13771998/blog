<?php

namespace App\Factories;

use App\Repositories\CommentRepository;
use App\Repositories\CommentVTwoRepository;
use App\Repositories\Interfaces\CommentRepositoryInterface;


class CommentRepoFactory
{
    /**
     * create CommentRepository or CommentVTwoRepository according to given name
     * @param string|null $name
     * @return CommentRepositoryInterface
     */
    public static function createRepository(string $name = null): CommentRepositoryInterface
    {
        switch ($name) {
            case 'comment_version_two':
                return resolve(CommentVTwoRepository::class);
            default:
                return resolve(CommentRepository::class);
        }
    }
}
