<?php


namespace App\Factories;


use App\Services\Curl\GuzzleCurl;
use App\Services\Interfaces\CurlServiceInterface;

class CurlerFactory
{
    public static function createCurler(string $name = ''): CurlServiceInterface
    {
        switch ($name)
        {
            case 'guzzle_curler':
                return resolve(GuzzleCurl::class);
            default:
                return resolve(CurlServiceInterface::class);
        }
    }
}