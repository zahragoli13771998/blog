<?php


namespace App\Factories;


use App\Services\Interfaces\LoggerServiceInterface;
use App\Services\Logger\LaravelLogger;

class LoggerFactory
{
    /**
     * Creates and returns desired logger driver
     * @param string $name
     * @return \Illuminate\Foundation\Application|mixed
     */
    public static function createLogger(string $name = ''): LoggerServiceInterface
    {
        switch ($name)
        {
            case "LaravelLogger":
                return resolve(LaravelLogger::class);
            default:
                return resolve(LoggerServiceInterface::class);
        }
    }
}