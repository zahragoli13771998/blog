<?php

namespace App\Services\CommentService;


use App\Factories\CommentRepoFactory;
use App\Models\Comment;
use App\Models\Interfaces\HasComments;
use App\Services\Interfaces\CommentFileHandlerService;
use App\Services\Interfaces\CommentServiceInterface;
use App\Services\Interfaces\UserAuthenticatorServiceInterface;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\CommentRepositoryInterface;

class CommentService implements CommentServiceInterface
{
    /**
     * Comment RepositoryInterface container
     * @var CommentRepositoryInterface container
     */
    private CommentRepositoryInterface $comment_repository;

    /**
     * User authenticator service container
     * @var UserAuthenticatorServiceInterface
     */
    protected UserAuthenticatorServiceInterface $user_authenticator_service;

    /**
     * @var CommentFileHandlerService
     */
    private CommentFileHandlerService $file_handler_service;

    /**
     * inject global currency repository
     * CurrenciesConversionService constructor.
     * @param CommentFileHandlerService $file_handler_service
     * @param UserAuthenticatorServiceInterface $user_authenticator_service
     */
    public function __construct(CommentFileHandlerService $file_handler_service, UserAuthenticatorServiceInterface $user_authenticator_service)
    {
        $this->comment_repository = CommentRepoFactory::createRepository('comment_version_two');
        $this->file_handler_service = $file_handler_service;
        $this->user_authenticator_service = $user_authenticator_service;
    }

    /**
     * create comment
     * @param HasComments $commentable_object
     * @param Request $request
     * @return Comment|null
     */
    public function createComment(HasComments $commentable_object, Request $request): ?Comment
    {

        $data = $request->only(['comment_id', 'depth_id', 'body', 'nickname', 'type', 'options', 'end_time', 'answer_index']);
        $data['user_id'] = $this->user_authenticator_service->getUserId($request);
        $comment = $this->comment_repository->createComment($commentable_object, $data);
        $result = $this->file_handler_service->createCommentFile($comment, $request);
        if (!$comment || !$result) {
            return null;
        }

        return $comment;
    }


    /**
     * update comment and file
     * @param Request $request
     * @return bool
     */
    public function updateComment(Request $request): bool
    {
        $comment = $this->comment_repository->getById($request->comment_id, false, true);
        $data = $request->only(['body', 'nickname']);

        $data['user_id'] = $this->user_authenticator_service->getUserId($request);

        if ($data['user_id'] != $comment->user_id) {//Todo move to policy and gaurds
            abort('403', 'Unauthorized action.');
        }

        $data['status'] = 0;

        if (isset($comment->file)) {
            $file = $this->file_handler_service->updateCommentFile($comment->file, $request);
        } else {
            $file = $this->file_handler_service->createCommentFile($comment, $request);
        }

        $is_updated = $this->comment_repository->updateComment($comment, $data);
        if (!$is_updated || !$file) {
            return false;
        }
        return true;
    }

    /**
     * delete comment and file
     * @param int $id
     * @param Request $request
     * @return bool
     */
    public function deleteComment(Request $request, int $id): bool
    {
        $comment = $this->comment_repository->getById($id);

        $user_id = $this->user_authenticator_service->getUserId($request);

        if ($user_id != $comment->user_id) {//Todo move to policy and gaurds
            abort('403', 'Unauthorized action.');
        }

        if (isset($comment->file)) {
            $delete_file_status = $this->file_handler_service->deleteCommentFile($comment->file);
        }
        $deleted = $this->comment_repository->delete($comment);
        if ((isset($delete_file_status) && $delete_file_status === false) || !$deleted) {
            return false;
        }

        return true;
    }
}
