<?php

namespace App\Services\PairCurrenciesInfoService;


use App\Repositories\Interfaces\GCurrencyRamzinexInfoInterface;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Repositories\Interfaces\PairsRepositoryInterface;
use App\Repositories\Interfaces\RamzinexCurrencyInfoApiInterface;
use App\Services\Interfaces\PairCurrenciesInfoServiceInterface;

class PairCurrenciesInfoService implements PairCurrenciesInfoServiceInterface
{
    /**
     * Global Currency repository container
     * @var GlobalCurrencyInterface
     */
    private GlobalCurrencyInterface $global_currency_repository;

    /**
     * Ramzinex pairs info reposository container
     * @var PairsRepositoryInterface
     */
    private PairsRepositoryInterface $ramzinex_currency_info_api;

    /**
     * Global ramzinex info Currency repository container
     * @var GCurrencyRamzinexInfoInterface
     */
    private GCurrencyRamzinexInfoInterface $currency_Ramzinex_Info_repository;

    /**
     * inject global currency repository
     * CurrenciesConversionService constructor.
     * @param GlobalCurrencyInterface $global_currency_repository
     * @param PairsRepositoryInterface $ramzinex_currency_info_api
     * @param GCurrencyRamzinexInfoInterface $currency_Ramzinex_Info_repository
     */
    public function __construct(GlobalCurrencyInterface $global_currency_repository, PairsRepositoryInterface $ramzinex_currency_info_api, GCurrencyRamzinexInfoInterface $currency_Ramzinex_Info_repository)
    {
        $this->global_currency_repository = $global_currency_repository;
        $this->ramzinex_currency_info_api = $ramzinex_currency_info_api;
        $this->currency_Ramzinex_Info_repository = $currency_Ramzinex_Info_repository;
    }

    /**
     * update all pair global currency ramzinex info
     * @return bool
     */
    public function updateRamzinexCurrenciesInfo(): bool
    {
        $currencies = $this->global_currency_repository->getPairGlobalCurrencies(true);
        foreach ($currencies as $currency) {
            $ramzinex_info = $this->global_currency_repository->isRamzinexCurrencyInfoExists($currency);
            $data = $this->createRamzinexInfo($currency->pair_id);
            if ($ramzinex_info) {
                $this->currency_Ramzinex_Info_repository->update($data, $ramzinex_info);
                continue;
            }
            $this->currency_Ramzinex_Info_repository->store($data, $currency);

        }
        return true;
    }

    /**
     * craete ramzinex info
     * @param int $pair_id
     * @return array
     */
    private function createRamzinexInfo(int $pair_id): array
    {
        $data = $this->ramzinex_currency_info_api->getAllPairsInfo($pair_id);
        $info = array();
        if (!is_null($data) && is_array($data)) {
            $info['buy'] = $data['buy'];
            $info['sell'] = $data['sell'];
            $info['open'] = $data['financial']['last24h']['open'];
            $info['close'] = $data['financial']['last24h']['close'];
            $info['high'] = $data['financial']['last24h']['highest'];
            $info['low'] = $data['financial']['last24h']['lowest'];
            $info['quote_volume'] = $data['financial']['last24h']['quote_volume'];
            $info['base_volume'] = $data['financial']['last24h']['base_volume'];
            $info['change_percent'] = $data['financial']['last24h']['change_percent'];
        }
        return $info;
    }
}
