<?php

namespace App\Services\TickerService;


use App\Factories\CoinApiRepositoryFactory;
use App\Models\GlobalCurrency;
use App\Models\Interval;
use App\Repositories\Interfaces\CurrencyChangesPercentRepositoryInterface;
use App\Repositories\Interfaces\CurrencyOHLCRepositoryInterface;
use App\Repositories\Interfaces\GlobalCurrencyApiInterface;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Repositories\Interfaces\RxtTetherRepoInterface;
use App\Services\Interfaces\TickerServiceInterface;
use Carbon\Carbon;

class TickerService implements TickerServiceInterface
{
    /**
     * Define max allowed requests number per second
     */
    const MIN_SECONDS_PER_REQUESTS = 10;

    /**
     * DB Global Currency Repository container
     * @var GlobalCurrencyInterface
     */
    private GlobalCurrencyInterface $global_currency_repository;

    /**
     * API Global Currency repository container
     * @var GlobalCurrencyApiInterface
     */
    private GlobalCurrencyApiInterface $global_currency_api_repository;

    /**
     * Currency changes percent repository container
     * @var CurrencyChangesPercentRepositoryInterface
     */
    protected CurrencyChangesPercentRepositoryInterface $currency_changes_percent_repository;

    /**
     * Currency ohlc repository container
     * @var CurrencyOHLCRepositoryInterface
     */
    protected CurrencyOHLCRepositoryInterface $currency_ohlc_repository;

    /**
     * RxtTether Repository container
     * @var RxtTetherRepoInterface
     */
    private RxtTetherRepoInterface $rxt_tether_repository;


    /**
     * TickerService constructor.
     * @param GlobalCurrencyInterface $global_currency_repository
     * @param CurrencyChangesPercentRepositoryInterface $currency_changes_percent_repository
     * @param CurrencyOHLCRepositoryInterface $currency_ohlc_repository
     * @param RxtTetherRepoInterface $rxt_tether_repository
     */
    public function __construct(GlobalCurrencyInterface $global_currency_repository, CurrencyChangesPercentRepositoryInterface $currency_changes_percent_repository, CurrencyOHLCRepositoryInterface $currency_ohlc_repository, RxtTetherRepoInterface $rxt_tether_repository)
    {
        $this->global_currency_api_repository = CoinApiRepositoryFactory::createService();
        $this->global_currency_repository = $global_currency_repository;
        $this->currency_changes_percent_repository = $currency_changes_percent_repository;
        $this->currency_ohlc_repository = $currency_ohlc_repository;
        $this->rxt_tether_repository = $rxt_tether_repository;
    }

    /**
     * create global currency list with api
     * @return bool
     */
    public function createGlobalCurrenciesList(): bool
    {
        $currencies = $this->global_currency_api_repository->getGlobalCurrenciesList();

        if (!count($currencies)) {
            return false;//Todo add Log with logger service
        }

        foreach ($currencies as $currency) {

            $api_currency = [];

            if (!$this->global_currency_repository->checkIfCurrencyExists(strtoupper($currency['symbol']))) {

                $api_currency['name_en'] = $currency['name'];
                $api_currency['name_fa'] = $currency['name'];
                $api_currency['symbol_id'] = $currency['id'];
                $api_currency['symbol'] = strtoupper($currency['symbol']);

                $this->global_currency_repository->createCurrency($api_currency);
            }

        }

        return true;
    }

    /**
     * take $take number of global currency from $from
     * and update
     * @param int|null $from
     * @param int|null $take
     * @return bool
     */
    public function updateAllGlobalCurrencyInformation(int $from = null, int $take = null): bool
    {
        $coins = $this->global_currency_repository
            ->getAllGlobalCurrencies(false, false, $from, $take, null, true);

        foreach ($coins as $coin) {
            $start_time = microtime(true);
            $data = $this->global_currency_api_repository
                ->getGlobalCurrencyTickerByID($coin['symbol_id']);

            if (!count($data)) {
                continue;
            }

            $this->updateGlobalCurrencyInfo($data, $coin);

            $this->createCurrencyChanges($data, $coin);

            $this->canFetchNextData($start_time);
        }
        return true;
    }

    /**
     * Update all whole existing global currencies
     * @return bool
     */
    public function updateWholeGlobalCurrencies(): bool
    {
        $global_currencies_infos = $this->global_currency_api_repository->tickersAllGlobalCurrencies();

        if (!count($global_currencies_infos)) {
            return false;
        }
        $globals_count = 0;
        foreach ($global_currencies_infos as $global_currencies_info) {
            $global_currency = $this->global_currency_repository->getById(null, false, false, null, $global_currencies_info['symbol']);
            $globals_count++;
            if ($globals_count == 500) {
                sleep(20);
                $globals_count = 0;
            }
            if (!$global_currency) {
                continue;
            }

            $this->updateGlobalCurrencyInfo($global_currencies_info, $global_currency);

            $this->createCurrencyChanges($global_currencies_info, $global_currency);
        }
        return true;
    }


    /**
     * updated one specific global currency
     * @param array $data
     * @param GlobalCurrency $global_currency
     * @return bool
     */
    public function updateGlobalCurrencyInfo(array $data, GlobalCurrency $global_currency): bool
    {
        $updated_data = [];

        if (isset($data['rank'])) {
            $updated_data['rank'] = $data['rank'];
        }
        if (isset($data['image'])) {
            $updated_data['icon'] = $data['image'];
        }

        if (isset($data['circulating_supply'])) {
            $updated_data['coin_circulation'] = $data['circulating_supply'];
        }

        if (isset($data['total_supply'])) {
            $updated_data['total_supply'] = $data['total_supply'];
        }

        if (isset($data['quotes']['USD']['price'])) {
            $updated_data['price'] = $data['quotes']['USD']['price'];
        }

        if (isset($data['quotes']['BTC']['price'])) {
            $updated_data['btc_price'] = $data['quotes']['BTC']['price'];
        }

        if (isset($data['quotes']['USD']['market_cap'])) {
            $updated_data['total_volume'] = $data['quotes']['USD']['market_cap'];
        }

        if (isset($data['quotes']['USD']['volume_24h'])) {
            $updated_data['daily_trading_volume'] = $data['quotes']['USD']['volume_24h'];
        }
        if (isset($data['quotes']['market_dominance'])) {
            $updated_data['market_dominance'] = $data['quotes']['market_dominance'];
        }

        if (isset($global_currency['pair_id']) && $global_currency->ramzinexInfo) {
            $updated_data['rial_price'] = $global_currency->ramzinexInfo->sell;

        } else {
            $tether_price = $this->rxt_tether_repository->getTetherPrice();
            $updated_data['rial_price'] = $tether_price * $global_currency['price'];
        }


        $this->global_currency_repository->update($updated_data, $global_currency);

        return true;
    }

    /**
     * create currency changes percent
     * @param array $data
     * @param GlobalCurrency $global_currency
     * @return void
     */
    private function createCurrencyChanges(array $data, GlobalCurrency $global_currency): void
    {
        if (isset($data['quotes']['USD']['percent_change_24h'])) {
            $this->createCurrencyChangePercent($global_currency, Interval::getDailyId(), $data['quotes']['USD']['percent_change_24h']);
        }
        if (isset($data['quotes']['USD']['percent_change_7d'])) {
            $this->createCurrencyChangePercent($global_currency, Interval::getWeeklyId(), $data['quotes']['USD']['percent_change_7d']);
        }
        if (isset($data['quotes']['USD']['percent_change_30d'])) {
            $this->createCurrencyChangePercent($global_currency, Interval::getMonthlyId(), $data['quotes']['USD']['percent_change_30d']);
        }

        if (isset($data['quotes']['USD']['percent_change_1y'])) {
            $this->createCurrencyChangePercent($global_currency, Interval::getYearlyId(), $data['quotes']['USD']['percent_change_1y']);
        }
    }

    /**
     * Create ohlc for global currencies
     * @param int|null $from
     * @param int|null $take
     * @return bool
     */
    public function createGlobalCurrenciesOHLC(int $from = null, int $take = null): bool
    {
        $coins = $this->global_currency_repository
            ->getAllGlobalCurrencies(false, false, $from, $take);

        $today = strtotime(Carbon::now()->toDateTimeString());
        $last_month = strtotime(Carbon::now()->subMonth()->toDateTimeString());
        foreach ($coins as $coin) {

            $start_time = microtime(true);
            $data = $this->global_currency_api_repository
                ->getGlobalCurrencyOHLC($coin['symbol_id'], $last_month, $today);

            if (!count($data)) {
                continue;
            }

            //Store Daily OHLC Data
            if (isset($data[array_key_last($data)])) {
                $today_data = $data[array_key_last($data)];
                $this->storeOHLCData($coin, $today_data, Interval::getDailyId());
            }

            //Store Weekly OHLC DATA
            if (isset($data[6])) {
                $weekly_data = $data[6];
                $this->storeOHLCData($coin, $weekly_data, Interval::getWeeklyId());
            }

            //Store Monthly OHLC Data
            if (isset($data[array_key_first($data)])) {
                $monthly_data = $data[array_key_first($data)];
                $this->storeOHLCData($coin, $monthly_data, Interval::getMonthlyId());
            }


            $this->canFetchNextData($start_time);
        }
        return true;
    }

    /**
     * Store OHLC Data into database
     * @param GlobalCurrency $currency
     * @param array $data
     * @param int $interval_id
     * @return void
     */
    public function storeOHLCData(GlobalCurrency $currency, array $data, int $interval_id)
    {
        if ($this->global_currency_repository->isCurrencyOHLCExists($currency, $interval_id)) {
            $ohlc = $this->currency_ohlc_repository->getOHLC([
                'interval_id' => $interval_id,
                'global_currency_id' => $currency->id
            ]);

            if ($ohlc) {
                $this->currency_ohlc_repository->update([
                    'high' => $data['high'] ?? 0,
                    'open' => $data['open'] ?? 0,
                    'low' => $data['low'] ?? 0,
                    'close' => $data['close'] ?? 0,
                    'volume' => $data['volume'] ?? 0,
                    'market_cap' => $data['market_cap'] ?? 0
                ], $ohlc);
            }
            return;
        }
        $this->global_currency_repository->disableCurrencyOHLC($currency, $interval_id);
        $this->global_currency_repository->createCurrencyOHLC($currency, [
            'interval_id' => $interval_id,
            'high' => $data['high'] ?? 0,
            'open' => $data['open'] ?? 0,
            'low' => $data['low'] ?? 0,
            'close' => $data['close'] ?? 0,
            'volume' => $data['volume'] ?? 0,
            'market_cap' => $data['market_cap'] ?? 0
        ]);
    }

    /**
     * Check if next request is executable or not
     * @param $start_time
     */
    private function canFetchNextData($start_time)
    {
        $end_time = microtime(true);
        $exec_time = $end_time - $start_time;

        if ($exec_time < static::MIN_SECONDS_PER_REQUESTS) {
            $wait_time = static::MIN_SECONDS_PER_REQUESTS - round($exec_time);
            sleep($wait_time);
        }
    }

    /**
     * Create/Update Currency change percent
     * @param GlobalCurrency $global_currency
     * @param int $interval_id
     * @param float $value
     * @return void
     */
    private function createCurrencyChangePercent(GlobalCurrency $global_currency, int $interval_id, float $value)
    {
        if ($this->global_currency_repository->isCurrencyChangePercentExists($global_currency, $interval_id)) {
            $change_percent = $this->currency_changes_percent_repository->getChangesPercent([
                'interval_id' => $interval_id,
                'global_currency_id' => $global_currency->id
            ]);

            if ($change_percent) {
                $this->currency_changes_percent_repository->update([
                    'value' => $value
                ], $change_percent);
            }
            return;
        }

        $this->global_currency_repository->disableCurrencyChangesPercent($global_currency, $interval_id);
        $this->global_currency_repository->createCurrencyChange([
            'value' => $value,
            'interval_id' => $interval_id
        ], $global_currency);
    }
}
