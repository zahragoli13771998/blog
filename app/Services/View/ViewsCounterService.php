<?php


namespace App\Services\View;

use App\Models\Interfaces\HasViews;
use App\Repositories\Interfaces\ViewRepositoryInterface;
use App\Services\Interfaces\ViewCounterServiceInterface;
use Illuminate\Http\Request;

class ViewsCounterService implements ViewCounterServiceInterface
{
    /**
     * View Repository container
     * @var ViewRepositoryInterface
     */
    protected ViewRepositoryInterface $view_repository;

    /**
     * Construct and inject necessary services to their related services
     * ViewsCounterService constructor.
     * @param ViewRepositoryInterface $view_repository
     */
    public function __construct(ViewRepositoryInterface $view_repository)
    {
        $this->view_repository = $view_repository;
    }

//    /**
//     * Add Views
//     * @param Request $request
//     * @param HasViews $viewable_object
//     * @return bool
//     */
//    public function addViews(Request $request, HasViews $viewable_object): bool
//    {
//        $cookie = md5(implode(',', $request->cookie()));
//        $ip = $request->ip();
//        $agent = md5($request->userAgent());
//
//        $has_view = $this->view_repository->hasViews($viewable_object, $ip, $agent);
//
//        if($has_view){
//            return false;
//        }
//
//        $result = $this->view_repository->addViews($viewable_object, [
//            'cookie_string' => $cookie,
//            'ip' => $ip,
//            'browser_agent' => $agent
//        ]);
//
//        if(!$result){
//            return false;
//        }
//
//        return true;
//    }

    public function addViews(HasViews $viewable_object,int $user_id = null): bool
    {
        $has_view = $this->view_repository->hasViews($viewable_object,$user_id);

        if($has_view){
            return false;
        }

        $result = $this->view_repository->addViews($viewable_object, [
           'user_id'=>$user_id
        ]);
        if(!$result){
            return false;
        }
        return true;
      }
}
