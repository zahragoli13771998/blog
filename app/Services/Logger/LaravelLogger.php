<?php


namespace App\Services\Logger;


use App\Services\Interfaces\LoggerServiceInterface;
use Illuminate\Support\Facades\Log;

class LaravelLogger implements LoggerServiceInterface
{
    /**
     * Define logger enabled or disabled
     * @var bool
     */
    protected bool $enabled = false;

    /**
     * Turn on logger
     * @return mixed|void
     */
    public function turnOn()
    {
        $this->enabled = true;
    }

    /**
     * Turn off logger
     * @return mixed|void
     */
    public function turnOff()
    {
        $this->enabled = false;
    }

    /**
     * Checks if logger is enabled or not, if enabled makes log
     * @param string $message
     * @param string $type
     * @void
     */
    public function log(string $message, string $type = 'info')
    {
        if(isset($this->enabled) && $this->enabled){
            $this->makeLog($type, $message);
        }
    }

    /**
     * creating log
     * @param string $type
     * @param string $message
     * @void
     */
    public function makeLog(string $type, string $message)
    {
        Log::$type($message);
    }
}