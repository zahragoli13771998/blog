<?php

namespace App\Services\LikeDislikeService;


use App\Models\Interfaces\HasDisLike;
use App\Models\Interfaces\HasLikes;
use App\Repositories\Interfaces\DislikeRepositoryInterface;
use App\Repositories\Interfaces\LikeRepositoryInterface;
use App\Services\UserAuthentication\UserAuthenticatorService;
use App\Services\Interfaces\LikeDislikeServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

/**
 * Class LikeDislikeService
 * @package App\Services\LikeDislikeService
 */
class LikeDislikeService implements LikeDislikeServiceInterface
{

    /**
     * User Authenticator Service container
     * @var UserAuthenticatorService
     */
    protected UserAuthenticatorService $user_authenticator_service;

    /**
     * like repository container
     * @var LikeRepositoryInterface
     */
    private LikeRepositoryInterface $like_repository;

    /**
     * dislike repository container
     * @var DislikeRepositoryInterface
     */
    private DislikeRepositoryInterface $dislike_repository;

    /**
     * inject global currency repository
     * CurrenciesConversionService constructor.
     * @param UserAuthenticatorService $user_authenticator_service
     * @param LikeRepositoryInterface $like_repository
     * @param DislikeRepositoryInterface $dislike_repository
     */
    public function __construct(UserAuthenticatorService $user_authenticator_service, LikeRepositoryInterface $like_repository, DislikeRepositoryInterface $dislike_repository)
    {
        $this->user_authenticator_service = $user_authenticator_service;
        $this->like_repository = $like_repository;
        $this->dislike_repository = $dislike_repository;
    }


    /**
     * create like
     * @param HasLikes $likeable_object
     * @param Request $request
     * @return bool
     */
    public function createLike(HasLikes $likeable_object, Request $request , string $type = null): bool
    {
        $user_id = $this->user_authenticator_service->getUserId($request);

        if ($this->like_repository->isLiked($likeable_object, $user_id)) {
            $result = $this->like_repository->deleteLike($likeable_object, $user_id,$type);
        } else {
            $result = $this->like_repository->createLike($likeable_object, $user_id,$type);

            $this->dislike_repository->deleteDislike($likeable_object, $user_id,$type);//Todo Improve code quality
        }
        return $result;
    }

    /**
     * create dislike
     * @param HasDisLike $dis_likeable_object
     * @param Request $request
     * @return bool
     */
    public function createDislike(HasDisLike $dis_likeable_object, Request $request, string $type = null): bool
    {

        $user_id = $this->user_authenticator_service->getUserId($request);

        if ($this->dislike_repository->isDisliked($dis_likeable_object, $user_id)) {
            $result = $this->dislike_repository->deleteDislike($dis_likeable_object, $user_id,$type);

        } else {
            $result = $this->dislike_repository->createDisLike($dis_likeable_object, $user_id,$type);
            $this->like_repository->deleteLike($dis_likeable_object, $user_id,$type);//Todo Improve code quality
        }
        return $result;
    }
}
