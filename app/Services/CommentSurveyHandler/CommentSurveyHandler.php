<?php


namespace App\Services\CommentSurveyHandler;


use App\Models\Comment;
use App\Repositories\Interfaces\SurveyAnswerRepositoryInterface;
use App\Repositories\Interfaces\SurveyOptionRepositoryInterface;
use App\Repositories\Interfaces\SurveyRepositoryInterface;
use App\Services\Interfaces\CommentSurveyHandlerService;
use Illuminate\Http\Request;


class CommentSurveyHandler implements CommentSurveyHandlerService
{
    /**
     * @var SurveyRepositoryInterface
     */
    private SurveyRepositoryInterface $survey_repository;

    /**
     * @var SurveyOptionRepositoryInterface
     */
    private SurveyOptionRepositoryInterface $survey_option_repository;

    /**
     * @var SurveyAnswerRepositoryInterface
     */
    private SurveyAnswerRepositoryInterface $survey_answer_repository;

    /**
     * @param SurveyRepositoryInterface $survey_repository
     * @param SurveyOptionRepositoryInterface $survey_option_repository
     * @param SurveyAnswerRepositoryInterface $survey_answer_repository
     */
    public function __construct(SurveyRepositoryInterface $survey_repository, SurveyOptionRepositoryInterface $survey_option_repository, SurveyAnswerRepositoryInterface $survey_answer_repository)
    {
        $this->survey_repository = $survey_repository;
        $this->survey_option_repository = $survey_option_repository;
        $this->survey_answer_repository = $survey_answer_repository;
    }


    /**
     * Create survey for comment
     * @param Comment $comment
     * @param Request $request
     * @return bool
     */
    public function createCommentSurvey(Comment $comment, Request $request): bool
    {
        if ($request->has('survey')) {
            //create survey
            $survey = $this->survey_repository->createSurvey($comment, ['end_time' => $request['survey']['end_time']]);
            // create survey options
            foreach ($request['survey']['options'] as $option) {
                $this->survey_option_repository->createSurveyOption($survey, ['body' => $option]);
            }
        }
        return true;
    }


    /**
     * @param Request $request
     * @param int $user_id
     * @return bool
     */
    public function submitSurvey(Request $request, int $user_id): bool
    {

        //destroy past survey answer
        $this->survey_answer_repository->deleteAllUserSubmitted($user_id, $request['survey_id']);
        //create new survey answer
        $survey_option = $this->survey_option_repository->getSurveyOptionById($request['survey_option_id']);
        $created = $this->survey_answer_repository->createSurveyAnswer($survey_option, ['survey_id' => $request['survey_id'], 'user_id' => $user_id]);
        if (!$created) {
            return false;
        }
        return true;
    }

    /**
     * return survey result
     * @param int $survey_id
     * @return array
     */
    public function returnSurveyResults(int $survey_id): array
    {
        $survey = $this->survey_repository->returnSurveyResult($survey_id);
        $options = $this->survey_option_repository->getSurveyOptionWithAnswers($survey_id);
        return ['survey' => $survey, 'options' => $options];
    }
}
