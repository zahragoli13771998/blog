<?php

namespace App\Services\CommentFileHandler;


use App\Models\Comment;
use App\Models\CommentFile;
use Exception;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Services\Interfaces\CommentFileHandlerService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class CommentFileHandler implements CommentFileHandlerService
{
    /** Comment Repository Interface container
     * @var CommentRepositoryInterface
     */
    private CommentRepositoryInterface $comment_repository;

    /**
     * inject global currency repository
     * CurrenciesConversionService constructor.
     * @param CommentRepositoryInterface $comment_repository
     */
    public function __construct(CommentRepositoryInterface $comment_repository)
    {
        $this->comment_repository = $comment_repository;
    }


    /**
     * Create file for comment
     * @param Comment $comment
     * @param Request $request
     * @return bool
     */
    public function createCommentFile(Comment $comment, Request $request): bool
    {
        if ($request->has('file')) {
            $file['path'] = $request->file('file')->store('comment', 'public');
            $file['type'] = getimagesize($request->file('file')) ? 'image' : 'file';
            $result = $this->comment_repository->createCommentFile($comment, $file);
            if (!$result) {
                return false;
            }
        }
        return true;
    }

    /**
     * Update existing comment file
     * @param CommentFile $comment_file
     * @param Request $request
     * @return bool
     */
    public function updateCommentFile(CommentFile $comment_file, Request $request): bool
    {
        if ($request->has('file')) {
            $this->deleteCommentFile($comment_file);
            $file['path'] = $request->file('file')->store('comment', 'public');
            $file['type'] = getimagesize($request->file('file')) ? 'image' : 'file';
            $result = $this->comment_repository->updateCommentFile($comment_file, $file);
            if ($result) {
                return true;
            }
        }
        return false;
    }

    /**
     * Delete Existing comment file
     * @param CommentFile $comment_file
     * @return bool
     */
    public function deleteCommentFile(CommentFile $comment_file): bool
    {
        try {
            Storage::delete($comment_file->path);
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return false;
        }
        return true;
    }
}
