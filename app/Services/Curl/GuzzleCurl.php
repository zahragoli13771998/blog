<?php


namespace App\Services\Curl;


use App\Factories\LoggerFactory;
use App\Services\Interfaces\CurlServiceInterface;
use App\Services\Interfaces\LoggerServiceInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class GuzzleCurl implements CurlServiceInterface
{

    /**
     * Guzzle http client container
     * @var Client
     */
    protected Client $client;

    /**
     * Query strings container
     * @var array
     */
    protected array $query_strings;

    /**
     * Request section Container
     * @var string
     */
    protected string $url;

    /**
     * Request Headers
     * @var array
     */
    protected array $headers;

    /**
     * Logger Service Container
     * @var LoggerServiceInterface|\Illuminate\Foundation\Application|mixed
     */
    protected LoggerServiceInterface $logger;

    /**
     * GuzzleCurl constructor.
     * Construct necessary classes and assign passed values to their container
     * @param string $base_url
     * @param string $url_section
     * @param array $query_strings
     */
    public function __construct(string $base_url = null, string $url_section = '', array $query_strings = [])
    {
        if($base_url){
            $this->client = new Client([
                'base_uri' => $base_url,
                'timeout' => 10.0
            ]);
            return $this;
        }

        $this->logger = LoggerFactory::createLogger();
        $this->logger->turnOn();

        if($url_section){
            $this->url = $url_section;
        }

        if(count($query_strings)){
            $this->query_strings = $query_strings;
        }

    }

    /**
     * Create Guzzle client
     * @param string $base_url
     * @return $this
     */
    public function createClient(string $base_url): CurlServiceInterface
    {
        $this->client = new Client([
            'base_uri' => $base_url,
            'timeout' => 50.0
        ]);
        return $this;
    }

    /**
     * Set Query strings that needs to send over http request (post, get)
     * @param array $query_strings
     * @return $this
     */
    public function setQueryStrings(array $query_strings): CurlServiceInterface
    {
        if(is_array($query_strings) && count($query_strings)){
            $this->query_strings = $query_strings;
        }

        return $this;
    }

    /**
     * Set section that wee need to send request over it
     * @param string $url
     * @return $this
     */
    public function setUrlSection(string $url): CurlServiceInterface
    {
        if($url && is_string($url)){
            $this->url = $url;
        }
        return $this;
    }

    /**
     * Set Request Headers
     * @param mixed ...$headers
     * @return CurlServiceInterface
     */
    public function setHeaders(array $headers): CurlServiceInterface
    {
        if(count($headers)){
            $this->headers = $headers;
        }

        return $this;
    }

    /**
     * Checks is request can execute, then executes it and returns desired response
     * @param string $method
     * @param string|null $content_type
     * @return array
     */
    public function executeRequest(string $method = 'GET', string $content_type = null)
    {
        if(!$this->requestCanExecute()){
            return ['status' => false, 'content' => 'Request is not prepared'];
        }

        $req_content_method = $this->getRequestContentParameter($method, $content_type);

        $request_body = $this->prepareRequestBody($req_content_method);

        try {
            $response = $this->client->request($method, $this->url, $request_body);
        } catch (GuzzleException $e) {
            $this->logger->log('unable to execute Guzzle curl request in crypto service update with error: '.$e->getMessage());
        }

        if(!isset($response) || !$response->getStatusCode() == 200){
            return ['status' => false, 'content' => "request failed"];
        }

        return ['status' => true, 'content' => json_decode($response->getBody(),true)];
    }

    /**
     * Check if params are set to get executed or not
     * @return bool
     */
    public function requestCanExecute()
    {
        if(!isset($this->url) || !is_string($this->url)){
           return false;
        };

        if(!isset($this->client)){
            return false;
        }

        return true;
    }

    /**
     * Converts verb for body contents of guzzle parameter sender title
     * @param string $method
     * @param string|null $content_type
     * @return string
     */
    public function getRequestContentParameter(string $method, string $content_type = null): string
    {
        if (strtolower($method) == 'get'){
            return 'query';
        }

        if(!$content_type){
            return 'form_params';
        }

        return $content_type;
    }

    /**
     * prepare and return guzzle request body
     * @param string $request_content_method
     * @return array
     */
    private function prepareRequestBody(string $request_content_method):array
    {
        $request_body = array();
        if(isset($this->query_strings) && count($this->query_strings))
        {
            $request_body[$request_content_method] = $this->query_strings;
        }

        if(isset($this->headers) && count($this->headers)){
            $request_body['headers'] = $this->headers;
        }
        return $request_body;
    }
}
