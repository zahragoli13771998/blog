<?php


namespace App\Services\Interfaces;


interface CurrenciesConversionServiceInterface
{


    /**
     * @param int $base_currency_id
     * @param int $converting_currency_id
     * @param float|int|null $converting_currency_count
     * @return float
     */
    public function convertCurrencies(int $base_currency_id, int $converting_currency_id, float $converting_currency_count = 1): float;

}
