<?php


namespace App\Services\Interfaces;


use App\Models\Comment;
use Illuminate\Http\Request;

interface CommentSurveyHandlerService
{

    /**
     * create comment survey
     * @param Comment $comment
     * @param Request $request
     * @return bool
     */
    public function createCommentSurvey(Comment $comment, Request $request): bool;

    /**
     * create comment survey answer
     * @param Request $request
     * @param int $user_id
     * @return bool
     */
    public function submitSurvey(Request $request, int $user_id): bool;

    /**
     * survey result
     * @param int $survey_id
     * @return array
     */
    public function returnSurveyResults(int $survey_id): array;


}
