<?php


namespace App\Services\Interfaces;

use App\Models\GlobalCurrency;

interface TickerServiceInterface
{
    /**
     * Create global Currencies list
     * @return mixed
     */
    public function createGlobalCurrenciesList(): bool;

    /**
     * update $take number of model from $from
     * @param int|null $from
     * @param int|null $take
     * @return mixed
     */
    public function updateAllGlobalCurrencyInformation(int $from = null, int $take = null): bool;

    /**
     * Update all whole existing global currencies
     * @return bool
     */
    public function updateWholeGlobalCurrencies(): bool;

    /**
     * update global currency info
     * @param array $data
     * @param GlobalCurrency $global_currency
     * @return bool
     */
    public function updateGlobalCurrencyInfo(array $data, GlobalCurrency $global_currency): bool;

    /**
     * Create ohlc for global currencies
     * @param int|null $from
     * @param int|null $take
     * @return bool
     */
    public function createGlobalCurrenciesOHLC(int $from = null, int $take = null): bool;
    /**
     * store ohlc for one global currency
     * @param GlobalCurrency $currency
     * @param array $data
     * @param int $interval_id
     * @return mixed
     */
    public function storeOHLCData(GlobalCurrency $currency, array $data, int $interval_id);

}
