<?php


namespace App\Services\Interfaces;


use App\Models\Interfaces\HasViews;
use Illuminate\Http\Request;

interface ViewCounterServiceInterface
{
//    /**
//     * Add Views
//     * @param Request $request
//     * @param HasViews $viewable_object
//     * @return bool
//     */
//    public function addViews(Request $request, HasViews $viewable_object): bool;

    public function addViews(HasViews $viewable_object,int $user_id = null): bool;
}
