<?php


namespace App\Services\Interfaces;

use App\Models\Interfaces\HasDisLike;
use App\Models\Interfaces\HasLikes;
use Illuminate\Http\Request;


interface LikeDislikeServiceInterface
{
    /**
     * create like
     * @param HasLikes $likeable_object
     * @param Request $request
     * @param string|null $type
     * @return bool
     */
    public function createLike(HasLikes $likeable_object, Request $request, string $type = null): bool;

    /**
     * create dislike
     * @param HasDisLike $dis_likeable_object
     * @param Request $request
     * @param string|null $type
     * @return bool
     */
    public function createDislike(HasDisLike $dis_likeable_object, Request $request, string $type = null): bool;
}
