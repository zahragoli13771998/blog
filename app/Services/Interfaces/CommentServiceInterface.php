<?php


namespace App\Services\Interfaces;

use App\Models\Comment;
use App\Models\Interfaces\HasComments;
use Illuminate\Http\Request;


interface CommentServiceInterface
{
    /**
     * create comment
     * @param HasComments $commentable_object
     * @param Request $request
     * @return Comment|null
     */
    public function createComment(HasComments $commentable_object, Request $request): ?Comment;

    /**
     * update comment
     * @param Request $request
     * @return bool
     */
    public function updateComment(Request $request): bool;

    /**
     * delete comment
     * @param Request $request
     * @param int $id
     * @return bool
     */
    public function deleteComment(Request $request, int $id): bool;
}
