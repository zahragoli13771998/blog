<?php


namespace App\Services\Interfaces;


interface LoggerServiceInterface
{
    /**
     * Turns Logger on
     * @return mixed
     */
    public function turnOn();

    /**
     * Turns logger of
     * @return mixed
     */
    public function turnOff();

    /**
     * Makes log
     * @param string $message
     * @param string $type
     * @return mixed
     */
    public function log(string $message, string $type = 'info');

    /**
     * Create Log
     * @param string $type
     * @param string $message
     * @return mixed
     */
    public function makeLog(string $type, string $message);
}