<?php


namespace App\Services\Interfaces;


use App\Services\Curl\GuzzleCurl;

interface CurlServiceInterface
{
    /**
     * GuzzleCurl constructor.
     * Construct necessary classes and assign passed values to their container
     * @param string $base_url
     * @param string $url_section
     * @param array $query_strings
     */
    public function __construct(string $base_url, string $url_section = '', array $query_strings = []);

    /**
     * Create Guzzle client
     * @param string $base_url
     * @return $this
     */
    public function createClient(string $base_url): CurlServiceInterface;

    /**
     * Set Query strings that needs to send over http request (post, get)
     * @param array $query_strings
     * @return $this
     */
    public function setQueryStrings(array $query_strings): CurlServiceInterface;

    /**
     * Set section that wee need to send request over it
     * @param string $url
     * @return $this
     */
    public function setUrlSection(string $url): CurlServiceInterface;

    /**
     * Set Request Headers
     * @param mixed ...$headers
     * @return CurlServiceInterface
     */
    public function setHeaders(array $headers): CurlServiceInterface;

    /**
     * Checks is request can execute, then executes it and returns desired response
     * @param string $method
     * @param string|null $content_type
     * @return array
     */
    public function executeRequest(string $method = 'GET', string $content_type = null);

    /**
     * Check if params are set to get executed or not
     * @return bool
     */
    public function requestCanExecute();
}