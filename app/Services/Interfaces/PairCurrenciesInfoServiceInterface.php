<?php


namespace App\Services\Interfaces;


interface PairCurrenciesInfoServiceInterface
{
    /**
     * update all pair global currency ramzinex info
     * @return bool
     */
    public function updateRamzinexCurrenciesInfo():bool;
}
