<?php


namespace App\Services\Interfaces;


use App\Models\Comment;
use App\Models\CommentFile;
use Illuminate\Http\Request;

interface CommentFileHandlerService
{

    /**
     * create image file
     * @param Comment $comment
     * @param Request $request
     * @return bool
     */
    public function createCommentFile(Comment $comment, Request $request): bool;

    /**
     * update image file
     * @param CommentFile $comment_file
     * @param Request $request
     * @return bool
     */
    public function updateCommentFile(CommentFile $comment_file, Request $request): bool;

    /**
     * delete image file from storage
     * @param CommentFile $comment_file
     * @return bool
     */
    public function deleteCommentFile(CommentFile $comment_file): bool;

}
