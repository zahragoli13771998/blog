<?php

namespace App\Services\CurrenciesConversion;


use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Services\Interfaces\CurrenciesConversionServiceInterface;

class CurrenciesConversionService implements CurrenciesConversionServiceInterface
{
    /**
     * @var GlobalCurrencyInterface
     */
    private GlobalCurrencyInterface $global_currency_repository;

    /**
     * inject global currency repository
     * CurrenciesConversionService constructor.
     * @param GlobalCurrencyInterface $global_currency_repository
     */
    public function __construct(GlobalCurrencyInterface $global_currency_repository)
    {
        $this->global_currency_repository = $global_currency_repository;
    }


    /**
     * @param int $base_currency_id
     * @param int $converting_currency_id
     * @param float|int|null $converting_currency_count
     * @return float
     */
    public function convertCurrencies(int $base_currency_id, int $converting_currency_id, float $converting_currency_count = 1): float
    {
        $converting_currency = $this->global_currency_repository->getById($converting_currency_id);
        $base_currency = $this->global_currency_repository->getById($base_currency_id);

        if ($converting_currency->price) {
            $price = $converting_currency_count * ($base_currency->price / $converting_currency->price);
            if (!is_null($base_currency->price_precision)) {
                $price = round($price,$base_currency->price_precision);
            }
            return $price;
        }

        return 0;
    }
}
