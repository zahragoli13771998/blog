<?php

namespace App\Events;

use App\Http\Resources\GlobalCurrenciesList;
use App\Models\GlobalCurrency;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class GlobalCurrenciesUpdatedEvent implements ShouldBroadcast, ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Global Currency container
     * @var GlobalCurrency
     */
    protected GlobalCurrency $global_currency;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(GlobalCurrency $global_currency)
    {
        $this->global_currency = $global_currency;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['global_currencies'];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith(): array
    {
        return ['global_currencies' =>
            [$this->global_currency->id => new GlobalCurrenciesList($this->global_currency)]
        ];
    }
}
