<?php

namespace App\Repositories;

use App\Repositories\Interfaces\BannerTypeRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use App\Models\BannerType;

class BannerTypeRepository implements BannerTypeRepositoryInterface
{
    /**
     * paginate banner types.
     * @param int $perpage
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate(int $perpage =10):LengthAwarePaginator
    {
        return $this->fetchQueryBuilder()->paginate();
    }

    /**
     * Get the value from the database.
     *
     * @return Builder[]|Collection
     */
    public function getAll():Collection
    {
        return $this->fetchQueryBuilder()->get();
    }

    /**
     * find by id the record with the given id.
     *
     * @param int $id
     * @return BannerType
     */
     public function findById(int $id):BannerType
    {
        return $this->fetchQueryBuilder()->findOrFail($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param array $value
     * @return bool
     */
     public function store(array $value):bool
    {
        $banner_types = new BannerType();
        $banner_types->title_fa = $value['title_fa'];
        $banner_types->title_en = $value['title_en'];
        try {
            $banner_types->save();
        }catch(QueryException $queryException){
            Log::error($queryException->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param array $value
     * @param BannerType $banner_types
     * @return bool
     */
     public function update(array $value, BannerType $banner_types):bool
    {
        $banner_types->title_fa = $value['title_fa'];
        $banner_types->title_en = $value['title_en'];
        try {
            $banner_types->save();
        }catch (QueryException $queryException){
            Log::error($queryException->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param BannerType $banner_types
     * @return bool
     * @throws \Exception
     */
     public function delete(BannerType $banner_types):bool
    {
        try {
            $banner_types->delete();
        }catch (QueryException $queryException){
            Log::error($queryException->getMessage());
            return false;
        }
        return true;
    }

    /**
     * fetch query builder indicator.
     *
     * @return Builder|mixed
     */
    private function fetchQueryBuilder():Builder
    {
        return BannerType::query();
    }

}
