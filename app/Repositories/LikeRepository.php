<?php

namespace App\Repositories;

use App\Models\Interfaces\HasLikes;
use App\Repositories\Interfaces\LikeRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class LikeRepository implements LikeRepositoryInterface
{
    private static $instance;

    /**
     * @return static
     */
    public static function getInstance()
    {

        if (!static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }


    /**
     * Add Users likes on related entity
     * @param HasLikes $likeable
     * @param int $user_id
     * @param string|null $type
     * @return boolean
     */
    public function createLike(HasLikes $likeable, int $user_id, string $type = null): bool
    {
        try {
            $likeable->likes()->attach($user_id);
            $likes_list = 'user_' . $user_id . '_liked_' . $type . '_list';
            $users_likes_list = $this->getUserLikesList($user_id, $type);
            Cache::put($likes_list, $users_likes_list, 86400);
        } catch (QueryException | \Exception$e) {
            Log::error('unable to add dislike with error: ' . $e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Check if likeable is liked before or not
     * @param HasLikes $likeable
     * @param int $user_id
     * @return bool
     */
    public function isLiked(HasLikes $likeable, int $user_id): bool
    {
        return $likeable->likes()->where('user_id', '=', $user_id)->exists();
    }

    /**
     * Get and return likes count for related entity
     * @param HasLikes $likeable
     * @return int
     */
    public function getLikeCount(HasLikes $likeable): int
    {
        return $likeable->likes()->count();
    }

    /**
     * Delete single like or likes for related entity
     * @param HasLikes $likeable
     * @param int|null $user_id
     * @param string|null $type
     * @return bool
     */
    public function deleteLike(HasLikes $likeable, int $user_id = null, string $type = null): bool
    {
        try {
            $likeable->likes()->detach($user_id);
            $likes_list = 'user_' . $user_id . '_liked_' . $type . '_list';
            if (Cache::has($likes_list)) {
                Cache::forget($likes_list);
            }
        } catch (\Exception | QueryException $ex) {
            Log::error($ex->getMessage());
            return false;
        }
        return true;

    }

    /**
     * @param int $user_id
     * @param string|null $type
     * @return Collection|string
     */
    public function getUserLikesList(int $user_id, string $type = null)
    {
        switch ($type) {
            case 'global':
                return DB::table('user_likes')->where('user_id', '=', $user_id)->where('likeable_type', '=', 'App\Models\GlobalCurrency')->pluck('likeable_id');
            case 'banner':
                return DB::table('user_likes')->where('user_id', '=', $user_id)->where('likeable_type', '=', 'App\Models\Banner')->pluck('likeable_id');
            case 'faq':
                return DB::table('user_likes')->where('user_id', '=', $user_id)->where('likeable_type', '=', 'App\Models\FaqQuestionAnswer')->pluck('likeable_id');
            case 'comment':
                return DB::table('user_likes')->where('user_id', '=', $user_id)->where('likeable_type', '=', 'App\Models\Comment')->pluck('likeable_id');
            default :
                return '**';
        }
    }

    /**
     * @param int $user_id
     * @return Collection
     */
    public function getUserCommentsLikesList(int $user_id): Collection
    {
        return DB::table('user_likes')->where('user_id', '=', $user_id)->where('likeable_type', '=', 'App\Models\Comment')->pluck('likeable_id');
    }
}
