<?php

namespace App\Repositories;


use App\Models\Interfaces\HasDisLike;
use App\Repositories\Interfaces\DislikeRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DislikeRepository implements DislikeRepositoryInterface
{
    /**
     * Create dislike for user on related entity
     * @param HasDisLike $dislikeable
     * @param int $user_id
     * @param string|null $type
     * @return boolean
     */
    public function createDislike(HasDisLike $dislikeable, int $user_id, string $type = null): bool
    {
        try {
            $dislikeable->dislikes()->attach($user_id);
            $users_likes_list = $this->getUserDisLikesList($user_id, $type);
            $dis_likes_list = 'user_' . $user_id . '_disliked_' . $type . '_list';
            Cache::put($dis_likes_list, $users_likes_list, 86400);
        } catch (QueryException | \Exception$e) {
            Log::error('unable to add dislike with error: ' . $e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * @param HasDisLike $dislikeable
     * @return int|mixed
     */
    public function getDisLikeCount(HasDisLike $dislikeable): int
    {
        return $dislikeable->dislikes()->count();
    }

    /**
     * Check if dislikeable is disliked before
     * @param HasDisLike $dislikeable
     * @param int $user_id
     * @return bool
     */
    public function isDisliked(HasDisLike $dislikeable, int $user_id): bool
    {
        return $dislikeable->dislikes()->where('user_id', '=', $user_id)->exists();
    }

    /**
     * Create single dislike or all of dislikes for related entity
     * @param HasDisLike $dislikeable
     * @param int|null $id
     * @param string|null $type
     * @return bool
     */
    public function deleteDislike(HasDisLike $dislikeable, int $id = null, string $type = null): bool
    {
        try {
            $dislikeable->disLikes()->detach($id);
            $dis_likes_list = 'user_' . $id . '_disliked_' . $type . '_list';
            if (Cache::has($dis_likes_list)) {
                Cache::forget($dis_likes_list);
            }
        } catch (QueryException | \Exception $ex) {
            Log::error($ex->getMessage());
            return false;
        }
        return true;
    }

    /**
     * @param int $user_id
     * @param string $type
     * @return Collection|string
     */
    public function getUserDisLikesList(int $user_id, string $type)
    {
        switch ($type) {
            case 'global':
                return DB::table('user_dislikes')->where('user_id', '=', $user_id)->where('dislikeable_type', '=', 'App\Models\GlobalCurrency')->pluck('dislikeable_id');
            case 'banner':
                return DB::table('user_dislikes')->where('user_id', '=', $user_id)->where('dislikeable_type', '=', 'App\Models\Banner')->pluck('dislikeable_id');
            case 'faq':
                return DB::table('user_dislikes')->where('user_id', '=', $user_id)->where('dislikeable_type', '=', 'App\Models\FaqQuestionAnswer')->pluck('dislikeable_id');
            case 'comment':
               return DB::table('user_dislikes')->where('user_id', '=', $user_id)->where('dislikeable_type', '=', 'App\Models\Comment')->pluck('dislikeable_id');
            default :
                return '**';
        }
    }

    /**
     * @param int $user_id
     * @return Collection
     */
    public function getUserCommentsDisLikesList(int $user_id): Collection
    {
        return DB::table('user_dislikes')->where('user_id', '=', $user_id)->where('dislikeable_type', '=', 'App\Models\Comment')->pluck('dislikeable_id');
    }
}
