<?php

namespace App\Repositories;

use App\Models\Sector;
use App\Repositories\Interfaces\SectorRepositoryInterface;
use Exception;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;


class SectorRepository implements SectorRepositoryInterface
{
    /**
     * @param int $id
     * @param bool $with_relation
     * @return Builder|Builder[]|Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getById(int $id, bool $with_relation = false): Sector
    {
        return Sector::query()->findOrFail($id);
    }

    /**
     * @return Collection
     */
    public function getAllSectors(): Collection
    {
        return Sector::all();
    }

    /**
     * @param Sector $sector
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getSectorsWithGlobals(Sector $sector, int $limit = 50, int $offset = 0)
    {
        return $sector->globalCurrencies->pluck('id')->skip($offset)->take($limit);
    }

    /**
     * @param int $per_page
     * @return Paginator
     */
    public function paginateSectors(int $per_page = 20): Paginator
    {
        return Sector::query()->paginate($per_page);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function createSector(array $data): bool
    {
        $sector = new Sector();
        $sector->title_fa = $data['title_fa'];
        $sector->title_en = $data['title_en'];
        try {
            $sector->save();

        } catch (\Exception | QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * @param Sector $sector
     * @param array $data
     * @return bool
     */
    public function attachGlobalCurrenciesToSectors(Sector $sector, array $data): bool
    {
        try {
            $sector->globalCurrencies()->attach($data);
        } catch (Exception | QueryException $exception) {
            Log::error('error while attaching global currency to sector' . $exception->getMessage());
            return false;
        }
        return true;
    }

    public function editAttachGlobalCurrenciesToSectors(Sector $sector, array $data): bool
    {
        try {
            $sector->globalCurrencies()->attach($data);
        } catch (Exception | QueryException $exception) {
            Log::error('error while attaching global currency to sector' . $exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * @param Sector|null $sector
     * @return bool
     */
    public function deleteSector(Sector $sector = null): bool
    {
        try {
            $sector->globalCurrencies()->detach();
            $sector->delete();
        } catch (\Exception | QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;

    }

    public function updateSector(Sector $sector, array $data): bool
    {
        $sector->title_fa = $data['title_fa'];
        $sector->title_en = $data['title_en'];
        try {
            $sector->save();

        } catch (\Exception | QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }


    public function detach(Sector $sector, int $global_currency_id)
    {
        try {
            $sector->globalCurrencies()->detach($global_currency_id);
        } catch (\Exception | QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }
}
