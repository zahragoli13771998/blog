<?php


namespace App\Repositories;

use App\Models\CurrencyChangesPercent;
use App\Models\CurrencyOHLC;
use App\Models\GCurrencyRamzinexInfo;
use App\Models\GlobalCurrency;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use Exception;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

/**
 * Class GlobalCurrencyRepository
 * @package App\Repositories
 */
class GlobalCurrencyRepository implements GlobalCurrencyInterface
{
    /**
     * Get and return single Global Currency by it's id
     * @param int|null $id
     * @param bool $relation
     * @param bool $only_active_changes
     * @param int|null $user_id
     * @param string|null $symbol
     * @param bool $with_ramzinex_info
     * @return GlobalCurrency|null
     */
    public function getById(int $id = null, bool $relation = false, bool $only_active_changes = false, int $user_id = null, string $symbol = null, bool $with_ramzinex_info = false): ?GlobalCurrency
    {
        $global_currency = $this->buildGlobalCurrency($relation, false, $only_active_changes, $user_id)
            ->when($relation, function (Builder $query) {
                $query->with(['ohlc', 'tags']);
            })
            ->when($with_ramzinex_info, function (Builder $query) {
                $query->with(['ramzinexInfo']);
            });
        if ($symbol) {
            return $global_currency->where('symbol', '=', $symbol)->first() ?? null;
        }
        return $global_currency->findOrFail($id);
    }


    /**
     * Get all global Currencies from DB
     * @param bool $relation
     * @param bool $only_short_info
     * @param int|null $from
     * @param int|null $take
     * @param int|null $user_id
     * @param bool $order_by_rank_asc
     * @param string|null $sort_by
     * @param array $search
     * @param bool $without_hidden
     * @return Collection
     */
    public function getAllGlobalCurrencies(bool $relation = false, bool $only_short_info = false, int $from = null, int $take = null, int $user_id = null, bool $order_by_rank_asc = false, string $sort_by = null, array $search = [], bool $without_hidden = false): Collection
    {
        return $this->buildGlobalCurrency($relation, $only_short_info, false, $user_id, $order_by_rank_asc, $sort_by, $search, $without_hidden)
            ->when($from, function (Builder $query) use ($from) {
                $query->skip($from);
            })->when($take, function (Builder $query) use ($take) {
                $query->take($take);
            })
            ->get();
    }

    /**
     * @param bool $with_relation
     * @return Collection
     */
    public function getPairGlobalCurrencies(bool $with_relation): Collection
    {
        return $this->buildGlobalCurrency($with_relation)->whereNotNull('pair_id')->get();
    }

    /**
     * paginate all global currencies
     * @param int $per_page
     * @param bool $order_by_rank_asc
     * @param string|null $sort_by
     * @param array $search
     * @return Paginator
     */
    public function paginate(int $per_page = 10, bool $order_by_rank_asc = false, string $sort_by = null, array $search = []): Paginator
    {
        return $this->buildGlobalCurrency(false, false, false, null, $order_by_rank_asc, $sort_by, $search)
            ->paginate($per_page);
    }

    /**
     * checks if currency with this symbol exists
     * @param string $symbol_id
     * @return bool
     */
    public function checkIfCurrencyExists(string $symbol_id): bool
    {
        return GlobalCurrency::query()->where('symbol', '=', $symbol_id)->exists();
    }

    /**
     * Check if currency change percent exists
     * @param GlobalCurrency $global_currency
     * @param int $interval_id
     * @return bool
     */
    public function isCurrencyChangePercentExists(GlobalCurrency $global_currency, int $interval_id): bool
    {
        return $global_currency->changePercent()->where('interval_id', '=', $interval_id)->exists();
    }

    /**
     * Check if currency has ramzinex info
     * @param GlobalCurrency $global_currency
     * @return GCurrencyRamzinexInfo
     */
    public function isRamzinexCurrencyInfoExists(GlobalCurrency $global_currency): ?GCurrencyRamzinexInfo
    {
        return $global_currency->ramzinexInfo()->first();
    }

    /**
     * Check if currency ohlc exists or not
     * @param GlobalCurrency $global_currency
     * @param int $interval_id
     * @return bool
     */
    public function isCurrencyOHLCExists(GlobalCurrency $global_currency, int $interval_id): bool
    {
        return $global_currency->ohlc()->where('interval_id', '=', $interval_id)->exists();
    }

    /**
     * return number of global currencies
     * @return int
     */
    public function globalCurrenciesCount(): int
    {
        return GlobalCurrency::query()->count();
    }

    /**
     * create new global currency model
     * @param array $data
     * @return bool
     */
    public function createCurrency(array $data): bool
    {
        $currency = new GlobalCurrency();

        $currency->name_en = $data['name_en'];

        if (isset($data['name_fa'])) {//Todo move to one function for resolving duplication error
            $currency->name_fa = $data['name_fa'];
        }

        if (isset($data['price'])) {
            $currency->price = $data['price'];
        }

        if (isset($data['price_precision']) && is_integer($data['price_precision'])) {
            $currency->price_precision = $data['price_precision'];
        }

        if (isset($data['rank'])) {
            $currency->rank = $data['rank'];
        }

        if (isset($data['currency_id'])) {
            $currency->currency_id = $data['currency_id'];
        }
        if (isset($data['pair_id']) && is_int($data['pair_id'])) {
            $currency->pair_id = $data['pair_id'];
        }

        if (isset($data['symbol_id'])) {
            $currency->symbol_id = $data['symbol_id'];
        }

        if (isset($data['symbol'])) {
            $currency->symbol = $data['symbol'];
        }

        if (isset($data['icon'])) {
            $currency->icon = $data['icon'];
        }

        if (isset($data['total_supply'])) {
            $currency->total_supply = $data['total_supply'];
        }

        if (isset($data['market_dominance'])) {
            $currency->market_dominance = $data['market_dominance'];
        }

        if (isset($data['total_volume'])) {
            $currency->total_volume = $data['total_volume'];
        }

        if (isset($data['daily_trading_volume'])) {
            $currency->daily_trading_volume = $data['daily_trading_volume'];
        }

        if (isset($data['coin_circulation'])) {
            $currency->coin_circulation = $data['coin_circulation'];
        }

        if (isset($data['rial_price'])) {
            $currency->rial_price = $data['rial_price'];
        }

        if (isset($data['btc_price'])) {
            $currency->btc_price = $data['btc_price'];
        }

        if (isset($data['description_fa'])) {
            $currency->description_fa = $data['description_fa'];
        }

        if (isset($data['description_en'])) {
            $currency->description_en = $data['description_en'];
        }

        try {
            $currency->save();

        } catch (QueryException | Exception $ex) {
            Log::error($ex->getmessage());
            return false;
        }
        return true;
    }

    /**
     * create CurrencyChange
     * @param array $data
     * @param GlobalCurrency $currency
     * @return bool
     */
    public function createCurrencyChange(array $data, GlobalCurrency $currency): bool
    {
        $change_percent = new CurrencyChangesPercent();
        $change_percent->interval_id = $data['interval_id'];
        $change_percent->value = $data['value'];

        try {
            $currency->changePercent()->save($change_percent);
        } catch (QueryException | Exception $ex) {
            Log::error($ex->getmessage());
            return false;
        }
        return true;
    }

    /**
     * Create new ohlc for existing global currency
     * @param GlobalCurrency $global_currency
     * @param array $data
     * @return bool
     */
    public function createCurrencyOHLC(GlobalCurrency $global_currency, array $data): bool
    {
        $ohlc = new CurrencyOHLC();
        $ohlc->interval_id = $data['interval_id'];
        $ohlc->high = $data['high'];
        $ohlc->close = $data['close'];
        $ohlc->low = $data['low'];
        $ohlc->open = $data['open'];
        $ohlc->market_cap = $data['market_cap'];
        $ohlc->volume = $data['volume'];

        try {
            $global_currency->ohlc()->save($ohlc);
        } catch (QueryException | Exception $e) {
            Log::error('Unable to store ohlc for global currency with error: ' . $e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * add banner to global currencies
     * @param GlobalCurrency $global_currency
     * @param array $banners
     * @return bool
     */
    public function attachBanners(GlobalCurrency $global_currency, array $banners): bool
    {
        try {
            $global_currency->banners()->attach($banners);
        } catch (Exception | QueryException $exception) {
            Log::error('error while syncing banners to global_currencies' . $exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * add banner to global currencies
     * @param GlobalCurrency $global_currency
     * @param array $banners
     * @return bool
     */
    public function editAttachBanners(GlobalCurrency $global_currency, array $banners): bool
    {
        try {
            $global_currency->banners()->sync($banners);
        } catch (Exception | QueryException $exception) {
            Log::error('error while syncing banners to global_currencies' . $exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * update specific global currency model
     * @param array $data
     * @param GlobalCurrency $currency
     * @return bool
     */
    public function update(array $data, GlobalCurrency $currency): bool
    {

        if (isset($data['name_en'])) {
            $currency->name_en = $data['name_en'];
        }

        if (isset($data['name_fa'])) {
            $currency->name_fa = $data['name_fa'];
        }

        if (isset($data['rank'])) {
            $currency->rank = $data['rank'];
        }

        if (isset($data['price'])) {
            $currency->price = $data['price'];
        }

        if (isset($data['price_precision'])) {
            $currency->price_precision = $data['price_precision'];
        }

        if (isset($data['currency_id'])) {
            $currency->currency_id = $data['currency_id'];
        }

        if (isset($data['symbol_id'])) {
            $currency->symbol_id = $data['symbol_id'];
        }

        if (isset($data['pair_id'])) {
            $currency->pair_id = $data['pair_id'];
        }
        if (isset($data['symbol'])) {
            $currency->symbol = $data['symbol'];
        }

        if (isset($data['icon'])) {
            $currency->icon = $data['icon'];
        }

        if (isset($data['total_supply'])) {
            $currency->total_supply = $data['total_supply'];
        }

        if (isset($data['market_dominance'])) {
            $currency->market_dominance = $data['market_dominance'];
        }

        if (isset($data['total_volume'])) {
            $currency->total_volume = $data['total_volume'];
        }

        if (isset($data['daily_trading_volume'])) {
            $currency->daily_trading_volume = $data['daily_trading_volume'];
        }

        if (isset($data['coin_circulation'])) {
            $currency->coin_circulation = $data['coin_circulation'];
        }

        if (isset($data['rial_price'])) {
            $currency->rial_price = $data['rial_price'];
        }

        if (isset($data['btc_price'])) {
            $currency->btc_price = $data['btc_price'];
        }

        if (isset($data['description_fa'])) {
            $currency->description_fa = $data['description_fa'];
        }

        if (isset($data['description_en'])) {
            $currency->description_en = $data['description_en'];
        }
        if (isset($data['is_hidden'])) {
            $currency->is_hidden = $data['is_hidden'];
        }

        try {
            $currency->save();
        } catch (QueryException | Exception $ex) {
            Log::error($ex->getmessage());
            return false;
        }
        return true;
    }

    /**
     * Disable all global currencies changes percents
     * @param GlobalCurrency $global_currency
     * @param int $interval_id
     * @return bool
     */
    public function disableCurrencyChangesPercent(GlobalCurrency $global_currency, int $interval_id): bool
    {
        try {
            /*            $global_currency->changePercent()->where('interval_id', '=', $interval_id)->update([
                'status' => 0,
            ]);*/

            $global_currency->changePercent()->where('interval_id', '=', $interval_id)
                ->delete();
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Disable and delete stored ohlc with specific period for global currency
     * @param GlobalCurrency $global_currency
     * @param int $interval_id
     * @return bool
     */
    public function disableCurrencyOHLC(GlobalCurrency $global_currency, int $interval_id): bool
    {
        try {
            $global_currency->ohlc()->where('interval_id', '=', $interval_id)
                ->delete();
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * delete specific global currency model
     * @param GlobalCurrency $currency
     * @return bool
     * @throws Exception
     */
    public function delete(GlobalCurrency $currency): bool
    {
        try {
            $currency->banners()->detach();
            $currency->delete();
        } catch (QueryException | Exception $ex) {
            Log::error($ex->getmessage());
            return false;
        }
        return true;
    }

    /**
     * @param array $data
     * @param string|null $sort_by
     * @return mixed
     */
    public function filterGlobals(array $data = [], string $sort_by = null)
    {

            switch ($sort_by) {
                case 'last_price':
                   return GlobalCurrency::query()->orderByDesc('id')->get();

                case 'daily_trading_volume' :
                    return GlobalCurrency::query()->orderByDesc('daily_trading_volume')->get();

                case 'change_percent' :
                    return GlobalCurrency::query()->orderByDesc('change_percent[0][value]')->get();

                case 'lowest_price':
                    return GlobalCurrency::query()->orderByDesc('ohlc[0][low]')->get();

                case 'highest_price':
                    return GlobalCurrency::query()->orderByDesc('ohlc[0][high]')->get();

            }

    }

    /**
     * builder of GlobalCurrency
     * @param bool $relation
     * @param bool $only_short_info
     * @param bool $only_active_changes_percent
     * @param int|null $user_id
     * @param bool $order_by_rank
     * @param string|null $sort_by
     * @param array $search
     * @param bool $without_hidden
     * @return Builder
     */
    private function buildGlobalCurrency(bool $relation = false, bool $only_short_info = false, bool $only_active_changes_percent = false, int $user_id = null, bool $order_by_rank = false, string $sort_by = null, array $search = [], bool $without_hidden = false): Builder
    {
        return GlobalCurrency::query()->when($relation, function (Builder $query) use ($only_active_changes_percent, $only_short_info) {
            $query->with(['changePercent' => function (HasMany $query) use ($only_active_changes_percent, $only_short_info) {
                if ($only_active_changes_percent) {
                    $query->where('status', '=', 1);
                }
                /*                    if($only_short_info){
                        $query->whereIn('interval_id', [Interval::getDailyId(), Interval::getWeeklyId()]); //Todo find a way to detach repository dependency to interval id
                    }*/

            }]);
        })
            ->when($only_short_info, function (Builder $query) {
                $query->select(['id', 'name_en', 'name_fa', 'rank', 'currency_id', 'icon', 'price', 'btc_price', 'rial_price', 'symbol', 'daily_trading_volume', 'coin_circulation', 'total_supply', 'price_precision', 'pair_id']);
            })
            ->when($user_id, function (Builder $query) use ($user_id) {
                $query->with(['likes' => function ($query) use ($user_id) {
                    $query->where('user_id', '=', $user_id);
                }, 'dislikes' => function ($query) use ($user_id) {
                    $query->where('user_id', '=', $user_id);
                }, 'favorites' => fn($query) => $query->where('user_id', '=', $user_id)]);
            })
            ->when($order_by_rank && !isset($sort_by), function (Builder $query) {
                $query->orderBy('rank', 'ASC');
                $query->where('rank', '>', 0);
            })
            ->when(isset($sort_by), function (Builder $query) use ($sort_by) {
                switch ($sort_by) {
                    case 'likes':
                        $query->orderByDesc('likes_count');
                        break;
                    case 'dislikes':
                        $query->orderByDesc('dislikes_count');
                        break;
                    case 'comments':
                        $query->orderByDesc('comments_count');
                        break;
                    case 'views':
                        $query->orderByDesc('views_count');
                        break;
                    case 'includes':
                        $query->orderByDesc('includes_count');
                }

            })->when(isset($search['request_name_en']), function (Builder $query) use ($search) {
                $query->where('name_en', 'like', '%' . $search['request_name_en'] . '%');
            })->when(isset($search['request_name_fa']), function (Builder $query) use ($search) {
                $query->where('name_fa', 'like', '%' . $search['request_name_fa'] . '%');
            })->when(isset($search['request_symbol']), function (Builder $query) use ($search) {
                $query->where('symbol', 'like', '%' . $search['request_symbol'] . '%');
            })
            ->when(isset($search['ramzinex_currencies']), function (Builder $query) use ($search) {
                $query->whereNotNull('pair_id');
            })
            ->when($without_hidden, function (Builder $query) {
                $query->where('is_hidden', '=', 0);
            })
            ->withCount(['likes', 'dislikes', 'views', 'comments', 'includes']);
    }

    public function getTopGlobalsWithRamzinexGlobals(bool $relation=false,bool $only_active_changes_percent=false)
    {
        $only_short_info = true;
        return GlobalCurrency::query()->when($relation, function (Builder $query) use ($only_active_changes_percent, $only_short_info) {
            $query->with(['changePercent' => function (HasMany $query) use ($only_active_changes_percent, $only_short_info) {
                if ($only_active_changes_percent) {
                    $query->where('status', '=', 1);
                }
                /*                    if($only_short_info){
                        $query->whereIn('interval_id', [Interval::getDailyId(), Interval::getWeeklyId()]); //Todo find a way to detach repository dependency to interval id
                    }*/

            }]);
        })->where('rank', '<',1100)->orWhereNotNull('pair_id',)
            ->withCount(['likes', 'dislikes', 'views', 'comments', 'includes'])
            ->select(['id', 'name_en', 'name_fa', 'rank', 'currency_id',
                'icon', 'price', 'btc_price', 'rial_price', 'symbol', 'daily_trading_volume',
                'coin_circulation', 'total_supply', 'price_precision', 'pair_id'])
            ->get();
    }
}
