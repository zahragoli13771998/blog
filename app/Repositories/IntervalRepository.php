<?php

namespace App\Repositories;

use App\Models\Interval;
use App\Repositories\Interfaces\IntervalRepositoryInterface;
use \Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class IntervalRepository implements IntervalRepositoryInterface
{
    /**
     * paginate interval.
     *
     * @param int $perpage
     * @return LengthAwarePaginator
     */
    public function paginate(int $perpage = 10): LengthAwarePaginator
    {
        return $this->fetchQueryBuilder()->paginate($perpage);
    }

    /**
     * findById the record with the given id.
     *
     * @param int $id
     * @return Interval|Builder|Builder[]
     */
    public function findById(int $id): Interval
    {
        return $this->fetchQueryBuilder()->findOrFail($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param array $value
     * @return bool
     */
    public function store(array $value): bool
    {
        $interval = new Interval();
        if (isset($value['title_fa'])) {
            $interval->title_fa = $value['title_fa'];
        }
        if (isset($value['title_en'])) {
            $interval->title_en = $value['title_en'];
        }

        try {
            $interval->save();
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param array $value
     * @param Interval $interval
     * @return bool
     */
    public function update(array $value, Interval $interval): bool
    {
        if (isset($value['title_fa'])) {
            $interval->title_fa = $value['title_fa'];
        }
        if (isset($value['title_en'])) {
            $interval->title_en = $value['title_en'];
        }
        try {
            $interval->save();
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Interval $interval
     * @return bool
     * @throws \Exception
     */
    public function delete(Interval $interval): bool
    {
        try {
            $interval->delete();
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * fetch Query Builder interval.
     *
     * @return Builder
     */
    private function fetchQueryBuilder(): Builder
    {
        return Interval::query();
    }

    /**
     * Get the value from the database.
     *
     * @return Builder[]|Collection
     */
    public function getAll(): Collection
    {
        return $this->fetchQueryBuilder()->with('currencyChange')->get();
    }

}
