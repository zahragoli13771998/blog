<?php


namespace App\Repositories;


use App\Services\Interfaces\CurlServiceInterface;

class RamzinexPairsRepository implements Interfaces\PairsRepositoryInterface
{
    /**
     * Curl service container
     * @var CurlServiceInterface
     */
    protected CurlServiceInterface $curler_service;

    /**
     * Construct and inject necessary services to their container
     * RamzinexPairsRepository constructor.
     */
    public function __construct(CurlServiceInterface $curler_service)
    {
        $this->curler_service = $curler_service;
    }

    /**
     * Get and return all pairs info from ramzinex api
     * @param int|null $pair_id
     * @inheritDoc
     */
    public function getAllPairsInfo(int $pair_id = null): ?array
    {
        $url_section = config('currency_services.url.ramzinex.pairs');

        if ($pair_id) {
            $url_section .= $pair_id;
        }

        $result = $this->curler_service->createClient(config('currency_services.url.ramzinex.base'))
            ->setUrlSection($url_section)
            ->setQueryStrings([
                'base_id' => 2
            ])
            ->executeRequest();
        if (is_null($result) && !is_array($result) || !isset($result['status']) || $result['status'] === false || $result['content']['status'] != 0) {
            return null;
        }//Todo move to repository layer

        return $result['content']['data'];
    }
}
