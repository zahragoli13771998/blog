<?php


namespace App\Repositories;

use App\Models\TrendingCurrency;
use App\Repositories\Interfaces\TrendingCurrencyRepositoryInterface;
use Exception;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class TrendingCurrencyRepository implements TrendingCurrencyRepositoryInterface
{
    /**
     * get and return TrendingCurrency according to given id
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getById(int $id): TrendingCurrency
    {
        return TrendingCurrency::query()->findOrFail($id);
    }

    /**
     * get and return all TrendingCurrency
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllTrendingCurrency(): Collection
    {
        return TrendingCurrency::query()->get();
    }

    /**
     * @param int $per_page
     * @return \Illuminate\Contracts\Pagination\Paginator
     */
    public function paginateTrendingCurrency(int $per_page = 10): Paginator
    {
       return TrendingCurrency::query()->paginate($per_page);
    }

    /**
     * create TrendingCurrency
     * @param array $data
     * @return bool
     */
    public function createTrendingCurrency(array $data): bool
    {
        $currency_trending = new TrendingCurrency();
        $currency_trending->id = $data['id'];

        try {
            $currency_trending->save();
        } catch (QueryException | Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * update TrendingCurrency
     * @param TrendingCurrency $currency_trending
     * @param array $data
     * @return bool
     */
    public function updateTrendingCurrency(TrendingCurrency $currency_trending, array $data): bool
    {
        if (isset($data['id'])) {
            $currency_trending->id = $data['id'];
        }

        try {
            $currency_trending->save();
        } catch (QueryException | Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * delete TrendingCurrency
     * @param TrendingCurrency $currency_trending
     * @return bool
     */
    public function deleteTrendingCurrency(TrendingCurrency $currency_trending): bool
    {
        try {
            $currency_trending->delete();
        } catch (Exception | QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }
}
