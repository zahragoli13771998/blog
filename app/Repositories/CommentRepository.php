<?php

namespace App\Repositories;

use App\Http\Resources\Pool;
use App\Models\Comment;
use App\Models\CommentFile;
use App\Models\Interfaces\HasComments;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Concerns\BuildsQueries;
use Illuminate\Database\Eloquent\Builder;
use  Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class CommentRepository implements CommentRepositoryInterface
{
    /**
     *get all comments
     * @param HasComments|null $commentable_obj
     * @param bool $only_root
     * @param bool $with_relation
     * @param int|null $offset
     * @param int|null $limit
     * @param bool $only_accepted
     * @param int|null $comment_id
     * @param int|null $user_id
     * @return Collection
     */
    public function getAllComment(HasComments $commentable_obj = null, bool $only_root = false, bool $with_relation = false, int $offset = null, int $limit = null, bool $only_accepted = false, int $comment_id = null, int $user_id = null): Collection
    {
        return
            $this->commentFetchQueryBuilder($only_root, $with_relation, $commentable_obj, $only_accepted, $comment_id, $user_id)
                ->when($offset, function (Builder $query) use ($offset) {
                    $query->skip($offset);
                })->when($limit, function (Builder $query) use ($limit) {
                    $query->take($limit);
                })->orderBy('id', 'DESC')->get();
    }

    /**
     * Get And return Comments Count
     * @param HasComments|null $commentable_obj
     * @param bool $only_accepted
     * @param int|null $comment_id
     * @param bool $only_root
     * @return int
     */
    public function getCommentsCount(HasComments $commentable_obj = null, bool $only_accepted = false, int $comment_id = null, bool $only_root = false): int
    {
        return $this->commentFetchQueryBuilder($only_root,false, $commentable_obj, $only_accepted, $comment_id)->where('status','=',1)->count();
    }

    /**
     * get survey answer comment
     * @param int $user_id
     * @param int $depth_id
     * @return Collection
     */
    public function getBySurveyAnswer(int $user_id,int $depth_id): Collection
    {
        return comment::query()->where('user_id','=',$user_id)->where('depth_id','=',$depth_id)->get();
    }

    /**
     * get comment according to given id
     * @param int $id
     * @param false $only_root
     * @param bool $with_reply
     * @return Comment|Builder|Builder[]|Collection|Model|null
     */
    public function getById(int $id, $only_root = false, bool $with_reply = false): Comment
    {
        return $this->commentFetchQueryBuilder($only_root, $with_reply)->findOrFail($id);
    }

    /**
     * paginate comments
     * @param bool $only_root
     * @param bool $with_relation
     * @param int $per_page
     * @param bool $with_commentable
     * @return Paginator
     */
    public function paginateComment(bool $only_root = false, bool $with_relation = false, int $per_page = 10, bool $with_commentable = false): Paginator
    {
        return $this->commentFetchQueryBuilder($only_root,$with_relation)->orderBy('id', 'DESC')
            ->when($with_commentable, function (Builder $query) {
                $query->with(['commentable']);
            })
            ->paginate($per_page);
    }

//    /**
//     * @param int $global_currency_id
//     * @return Builder[]|Collection
//     */
//    public function getPool(int $global_currency_id){
//       return Comment::query()
//           ->where('commentable_type','=','App\Models\GlobalCurrency')
//           ->where('commentable_id','=',$global_currency_id)
//           ->where('type','=','survey')->get();
//    }

    /**
     * create a new comment (polymorphic)
     * @param HasComments $commentable_obj
     * @param array $data
     * @return Comment
     */
    public function createComment(HasComments $commentable_obj, array $data): ?Comment
    {
        $comment = new Comment();
        $comment->user_id = $data['user_id'];
        $comment->body = $data['body'];

        if (isset($data['nickname'])) {
            $comment->nickname = $data['nickname'];
        }
        if (isset($data['comment_id'])) {
            $comment->comment_id = $data['comment_id'];
        }
        //Todo:fix
        if ((!isset($data['depth_id'] ) ||  ($data['depth_id'] == null )
            && $data['comment_id'] != null)) {
            $comment->depth_id = $data['comment_id'];
        }
        if (isset($data['type'])) {
            $comment->type = $data['type'];
        }else{
            $comment->type = 'comment';
        }
        if (isset($data['options'])) {
            $comment->options = $data['options'];
        }
        if (isset($data['end_time'])) {
            $comment->end_time = Carbon::now()->addHours(24)->toDateString();
        }
        if (isset($data['answer_index'])) {
            $comment->answer_index = $data['answer_index'];
        }
        try {
            $commentable_obj->comments()->save($comment);
        } catch (Exception | QueryException $ex) {
            Log::error($ex->getMessage());
            return null;
        }
        return $comment;
    }

    /**
     * create pool answer
     * @param HasComments $commentable_obj
     * @param array $data
     * @return Comment|null
     */
    public function createPoolAnswer(HasComments $commentable_obj, array $data): ?Comment
    {
        $comment = new Comment();
        $comment->user_id = $data['user_id'];
        if (isset($data['nickname'])) {
            $comment->nickname = $data['nickname'];
        }
        if (isset($data['comment_id'])) {
            $comment->comment_id = $data['comment_id'];
        }
        //Todo:fix
        if ((!isset($data['depth_id'] ) ||  ($data['depth_id'] == null )
            && $data['comment_id'] != null)) {
            $comment->depth_id = $data['comment_id'];
        }
        $comment->depth_id = $data['comment_id'];

        $comment->type = 'survey_answer';

        if (isset($data['answer_index'])) {
            $comment->answer_index = $data['answer_index'];
        }
        $comment->status = 1;
        try {
            $commentable_obj->comments()->save($comment);
        } catch (Exception | QueryException $ex) {
            Log::error($ex->getMessage());
            return null;
        }
        return $comment;
    }
    /**
     * create file comment
     * @param Comment $comment
     * @param array $data
     * @return bool
     */
    public function createCommentFile(Comment $comment, array $data): bool
    {
        $file = new CommentFile();
        $file->path = $data['path'];

        if (isset($data['type'])) {
            $file->type = $data['type'];
        }

        try {
            $comment->file()->save($file);
        } catch (Exception | QueryException $ex) {
            Log::error($ex->getMessage());
            return false;
        }
        return true;
    }

    /**
     * update comments
     * @param array $data
     * @param Comment $comment
     * @return bool
     */
    public function updateComment(Comment $comment, array $data): bool
    {
        if (isset($data['user_id'])) {
            $comment->user_id = $data['user_id'];
        }
        if (isset($data['comment_id'])) {
            $comment->comment_id = $data['comment_id'];
        }
        if (isset($data['body'])) {
            $comment->body = $data['body'];
        }
        if (isset($data['nickname'])) {
            $comment->nickname = $data['nickname'];
        }
        if (isset($data['status'])) {
            $comment->status = $data['status'];
        }

        try {
            $comment->save();
        } catch (Exception | QueryException $ex) {
            Log::error($ex->getMessage());
            return false;
        }
        return true;
    }

    /**
     * update comment file
     * @param CommentFile $comment_file
     * @param array $data
     * @return bool
     */
    public function updateCommentFile(CommentFile $comment_file, array $data): bool
    {
        if (isset($data['path'])) {
            $comment_file->path = $data['path'];
        }
        if (isset($data['type'])) {
            $comment_file->type = $data['type'];
        }
        try {
            $comment_file->save();
        } catch (Exception | QueryException $ex) {
            Log::error($ex->getMessage());
            return false;
        }
        return true;
    }

    /**
     * delete a comment
     * @param Comment|null $comment
     * @param HasComments|null $commentable_obj
     * @return bool|void
     */
    public function delete(Comment $comment = null, HasComments $commentable_obj = null): bool
    {
        try {
            if ($commentable_obj) {
                $commentable_obj->comments()->delete();
            }

            if ($comment) {
                $comment->replies()->delete();
                $comment->file()->delete();
                $comment->delete();
            }
        } catch (Exception | QueryException $ex) {
            Log::error($ex->getMessage());
            return false;
        }
        return true;
    }


    /**
     * comments query builder
     * @param bool $only_root
     * @param bool $with_relation
     * @param HasComments|null $commentable_obj
     * @param bool $only_accepted
     * @param int|null $comment_id
     * @param int|null $user_id
     * @return BuildsQueries|MorphMany|mixed
     */
    protected  function commentFetchQueryBuilder(bool $only_root = false, bool $with_relation = false, HasComments $commentable_obj = null, bool $only_accepted = false, int $comment_id = null, int $user_id = null)
    {
        return (($commentable_obj) ? $commentable_obj->comments() : Comment::query())->when($only_root, function (Builder $query) {
            $query->whereNull('comment_id');
        })->where('type','=','comment')->when($with_relation, function (Builder $query) use ($user_id, $only_accepted) {
            $query->with(['replies' => function ($query) use ($only_accepted, $user_id) {
                if ($only_accepted) {
                    $query->where('status', '=', 1);
                }

                $query->withCount(['replies' => function ($query) use ($only_accepted) {
                    if ($only_accepted) {
                        $query->where('status', '=', 1);
                    }
                }, 'likes', 'dislikes']);

                $query->when($user_id, function (Builder $query) use ($user_id) {
                    $query->with([
                        'likes' => fn ($query) => $query->where('user_id', '=', $user_id),
                        'dislikes' => fn ($query) => $query->where('user_id', '=', $user_id)
                    ]);
                });

            }, 'replies.file', 'replies.user:id,sid', 'file', 'user:id,sid']);
//            $query->whereHas('getAnswersCount')->with('getAnswersCount');
            $query->withCount(['replies' => function ($query) use ($only_accepted) {
                if ($only_accepted) {
                    $query->where('status', '=', 1);
                }
            }, 'likes', 'dislikes']);
        })->when($only_accepted, function (Builder $query) {
            $query->where('status', '=', 1);
        })->when(isset($comment_id), function (Builder $query) use ($comment_id) {
            $query->where('comment_id', '=', $comment_id);
        })->when($user_id, function (Builder $query) use ($user_id) {
            $query->with([
                'likes' => fn ($query) => $query->where('user_id', '=', $user_id),
                'dislikes' => fn ($query) => $query->where('user_id', '=', $user_id)
            ]);
        });
    }

}
