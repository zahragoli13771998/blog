<?php


namespace App\Repositories;

use App\Models\DeepLink;
use App\Models\Interfaces\HasDeepLink;
use App\Repositories\Interfaces\DeepLinkRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;


class DeepLinkRepository implements DeepLinkRepositoryInterface
{
    /**
     * check if deeplink exists or not
     * if yes returns the first one
     * @param HasDeepLink $deep_linkable
     * @return DeepLink|null
     */
    public function getDeeplink(HasDeepLink $deep_linkable):?DeepLink
    {
        return $deep_linkable->deeplink()->first();
    }

    /**
     * get Deeplink by given id
     * @param int $id
     * @return DeepLink
     */
    public function getSingleDeeplink(int $id):DeepLink
    {
        return DeepLink::query()->find($id);
    }

    /**
     * cretes a polymorphic deeplink data for a specific data
     * @param HasDeepLink $deep_linkable
     * @param array $data
     * @return DeepLink|null
     */
    public function createDeeplink(HasDeepLink $deep_linkable, array $data): ?DeepLink
    {
        $deeplink = new DeepLink();

        if(isset($data['link'])){
            $deeplink->link = $data['link'];
        }

        if(isset($data['title_fa'])){
            $deeplink->title_fa = $data['title_fa'];
        }

        if(isset($data['title_en'])){
            $deeplink->title_en = $data['title_en'];
        }

        if(isset($data['status'])){
            $deeplink->status = $data['status'];
        }

        try{
            $deep_linkable->deeplink()->save($deeplink);
            return $deeplink;
        }catch (Exception | QueryException $ex) {
            Log::error($ex->getMessage());
            return null;
        }
    }

    /**
     * updates deeplink with given data
     * @param DeepLink $deeplink
     * @param array $data
     * @return bool
     */
    public function updateDeeplink(DeepLink $deeplink, array $data): bool
    {
        if(isset($data['link'])){
            $deeplink->link = $data['link'];
        }

        if(isset($data['title_fa'])){
            $deeplink->title_fa = $data['title_fa'];
        }

        if(isset($data['title_en'])){
            $deeplink->title_en = $data['title_en'];
        }

        if(isset($data['status'])){
            $deeplink->status = $data['status'];
        }

        try{
            $deeplink->save();
            return true;
        }catch (Exception | QueryException $ex) {
            Log::error($ex->getMessage());
            return false;
        }
    }

    /**
     * delete deeplink
     * @param DeepLink $deep_link
     * @return bool
     */
    public function deleteDeeplink(DeepLink $deep_link): bool
    {
        try {
            $deep_link->delete();
        } catch (QueryException | Exception $ex) {
            Log::error($ex->getmessage());
            return false;
        }
        return true;
    }
}
