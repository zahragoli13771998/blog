<?php

namespace App\Repositories;

use App\Models\Currency;
use App\Repositories\Interfaces\CurrencyRepositoryInterface;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class CurrencyRepository implements CurrencyRepositoryInterface
{

    /**
     * @param int $id
     * @return Currency
     */
    public function getById(int $id): Currency
    {
        return Currency::findOrFail($id);
    }

    public function getAllCurrencies(): Collection
    {
        return Currency::all();
    }

    public function paginateCurrencies(int $per_page = 20): Paginator
    {
        return Currency::query()->paginate($per_page);
    }

    public function createCurrency(array $data): bool
    {
        $currency = new Currency();
        $currency->id = $data['id'];
        $currency->title = $data['title'];
        try {
            $currency->save();
        } catch (\Exception | QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    public function updateCurrency(Currency $currency, array $data): bool
    {
        if (isset($data['id'])) {
            $currency->id = $data['id'];
        }
        if (isset($data['title'])) {
            $currency->title = $data['title'];
        }
        try {
            $currency->save();
        } catch (\Exception | QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    public function deleteCurrency(Currency $currency = null): bool
    {
        try {
            $currency->delete();
        } catch (\Exception | QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;

    }

}
