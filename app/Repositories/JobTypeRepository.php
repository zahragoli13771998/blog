<?php

namespace App\Repositories;

use App\Models\JobTypes;
use Exception;
use App\Repositories\Interfaces\JobTypeRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;


class JobTypeRepository implements JobTypeRepositoryInterface
{
    /**
     * for getting all existing job type from model
     * @param bool $with_relation
     * @param bool $with_limit
     * @return array|Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll(bool $with_relation = false, bool $with_limit = true)
    {
        return JobTypes::query()->when($with_relation, function (Builder $query) {
            $query->with(['jobVaccancy']);
        })->get();
    }

    /**
     * for getting all existing job type from model
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate(): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return JobTypes::query()->paginate();
    }

    /**
     * for storing new job type
     * @param array $data
     * @return mixed|void
     */
    public function store(array $data): bool
    {
        $job_type = new JobTypes;
        $job_type->title_fa = $data['title_fa'];
        $job_type->title_en = $data['title_en'];
        try {
            $job_type->save();
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * for getting job types using their ids
     * @param int $id
     * @return Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed|null
     */
    public function getJobTypeById(int $id, bool $with_relation = false): JobTypes
    {
        return JobTypes::query()->when($with_relation, function (Builder $query) {
            $query->with(['jobVaccancy']);
        })->findOrFail($id);
    }

    /**
     * for updating  existing job types
     * @param JobTypes $job_type
     * @param array $data
     * @return mixed|void
     */
    public function update(JobTypes $job_type, array $data): bool
    {
        if (isset($data['title_fa'])) {
            $job_type->title_fa = $data['title_fa'];
        }
        if (isset($data['title_en'])) {
            $job_type->title_en = $data['title_en'];
        }
        try {
            $job_type->save();
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * for deleting existing job type
     * @param JobTypes $job_type
     * @return mixed|void
     * @throws \Exception
     */
    public function delete(JobTypes $job_type): bool
    {
        try {
            $job_type->delete();
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

}
