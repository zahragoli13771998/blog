<?php

namespace App\Repositories;

use App\Models\Comment;
use App\Models\Interfaces\HasComments;
use Exception;
use Illuminate\Database\Concerns\BuildsQueries;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class CommentVTwoRepository extends CommentRepository
{
    /**
     * create a new comment (polymorphic)
     * @param HasComments $commentable_obj
     * @param array $data
     * @return Comment
     */
    public function createComment(HasComments $commentable_obj, array $data): ?Comment
    {
        $comment = new Comment();

        $comment->user_id = $data['user_id'];

        $comment->body = $data['body'];

        if (isset($data['nickname'])) {
            $comment->nickname = $data['nickname'];
        }

        if (isset($data['comment_id'])) {
            $comment->comment_id = $data['comment_id'];
        }
        if (isset($data['depth_id'])) {
            $comment->depth_id = $data['depth_id'];
        }
        if (isset($data['type'])) {
            $comment->type = $data['type'];
        }else{
            $comment->type = 'comment';
        }
        if (isset($data['options'])) {
            $comment->options = $data['options'];
        }
        if (isset($data['end_time'])) {
            $comment->end_time = $data['end_time'];
        }
        if (isset($data['answer_index'])) {
            $comment->answer_index = $data['answer_index'];
        }


        try {
            $commentable_obj->comments()->save($comment);
        } catch (Exception | QueryException $ex) {
            Log::error($ex->getMessage());
            return null;
        }
        return $comment;
    }

    /**
     * update comments
     * @param array $data
     * @param Comment $comment
     * @return bool
     */
    public function updateComment(Comment $comment, array $data): bool//Todo remove if not needed
    {
        if (isset($data['user_id'])) {
            $comment->user_id = $data['user_id'];
        }
        if (isset($data['comment_id'])) {
            $comment->comment_id = $data['comment_id'];
        }
        if (isset($data['depth_id'])){
            $comment->depth_id = $data['depth_id'];
        }

        if (isset($data['body'])) {
            $comment->body = $data['body'];
        }
        if (isset($data['nickname'])) {
            $comment->nickname = $data['nickname'];
        }
        if (isset($data['status'])) {
            $comment->status = $data['status'];
        }

        try {
            $comment->save();
        } catch (Exception | QueryException $ex) {
            Log::error($ex->getMessage());
            return false;
        }
        return true;
    }

    /**
     * delete a comment
     * @param Comment|null $comment
     * @param HasComments|null $commentable_obj
     * @return bool|void
     */
    public function delete(Comment $comment = null, HasComments $commentable_obj = null): bool
    {
        try {
            if ($commentable_obj) {
                $commentable_obj->comments()->delete();
            }

            if ($comment) {
                $comment->replies()->delete();
                $comment->file()->delete();
                $comment->depths()->delete();
                $comment->delete();
            }
        } catch (Exception | QueryException $ex) {
            Log::error($ex->getMessage());
            return false;
        }
        return true;
    }

    /**
     * comments query builder
     * @param bool $only_root
     * @param bool $with_relation
     * @param HasComments|null $commentable_obj
     * @param bool $only_accepted
     * @param int|null $depth_id
     * @param int|null $user_id
     * @return BuildsQueries|MorphMany|mixed
     */
    protected function commentFetchQueryBuilder(bool $only_root = false, bool $with_relation = false, HasComments $commentable_obj = null, bool $only_accepted = false, int $depth_id = null, int $user_id = null)
    {
        return (($commentable_obj) ? $commentable_obj->comments() : Comment::query())->when($only_root, function (Builder $query) {
            $query->whereNull('depth_id');
        })->where('type','=','comment')->when($with_relation, function (Builder $query) use ($user_id, $only_accepted) {
            $query->with(['depths' => function ($query) use ($only_accepted, $user_id) {
                if ($only_accepted) {
                    $query->where('status', '=', 1);
                }

                $query->withCount(['depths' => function ($query) use ($only_accepted) {
                    if ($only_accepted) {
                        $query->where('status', '=', 1);
                    }
                }, 'likes', 'dislikes']);

                $query->when($user_id, function (Builder $query) use ($user_id) {
                    $query->with([
                        'likes' => fn ($query) => $query->where('user_id', '=', $user_id),
                        'dislikes' => fn ($query) => $query->where('user_id', '=', $user_id)
                    ]);
                });

            }, 'depths.file', 'depths.user:id,sid', 'file', 'user:id,sid']);

            $query->withCount(['depths' => function ($query) use ($only_accepted) {
                if ($only_accepted) {
                    $query->where('status', '=', 1);
                }
            }, 'likes', 'dislikes']);
        })
            ->when($only_accepted, function (Builder $query) {
                $query->where('status', '=', 1);
            })
            ->when(isset($depth_id), function (Builder $query) use ($depth_id) {
                $query->where('depth_id', '=', $depth_id);
            })
            ->when($user_id, function (Builder $query) use ($user_id) {
                $query->with([
                    'likes' => fn ($query) => $query->where('user_id', '=', $user_id),
                    'dislikes' => fn ($query) => $query->where('user_id', '=', $user_id)
                ]);
            });
    }
}

