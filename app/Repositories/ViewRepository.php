<?php


namespace App\Repositories;


use App\Models\Interfaces\HasViews;
use App\Models\Views;
use App\Repositories\Interfaces\ViewRepositoryInterface;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class ViewRepository implements ViewRepositoryInterface
{

    /**
     * Get all views and returns them
     * @return Collection
     */
    public function getViews(): Collection
    {
        return Views::with(['viewable'])->select(['viewable_type', 'viewable_id'])->get();
    }

    /**
     * Get views count
     * If param exists it will return desired view count
     * If not returns all views count
     * @param HasViews|null $viewable_obj
     * @return int
     */
    public function viewsCount(HasViews $viewable_obj): int
    {
        return $viewable_obj->views()->count();
    }

    /**
     * Paginate through all views
     * with option to customize paginator count per page
     * @param int $paginate_count
     * @return Paginator
     */
    public function paginateViews(int $paginate_count = 10): Paginator
    {
        $views = Views::with(['viewable'])->select(['viewable_type', 'viewable_id'])
            ->paginate($paginate_count);
        return $views;
    }

//    /**
//     * Check if a view has been already existed for Viewable object or not
//     * @param HasViews $viewable_object
//     * @param string $ip
//     * @param string|null $agent
//     * @param string|null $cookie
//     * @return bool
//     */
//    public function hasViews(HasViews $viewable_object, string $ip, string $agent = null, string $cookie = null): bool
//    {
//        return $viewable_object->views()->where('ip','=',$ip)
//            ->when($agent, function (Builder $query) use ($agent){
//                $query->where('browser_agent','=',$agent);
//            })
//            ->when($cookie, function (Builder $query) use ($cookie){
//                $query->where('cookie_string', '=', $cookie);
//            })
//            ->exists();
//    }

//    /**
//     * Add Views to related object
//     * @param HasViews $has_viewable_object
//     * @param array $data
//     * @return bool
//     */
//    public function addViews(HasViews $has_viewable_object, array $data): bool
//    {
//        $viewable_object = $has_viewable_object->views();
//
//        try{
//            $viewable_object->create([
//                'ip' => $data['ip'],
//                'cookie_string' => $data['cookie_string'],
//                'browser_agent' => $data['browser_agent']
//            ]);
//        }catch (QueryException | \Exception $e){
//            Log::error('Unable to add view with error: '.$e->getMessage());
//            return false;
//        }
//        return true;
//    }

    public function hasViews(HasViews $viewable_object, int $user_id = null): bool
    {
        return $viewable_object->views()->when($user_id,function (Builder $query) use ($user_id) {
            $query->where('user_id','=',$user_id);
        })->exists();

    }

    public function addViews(HasViews $has_viewable_object, array $data): bool
    {
        $viewable_object = $has_viewable_object->views();
        try{
            $viewable_object->create([
                'user_id' => $data['user_id']
            ]);
        }catch (QueryException | \Exception $e){
            Log::error('Unable to add view with error: '.$e->getMessage());
            return false;
        }
        return true;

    }
}
