<?php

namespace App\Repositories;

use App\Models\CurrencyOHLC;
use App\Repositories\Interfaces\CurrencyOHLCRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class CurrencyOHLCRepository implements CurrencyOHLCRepositoryInterface
{
    /**
     * Get and return single currency OHLC
     * @param array $input
     * @return CurrencyOHLC
     */
    public function getOHLC(array $input): ?CurrencyOHLC
    {
        return $this->fetchQueryBuilder($input)->first() ?? null;
    }

    /**
     * paginate Profit and damage.
     *
     * @param array $input
     * @param int $perpage
     * @return LengthAwarePaginator
     */
    public function paginate(array $input = [], int $perpage = 10): LengthAwarePaginator
    {
        return $this->fetchQueryBuilder($input)->paginate($perpage);
    }

    /**
     * Get the get all value from the database.
     *
     * @param array $input
     * @return Collection
     */
    public function getAll(array $input = []): Collection
    {
        return $this->fetchQueryBuilder($input)->get();
    }

    /**
     * find by id the record with the given id.
     *
     * @param int $id
     * @return CurrencyOHLC|Builder|Builder[]
     */
    public function findById(int $id): CurrencyOHLC
    {
        return $this->fetchQueryBuilder()->findOrFail($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param array $data
     * @return bool
     */
    public function store(array $data): bool
    {
        $currency_ohlc = new CurrencyOHLC();
        $currency_ohlc->global_currency_id = $data['global_currency_id'];
        $currency_ohlc->interval_id = $data['interval_id'];
        $currency_ohlc->high = $data['high'];
        $currency_ohlc->close = $data['close'];
        $currency_ohlc->low = $data['low'];
        $currency_ohlc->open = $data['open'];
        $currency_ohlc->market_cap = $data['market_cap'];
        $currency_ohlc->volume = $data['volume'];
        try {
            $currency_ohlc->save();
        } catch (QueryException | \Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param array $data
     * @param CurrencyOHLC $currency_ohlc
     * @return bool
     */
    public function update(array $data, CurrencyOHLC $currency_ohlc): bool
    {
        if (isset($data['global_currency_id'])) {
            $currency_ohlc->global_currency_id = $data['global_currency_id'];
        }

        if (isset($data['interval_id'])) {
            $currency_ohlc->interval_id = $data['interval_id'];
        }

        if (isset($data['high'])) {
            $currency_ohlc->high = $data['high'];
        }

        if (isset($data['close'])) {
            $currency_ohlc->close = $data['close'];
        }

        if (isset($data['low'])) {
            $currency_ohlc->low = $data['low'];
        }

        if (isset($data['open'])) {
            $currency_ohlc->open = $data['open'];
        }

        if (isset($data['market_cap'])) {
            $currency_ohlc->market_cap = $data['market_cap'];
        }

        if (isset($data['volume'])) {
            $currency_ohlc->volume = $data['volume'];
        }

        try {
            $currency_ohlc->save();
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param CurrencyOHLC $currency_ohlc
     * @return bool
     * @throws \Exception
     */
    public function delete(CurrencyOHLC $currency_ohlc): bool
    {
        try {
            $currency_ohlc->delete();
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * fetch query builder profit and Damage.
     *
     * @param array $input
     * @return Builder|mixed
     */
    private function fetchQueryBuilder(array $input = []): Builder
    {
        return CurrencyOHLC::query()
            ->when(isset($input['global_currency_id']), function (Builder $query) use ($input) {
                $query->where('global_currency_id', '=', $input['global_currency_id']);
            })
            ->when(isset($input['interval_id']), function (Builder $query) use ($input) {
                $query->where('interval_id', '=', $input['interval_id']);
            });
    }
}
