<?php

namespace App\Repositories;

use App\Models\JobVacancies;
use Exception;
use App\Repositories\Interfaces\JobVacancyRepositoryInterface;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class JobVacancyRepository implements JobVacancyRepositoryInterface
{
    /**
     * for getting all existing job vacancies from model
     * @return JobVacancies[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function getAll(int $job_type_id = null): Collection
    {
        return JobVacancies::query()->when($job_type_id,function (Builder $query) use ($job_type_id) {
            $query->where('job_type_id','=',$job_type_id);
        })->where('status','=',true)->get();
    }

    public function paginate(int $id){
        return JobVacancies::query()->where('job_type_id','=',$id)->paginate();
    }
    /**
     * for storing new job vacancy
     * @param array $data
     * @return mixed|void
     */
    public function store(array $data): bool
    {
        $job_vacancy = new JobVacancies;
        $job_vacancy->title_fa = $data['title_fa'];
        $job_vacancy->title_en = $data['title_en'];
        $job_vacancy->description = $data['description'];
        $job_vacancy->job_type_id = $data['job_type_id'];
        $job_vacancy->expire_time = $data['expire_time'];
        try {
            $job_vacancy->save();
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * for getting job vacancies using their ids
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed|null
     */
    public function getJobVacancyById(int $id): JobVacancies
    {
        return JobVacancies::query()->with('jobtype')->findOrFail($id);
    }

    /**
     * for updating  existing job vacancies
     * @param JobVacancies $job_vacancy
     * @param array $data
     * @return mixed|void
     */
    public function update(JobVacancies $job_vacancy, array $data): bool
    {
        if (isset($data['title_fa'])) {
            $job_vacancy->title_fa = $data['title_fa'];
        }
        if (isset($data['title_en'])) {
            $job_vacancy->title_en = $data['title_en'];
        }
        if (isset($data['description'])) {
            $job_vacancy->description = $data['description'];
        }
        if (isset($data['job_type_id'])) {
            $job_vacancy->job_type_id = $data['job_type_id'];
        }

        if (isset($data['status'])) {
            $job_vacancy->status = $data['status'];
        }

        if (isset($data['expire_time'])) {
            $job_vacancy->expire_time = $data['expire_time'];
        }
        try {
            $job_vacancy->save();
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * for deleting existing job vacancy
     * @param JobVacancies $job_vacancy
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function delete(JobVacancies $job_vacancy): bool
    {
        try {
            $job_vacancy->delete();
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }
}
