<?php


namespace App\Repositories;


use App\Models\GameMessage;
use App\Models\GameUser;
use App\Repositories\Interfaces\GameUserRepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Builder;

class GameUserRepository implements GameUserRepositoryInterface
{
    /**
     * Define max allowed play number per user
     */
    const ALLOWED_PLAY_NUMBER = 2;

    /**
     * paginate all game users
     * @param int $per_page
     * @param bool $with_relation
     * @param array $search
     * @return LengthAwarePaginator
     */
    public function paginateGameUsers(int $per_page, bool $with_relation, array $search = []): LengthAwarePaginator
    {
        return $this->userFetchQueryBuilder($with_relation, $search)->paginate($per_page);
    }

    /**
     * Checks if user exists or not
     * @param string $phone
     * @return GameUser
     */
    public function getGameUser(string $phone): ?GameUser
    {
        return $this->userFetchQueryBuilder()->where('phone_number', '=', $phone)->first();
    }

    /**
     * Checks if user exists or not
     * @param string $phone
     * @return bool
     */
    public function doesGameUserExist(string $phone): bool
    {
        return $this->userFetchQueryBuilder()->where('phone_number', '=', $phone)->exists();
    }

    /**
     * return active invited users count
     * @param GameUser $user
     * @return int
     */
    public function activeInvitedCount(GameUser $user): int
    {
        return $user->invited()->whereHas('scores')->count();
    }

    /**
     * return active invited users
     * @param GameUser $user
     * @return Collection
     */
    public function activeInvited(GameUser $user): Collection
    {
        return $user->invited()->whereHas('scores')->get();
    }

    /**
     * Creates a new user
     * @param array $data
     * @return GameUser|null
     */
    public function createGameUser(array $data): ?GameUser
    {
        $user = new GameUser();

        $user->phone_number = $data['phone_number'];

        if (isset($data['inviter_id'])) {
            $user->inviter_id = $data['inviter_id'];
        }

        try {
            $user->save();
        } catch (QueryException | Exception $e) {
            Log::error('Unable to create user with error: ' . $e->getMessage());
            return null;
        }

        return $user;
    }

    /**
     * game user's query builder
     * @param bool $with_relation
     * @param array $search
     * @return Builder
     */
    private function userFetchQueryBuilder(bool $with_relation = null, array $search = []): Builder
    {
        return GameUser::query()->when($with_relation, function (Builder $query) {
            $query->with(['scores', 'invited']);
        })->when(isset($search['phone']), function (Builder $query) use ($search) {
            $query->where('phone_number', 'like', '%' . $search['phone'] . '%');
        });
    }

    /**
     * @param GameUser $gameUser
     * @return mixed|void
     */
    public function canUserPlay(GameUser $gameUser): bool
    {
        $played_count = $gameUser->scores()->count();
        $allowed_count = self::ALLOWED_PLAY_NUMBER + $this->activeInvitedCount($gameUser);
        if ($played_count < $allowed_count) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * get user bonus
     * @param GameUser $gameUser
     * @return int
     */
    public function getUserBonus(GameUser $gameUser): int
    {
        $played_count = $gameUser->scores()->count();
        $allowed_count = self::ALLOWED_PLAY_NUMBER + $this->activeInvitedCount($gameUser);
        return $allowed_count - $played_count;
    }
}
