<?php


namespace App\Repositories;


use App\Models\GlobalCurrency;
use App\Models\Trend;
use App\Models\Views;
use App\Repositories\Interfaces\TrendRepositoryInterfac;
use Exception;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class TrendRepository implements TrendRepositoryInterfac
{
    /**
     * get and return Trend according to given id
     * @param int $id
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getById(int $id): Trend
    {
        return Trend::query()->findOrFail($id);
    }

    /**
     * get and return all Trends
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllTrends(): Collection
    {
        return Trend::query()->get();
    }

    /**
     * get daily trends
     * @param string $start_duration
     * @param string $now
     * @return Builder[]|Collection
     */
    public function dailyTrends(string $start_duration, string $now): Collection
    {
        return Views::query()->where('viewable_type', '=', 'App\Models\GlobalCurrency')
            ->wherein('viewable_id', [1, 2, 10, 4, 5, 6, 3, 14, 32, 9, 16, 12, 8, 7, 19, 33, 17, 45, 26, 15, 11, 124, 86, 36, 41, 6689, 135, 73, 20, 155, 1660, 189, 69, 6704, 6948, 25, 1656, 40, 138, 24, 65, 35, 52, 97, 90, 34, 28, 1657, 9275,
                7604, 50, 150, 8339, 6963, 9244, 6944, 55, 38, 102, 7523, 47, 101])
            ->whereBetween('created_at', [$start_duration, $now])->get();
    }

    /**
     * get daily trends
     * @param string $start_duration
     * @param string $now
     * @return Builder[]|Collection
     */
    public function dailyGlobalTrends(string $start_duration, string $now): Collection
    {
        return Views::query()->where('viewable_type', '=', 'App\Models\GlobalCurrency')
            ->whereBetween('created_at', [$start_duration, $now])->get();
    }

    /**
     * paginate Trends
     * @param int $per_page
     * @return Paginator
     */
    public function paginateTrends(int $per_page = 4): Paginator
    {
        return Trend::query()->paginate($per_page);
    }

    /**
     * create a new Trend
     * @param array $data
     * @return bool
     */
    public function createTrend(array $data): bool
    {
        $trend = new Trend();
        $trend->id = $data['id'];
        try {
            $trend->save();
        } catch (QueryException | Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * update an existing Trend
     * @param Trend $trend
     * @param array $data
     * @return bool
     */
    public function updateTrend(Trend $trend, array $data): bool
    {
        if (isset($data['id'])) {
            $trend->id = $data['id'];
        }
        try {
            $trend->save();
        } catch (QueryException | Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * delete Trend
     * @param Trend|null $trend
     * @return bool
     */
    public function deleteTrend(Trend $trend = null): bool
    {
        try {
            if ($trend) {
                $trend->delete();
            }

            if (!$trend) {
                Trend::query()->delete();
            }

        } catch (QueryException | Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

}
