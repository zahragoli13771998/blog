<?php

namespace App\Repositories;

use App\Models\Degrees;
use Exception;
use App\Repositories\Interfaces\DegreeRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class DegreeRepository implements DegreeRepositoryInterface
{
    /**
     * for getting all existing degree from model
     * @return Degrees[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function getAll(): Collection
    {
        return Degrees::query()->get();
    }

    /**
     * for getting all existing degree from model
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate(): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return Degrees::query()->paginate(10);
    }

    /**
     * for storing new degree
     * @param array $data
     * @return mixed|void
     */
    public function store(array $data): bool
    {
        $degree = new Degrees();
        $degree->title_fa = $data['title_fa'];
        $degree->title_en = $data['title_en'];
        try {
            $degree->save();
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * for getting Degrees using their ids
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed|null
     */
    public function getDegreeById(int $id): Degrees
    {
        return Degrees::query()->findOrFail($id);
    }

    /**
     * for updating  existing Degrees
     * @param Degrees $degree
     * @param array $data
     * @return mixed|void
     */
    public function update(Degrees $degree, array $data): bool
    {
        if (isset($data['title_fa'])) {
            $degree->title_fa = $data['title_fa'];
        }
            if (isset($data['title_en'])) {
            $degree->title_en = $data['title_en'];
        }

        try {
            $degree->save();
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * for deleting existing degree
     * @param Degrees $degree
     * @return bool
     * @throws \Exception
     */
    public function delete(Degrees $degree): bool
    {
        try {
            $degree->delete();
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }
}
