<?php


namespace App\Repositories\Interfaces;

use Illuminate\Contracts\Pagination\Paginator;
use App\Models\Interfaces\HasIncludes;


interface IncludeToRazminexRepositoryInterface
{
    /**
     * get the number of IncludeToRamzinex based on given includable_obj
     * @param \App\Models\Interfaces\HasIncludes $includable_obj
     * @return int
     */
    public function getIncludeToRamzinexCount(HasIncludes $includable_obj): int;

    /**
     * check if selected global_currency is included by this user or not
     * @param \App\Models\Interfaces\HasIncludes $includable_obj
     * @param int $user_id
     * @return bool
     */
    public function checkIfIncludeToRamzinexExists(HasIncludes $includable_obj, int $user_id): bool;

    /**
     * create a new IncludeToRamzinex
     * @param HasIncludes $includable_obj
     * @param int $user_id
     * @return mixed
     */
    public function createIncludeToRamzinex(HasIncludes $includable_obj, int $user_id);

    /**
     * delete a IncludeToRamzinex
     * @param HasIncludes $includable_obj
     * @return bool
     */
    public function deleteIncludeToRamzinex(HasIncludes $includable_obj): bool;

}
