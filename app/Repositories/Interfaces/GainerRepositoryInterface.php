<?php


namespace App\Repositories\Interfaces;


use App\Models\Gainer;
use Illuminate\Database\Eloquent\Collection;
use  Illuminate\Contracts\Pagination\Paginator;

interface GainerRepositoryInterface
{

    /**
     * get gainer according to given id and return it
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getById(int $id): Gainer;

    /**
     * get and return all gainers
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllGainers(bool $order_by_value = false): Collection;

    /**
     * paginate gainers
     * @param int $per_page
     * @return Paginator
     */
    public function paginateGainers(int $per_page = 10): Paginator;

    /**
     * create a new gainer
     * @param array $data
     * @return bool
     */
    public function createGainer(array $data): bool;

    /**
     * update an existing gainer
     * @param Gainer $gainer
     * @param array $data
     * @return bool
     */
    public function updateGainer(Gainer $gainer, array $data): bool;

    /**
     * delete gainer
     * @param Gainer|null $gainer
     * @return bool
     */
    public function deleteGainer(Gainer $gainer = null): bool;
}
