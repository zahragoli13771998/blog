<?php


namespace App\Repositories\Interfaces;


use App\Models\Looser;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

interface LooserRepositoryInterface
{

    /**
     * get Looser according to given id and return it
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getById(int $id): Looser;

    /**
     * get and return all Loosers
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllLoosers(bool $order_by_value = false): Collection;

    /**
     * paginate Looser
     * @param int $per_page
     * @return Paginator
     */
    public function paginateLooser(int $per_page = 10): Paginator;

    /**
     * create a new Looser
     * @param array $data
     * @return bool
     */
    public function createLooser(array $data): bool;

    /**
     * update an existing Looser
     * @param Looser $looser
     * @param array $data
     * @return bool
     */
    public function updateLooser(Looser $looser, array $data): bool;

    /**
     * delete Looser
     * @param Looser|null $looser
     * @return bool
     */
    public function deleteLooser(Looser $looser = null): bool;

}
