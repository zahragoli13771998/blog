<?php

namespace App\Repositories\Interfaces;

use App\Models\Interval;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

interface IntervalRepositoryInterface
{
    /**
     * paginate interval.
     *
     * @param int $perpage
     * @return LengthAwarePaginator
     */
    public function paginate(int $perpage =10):LengthAwarePaginator;

    /**
     * Get the value from the database.
     *
     * @return mixed
     */
    public function getAll():Collection;

    /**
     * find by id the record with the given id.
     *
     * @param int $id
     * @return Interval
     */
    public function findById(int $id): Interval;

    /**
     * Store a newly created resource in storage.
     *
     * @param array $value
     * @return bool
     */
    public function store(array $value):bool;

    /**
     * Update the specified resource in storage.
     *
     * @param array $value
     * @param Interval $interval
     * @return bool
     */
    public function update(array $value, Interval $interval):bool;

    /**
     * Remove the specified resource from storage.
     *
     * @param Interval $interval
     * @return bool
     */
    public function delete(Interval $interval):bool;
}
