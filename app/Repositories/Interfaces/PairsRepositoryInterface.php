<?php


namespace App\Repositories\Interfaces;


interface PairsRepositoryInterface
{

    /**
     * Get and return all pairs info from ramzinex api
     * @param int|null $pair_id
     * @return array|null
     */
    public function getAllPairsInfo(int $pair_id = null): ?array;
}
