<?php

namespace App\Repositories\Interfaces;

use App\Models\BannerType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

interface BannerTypeRepositoryInterface
{
    /**
     * paginate banner types.
     *
     * @param int $perpage
     * @return LengthAwarePaginator
     */
    public function paginate(int $perpage =10):LengthAwarePaginator;

    /**
     * Get the value from the database.
     *
     * @return mixed
     */
    public function getAll():Collection;

    /**
     * find by id the record with the given id.
     *
     * @param int $id
     * @return BannerType
     */
    public function findById(int $id):BannerType;

    /**
     * Store a newly created resource in storage.
     *
     * @param array $value
     * @return bool
     */
    public function store(array $value):bool;

    /**
     * Update the specified resource in storage.
     *
     * @param array $value
     * @param BannerType $banner_types
     * @return bool
     */
    public function update(array $value, BannerType $banner_types):bool;

    /**
     * Remove the specified resource from storage.
     *
     * @param BannerType $banner_types
     * @return bool
     */
    public function delete(BannerType $banner_types):bool;
}
