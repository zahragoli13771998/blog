<?php


namespace App\Repositories\Interfaces;
use App\Models\Interfaces\HasViews;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

interface ViewRepositoryInterface
{
    /**
     * Get all views and returns them
     * @return Collection
     */
    public function getViews(): Collection;


    /**
     * Get views count
     * If param exists it will return desired view count
     * If not returns all views count
     * @param HasViews|null $viewable_obj
     * @return int
     */
    public function viewsCount(HasViews $viewable_obj): int;


    /**
     * Paginate through all views
     * with option to customize paginator count per page
     * @param int $paginate_count
     * @return Paginator
     */
    public function paginateViews(int $paginate_count = 10): Paginator;


    /**
     * Check if a view has been already existed for Viewable object or not
     * @param HasViews $viewable_object
     * @param int|null $user_id
     * @return bool
     */
//    public function hasViews(HasViews $viewable_object, string $ip, string $agent = null, string $cookie = null): bool;
    public function hasViews(HasViews $viewable_object, int $user_id = null): bool;


    /**
     * Add Views to related object
     * @param HasViews $has_viewable_object
     * @param array $data
     * @return bool
     */
    public function addViews(HasViews $has_viewable_object, array $data): bool;




}
