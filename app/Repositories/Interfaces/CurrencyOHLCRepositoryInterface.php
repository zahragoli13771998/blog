<?php

namespace App\Repositories\Interfaces;

use App\Models\CurrencyOHLC;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

interface CurrencyOHLCRepositoryInterface
{
    /**
     * Get and return single currency OHLC
     * @param array $input
     * @return CurrencyOHLC
     */
    public function getOHLC(array $input): ?CurrencyOHLC;

    /**
     * paginate currencies change percent.
     *
     * @param array $input
     * @param int $perpage
     * @return LengthAwarePaginator
     */
    public function paginate(array $input = [], int $perpage =10): LengthAwarePaginator;

    /**
     * Get the get all value from the database.
     *
     * @param array $input
     * @return Collection
     */
    public function getAll(array $input = []): Collection;

    /**
     * find by id the record with the given id.
     *
     * @param int $id
     * @return CurrencyOHLC
     */
    public function findById(int $id): CurrencyOHLC;

    /**
     * Store a newly created resource in storage.
     *
     * @param array $data
     * @return bool
     */
    public function store(array $data): bool;

    /**
     * Update the specified resource in storage.
     *
     * @param array $data
     * @param CurrencyOHLC $profitDamage
     * @return bool
     */
    public function update(array $data, CurrencyOHLC $profitDamage):bool;

    /**
     * Remove the specified resource from storage.
     *
     * @param CurrencyOHLC $currenccyohlc
     * @return bool
     */
    public function delete(CurrencyOHLC $currenccyohlc): bool;
}
