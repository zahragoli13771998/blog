<?php

namespace App\Repositories\Interfaces;

use App\Models\FaqCategory;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;


interface FaqCategoryRepositoryInterface
{

    /**
     * gets all categories
     * @param bool $only_root
     * @param bool $load_relation
     * @param array|null $search
     * @return Collection
     */
    public function getAllCategories(bool $only_root = false ,bool $load_relation = false, string $search = null): Collection;


    /**
     * returns FaqCategoryService instance according to given id
     * @param int $id
     * @param bool $load_relation
     * @param int|null $user_id
     * @return FaqCategory
     */
    public function getById(int $id, bool $load_relation = false, int $user_id = null): FaqCategory;


    /**
     * paginate categories
     * @param int $per_page
     * @param bool $only_root
     * @param bool $load_relation
     * @return Paginator
     */
    public function paginateCategory(int $per_page = 10 ,bool $only_root = false ,bool $load_relation = false): paginator;

    /**
     * return category with its questionAnswers according to given id
     * @param array $data
     * @param bool $only_root
     * @param bool $load_relation
     * @return Builder[]|Collection|mixed
     */
    public function getCategoryAndRelatedQuestions(array $data =[] , bool $only_root = false ,bool $load_relation = false):Collection;

    /**
     * it creates a new FaqCategoryService
     * @param array $data
     * @return bool
     */
    public function addCategory(array $data): bool;


    /**
     * it edits an existing FaqCategoryService
     * @param array $data
     * @param FaqCategory $category
     * @return bool
     */
    public function editCategory(array $data, FaqCategory $category): bool;


    /**
     * it deletes an existing FaqCategoryService
     * @param FaqCategory $category
     * @return bool
     */
    public function deleteCategory(FaqCategory $category): bool;
}
