<?php
namespace App\Repositories\Interfaces;

use App\Models\DeepLink;
use App\Models\Interfaces\HasDeepLink;

interface DeepLinkRepositoryInterface
{
    /**
     * * check if deeplink exists or not
     * if yes returns the first one
     * @param HasDeepLink $deep_linkable
     * @return DeepLink|null
     */
    public function getDeeplink(HasDeepLink $deep_linkable):?DeepLink;

    /**
     * get Deeplink by given id
     * @param int $id
     * @return DeepLink
     */
    public function getSingleDeeplink(int $id):DeepLink;

    /**
     * cretes a polymorphic deeplink data for a specific data
     * @param HasDeepLink $deep_linkable
     * @param array $data
     * @return DeepLink|null
     */
    public function createDeeplink(HasDeepLink $deep_linkable, array $data): ?DeepLink;

    /**
     * updates deeplink with given data
     * @param DeepLink $deeplink
     * @param array $data
     * @return bool
     */
    public function updateDeeplink(DeepLink $deeplink, array $data): bool;

    /**
     * delete deeplink
     * @param DeepLink $deep_link
     * @return bool
     */
    public function deleteDeeplink(DeepLink $deep_link): bool;

}
