<?php

namespace App\Repositories\Interfaces;

use App\Models\FaqCategory;
use App\Models\FaqQuestionAnswer;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;


interface FaqQuestionAnswerRepositoryInterface
{

    /**
     * return an instance of FaqQuestionAnswer model according to given id
     * @param int $id
     * @param bool $with_counts
     * @param int|null $user_id
     * @return FaqQuestionAnswer
     */
    public function getById(int $id, bool $with_counts = false, int $user_id = null): FaqQuestionAnswer;

    /**
     * @return Builder[]|Collection
     */
    public function getFaq();

    public function getByFaqCategoryId($id);

    /**
     * @param bool $only_repetitive
     * @param bool $with_counts
     * @param array $search
     * @return Builder[]|Collection
     */
    public function getAllQuestionAnswer(bool $only_repetitive = false, bool $with_counts = false, string $search = null): Collection;

    /**
     * paginate questionAnswers
     * @param array $data
     * @param int $per_page
     * @param bool $with_counts
     * @return Paginator
     */
    public function paginateQuestionAnswer(array $data = [], int $per_page = 10, bool $with_counts = false): paginator;

    /**
     * create a questionAnswer
     * @param array $data
     * @param $faqCategory
     * @return bool
     */
    public function addQuestionAnswer(array $data, FaqCategory $faqCategory): bool;


    /**
     * update an existing questionAnswer
     * @param array $data
     * @param FaqQuestionAnswer $question_answer
     * @return bool
     */
    public function updateQuestionAnswer(array $data, FaqQuestionAnswer $question_answer): bool;


    /**
     * delete  an existing questionAnswer
     * @param FaqQuestionAnswer $question_answer
     * @return bool
     */
    public function deleteQuestionAnswer(FaqQuestionAnswer $question_answer): bool;

}
