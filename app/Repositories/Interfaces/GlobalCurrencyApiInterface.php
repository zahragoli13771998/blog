<?php


namespace App\Repositories\Interfaces;

/**
 * Interface GlobalCurrenciesApiInterface
 * @package App\Repositories\Interfaces
 */
interface GlobalCurrencyApiInterface
{
    /**
     * Get all Global Currencies list from API Service
     * @return array
     */
    public function getGlobalCurrenciesList(): array;

    /**
     * Get specific Global Currency information by it's id from service
     * @param string $id
     * @return array|void
     */
    public function getGlobalCurrencyTickerByID(string $id): array;

    /**
     * Get all Global Currencies information from service
     * @return array
     */
    public function tickersAllGlobalCurrencies(): array;

    /**
     * Get desired global currency ohlc and return it
     * @param string $coin_id
     * @param int $unix_from
     * @param int $unix_to
     * @return array
     */
    public function getGlobalCurrencyOHLC(string $coin_id, int $unix_from, int $unix_to): array;
}
