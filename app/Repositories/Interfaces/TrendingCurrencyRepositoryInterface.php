<?php


namespace App\Repositories\Interfaces;

use App\Models\TrendingCurrency;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\Paginator;

interface TrendingCurrencyRepositoryInterface
{
    /**
     * get and return trending_currency according to given id
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getById(int $id): TrendingCurrency;

    /**
     * get and return all trending_currency
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllTrendingCurrency(): Collection;

    /**
     * paginate trending_currency
     * @param int $per_page
     * @return Paginator
     */
    public function paginateTrendingCurrency(int $per_page = 10): Paginator;
    /**
     * create trending_currency
     * @param array $data
     * @return bool
     */
    public function createTrendingCurrency(array $data): bool;

    /**
     * update trending_currency
     * @param TrendingCurrency $currency_trending
     * @param array $data
     * @return bool
     */
    public function updateTrendingCurrency(TrendingCurrency $currency_trending, array $data): bool;

    /**
     * delete trending_currency
     * @param TrendingCurrency $currency_trending
     * @return mixed
     */
    public function deleteTrendingCurrency(TrendingCurrency $currency_trending);
}
