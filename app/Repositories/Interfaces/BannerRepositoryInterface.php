<?php

namespace App\Repositories\Interfaces;

use App\Models\Banner;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

interface BannerRepositoryInterface
{

    /**
     *  it returns an instance of banner model according to given id
     * @param int $id
     * @param bool $with_relation
     * @param bool $with_counts
     * @param int|null $user_id
     * @return Banner|Builder|Builder[]|Collection|Model
     */
    public function getById(int $id, bool $with_relation = false, bool $with_counts = false, int $user_id = null): Banner;

    /**
     * this paginate the Banners
     * @param bool $with_counts
     * @param int $count
     * @param array $input
     * @return Paginator
     */
    public function paginateBanner(bool $with_counts = false, int $count = 10, array $input = []): Paginator;

    /**
     * it returns all active banners
     * @param int|null $type_id
     * @param int|null $from
     * @param int|null $limit
     * @param bool $only_enabled
     * @param bool $with_relation
     * @param bool $with_counts
     * @param int|null $user_id
     * @param bool $sort_desc
     * @return Collection
     */
    public function getAllBanners(int $type_id = null, int $from = null, int $limit = null, bool $only_enabled = true, bool $with_relation = false, bool $with_counts = false, int $user_id = null, bool $sort_desc = false): Collection;


    public function getSliders();

    /**
     * Get and return all banners counts
     * @param int|null $type_id
     * @param bool $only_enabled
     * @return int
     */
    public function getBannersCount(int $type_id = null, bool $only_enabled = true): int;

    /**
     * add global_currencies to banner
     * @param Banner $banner
     * @param array $currencies
     * @return bool
     */
    public function attachGlobalCurrencies(Banner $banner, array $currencies): bool;

    /**
     * it create a new Banner or update an existing Banner
     * @param array $data
     * @return bool
     */
    public function createBanner(array $data): bool;


    /**
     * update existing Banners
     * @param array $data
     * @param Banner $banner
     * @return bool
     */
    public function updateBanner(array $data, Banner $banner): bool;


    /**
     * this function deletes a Banner
     * @param Banner $banner
     * @return bool
     */
    public function destroyBanners(Banner $banner): bool;
}
