<?php


namespace App\Repositories\Interfaces;


use App\Models\GameUser;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

interface GameUserRepositoryInterface
{
    /**
     * Checks if user exists or not
     * @param int $per_page
     * @param bool $with_relation
     * @param array $search
     * @return LengthAwarePaginator
     */
    public function paginateGameUsers(int $per_page, bool $with_relation, array $search = [] ): LengthAwarePaginator;

    /**
     * Checks if user exists or not
     * @param string $phone
     * @return bool
     */
    public function doesGameUserExist(string $phone): bool;

    /**
     * return active invited users count
     * @param GameUser $user
     * @return int
     */
    public function activeInvitedCount(GameUser $user): int;

    /**
     * return active invited users
     * @param GameUser $user
     * @return Collection
     */
    public function activeInvited(GameUser $user): Collection;

    /**
     * Checks if user exists or not
     * @param string $phone
     * @return GameUser
     */
    public function getGameUser(string $phone): ?GameUser;

    /**
     * Creates a new user
     * @param array $data
     * @return GameUser|null
     */
    public function createGameUser(array $data): ?GameUser;

    /**
     * check user can play or not
     * @param GameUser $gameUser
     * @return mixed
     */
    public function canUserPlay(GameUser $gameUser):bool;

    /**
     * get user bonus
     * @param GameUser $gameUser
     * @return int
     */
    public function getUserBonus(GameUser $gameUser):int;
}
