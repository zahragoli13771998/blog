<?php


namespace App\Repositories\Interfaces;

use App\Models\GCurrencyRamzinexInfo;
use App\Models\GlobalCurrency;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

interface GCurrencyRamzinexInfoInterface
{
    /**
     * Get all global Currencies ramzinex info
     * @param bool $with_relation
     * @return Collection
     */
    public function getAll(bool $with_relation = false): Collection;

    /**
     * paginate all global currency ramzinex info.
     * @param int $perpage
     * @param bool $with_relation
     * @return LengthAwarePaginator
     */
    public function paginate(int $perpage = 10, bool $with_relation = false): LengthAwarePaginator;

    /**
     * findById the record with the given id.
     *
     * @param int $id
     * @return GCurrencyRamzinexInfo
     */
    public function findById(int $id): GCurrencyRamzinexInfo;

    /**
     * create global currency ramzinex info
     * @param array $data
     * @param GlobalCurrency $currency
     * @return bool
     */
    public function store(array $data, GlobalCurrency $currency): bool;

    /**
     * update currency ramzinex info
     * @param array $data
     * @param GCurrencyRamzinexInfo $ramzinex_info
     * @return bool
     */
    public function update(array $data, GCurrencyRamzinexInfo $ramzinex_info): bool;

    /**
     * Remove the specified resource from storage.
     * @param GCurrencyRamzinexInfo $info
     * @return bool
     * @throws \Exception
     */
    public function delete(GCurrencyRamzinexInfo $info): bool;
}
