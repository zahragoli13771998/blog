<?php

namespace App\Repositories\Interfaces;

use App\Models\GlobalCurrency;
use Illuminate\Support\Collection;

interface GlobalCurrencyV2Interface
{
    /**
     * Get and return single Global Currency by it's id
     * @param int|null $id
     * @param bool $relation
     * @param bool $only_active_changes
     * @param string|null $symbol
     * @param bool $with_ramzinex_info
     * @return GlobalCurrency|null
     */
    public function getById(int $id = null, bool $relation = false, bool $only_active_changes = false, string $symbol = null, bool $with_ramzinex_info = false): ?GlobalCurrency;

    /**
     * Get all global Currencies from DB
     * @param bool $relation
     * @param bool $only_short_info
     * @param int|null $from
     * @param int|null $take
     * @param bool $order_by_rank_asc
     * @param string|null $sort_by
     * @param array $search
     * @param bool $without_hidden
     * @return Collection
     */
    public function getAllGlobalCurrencies(bool $relation = false, bool $only_short_info = false, int $from = null, int $take = null, bool $order_by_rank_asc = false, string $sort_by = null, array $search = [], bool $without_hidden = false): Collection;

    /**
     * return number of global currencies
     * @return int
     */
    public function globalCurrenciesCount(): int;

    public function getUserLikesList(int $user_id);
}
