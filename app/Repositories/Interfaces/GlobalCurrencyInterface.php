<?php


namespace App\Repositories\Interfaces;

use App\Models\GCurrencyRamzinexInfo;
use App\Models\GlobalCurrency;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

interface GlobalCurrencyInterface
{
    /**
     * Create new Currency
     * @param array $data
     * @return bool
     */
    public function createCurrency(array $data): bool;

    /**
     * Create change percent for Global Currency
     * @param array $data
     * @param GlobalCurrency $currency
     * @return bool
     */
    public function createCurrencyChange(array $data, GlobalCurrency $currency): bool;

    /**
     * Create new ohlc for existing global currency
     * @param GlobalCurrency $global_currency
     * @param array $data
     * @return bool
     */
    public function createCurrencyOHLC(GlobalCurrency $global_currency, array $data): bool;

    /**
     *  add banner to global currencies
     * @param GlobalCurrency $global_currency
     * @param array $banners
     * @return bool
     */
    public function attachBanners(GlobalCurrency $global_currency, array $banners): bool;

    /**
     * Get all global Currencies from DB
     * @param bool $relation
     * @param bool $only_short_info
     * @param int|null $from
     * @param int|null $take
     * @param int|null $user_id
     * @param bool $order_by_rank_asc
     * @param string|null $sort_by
     * @param array $search
     * @param bool $without_hidden
     * @return Collection
     */
    public function getAllGlobalCurrencies(bool $relation = false, bool $only_short_info = false, int $from = null, int $take = null, int $user_id = null,  bool $order_by_rank_asc = false , string $sort_by = null ,array $search = [],bool $without_hidden = false): Collection;

    /**
     * return all pair global currencies
     * @param bool $with_relation
     * @return Collection
     */
    public function getPairGlobalCurrencies(bool $with_relation): Collection;


    /**
     * paginate all global currencies
     * @param int $per_page
     * @param bool $order_by_rank_asc
     * @param string|null $sort_by
     * @param array $search
     * @return Paginator
     */
    public function paginate(int $per_page = 10, bool $order_by_rank_asc = false, string $sort_by = null , array $search = []): Paginator;


    /**
     * checks if currency with this symbol exists
     * @param string $symbol_id
     * @return bool
     */
    public function checkIfCurrencyExists(string $symbol_id): bool;

    /**
     * Check if currency change percent exists
     * @param GlobalCurrency $global_currency
     * @param int $interval_id
     * @return bool
     */
    public function isCurrencyChangePercentExists(GlobalCurrency $global_currency, int $interval_id): bool;

    /**
     * Check if currency has ramzinex info
     * @param GlobalCurrency $global_currency
     * @return GCurrencyRamzinexInfo
     */
    public function isRamzinexCurrencyInfoExists(GlobalCurrency $global_currency): ?GCurrencyRamzinexInfo;

    /**
     * Check if currency ohlc exists or not
     * @param GlobalCurrency $global_currency
     * @param int $interval_id
     * @return bool
     */
    public function isCurrencyOHLCExists(GlobalCurrency $global_currency, int $interval_id): bool;

    /**
     * return number of global currencies
     * @return int
     */
    public function globalCurrenciesCount(): int;

    /**
     * Get single Global Currencies by id
     * @param int|null $id
     * @param bool $relation
     * @param bool $only_active_changes
     * @param int|null $user_id
     * @param string|null $symbol
     * @param bool $with_ramzinex_info
     * @return GlobalCurrency
     */
    public function getById(int $id = null, bool $relation = false,  bool $only_active_changes = false, int $user_id = null, string $symbol = null, bool $with_ramzinex_info = false): ?GlobalCurrency;

    public function filterGlobals(array $data = [] , string $sort_by = null);
    /**
     * Update existing crypto Currencies
     * @param array $data
     * @param GlobalCurrency $currency
     * @return bool
     */
    public function update(array $data, GlobalCurrency $currency): bool;

    /**
     * disable old currency changes
     * @param GlobalCurrency $global_Currency
     * @param int $interval_id
     * @return bool
     */
    public function disableCurrencyChangesPercent(GlobalCurrency $global_Currency, int $interval_id): bool;

    /**
     * Disable and delete stored ohlc with specific period for global currency
     * @param GlobalCurrency $global_currency
     * @param int $interval_id
     * @return bool
     */
    public function disableCurrencyOHLC(GlobalCurrency $global_currency, int $interval_id): bool;

    /**
     * Delete existing crypto Currency
     * @param GlobalCurrency $currency
     * @return bool
     */
    public function delete(GlobalCurrency $currency): bool;

    public function getTopGlobalsWithRamzinexGlobals(bool $relation=false,bool $only_active_changes_percent=false);
}
