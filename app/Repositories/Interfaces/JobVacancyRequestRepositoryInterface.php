<?php

namespace App\Repositories\Interfaces;

use App\Models\JobVacancies;
use App\Models\JobVacancyRequest;
use Illuminate\Http\Request;

interface JobVacancyRequestRepositoryInterface
{
    public function getAll();

    /**
     * for storing information and resume file
     * @param JobVacancies|null $job_vacancy
     * @param array $data
     * @return mixed
     */
    public function store(array $data,JobVacancies $job_vacancy = null);

    /**
     * for getting requests using id
     * @param int $id
     * @return mixed
     */
    public function getJobVacancyRequestById(int $id);

    /**
     * for showing job vacancies request for each job vacancy using id
     * @param int $id
     * @return mixed
     */
    public function showById(int $id);

    /**
     * for deleting existing request using id
     * @param JobVacancyRequest $user_job
     * @return mixed
     */
    public function delete(JobVacancyRequest $user_job);

    /**
     * for updating status by admin
     * @param JobVacancyRequest $user_job
     * @param array $data
     * @return mixed
     */
    public function update(JobVacancyRequest $user_job,array $data);

    /**
     * for getting recorded  job request by job type
     * @param int $id
     * @return mixed
     */
    public function getJobVacancyRequestByType(int $id);


    /**
     * for search in job vacancies using job types
     * @param int $id
     * @return mixed
     */
    public function search(int $id);
}
