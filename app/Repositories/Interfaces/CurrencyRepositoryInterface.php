<?php

namespace App\Repositories\Interfaces;

use App\Models\Currency;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

interface CurrencyRepositoryInterface
{
    /**
     * get Currency according to given id and return it
     * @param int $id
     * @return Builder[]|Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getById(int $id): Currency;

    /**
     * get and return all Currencies
     * @return Builder[]|Collection
     */
    public function getAllCurrencies(): Collection;

    /**
     * paginate Currencies
     * @param int $per_page
     * @return Paginator
     */
    public function paginateCurrencies(int $per_page = 10): Paginator;

    /**
     * create a new Currency
     * @param array $data
     * @return bool
     */
    public function createCurrency(array $data): bool;

    /**
     * update an existing Currency
     * @param Currency $currency
     * @param array $data
     * @return bool
     */
    public function updateCurrency(Currency $currency, array $data): bool;

    /**
     * delete Currency
     * @param Currency|null $currency
     * @return bool
     */
    public function deleteCurrency(Currency $currency = null): bool;
    

}
