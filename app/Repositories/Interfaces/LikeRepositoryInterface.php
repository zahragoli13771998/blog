<?php

namespace App\Repositories\Interfaces;

use App\Models\Interfaces\HasLikes;


interface LikeRepositoryInterface
{
    /**
     * Get and return likes count for related entity
     * @param HasLikes $likeable
     * @return int
     */
    public function getLikeCount(HasLikes $likeable): int;

    /**
     * Check if likeable is liked before or not
     * @param HasLikes $likeable
     * @param int $user_id
     * @return bool
     */
    public function isLiked(HasLikes $likeable, int $user_id): bool;

    /**
     * Add Users likes on related entity
     * @param HasLikes $likeable
     * @param int $user_id
     * @param string|null $type
     * @return boolean
     */
    public function createLike(HasLikes $likeable, int $user_id, string $type = null): bool;

    /**
     * Delete single like or likes for related entity
     * @param HasLikes $likeable
     * @param int|null $user_id
     * @param string|null $type
     * @return boolean
     */
    public function deleteLike(HasLikes $likeable, int $user_id = null , string $type = null): bool;

    public function getUserLikesList(int $user_id,string $type=null);

    public function getUserCommentsLikesList(int $user_id);
}
