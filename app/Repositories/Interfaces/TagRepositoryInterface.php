<?php


namespace App\Repositories\Interfaces;


use App\Models\Interfaces\HasTag;
use App\Models\Tag;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

interface TagRepositoryInterface
{
    /**
     * get tag and return it according to given id
     * @param int $id
     * @return Tag
     */
    public function getById(int $id): Tag;

    /**
     *  get all tags and return them
     * @return Collection
     */
    public function getAll(): Collection;

    /**
     * paginate tags
     * @param int $per_pag
     * @return paginator
     */
    public function paginateTag(int $per_pag = 10): Paginator;

    /**
     * create a new tag
     * @param \App\Models\Interfaces\HasTag $taggable_obj
     * @param array $data
     * @return bool
     */
    public function create(HasTag $taggable_obj, array $data): bool;

    /**
     * update an existing tag
     * @param \App\Models\Tag $tag
     * @param array $data
     * @return bool
     */
    public function Update(Tag $tag, array $data): bool;

    /**
     * delete an existing tag
     * @param \App\Models\Tag $tag
     * @return bool
     */
    public function delete(Tag $tag): bool;
}
