<?php

namespace App\Repositories\Interfaces;

use App\Models\Sector;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

interface SectorRepositoryInterface
{
    /**
     * @param int $id
     * @param bool $with_relation
     * @return mixed
     */
    public function getById(int $id, bool $with_relation = false);

    /**
     * @return Collection
     */
    public function getAllSectors(): Collection;

    /**
     * @param Sector $sector
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getSectorsWithGlobals(Sector $sector, int $limit = 50, int $offset = 0);

    /**
     * @param int $per_page
     * @return Paginator
     */
    public function paginateSectors(int $per_page = 20): Paginator;

    /**
     * @param array $data
     * @return bool
     */
    public function createSector(array $data): bool;

    /**attach gc to sector
     * @param Sector $sector
     * @param array $data
     * @return mixed
     */
    public function attachGlobalCurrenciesToSectors(Sector $sector, array $data);

    public function editAttachGlobalCurrenciesToSectors(Sector $sector, array $data);

    /**
     * @param Sector $sector
     * @param array $data
     * @return bool
     */
    public function updateSector(Sector $sector, array $data): bool;

    /**
     * @param Sector|null $sector
     * @return bool
     */
    public function deleteSector(Sector $sector = null): bool;

    public function detach(Sector $sector, int $global_currency_id);

}
