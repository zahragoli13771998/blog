<?php


namespace App\Repositories\Interfaces;

use App\Models\Interfaces\HasFavorite;

interface FavoriteRepositoryInterface
{
    /**
     * Get and return favorites count
     * @param HasFavorite $favorable
     * @return int
     */
    public function getFavoriteCount(HasFavorite $favorable): int;

    /**
     * Check if favorable is checked as Favorite before or not
     * @param HasFavorite $favorable
     * @param int $user_id
     * @return bool
     */
    public function isAddedToFavorites(HasFavorite $favorable, int $user_id): bool;

    /**
     * Add Users favorites
     * @param HasFavorite $favorable
     * @param int $user_id
     * @param string|null $type
     * @return boolean
     */
    public function createFavorite(HasFavorite $favorable, int $user_id , string $type = null): bool;

    /**
     * Delete  favorite
     * @param HasFavorite $favorable
     * @param int|null $user_id
     * @param string|null $type
     * @return boolean
     */
    public function deleteFavorite(HasFavorite $favorable, int $user_id = null,string $type = null): bool;

    public function getUserFavoredGlobalList(int $user_id , string $type);

}
