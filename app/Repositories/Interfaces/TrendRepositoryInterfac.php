<?php


namespace App\Repositories\Interfaces;


use App\Models\Trend;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

interface TrendRepositoryInterfac
{

    /**
     * get Trend according to given id and return it
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getById(int $id): Trend;

    /**
     * get and return all Trends
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllTrends(): Collection;

    /**
     * get daily trends
     * @param string $start_duration
     * @param string $now
     * @return Collection
     */
    public function dailyTrends(string $start_duration,string $now):Collection;

    /**
     * get daily trends
     * @param string $start_duration
     * @param string $now
     * @return Collection
     */
    public function dailyGlobalTrends(string $start_duration,string $now):Collection;

    /**
     * paginate Trends
     * @param int $per_page
     * @return Paginator
     */
    public function paginateTrends(int $per_page = 10): Paginator;

    /**
     * create a new Trend
     * @param array $data
     * @return bool
     */
    public function createTrend(array $data): bool;

    /**
     * update an existing Trend
     * @param Trend $trend
     * @param array $data
     * @return bool
     */
    public function updateTrend(Trend $trend, array $data): bool;

    /**
     * delete Trend
     * @param Trend|null $trend
     * @return bool
     */
    public function deleteTrend(Trend $trend = null): bool;


}
