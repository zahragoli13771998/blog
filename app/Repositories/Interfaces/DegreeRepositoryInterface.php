<?php
namespace App\Repositories\Interfaces;

use App\Models\Degrees;

interface DegreeRepositoryInterface
{
    /**
     * for getting degree using id
     * @param int $id
     * @return mixed
     */
    public function getDegreeById(int $id);


    /**
     * for showing degree
     * @return mixed
     */
    public function getAll();
    public function paginate();

    /**
     * for storing degree with validation
     * @param array $date
     * @return mixed
     */
    public function store(array $date);

    /**
     * for updating degree
     * @param Degrees $degree
     * @param array $data
     * @return mixed
     */
    public function update(Degrees $degree,array $data);

    /**
     * for deleting degree
     * @param Degrees $degree
     * @return mixed
     */
    public function delete(Degrees $degree);
}
