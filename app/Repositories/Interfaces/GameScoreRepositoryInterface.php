<?php


namespace App\Repositories\Interfaces;


use App\Models\GameScore;
use App\Models\GameUser;
use Illuminate\Support\Collection;

interface GameScoreRepositoryInterface
{

    /**
     * get user scores
     * @param int $user_id
     * @param bool|false $with_relation
     * @return mixed
     */
    public function getUserScore(int $user_id, bool $with_relation = false);

    /**
     * get user scores
     * @param int $user_id
     * @param bool|false $with_relation
     * @return Collection
     */
    public function getAllUserScores(int $user_id, bool $with_relation = false):Collection;

    /**
     * get score by id
     * @param int $id
     * @param bool $with_relation
     * @return GameScore
     */
    public function getScoreById(int $id,bool $with_relation = false): GameScore;

    /**
     * Creates a new user
     * @param GameUser $user
     * @param array $data
     * @return bool
     */
    public function createScore(GameUser $user, array $data): bool;

    /**
     * this function deletes a score
     * @param GameScore $score
     * @return bool
     */
    public function destroyScore(GameScore $score): bool;

}
