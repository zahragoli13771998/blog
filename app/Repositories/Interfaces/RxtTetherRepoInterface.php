<?php

namespace App\Repositories\Interfaces;

interface RxtTetherRepoInterface
{
    /**
     * get tether price from ramzinex api
     * @return false|mixed
     */
    public function fetchTetherPrice(): ?int;

    /**
     * get tether price and save to cache
     * @return bool
     */
    public function cacheTetherPrice(): bool;

    /**
     * check if data with caceh key exists or not
     * if not save it to cache
     * returns data from cache
     * @return int
     */
    public function getTetherPrice(): int;
}
