<?php

namespace App\Repositories\Interfaces;

use App\Models\Comment;
use App\Models\CommentFile;
use App\Models\Interfaces\HasComments;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

interface CommentRepositoryInterface
{
    /**
     * get all comments
     * @param HasComments|null $commentable_obj
     * @param bool $only_root
     * @param bool $with_relation
     * @param int|null $offset
     * @param int|null $limit
     * @param bool $only_accepted
     * @param int|null $parent_id
     * @param int|null $user_id
     * @return Collection
     */
    public function getAllComment(HasComments $commentable_obj = null, bool $only_root = false, bool $with_relation = false, int $offset = null, int $limit = null, bool $only_accepted = false , int $parent_id = null, int $user_id = null): Collection;

    /**
     * Get And return Comments Count
     * @param HasComments|null $commentable_obj
     * @param bool $only_accepted
     * @param int|null $parent_id
     * @param bool $only_root
     * @return int
     */
    public function getCommentsCount(HasComments $commentable_obj = null, bool $only_accepted = false, int $parent_id = null, bool $only_root = false): int;

    /**
     * get comment according to given id
     * @param int $id
     * @param bool $only_root
     * @param bool $with_reply
     * @return Comment
     */
    public function getById(int $id, bool $only_root = false, bool $with_reply = false): Comment;

    /**
     * paginate comments
     * @param bool $only_root
     * @param bool $with_relation
     * @param int $per_page
     * @param bool $with_commentable
     * @return paginator
     */
    public function paginateComment(bool $only_root = false, bool $with_relation = false, int $per_page = 10, bool $with_commentable = false): Paginator;

    /**
     * @param int $global_currency_id
     * @return Builder[]|Collection
     */
//    public function getPool(int $global_currency_id);
    /**
     * create new comments(polymorphic)
     * @param HasComments $commentable_obj
     * @param array $data
     * @return Comment
     */
    public function createComment(HasComments $commentable_obj, array $data): ?Comment;

    /**
     * @param HasComments $commentable_obj
     * @param array $data
     * @return Comment|null
     */
    public function createPoolAnswer(HasComments $commentable_obj,array $data) :?Comment;
    /**
     * get survey answer comment
     * @param int $user_id
     * @param int $depth_id
     * @return Collection
     */
    public function getBySurveyAnswer(int $user_id,int $depth_id): Collection;

    /**
     * create new comment file
     * @param Comment $comment
     * @param array $data
     * @return bool
     */
    public function createCommentFile(Comment $comment, array $data): bool;

    /**
     * update comment
     * @param array $data
     * @param Comment $comment
     * @return bool
     */
    public function updateComment(Comment $comment, array $data): bool;

    /**
     * update comment file
     * @param array $data
     * @param CommentFile $comment_file
     * @return bool
     */
    public function updateCommentFile(CommentFile $comment_file, array $data): bool;

    /**
     * delete a comment
     * @param Comment|null $comment
     * @param HasComments|null $commentable_obj
     * @return bool
     */
    public function delete(Comment $comment = null, HasComments $commentable_obj = null): bool;
}
