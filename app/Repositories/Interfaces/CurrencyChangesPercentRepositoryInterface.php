<?php

namespace App\Repositories\Interfaces;

use App\Models\CurrencyChangesPercent;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

interface CurrencyChangesPercentRepositoryInterface
{
    /**
     * Get and return currency changes percent
     * @param array $input
     * @return CurrencyChangesPercent|null
     */
    public function getChangesPercent(array $input): ?CurrencyChangesPercent;

    /**
     * paginate currencies change percent.
     *
     * @param array $input
     * @param int $perpage
     * @return LengthAwarePaginator
     */
    public function paginate(array $input = [], int $perpage =10): LengthAwarePaginator;

    /**
     * Get the get all value from the database.
     *
     * @param array $input
     * @return Collection
     */
    public function getAll(array $input = []): Collection;

    /**
     * find by id the record with the given id.
     *
     * @param int $id
     * @return CurrencyChangesPercent
     */
    public function findById(int $id): CurrencyChangesPercent;

    /**
     * Store a newly created resource in storage.
     *
     * @param array $value
     * @return bool
     */
    public function store(array $value): bool;

    /**
     * Update the specified resource in storage.
     *
     * @param array $value
     * @param CurrencyChangesPercent $profitDamage
     * @return bool
     */
    public function update(array $value, CurrencyChangesPercent $profitDamage):bool;

    /**
     * Remove the specified resource from storage.
     *
     * @param CurrencyChangesPercent $profitDamage
     * @return bool
     */
    public function delete(CurrencyChangesPercent $profitDamage): bool;
}
