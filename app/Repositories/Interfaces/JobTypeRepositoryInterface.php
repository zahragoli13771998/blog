<?php

namespace App\Repositories\Interfaces;

use App\Models\JobTypes;

interface JobTypeRepositoryInterface
{
    /**
     * for getting job type using id
     * @param int $id
     * @param bool $with_relation
     * @return mixed
     */
    public function getJobTypeById(int $id,bool $with_relation = false);

    /**
     * for showing job type
     * @return mixed
     */
    public function getAll(bool $with_relation = false,bool $with_limit = true);

    /**
     * @return mixed
     */
    public function paginate();

    /**
     * for storing job type with validation
     * @param array $date
     * @return mixed
     */
    public function store(array $date);

    /**
     * for updating job type
     * @param JobTypes $job_type
     * @param array $data
     * @return mixed
     */
    public function update(JobTypes $job_type, array $data);

    /**
     * for deleting job type
     * @param JobTypes $job_type
     * @return mixed
     */
    public function delete(JobTypes $job_type);

}
