<?php

namespace App\Repositories\Interfaces;

use App\Models\Interfaces\HasDisLike;
use Illuminate\Support\Collection;


interface DislikeRepositoryInterface
{
    /**
     * @param HasDisLike $dislikeable
     * @return mixed
     */
    public function getDisLikeCount(HasDisLike $dislikeable);

    /**
     * Check if dislikeable is disliked before
     * @param HasDisLike $dislikeable
     * @param int $user_id
     * @return bool
     */
    public function isDisliked(HasDisLike $dislikeable, int $user_id): bool;

    /**
     * get user's feedback on each question
     * @param HasDisLike $dislikeable
     * @param int $user_id
     * @param string|null $type
     * @return boolean
     */
    public function createDislike(HasDisLike $dislikeable, int $user_id, string $type = null): bool;

    /**
     * delete Dislike
     * @param HasDisLike $dislikeable
     * @param int|null $id
     * @param string|null $type
     * @return boolean
     */
    public function deleteDislike(HasDisLike $dislikeable, int $id = null, string $type = null): bool;

    /**
     * @param int $user_id
     * @param string $type
     * @return Collection|string
     */
    public function getUserDisLikesList(int $user_id, string $type);

    /**
     * @param int $user_id
     * @return mixed
     */
    public function getUserCommentsDisLikesList(int $user_id );
}
