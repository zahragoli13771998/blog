<?php

namespace App\Repositories\Interfaces;

use App\Models\JobVacancies;

interface JobVacancyRepositoryInterface
{
    /**
     * for getting job vacancy using id
     * @param int $id
     * @return mixed
     */
    public function getJobVacancyById(int $id);

    /**
     * for showing job vacancy
     * @return mixed
     */
    public function getAll(int $job_type_id = null);

    public function paginate(int $id);

    /**
     * for storing job vacancy with validation
     * @param array $date
     * @return mixed
     */
    public function store(array $date);

    /**
     * for updating job vacancy
     * @param JobVacancies $job_vacancy
     * @param array $date
     * @return mixed
     */
    public function update(JobVacancies $job_vacancy, array $date);

    /**
     * for deleting job vacancy
     * @param JobVacancies $job_vacancy
     * @return mixed
     */
    public function delete(JobVacancies $job_vacancy);
}
