<?php

namespace App\Repositories;

use App\Models\FaqCategory;
use App\Models\FaqQuestionAnswer;
use App\Repositories\Interfaces\FaqQuestionAnswerRepositoryInterface;
use Illuminate\Contracts\Pagination\Paginator;
use  Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class FaqQuestionAnswerRepository implements FaqQuestionAnswerRepositoryInterface
{
    /**
     * return an instance of FaqQuestionAnswer model according to given id
     * @param int $id
     * @param bool $with_counts
     * @param int|null $user_id
     * @return FaqQuestionAnswer|Builder|Builder[]|Collection|Model|null
     */
    public function getById(int $id, bool $with_counts = false, int $user_id = null): FaqQuestionAnswer
    {
        return $this->questionAnswerFetchQueryBuilder([], $with_counts, $user_id)->with(['deeplink'])
            ->findOrFail($id);
    }

    /**
     * @return Builder[]|Collection
     */
    public function getFaq(){
        return $this->questionAnswerFetchQueryBuilder([])->where('is_repetitive','=',true)->get();
    }
    public function getByFaqCategoryId($id)
    {
        return FaqQuestionAnswer::query()->where('faq_category_id', $id)->get();
    }

    /**
     * paginate questionAnswers
     * @param array $data
     * @param int $per_page
     * @param bool $with_counts
     * @return Paginator
     */
    public function paginateQuestionAnswer(array $data = [], int $per_page = 10, bool $with_counts = false): paginator
    {
        return $this->questionAnswerFetchQueryBuilder($data, $with_counts)->paginate($per_page);

    }

    /**
     * return all questionAnswers
     * @param bool $only_repetitive
     * @param bool $with_counts
     * @param string|null $search
     * @return Builder[]|Collection
     */
    public function getAllQuestionAnswer(bool $only_repetitive = false, bool $with_counts = false, string $search = null): Collection
    {
        return $this->questionAnswerFetchQueryBuilder([], $with_counts)
            ->when($search, function (Builder $query) use ($search) {
                $query->where(function ($query) use ($search) {
                    $query->where('question_fa', 'like', '%' . $search . '%')
                        ->orWhere('question_en', 'like', '%' . $search . '%');
                })
                    ->orWhere(function ($query) use ($search) {
                        $query->where('complete_answer_fa', 'like', '%' . $search . '%')
                            ->orWhere('complete_answer_en', 'like', '%' . $search . '%');
                    });
            })->when($only_repetitive, function (Builder $query) {
                $query->where('is_repetitive', '=', 1);
            })->get();
    }

    /**
     * create a questionAnswer and save through relation
     * @param array $data
     * @param \App\Models\FaqCategory $faqCategory
     * @return bool
     */
    public function addQuestionAnswer(array $data, FaqCategory $faqCategory): bool
    {
        $question_answer = new FaqQuestionAnswer();
        $question_answer->faq_category_id = $data['faq_category_id'];
        $question_answer->show_order = $data['show_order'];
        $question_answer->is_repetitive = $data['is_repetitive'];
        $question_answer->question_fa = $data['question_fa'];
        $question_answer->question_en = $data['question_en'];
        $question_answer->short_answer_fa = $data['short_answer_fa'];
        $question_answer->short_answer_en = $data['short_answer_en'];
        $question_answer->complete_answer_fa = $data['complete_answer_fa'];
        $question_answer->complete_answer_en = $data['complete_answer_en'];
        if (isset($data['external_video'])) {
            $question_answer->external_video = $data['external_video'];
        }
        if (isset($data['image'])) {
            $question_answer->image = $data['image'];
        }
        if (isset($data['video'])) {
            $question_answer->video = $data['video'];
        }

        try {
            $faqCategory->faqQuestionAnswers()->save($question_answer);

        } catch (QueryException|\Exception $ex) {
            Log::error($ex->getMessage());
            return false;
        }
        return true;
    }

    /**
     * update an existing questionAnswer
     * @param array $data
     * @param FaqQuestionAnswer $question_answer
     * @return bool
     */
    public function updateQuestionAnswer(array $data, FaqQuestionAnswer $question_answer): bool
    {
        if (isset($data['faq_category_id'])) {
            $question_answer->faq_category_id = $data['faq_category_id'];
        }

        if (isset($data['show_order'])) {
            $question_answer->show_order = $data['show_order'];
        }

        if (isset($data['is_repetitive'])) {
            $question_answer->is_repetitive = $data['is_repetitive'];
        }

        if (isset($data['question_fa'])) {
            $question_answer->question_fa = $data['question_fa'];
        }

        if (isset($data['question_en'])) {
            $question_answer->question_en = $data['question_en'];
        }

        if (isset($data['short_answer_fa'])) {
            $question_answer->short_answer_fa = $data['short_answer_fa'];
        }

        if (isset($data['short_answer_en'])) {
            $question_answer->short_answer_en = $data['short_answer_en'];
        }

        if (isset($data['complete_answer_fa'])) {
            $question_answer->complete_answer_fa = $data['complete_answer_fa'];
        }

        if (isset($data['complete_answer_en'])) {
            $question_answer->complete_answer_en = $data['complete_answer_en'];
        }

        if (isset($data['image'])) {
            $question_answer->image = $data['image'];
        }

        if (isset($data['video'])) {
            $question_answer->video = $data['video'];
        }

        if (isset($data['external_video'])) {
            $question_answer->external_video = $data['external_video'];
        }

        try {

            $question_answer->save();
        } catch (QueryException|\Exception $ex) {
            Log::error($ex->getMessage());
            return false;
        }

        return true;
    }

    /**
     * delete  an existing questionAnswer
     * @param FaqQuestionAnswer $question_answer
     * @return bool
     */
    public function deleteQuestionAnswer(FaqQuestionAnswer $question_answer): bool
    {
        try {
            $question_answer->delete();
        } catch (QueryException|\Exception $ex) {
            Log::error($ex->getMessage());
            return false;
        }
        return true;

    }

    /**
     * @param array $data
     * @param bool $with_counts
     * @param int|null $user_id
     * @return Builder
     */
    private function questionAnswerFetchQueryBuilder(array $data = [], bool $with_counts = false, int $user_id = null): Builder
    {
        return FaqQuestionAnswer::query()->when(isset($data['question_title']), function (Builder $query) use ($data) {
            $query->where('question_fa', 'like', '%' . $data['question_title'] . '%');
        })->when($with_counts, function (Builder $query) {
            $query->withCount(['likes', 'dislikes', 'views', 'comments' => function ($query) {
                $query->where('status', '=', true);//Todo add to models attributes
            }]);
        })->when($user_id, function (Builder $query) use ($user_id) {//Todo Make it better duplication code made, Suggestion is to move it to model layer
            $query->with(['likes' => function ($query) use ($user_id) {
                $query->where('user_id', '=', $user_id);
            }, 'dislikes' => function ($query) use ($user_id) {
                $query->where('user_id', '=', $user_id);
            }, 'favorites' => fn($query) => $query->where('user_id', '=', $user_id)]);
        });
    }
}



