<?php


namespace App\Repositories;

use App\Models\Gainer;
use App\Repositories\Interfaces\GainerRepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class GainerRepository implements GainerRepositoryInterface
{

    /**
     * get and return gainer according to given id
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getById(int $id): Gainer
    {
        return Gainer::query()->findOrFail($id);
    }

    /**
     * get and return all gainers
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllGainers(bool $order_by_value = false): Collection
    {
        return Gainer::query()->when($order_by_value, function (Builder $query) {
            $query->orderByDesc('value');
        })->get();
    }

    /**
     * paginate gainers
     * @param int $per_page
     * @return Paginator
     */
    public function paginateGainers(int $per_page = 10): Paginator
    {
        return Gainer::query()->paginate($per_page);
    }

    /**
     * create a new gainer
     * @param array $data
     * @return bool
     */
    public function createGainer(array $data): bool
    {
        $gainer = new Gainer();
        $gainer->id = $data['id'];
        $gainer->value = $data['value'];

        try {

            $gainer->save();
        } catch (QueryException | Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * update an existing gainer
     * @param Gainer $gainer
     * @param array $data
     * @return bool
     */
    public function updateGainer(Gainer $gainer, array $data): bool
    {
        if (isset($data['id'])) {
            $gainer->id = $data['id'];
        }
        if (isset($data['value'])) {
            $gainer->value = $data['value'];
        }
        try {
            $gainer->save();
        } catch (QueryException | Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * delete gainer
     * @param Gainer|null $gainer
     * @return bool
     */
    public function deleteGainer(Gainer $gainer = null): bool
    {
        try {
            if ($gainer) {
                $gainer->delete();
            }

            if (!$gainer) {
                Gainer::query()->delete();
            }

        } catch (QueryException | Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

}
