<?php

namespace App\Repositories;

use App\Models\FaqCategory;
use App\Repositories\Interfaces\FaqCategoryRepositoryInterface;
use Exception;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Concerns\BuildsQueries;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;


class FaqCategoryRepository implements FaqCategoryRepositoryInterface
{

    /**
     * get categories
     * @param bool $only_root
     * @param bool $load_relation
     * @param array|null $search
     * @return Builder[]|Collection|\Illuminate\Support\Collection
     */
    public function getAllCategories(bool $only_root = false, bool $load_relation = false, string $search = null): Collection
    {
        return $this->categoryFetchQueryBuilder($only_root, $load_relation)
            ->when($search, function (Builder $query) use ($search) {
                $query->without(['subCategories'])
                ->where('name_fa', 'like', '%'.$search.'%')
                ->orWhere('name_en', 'like', '%'.$search.'%');
        })->get();
    }


    /**
     * returns FaqCategoryService instance according to given id
     * @param int $id
     * @param bool $load_relation
     * @param int|null $user_id
     * @return FaqCategory
     */
    public function getById(int $id, bool $load_relation = false, int $user_id = null): FaqCategory
    {
        return $this->categoryFetchQueryBuilder(false, $load_relation, $user_id)->findOrFail($id);
    }


    /**
     * paginate categories
     * @param int $per_page
     * @param bool $only_root
     * @param bool $load_relation
     * @return Paginator
     */
    public function paginateCategory(int $per_page = 10, bool $only_root = false ,bool $load_relation = false): paginator
    {
        return $this->categoryFetchQueryBuilder($only_root,$load_relation)->paginate($per_page);
    }


    /**
     * return category with its questionAnswers according to given id
     * @param array $data
     * @param bool $only_root
     * @param bool $load_relation
     * @return Builder[]|Collection|mixed
     */
    public function getCategoryAndRelatedQuestions(array $data = [], bool $only_root = false ,bool $load_relation = false):Collection
    {
        return $this->categoryFetchQueryBuilder($only_root,$load_relation)
            ->where('id' , '=',$data['id'])
            ->with('faqQuestionAnswers')
            ->get();
    }


    /**
     *  create a new FaqCategoryService
     * @param array $data
     * @return bool
     */
    public function addCategory(array $data): bool
    {
        $category = new FaqCategory();

        $category->name_fa = $data['name_fa'];
        $category->name_en = $data['name_en'];
        $category->description = $data['description'];

        if (isset($data['parent_id'])) {
            $category->parent_id = $data['parent_id'];
        }

        if (isset($data['icon'])){
            $category->icon = $data['icon'];
        }

        try {
            $category->save();
        } catch (QueryException | Exception $ex) {
            Log::error($ex->getMessage());
            return false;
        }

        return true;
    }


    /**
     * it edits an existing FaqCategoryService
     * @param array $data
     * @param FaqCategory $category
     * @return bool
     */
    public function editCategory(array $data, FaqCategory $category): bool
    {
        $category->parent_id = (isset($data['parent_id'])) ? $data['parent_id'] : null;

        if (isset($data['name_fa'])) {
            $category->name_fa = $data['name_fa'];
        }

        if(isset($data['name_en'])){
            $category->name_en = $data['name_en'];
        }

        if (isset($data['icon'])){
            $category->icon = $data['icon'];
        }
        if (isset($data['description'])){
            $category->icon = $data['description'];
        }

        try {
            $category->save();

        } catch (QueryException | Exception $ex) {
            Log::error($ex->getMessage());
            return false;
        }

        return true;
    }


    /**
     * it deletes an existing FaqCategoryService
     * @param FaqCategory $category
     * @return bool
     */
    public function deleteCategory(FaqCategory $category): bool
    {
        try {
            $category->delete();
        } catch (QueryException | Exception $ex) {
            Log::error($ex->getMessage());
            return false;
        }

        return true;
    }


    /**
     * FaqCategoryService builder
     * @param bool $only_root
     * @param bool $load_relation
     * @param int|null $user_id
     * @return BuildsQueries|Builder|mixed
     */
    private function categoryFetchQueryBuilder(bool $only_root = false, bool $load_relation = false, int $user_id = null)
    {
        return FaqCategory::query()->when($only_root,function (Builder $query){
            $query->whereNull('parent_id');
        })->when($load_relation,function (Builder $query) use ($user_id){
            $query->with(['subCategories', 'faqQuestionAnswers' => function ($query) use ($user_id){
                $query->withCount(['likes','dislikes', 'views']);
                if($user_id){
                    $query->with(['likes' => function($query) use ($user_id){
                        $query->where('user_id', '=', $user_id);
                    }, 'dislikes' => function($query) use ($user_id){
                        $query->where('user_id', '=', $user_id);
                    }]);
                }
            }]);
        });
    }
}
