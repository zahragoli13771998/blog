<?php


namespace App\Repositories;

use App\Models\Interfaces\HasFavorite;
use App\Repositories\Interfaces\FavoriteRepositoryInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FavoriteRepository implements FavoriteRepositoryInterface
{
    /**
     * Get and return favorites count
     * @param HasFavorite $favorable
     * @return int
     */
    public function getFavoriteCount(HasFavorite $favorable): int
    {
        return $favorable->favorites()->count();
    }

    /**
     * Check if favorable is checked as Favorite before or not
     * @param HasFavorite $favorable
     * @param int $user_id
     * @return bool
     */
    public function isAddedToFavorites(HasFavorite $favorable, int $user_id): bool
    {
        return $favorable->favorites()->where('user_id', '=', $user_id)->exists();
    }

    /**
     * Add Users favorites
     * @param HasFavorite $favorable
     * @param int $user_id
     * @param string|null $type
     * @return bool
     */
    public function createFavorite(HasFavorite $favorable, int $user_id, string $type = null): bool
    {
        try {
            $favorable->favorites()->attach($user_id);
            $favored_list = 'user_' . $user_id . '_favored_'.$type.'_list';
            $users_favorites_list = $this->getUserFavoredGlobalList($user_id, $type);
            Cache::put($favored_list, $users_favorites_list, 600);
        } catch (QueryException | \Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;

    }

    /**
     * Delete  favorite
     * @param HasFavorite $favorable
     * @param int|null $user_id
     * @param string|null $type
     * @return bool
     */
    public function deleteFavorite(HasFavorite $favorable, int $user_id = null, string $type = null): bool
    {
        try {
            $favorable->favorites()->detach($user_id);
            $favored_list = 'user_' . $user_id . '_favored_'.$type.'_list';
            if (Cache::has($favored_list)){
                Cache::forget($favored_list);
            }
        } catch (QueryException | \Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * @param int $user_id
     * @param string $type
     * @return Collection
     */
    public function getUserFavoredGlobalList(int $user_id, string $type): Collection
    {
        switch ($type) {
            case 'global':
                return DB::table('favorites')->where('user_id', '=', $user_id)->where('favorable_type', '=', 'App\Models\GlobalCurrency ')->pluck('favorable_id');
            case 'banner':
                return DB::table('favorites')->where('user_id', '=', $user_id)->where('favorable_type', '=', 'App\Models\Banner ')->pluck('favorable_id');
            case 'faq':
                return DB::table('favorites')->where('user_id', '=', $user_id)->where('favorable_type', '=', 'App\Models\FaqQuestionAnswer ')->pluck('favorable_id');

        }
    }
}
