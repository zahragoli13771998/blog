<?php


namespace App\Repositories;


use App\Models\GameScore;
use App\Models\GameUser;
use App\Repositories\Interfaces\GameScoreRepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GameScoreRepository implements GameScoreRepositoryInterface
{


    /**
     * @param int $count
     * @param bool $with_relation
     * @return Collection
     */
    public function topAccounts(int $count = 5, bool $with_relation = false): Collection
    {
        return $this->scoreFetchQueryBuilder($with_relation)
            ->select('user_id', DB::raw('SUM(score) as scores'))
            ->groupBy('user_id')
            ->orderBy(DB::raw('SUM(score)'), 'DESC')
            ->take($count)
            ->get();
    }

    /**
     * get user scores
     * @param int $user_id
     * @param bool $with_relation
     * @return int
     */
    public function getUserScore(int $user_id, bool $with_relation = false): int
    {
        return $this->scoreFetchQueryBuilder($with_relation)->where('user_id', '=', $user_id)
            ->sum('score');
    }

    /**
     * @param int $user_id
     * @param bool $with_relation
     * @return Collection
     */
    public function getAllUserScores(int $user_id, bool $with_relation = false): Collection
    {
        return $this->scoreFetchQueryBuilder($with_relation)->where('user_id', '=', $user_id)
            ->orderBy('score', 'asc')->get();
    }


    /**
     * get score by id
     * @param int $id
     * @param bool $with_relation
     * @return GameScore
     */
    public function getScoreById(int $id, bool $with_relation = false): GameScore
    {
        return $this->scoreFetchQueryBuilder($with_relation)->findOrFail($id);

    }

    /**
     * Creates a new user
     * @param GameUser $user
     * @param array $data
     * @return bool
     */
    public function createScore(GameUser $user, array $data): bool
    {
        $score = new GameScore();

        if (isset($data['score'])) {
            $score->score = $data['score'];
        }
        if (isset($data['coin'])) {
            $score->coin = $data['coin'];
        }

        try {
            $user->scores()->save($score);
        } catch (QueryException | Exception $e) {
            Log::error('Unable to create score with error: ' . $e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * this function deletes a score
     * @param GameScore $score
     * @return bool
     */
    public function destroyScore(GameScore $score): bool
    {
        try {
            $score->delete();
        } catch (QueryException | Exception $ex) {
            Log::error($ex->getmessage());
            return false;
        }
        return true;
    }

    /**
     * game user's query builder
     * @param bool $with_relation
     * @return Builder
     */
    private function scoreFetchQueryBuilder(bool $with_relation = false): Builder
    {
        return GameScore::query()->when($with_relation, function (Builder $query) {
            $query->with(['user']);
        });
    }

}
