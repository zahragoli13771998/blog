<?php


namespace App\Repositories;

use App\Repositories\Interfaces\GlobalCurrencyApiInterface;
use App\Services\Interfaces\CurlServiceInterface;

/**
 * Class CoinPaprikaRepository
 * @package App\Repositories
 */
class CoinPaprikaRepository implements GlobalCurrencyApiInterface
{
    /**
     * Curl service container
     * @var CurlServiceInterface
     */
    private CurlServiceInterface $curler;

    /**
     * CoinPaprikaRepository constructor.
     * inject guzzleCurl service
     * @param CurlServiceInterface $curler
     */
    public function __construct(CurlServiceInterface $curler)
    {
        $this->curler = $curler;
    }

    /**
     * Get all Global Currencies list from API Service
     * @return array|void
     */
    public function getGlobalCurrenciesList(): array
    {
        $result = $this->curler->createClient(config('currency_services.url.coin_paprika.base'))
            ->setUrlSection(config('currency_services.url.coin_paprika.coins'))
            ->executeRequest();

        if(isset($result['status']) && $result['status']){
            return $result['content'];
        }

        return [];
    }

    /**
     * Get specific Global Currency information by it's id from service
     * @param string $id
     * @return array|void
     */
    public function getGlobalCurrencyTickerByID(string $id): array
    {
        $result = $this->curler->createClient(config('currency_services.url.coin_paprika.base'))
            ->setUrlSection(config('currency_services.url.coin_paprika.tickers').'/'.$id.'?quotes=USD,BTC')
            ->executeRequest();

        if (isset($result['status']) && $result['status']) {
            return $result['content'];
        }

        return [];
    }

    /**
     * Get all Global Currencies information from service
     * @return array|void
     */
    public function tickersAllGlobalCurrencies(): array
    {
        $result = $this->curler->createClient(config('currency_services.url.coin_paprika.base'))
            ->setUrlSection(config('currency_services.url.coin_paprika.tickers').'?quotes=USD,BTC')->executeRequest();
        if (isset($result['status']) && $result['status'] && isset($result['content'])) {
            return $result['content'];
        }

        return [];
    }

    /**
     * Get desired global currency ohlc and return it
     * @param string $coin_id
     * @param int $unix_from
     * @param int $unix_to
     * @return array
     */
    public function getGlobalCurrencyOHLC(string $coin_id, int $unix_from, int $unix_to): array
    {
        $result = $this->curler->createClient(config('currency_services.url.coin_paprika.base'))
            ->setUrlSection(config('currency_services.url.coin_paprika.coins').$coin_id.config('currency_services.url.coin_paprika.ohlc'))
            ->setQueryStrings([
                'start' => $unix_from,
                'end' => $unix_to
            ])
            ->executeRequest();

        if (isset($result['status']) && $result['status']) {
            return $result['content'];
        }

        return [];
    }
}
