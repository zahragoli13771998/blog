<?php

namespace App\Repositories;

use App\Models\JobTypes;
use App\Models\JobVacancies;
use App\Models\JobVacancyRequest;
use App\Models\ResumeFile;
use Exception;
use App\Repositories\Interfaces\JobVacancyRequestRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class JobVacancyRequestRepository implements JobVacancyRequestRepositoryInterface
{
    public function getAll(){
        return JobVacancyRequest::query()->whereNotIn('id',10)->get();
    }
    /**
     * for storing job vacancy request
     * @param JobVacancies|null $job_vacancy
     * @param array $data
     * @return bool
     */
    public function store(array $data, JobVacancies $job_vacancy = null): bool
    {
        $job_vacancy_request = new JobVacancyRequest;
        $job_vacancy_request->full_name = $data['full_name'];
        $job_vacancy_request->phone = $data['phone'];
        $job_vacancy_request->resume_path = $data['file'];
        $job_vacancy_request->email = $data['email'];
        try {
            $job_vacancy->jobRequest()->save($job_vacancy_request);
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * for getting job vacancy request using id
     * @param int $id
     * @return JobVacancyRequest
     */
    public function getJobVacancyRequestById(int $id): JobVacancyRequest
    {
        return JobVacancyRequest::query()->findOrFail($id);
    }

    /**
     * for showing recorded request for each job vacancy
     * @param int $id
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function showById(int $id): Collection
    {
        return JobVacancyRequest::query()->where('job_vacancy_id','=',$id)->get();
//        return JobVacancyRequest::query()->where('job_vacancy_id', $id)->where('status', '=', true)->get();
    }

    /**
     * for updating job vacancy request status by admin
     * @param JobVacancyRequest $user_job
     * @param array $data
     * @return bool
     */
    public function update(JobVacancyRequest $user_job, array $data): bool
    {
        if (isset($data['status'])) {
            $user_job->status = $data['status'];
        }
        try {
            $user_job->save();
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * for deleting job vacancy request by admin
     * @param JobVacancyRequest $user_job
     * @return bool
     * @throws \Exception
     */
    public function delete(JobVacancyRequest $user_job): bool
    {
        try {
            $user_job->delete();
            $path = $user_job->resume_path;
            Storage::delete($path);
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * for getting job request by job types
     * @param int $id
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function getJobVacancyRequestByType(int $id): Collection
    {
        return JobTypes::with('jobVacancyRequest')->where('job_vacancy_id', $id)->where('status', '=', true)->find($id);
    }

    /**
     * for showing results depend on selected value
     * @param int $id
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function search(int $id): Collection
    {
        return JobVacancies::with('jobtype')->where('job_type_id', $id)->get();
    }
}
