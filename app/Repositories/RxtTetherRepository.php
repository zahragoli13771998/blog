<?php


namespace App\Repositories;

use App\Repositories\Interfaces\RxtTetherRepoInterface;
use App\Services\Interfaces\CurlServiceInterface;
use Illuminate\Support\Facades\Cache;

class RxtTetherRepository implements RxtTetherRepoInterface
{
    /**
     * Guzzle curl service container
     * @var CurlServiceInterface
     */
    private CurlServiceInterface $guzzle_curl;

    /**
     * cache key to assign value
     * @var string
     */
    private string $cache_key = 'ramzinex_tether_price';

    /**
     * RxtTetherRepository constructor.
     * @param CurlServiceInterface $guzzle_curl
     */
    public function __construct(CurlServiceInterface $guzzle_curl)
    {
        $this->guzzle_curl = $guzzle_curl;
    }

    /**
     * get tether price from ramzinex api
     * @return false|mixed
     */
    public function fetchTetherPrice(): ?int
    {
        $base_url = config('currency_services.url.ramzinex.base');
        $section_url = config('currency_services.url.ramzinex.prices');
        $result = $this->guzzle_curl
            ->createClient($base_url)
            ->setUrlSection($section_url)
            ->executeRequest();

        if ($result['status'] && isset($result['content']['data'])) {
            $prices = $result['content']['data'];
            $tether_price = $prices['usdtirr']['sell'];
            return $tether_price;
        }
        return null;
    }

    /**
     * get tether price and save to cache
     * @return bool
     */
    public function cacheTetherPrice(): bool
    {
         $tether_price = $this->fetchTetherPrice();
         if ($tether_price) {
             Cache::put($this->cache_key, $tether_price);
             return true;
         }
         return false;
    }

    /**
     * check if data with caceh key exists or not
     * if not save it to cache
     * returns data from cache
     * @return int
     */
    public function getTetherPrice(): int
    {
        if (!Cache::has($this->cache_key)) {
            $this->cacheTetherPrice();
        }
        return Cache::get($this->cache_key);
    }
}
