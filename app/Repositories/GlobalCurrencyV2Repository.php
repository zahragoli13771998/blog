<?php

namespace App\Repositories;

use App\Models\GlobalCurrency;
use App\Repositories\Interfaces\GlobalCurrencyV2Interface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class GlobalCurrencyV2Repository implements GlobalCurrencyV2Interface
{
    /**
     * Get and return single Global Currency by it's id
     * @param int|null $id
     * @param bool $relation
     * @param bool $only_active_changes
     * @param string|null $symbol
     * @param bool $with_ramzinex_info
     * @return GlobalCurrency|null
     */
    public function getById(int $id = null, bool $relation = false, bool $only_active_changes = false, string $symbol = null, bool $with_ramzinex_info = false): ?GlobalCurrency
    {
        $global_currency = $this->buildGlobalCurrency($relation, false, $only_active_changes)
            ->when($relation, function (Builder $query) {
                $query->with(['ohlc', 'tags', 'banners']);
            })
            ->when($with_ramzinex_info, function (Builder $query) {
                $query->with(['ramzinexInfo']);
            });
        if ($symbol) {
            return $global_currency->where('symbol', '=', $symbol)->first() ?? null;
        }
        return $global_currency->findOrFail($id);
    }

    /**
     * Get all global Currencies from DB
     * @param bool $relation
     * @param bool $only_short_info
     * @param int|null $from
     * @param int|null $take
     * @param bool $order_by_rank_asc
     * @param string|null $sort_by
     * @param array $search
     * @param bool $without_hidden
     * @return Collection
     */
    public function getAllGlobalCurrencies(bool $relation = false, bool $only_short_info = false, int $from = null, int $take = null, bool $order_by_rank_asc = false, string $sort_by = null, array $search = [], bool $without_hidden = false): Collection
    {
        return $this->buildGlobalCurrency($relation, $only_short_info, false, $order_by_rank_asc, $sort_by, $search, $without_hidden)
            ->when($from, function (Builder $query) use ($from) {
                $query->skip($from);
            })->when($take, function (Builder $query) use ($take) {
                $query->take($take);
            })
            ->get();
    }

    public function getUserLikesList(int $user_id)
    {
       return DB::table('user_likes')->where('user_id','=',$user_id)->where('likeable_type','=','App\Models\GlobalCurrency ')->pluck('likeable_id');
    }

    /**
     * return number of global currencies
     * @return int
     */
    public function globalCurrenciesCount(): int
    {
        return GlobalCurrency::query()->count();
    }

    /**
     * builder of GlobalCurrency
     * @param bool $relation
     * @param bool $only_short_info
     * @param bool $only_active_changes_percent .
     * @param bool $order_by_rank
     * @param string|null $sort_by
     * @param array $search
     * @param bool $without_hidden
     * @return Builder
     */
    private function buildGlobalCurrency(bool $relation = false, bool $only_short_info = false, bool $only_active_changes_percent = false, bool $order_by_rank = false, string $sort_by = null, array $search = [], bool $without_hidden = false): Builder
    {
        return GlobalCurrency::query()->when($relation, function (Builder $query) use ($only_active_changes_percent, $only_short_info) {
            $query->with(['changePercent' => function (HasMany $query) use ($only_active_changes_percent, $only_short_info) {
                if ($only_active_changes_percent) {
                    $query->where('status', '=', 1);
                }
                /*                    if($only_short_info){
                        $query->whereIn('interval_id', [Interval::getDailyId(), Interval::getWeeklyId()]); //Todo find a way to detach repository dependency to interval id
                    }*/

            }]);
        })
            ->when($only_short_info, function (Builder $query) {
                $query->select(['id', 'name_en', 'name_fa', 'rank', 'currency_id', 'icon', 'price', 'btc_price', 'rial_price', 'symbol', 'daily_trading_volume', 'coin_circulation', 'total_supply', 'price_precision', 'pair_id']);
            })
            ->when($order_by_rank && !isset($sort_by), function (Builder $query) {
                $query->orderBy('rank', 'ASC');
                $query->where('rank', '>', 0);
            })
            ->when(isset($sort_by), function (Builder $query) use ($sort_by) {
                switch ($sort_by) {
                    case 'likes':
                        $query->orderByDesc('likes_count');
                        break;
                    case 'dislikes':
                        $query->orderByDesc('dislikes_count');
                        break;
                    case 'comments':
                        $query->orderByDesc('comments_count');
                        break;
                    case 'views':
                        $query->orderByDesc('views_count');
                        break;
                    case 'includes':
                        $query->orderByDesc('includes_count');
                }

            })->when(isset($search['request_name_en']), function (Builder $query) use ($search) {
                $query->where('name_en', 'like', '%' . $search['request_name_en'] . '%');
            })->when(isset($search['request_name_fa']), function (Builder $query) use ($search) {
                $query->where('name_fa', 'like', '%' . $search['request_name_fa'] . '%');
            })->when(isset($search['request_symbol']), function (Builder $query) use ($search) {
                $query->where('symbol', 'like', '%' . $search['request_symbol'] . '%');
            })
            ->when(isset($search['ramzinex_currencies']), function (Builder $query) use ($search) {
                $query->whereNotNull('pair_id');
            })
            ->when($without_hidden, function (Builder $query) {
                $query->where('is_hidden', '=', 0);
            })
            ->withCount(['likes', 'dislikes', 'views', 'comments', 'includes']);
    }
}
