<?php


namespace App\Repositories;

use App\Models\Interfaces\HasIncludes;
use App\Repositories\Interfaces\IncludeToRazminexRepositoryInterface;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class IncludeToRazminexRepository implements IncludeToRazminexRepositoryInterface
{
    /**
     * get the number of IncludeToRamzinex based on given includable_obj
     * @param HasIncludes $includable_obj
     * @return int
     */
    public function getIncludeToRamzinexCount(HasIncludes $includable_obj): int
    {
        return $includable_obj->includes()->count();
    }

    /**
     * check if selected global_currency is included by this user or not
     * @param HasIncludes $includable_obj
     * @param int $user_id
     * @return bool
     */
    public function checkIfIncludeToRamzinexExists(HasIncludes $includable_obj, int $user_id): bool
    {
        return $includable_obj->includes()->where('user_id', '=', $user_id)->exists();
    }

    /**
     * create a new IncludeToRamzinex
     * @param HasIncludes $includable_obj
     * @param int $user_id
     * @return bool
     */
    public function createIncludeToRamzinex(HasIncludes $includable_obj, int $user_id): bool
    {

        try {
            $includable_obj->includes()->attach($user_id);
        } catch (QueryException | Exception $exception) {
            Log::error('error while creating IncludeToRamzinex' . $exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * delete a IncludeToRamzinex
     * @param HasIncludes $includable_obj
     * @return bool
     */
    public function deleteIncludeToRamzinex(HasIncludes $includable_obj):bool
    {
        try {
            $includable_obj->includes()->detach($includable_obj);
        } catch (QueryException | Exception $exception) {
            Log::error('error while deleting' . $exception->getMessage());
            return false;
        }
        return true;
    }


}
