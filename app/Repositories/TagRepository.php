<?php


namespace App\Repositories;


use App\Models\Interfaces\HasTag;
use App\Models\Tag;
use App\Repositories\Interfaces\TagRepositoryInterface;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class TagRepository implements TagRepositoryInterface
{

    /**
     * get tag and return it according to given id
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getById(int $id): Tag
    {
        return $this->tagFetchQueryBuilder()->findOrFail($id);
    }

    /**
     * get all tags and return them
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll(): Collection
    {
        return $this->tagFetchQueryBuilder()->get();
    }

    /**
     * paginate tags
     * @param int $per_pag
     * @return paginator
     */
    public function paginateTag(int $per_pag = 10): Paginator
    {
        return $this->tagFetchQueryBuilder()->with(['banners','globalCurrencies'])->paginate($per_pag);
    }

    /**
     * create a tag via polymorphic relation
     * @param \App\Models\Interfaces\HasTag $taggable_obj
     * @param array $data
     * @return bool
     */
    public function create(HasTag $taggable_obj, array $data): bool
    {
        $tag = new Tag();
        $tag->title = $data['title'];

        try {
            $taggable_obj->tags()->save($tag);
        } catch (\Exception | QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;

    }

    /**
     * update a tag
     * @param \App\Models\Tag $tag
     * @param array $data
     * @return bool
     */
    public function Update(Tag $tag, array $data): bool
    {
        if (isset($data['title'])) {
            $tag->title = $data['title'];
        }
        try {
            $tag->save();
        } catch (\Exception | QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * delete a tag
     * @param \App\Models\Tag $tag
     * @return bool
     */
    public function delete(Tag $tag): bool
    {
        try {
            $tag->delete();

        } catch (\Exception | QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * tag's query builder
     * @return Builder
     */
    private function tagFetchQueryBuilder(): Builder
    {
        return Tag::query();
    }


}
