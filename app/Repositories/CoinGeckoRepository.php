<?php


namespace App\Repositories;

use App\Repositories\Interfaces\GlobalCurrencyApiInterface;
use App\Services\Interfaces\CurlServiceInterface;

/**
 * Class CoinPaprikaRepository
 * @package App\Repositories
 */
class CoinGeckoRepository implements GlobalCurrencyApiInterface
{

    /**
     * Curl service container
     * @var CurlServiceInterface
     */
    private CurlServiceInterface $request;

    /**
     * CoinPaprikaRepository constructor.
     * inject guzzleCurl service
     * @param CurlServiceInterface $request
     */
    public function __construct(CurlServiceInterface $request)
    {
        $this->request = $request;
    }

    /**
     * Get all Global Currencies list from API Service
     * @return array|void
     */
    public function getGlobalCurrenciesList(): array
    {

        $result = $this->request->createClient(config('currency_services.url.coin_gecko.base'))
            ->setUrlSection(config('currency_services.url.coin_gecko.list'))
            ->executeRequest();

        if (isset($result['status']) && $result['status']) {
            return $result['content'];
        }

        return [];
    }

    /**
     * Get specific Global Currency information by it's id from service
     * @param string $id
     * @return array|void
     */
    public function getGlobalCurrencyTickerByID(string $id): array
    {
        //Todo get fixed passing param too config
        $request_answer = $this->request->createClient(config('currency_services.url.coin_gecko.base'))->setUrlSection(config('currency_services.url.coin_gecko.coins')."$id")->executeRequest();
        if (isset($request_answer['status']) && $request_answer['status']) {

            $global_currency_info = [];
            $global_currency_info['rank'] = $request_answer['content']['coingecko_rank'];
            $global_currency_info['image'] = $request_answer['content']['image']['thumb'];
            $global_currency_info['circulating_supply'] = $request_answer['content']['market_data']['circulating_supply'];
            $global_currency_info['total_supply'] = $request_answer['content']['market_data']['total_supply'];
            $global_currency_info['quotes']['USD']['price'] = $request_answer['content']['market_data']['current_price']['usd'];
            $global_currency_info['quotes']['USD']['market_cap'] = $request_answer['content']['market_data']['market_cap']['usd'];
            $global_currency_info['quotes']['USD']['volume_24h'] = $request_answer['content']['market_data']['total_volume']['usd'];
            $global_currency_info['quotes']['USD']['percent_change_24h'] = $request_answer['content']['market_data']['price_change_percentage_24h'];
            $global_currency_info['quotes']['USD']['percent_change_7d'] = $request_answer['content']['market_data']['price_change_percentage_7d'];
            $global_currency_info['quotes']['USD']['percent_change_30d'] = $request_answer['content']['market_data']['price_change_percentage_30d'];
            $global_currency_info['quotes']['USD']['percent_change_1y'] = $request_answer['content']['market_data']['price_change_percentage_1y'];
            $global_currency_info['max_supply'] = $request_answer['content']['market_data']['max_supply'];

            return $global_currency_info;
        }

        return [];
    }

    /**
     * Get all Global Currencies information from service
     * @return array|void
     */
    public function tickersAllGlobalCurrencies(): array
    {
        $result = $this->request->createClient(config('currency_services.url.coin_gecko.base'))->setUrlSection('tickers')->executeRequest();

        if (isset($result['status']) && $result['status']) {
            return $result['content'];
        }

        return [];
    }

}
