<?php

namespace App\Repositories;

use App\Models\Banner;
use App\Repositories\Interfaces\BannerRepositoryInterface;
use Exception;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class  BannerRepository implements BannerRepositoryInterface
{
    /**
     * it returns an instance of banner model according to given id
     * @param int $id
     * @param bool $with_relation
     * @param bool $with_counts
     * @param int|null $user_id
     * @return Banner|Builder|Builder[]|Collection|Model
     */
    public function getById(int $id, bool $with_relation = false, bool $with_counts = false, int $user_id = null): Banner
    {
        return $this->buildBanner($with_relation, null, $with_counts, $user_id, true)
            ->when($with_relation, function (Builder $query) {
                $query->with(['tags']);
            })->findOrFail($id);
    }

    /**
     * Paginate and get Banners
     * @param bool $with_counts
     * @param int $count
     * @param array $input
     * @return Paginator
     */
    public function paginateBanner(bool $with_counts = false, int $count = 10, array $input = []): Paginator
    {
        return $this->buildBanner(false, null, $with_counts, null, false, true, $input)->paginate($count);
    }

    /**
     *  it returns all active  banners
     * @param int|null $type_id
     * @param int|null $from
     * @param int|null $limit
     * @param bool $only_enabled
     * @param bool $with_relation
     * @param bool $with_counts
     * @param int|null $user_id
     * @param bool $sort_desc
     * @return Builder[]|Collection
     */
    public function getAllBanners(int $type_id = null, int $from = null, int $limit = null, bool $only_enabled = true, bool $with_relation = false, bool $with_counts = false, int $user_id = null, bool $sort_desc = false): Collection
    {
        return $this->buildBanner($with_relation, $type_id, $with_counts, $user_id, $only_enabled, $sort_desc)->when($from, function (Builder $query) use ($from) {
            $query->skip($from);
        })->when($limit, function (Builder $query) use ($limit) {
            $query->take($limit);
        })->get();
    }



    /**
     * @return Collection
     */
    public function getSliders(): Collection
    {
        return Banner::query()->where('is_slider','=',1)->where('is_enabled','=',1)->take(8)->orderBy('created_at','desc')->get();
    }
    /**
     * Get and return all banners counts
     * @param int|null $type_id
     * @param bool $only_enabled
     * @return int
     */
    public function getBannersCount(int $type_id = null, bool $only_enabled = true): int
    {
        return $this->buildBanner(false, $type_id, false, null, $only_enabled)->count();
    }

    /**
     * it create new Banners
     * @param array $data
     * @return bool
     */
    public function createBanner(array $data): bool
    {
        $banner = new Banner();
        $banner->URL = $data['URL'];
        $banner->title_fa = $data['title_fa'];
        $banner->title_en = $data['title_en'];
        $banner->description_fa = $data['description_fa'];
        $banner->description_en = $data['description_en'];
        $banner->html_description = $data['html_description'];
        $banner->image = $data['image'];
        $banner->banner_type_id = $data['banner_type_id'];
        try {
            $banner->save();
        } catch (QueryException | Exception $ex) {
            Log::error('error while creating' . $ex->getmessage());
            return false;
        }
        return true;
    }

    /**
     * add global currencies to banner
     * @param Banner $banner
     * @param array $currencies
     * @return bool
     */
    public function attachGlobalCurrencies(Banner $banner, array $currencies): bool
    {
        try {
            $banner->globalCurrencies()->sync($currencies);
        } catch (Exception | QueryException $exception) {
            Log::error('error while syncing global_currencies to banner' . $exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * update existing Banners
     * @param array $data
     * @param Banner $banner
     * @return bool
     */
    public function updateBanner(array $data, Banner $banner): bool
    {
        if (isset($data['image'])) {
            $banner->image = $data['image'];
        }

        if (isset($data['URL'])) {
            $banner->URL = $data['URL'];
        }

        if (isset($data['title_fa'])) {
            $banner->title_fa = $data['title_fa'];
        }

        if (isset($data['title_en'])) {
            $banner->title_en = $data['title_en'];
        }

        if (isset($data['description_fa'])) {
            $banner->description_fa = $data['description_fa'];
        }

        if (isset($data['description_en'])) {
            $banner->description_en = $data['description_en'];
        }

        if (isset($data['is_enabled'])) {
            $banner->is_enabled = $data['is_enabled'];
        }

        if (isset($data['is_slider'])) {
            $banner->is_slider = $data['is_slider'];
        }

        if (isset($data['banner_type_id'])) {
            $banner->banner_type_id = $data['banner_type_id'];
        }
        if (isset($data['html_description'])) {
            $banner->html_description = $data['html_description'];
        }
        try {
            $banner->save();
        } catch (QueryException | Exception $ex) {
            Log::error('error while updating' . $ex->getmessage());
            return false;
        }
        return true;
    }


    /**
     * it deletes an existing Banner
     * @param Banner $banner
     * @return bool
     */
    public function destroyBanners(Banner $banner): bool
    {
        try {
            $banner->globalCurrencies()->detach();
            $banner->delete();
        } catch (QueryException | Exception $ex) {
            Log::error($ex->getmessage());
            return false;
        }
        return true;
    }

    /**
     * it is the query builder of Banners
     * @param bool $with_relation
     * @param int|null $type_id
     * @param bool $with_counts
     * @param int|null $user_id
     * @param bool $only_enabled
     * @param bool $sort_desc
     * @param array $input
     * @return Builder
     */
    private function buildBanner(bool $with_relation = false, int $type_id = null, bool $with_counts = false, int $user_id = null, bool $only_enabled = false, bool $sort_desc = false, array $input = []): Builder
    {
        return Banner::query()->when($type_id, function (Builder $query) use ($type_id) {
            $query->where('banner_type_id', '=', $type_id);
        })->when($with_relation, function (Builder $query) {
            $query->with(['bannerType','globalCurrencies']);
        })->when($with_counts, function (Builder $query) {
            $query->withCount(['dislikes', 'likes','comments', 'views']);
        })->when($user_id, function (Builder $query) use ($user_id) {
            $query->with(['likes' => function ($query) use ($user_id) {
                $query->where('user_id', '=', $user_id);
            }, 'dislikes' => function ($query) use ($user_id) {
                $query->where('user_id', '=', $user_id);
            }, 'favorites' => fn($query) => $query->where('user_id', '=', $user_id)]);
        })->when($only_enabled, function (Builder $query) {
            $query->where('is_enabled', '=', 1);
        })->when($sort_desc, function (Builder $query) {
            $query->orderBy('created_at', 'DESC');
        })->when(isset($input['title_fa']), function (Builder $query) use ($input) {
            $query->where('title_fa', 'LIKE', '%'. $input['title_fa']. '%');
        })->when(isset($input['title_en']), function (Builder $query) use ($input) {
            $query->where('title_en', 'LIKE', '%'. $input['title_en']. '%');
        })->when(isset($input['description_fa']), function (Builder $query) use ($input) {
            $query->where('description_fa', 'LIKE', '%'. $input['description_fa']. '%');
        })->when(isset($input['description_en']), function (Builder $query) use ($input) {
            $query->where('description_en', 'LIKE', '%'. $input['description_en']. '%');
        })->when(isset($input['url']), function (Builder $query) use ($input) {
            $query->where('url', '=', $input['url']);
        })->when(isset($input['banner_type_id']), function (Builder $query) use ($input) {
            $query->where('banner_type_id', '=', $input['banner_type_id']);
        });
    }
}
