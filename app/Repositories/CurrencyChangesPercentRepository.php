<?php

namespace App\Repositories;

use App\Models\CurrencyChangesPercent;
use App\Repositories\Interfaces\CurrencyChangesPercentRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class CurrencyChangesPercentRepository implements CurrencyChangesPercentRepositoryInterface
{
    /**
     * Get and return currency changes percent
     * @param array $input
     * @return CurrencyChangesPercent|null
     */
    public function getChangesPercent(array $input): ?CurrencyChangesPercent
    {
        return $this->fetchQueryBuilder($input)->first() ?? null;
    }

    /**
     * paginate Profit and damage.
     *
     * @param array $input
     * @param int $perpage
     * @return LengthAwarePaginator
     */
    public function paginate(array $input = [], int $perpage = 10): LengthAwarePaginator
    {
        return $this->fetchQueryBuilder($input)->paginate($perpage);
    }


    /**
     * Get the get all value from the database.
     *
     * @param array $input
     * @return Collection
     */
    public function getAll(array $input = []): Collection
    {
        return $this->fetchQueryBuilder($input)->get();
    }

    /**
     * find by id the record with the given id.
     *
     * @param int $id
     * @return CurrencyChangesPercent|Builder|Builder[]
     */
    public function findById(int $id): CurrencyChangesPercent
    {
        return $this->fetchQueryBuilder()->findOrFail($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param array $value
     * @return bool
     */
    public function store(array $value): bool
    {
        $profit_damage = new CurrencyChangesPercent();
        $profit_damage->value = $value['value'];
        $profit_damage->global_currency_id = $value['global_currency_id'];
        $profit_damage->interval_id = $value['interval_id'];
        try {
            $profit_damage->save();
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param array $value
     * @param CurrencyChangesPercent $profit_damage
     * @return bool
     */
    public function update(array $value, CurrencyChangesPercent $profit_damage): bool
    {
        if (isset($value['value'])) {
            $profit_damage->value = $value['value'];
        }
        if (isset($value['global_currency_id'])) {
            $profit_damage->global_currency_id = $value['global_currency_id'];
        }
        if (isset($value['interval_id'])) {
            $profit_damage->interval_id = $value['interval_id'];
        }
        try {
            $profit_damage->save();
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param CurrencyChangesPercent $profit_damage
     * @return bool
     * @throws \Exception
     */
    public function delete(CurrencyChangesPercent $profit_damage): bool
    {
        try {
            $profit_damage->delete();
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * fetch query builder profit and Damage.
     *
     * @param $input
     * @return Builder|mixed
     */
    private function fetchQueryBuilder(array $input = []): Builder
    {
        return CurrencyChangesPercent::query()
            ->when(isset($input['global_currency_id']), function (Builder $query) use ($input) {
                $query->where('global_currency_id', '=', $input['global_currency_id']);
            })
            ->when(isset($input["interval_id"]), function (Builder $query) use ($input) {
                $query->where("interval_id", '=', $input["interval_id"]);
            });
    }
}
