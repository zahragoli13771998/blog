<?php


namespace App\Repositories;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use  Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use App\Models\Looser;
use App\Repositories\Interfaces\LooserRepositoryInterface;

class LooserRepository implements LooserRepositoryInterface
{
    /**
     * get and return Looser according to given id
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getById(int $id): Looser
    {
        return Looser::query()->findOrFail($id);
    }

    /**
     * get and return all loosers
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllLoosers(bool $order_by_value = false): Collection
    {
        return Looser::query()->when($order_by_value, function (Builder $query) {
            $query->orderByDesc('value');
        })->get();
    }

    /**
     * paginate Loosers
     * @param int $per_page
     * @return Paginator
     */
    public function paginateLooser(int $per_page = 10): Paginator
    {
        return Looser::query()->paginate($per_page);
    }

    /**
     * create a new Looser
     * @param array $data
     * @return bool
     */
    public function createLooser(array $data): bool
    {
        $Looser = new Looser();
        $Looser->id = $data['id'];
        $Looser->value = $data['value'];
        try {
            $Looser->save();
        } catch (QueryException | Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * update an existing Looser
     * @param Looser $looser
     * @param array $data
     * @return bool
     */
    public function updateLooser(Looser $looser, array $data): bool
    {
        if (isset($data['id'])) {
            $looser->id = $data['id'];
        }
        if (isset($data['value'])) {
            $looser->value = $data['value'];
        }
        try {
            $looser->save();
        } catch (QueryException | Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }

    /**
     * delete Looser
     * @param Looser|null $looser
     * @return bool
     */
    public function deleteLooser(Looser $looser = null): bool
    {
        try {
            if ($looser) {
                $looser->delete();
            }

            if (!$looser) {
                Looser::query()->delete();
            }
        } catch (QueryException | Exception $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }



}
