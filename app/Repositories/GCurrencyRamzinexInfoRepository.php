<?php


namespace App\Repositories;


use App\Models\GCurrencyRamzinexInfo;
use App\Models\GlobalCurrency;
use App\Repositories\Interfaces\GCurrencyRamzinexInfoInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;


/**
 * Class GlobalCurrencyRepository
 * @package App\Repositories
 */
class GCurrencyRamzinexInfoRepository implements GCurrencyRamzinexInfoInterface
{
    /**
     * Get all global Currencies from DB
     * @param bool $with_relation
     * @return Collection
     */
    public function getAll(bool $with_relation = false): Collection
    {
        return $this->fetchQueryBuilder($with_relation)->get();
    }

    /**
     * paginate interval.
     * @param int $perpage
     * @param bool $with_relation
     * @return LengthAwarePaginator
     */
    public function paginate(int $perpage = 10, bool $with_relation = false): LengthAwarePaginator
    {
        return $this->fetchQueryBuilder($with_relation)->paginate($perpage);
    }

    /**
     * findById the record with the given id.
     *
     * @param int $id
     * @return GCurrencyRamzinexInfo
     */
    public function findById(int $id): GCurrencyRamzinexInfo
    {
        return $this->fetchQueryBuilder()->findOrFail($id);
    }

    /**
     * create currency ramzinex info
     * @param array $data
     * @param GlobalCurrency $currency
     * @return bool
     */
    public function store(array $data, GlobalCurrency $currency): bool
    {
        $info = new GCurrencyRamzinexInfo();

        if (isset($data['open'])) {
            $info->open = $data['open'];
        }

        if (isset($data['close'])) {
            $info->close = $data['close'];
        }
        if (isset($data['buy'])) {
            $info->buy = $data['buy'];
        }
        if (isset($data['sell'])) {
            $info->sell = $data['sell'];
        }

        if (isset($data['high'])) {
            $info->high = $data['high'];
        }

        if (isset($data['low'])) {
            $info->low = $data['low'];
        }

        if (isset($data['quote_volume'])) {
            $info->quote_volume = $data['quote_volume'];
        }
        if (isset($data['base_volume'])) {
            $info->base_volume = $data['base_volume'];
        }

        if (isset($data['change_percent'])) {
            $info->change_percent = $data['change_percent'];
        }
        try {
            $currency->ramzinexInfo()->save($info);

        } catch (QueryException | \Exception $ex) {
            Log::error($ex->getmessage());
            return false;
        }
        return true;
    }

    /**
     * update specific global currency model
     * @param array $data
     * @param GCurrencyRamzinexInfo $ramzinex_info
     * @return bool
     */
    public function update(array $data, GCurrencyRamzinexInfo $ramzinex_info): bool
    {
        if (isset($data['open'])) {
            $ramzinex_info->open = $data['open'];
        }
        if (isset($data['close'])) {
            $ramzinex_info->close = $data['close'];
        }
        if (isset($data['buy'])) {
            $ramzinex_info->buy = $data['buy'];
        }
        if (isset($data['sell'])) {
            $ramzinex_info->sell = $data['sell'];
        }

        if (isset($data['high'])) {
            $ramzinex_info->high = $data['high'];
        }

        if (isset($data['low'])) {
            $ramzinex_info->low = $data['low'];
        }

        if (isset($data['quote_volume'])) {
            $ramzinex_info->quote_volume = $data['quote_volume'];
        }
        if (isset($data['base_volume'])) {
            $ramzinex_info->base_volume = $data['base_volume'];
        }

        if (isset($data['change_percent'])) {
            $ramzinex_info->change_percent = $data['change_percent'];
        }
        try {
            $ramzinex_info->save();

        } catch (QueryException | \Exception $ex) {
            Log::error($ex->getmessage());
            return false;
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     * @param GCurrencyRamzinexInfo $info
     * @return bool
     * @throws \Exception
     */
    public function delete(GCurrencyRamzinexInfo $info): bool
    {
        try {
            $info->delete();
        } catch (QueryException $exception) {
            Log::error($exception->getMessage());
            return false;
        }
        return true;
    }


    /**
     * @param bool|false $with_relation
     * @return Builder
     */
    private function fetchQueryBuilder(bool $with_relation = false): Builder
    {
        return GCurrencyRamzinexInfo::query()->when($with_relation, function (Builder $query) {
            $query->with(['globalCurrency']);
        });
    }
}
