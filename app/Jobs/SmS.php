<?php

namespace App\Jobs;

use App\Logic\Sms\KaveNegarHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SmS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct($receptor, $message)
    {
        $this->message = $message;
        $this->receptor = $receptor;
    }

    /**
     *  create dislike via DisLikeRepository
     */
    public function handle()
    {
        KaveNegarHelper::getInstance()->send($this->receptor, $this->message);

    }
}

{

}
