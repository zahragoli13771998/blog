<?php

namespace App\Jobs;

use App\Services\Interfaces\TickerServiceInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateGlobalCurrenciesListJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * create all global currency in one time
     * @param TickerServiceInterface $ticker_service
     */
    public function handle(TickerServiceInterface $ticker_service)
    {
        $ticker_service->createGlobalCurrenciesList();
    }
}
