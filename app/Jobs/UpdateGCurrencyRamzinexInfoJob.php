<?php

namespace App\Jobs;

use App\Services\Interfaces\PairCurrenciesInfoServiceInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateGCurrencyRamzinexInfoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     * update global currency ramzinex info
     * @return void
     */
    public function handle(PairCurrenciesInfoServiceInterface $pair_currencies_info_service)
    {
        $pair_currencies_info_service->updateRamzinexCurrenciesInfo();
    }
}
