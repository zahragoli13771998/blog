<?php

namespace App\Jobs;

use App\Services\Interfaces\TickerServiceInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateGlobalCurrenciesOHLCJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Define from
     * @var int
     */
    private int $from;
    /**
     * Define take amount
     * @var int
     */
    private int $take;

    /**
     * UpdateGlobalCurrenciesJob constructor.
     * @param int $from
     * @param int $take
     */
    public function __construct(int $from , int $take)
    {
        $this->from=$from;
        $this->take=$take;
    }

    /**
     * update all global currency from $from $take number of currency
     * @param TickerServiceInterface $ticker_service
     */
    public function handle(TickerServiceInterface $ticker_service)
    {
        $ticker_service->createGlobalCurrenciesOHLC($this->from,$this->take);
    }
}
