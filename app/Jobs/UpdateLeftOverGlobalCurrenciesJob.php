<?php

namespace App\Jobs;

use App\Repositories\CoinGeckoRepository;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Services\Interfaces\TickerServiceInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateLeftOverGlobalCurrenciesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public array $global_currencies=['dydx'];


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(GlobalCurrencyInterface $global_currency_repository,TickerServiceInterface $ticker_service,CoinGeckoRepository $coin_gecko_repository)
    {
        foreach ($this->global_currencies as $id){

            $global_currency=$global_currency_repository->getById(null,false,false,false,$id);
            $data=$coin_gecko_repository->getGlobalCurrencyTickerByID($id);

            if ($global_currency && !empty($data)){

                $ticker_service->updateGlobalCurrencyInfo($data, $global_currency);
                $ticker_service->createCurrencyChanges($data, $global_currency);
            }
        }
    }
}
