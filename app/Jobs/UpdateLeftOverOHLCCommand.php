<?php

namespace App\Jobs;

use App\Models\Interval;
use App\Repositories\CoinGeckoRepository;
use App\Repositories\Interfaces\GlobalCurrencyInterface;
use App\Services\Interfaces\TickerServiceInterface;
use App\Services\TickerService\TickerService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateLeftOverOHLCCommand implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public array $global_currencies = ['dydx'];


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(GlobalCurrencyInterface $global_currency_repository, TickerServiceInterface $ticker_service, CoinGeckoRepository $coin_gecko_repository)
    {
        foreach ($this->global_currencies as $coin) {

            $global_currency = $global_currency_repository->getById(null, false, false, false, $coin);
            $data = $coin_gecko_repository->getGlobalCurrencyOHLC1($coin, 30, 0);

            if (!count($data)) {
                continue;
            }
            //Store Daily OHLC Data
            if (isset($data[array_key_last($data)])) {
                $today_data['open'] = $data[array_key_last($data)][1];
                $today_data['high'] = $data[array_key_last($data)][2];
                $today_data['low'] = $data[array_key_last($data)][3];
                $today_data['close'] = $data[array_key_last($data)][4];

                $ticker_service->storeOHLCData($global_currency, $today_data, Interval::getDailyId());
            }

            //Store Weekly OHLC DATA
            if (isset($data[140])) {
                $weekly_data['open'] = $data[140][1];
                $weekly_data['high'] = $data[140][2];
                $weekly_data['low'] = $data[140][3];
                $weekly_data['close'] = $data[140][4];
                $ticker_service->storeOHLCData($global_currency, $weekly_data, Interval::getWeeklyId());
            }

            //Store Monthly OHLC Data
            if (isset($data[array_key_first($data)])) {
                $monthly_data['open'] = $data[array_key_first($data)][1];
                $monthly_data['high'] = $data[array_key_first($data)][2];
                $monthly_data['low'] = $data[array_key_first($data)][3];
                $monthly_data['close'] = $data[array_key_first($data)][4];
                $ticker_service->storeOHLCData($global_currency, $monthly_data, Interval::getMonthlyId());
            }
        }
    }
}
