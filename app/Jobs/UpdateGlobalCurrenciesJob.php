<?php

namespace App\Jobs;

use App\Services\Interfaces\TickerServiceInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;

class UpdateGlobalCurrenciesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Define atomic lock string key
     * @var string
     */
    private string $atomic_lock_key = 'update_global_currencies_lock:';

    /**
     * Define atomic lock time amount
     * @var int
     */
    private int $atomic_lock_seconds = 420;

    /**
     * Define from
     * @var int
     */
    private int $from;
    /**
     * Define take amount
     * @var int
     */
    private int $take;

    /**
     * UpdateGlobalCurrenciesJob constructor.
     * @param int $from
     * @param int $take
     */
    public function __construct(int $from , int $take)
    {
        $this->from=$from;
        $this->take=$take;
    }

    /**
     * update all global currency from $from $take number of currency
     * @param TickerServiceInterface $ticker_service
     */
    public function handle(TickerServiceInterface $ticker_service)
    {
        $lock = Cache::lock($this->atomic_lock_key.$this->from, $this->atomic_lock_seconds);

        if ($lock->get()) {
            $ticker_service->updateAllGlobalCurrencyInformation($this->from,$this->take);
            $lock->forceRelease();
        }
    }
}
