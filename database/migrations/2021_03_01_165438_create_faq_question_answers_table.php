<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqQuestionAnswersTable  extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_question_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('faq_category_id');
            $table->unsignedInteger('show_order');
            $table->boolean('is_repetitive')->default(0);
            $table->string('question_fa');
            $table->string('short_answer_fa');
            $table->text('complete_answer_fa');
            $table->string('image')->nullable();
            $table->string('video')->nullable();
            $table->string('external_video',300)->nullable();
            $table->softDeletes();

            $table->foreign('faq_category_id')->references('id')->on('faq_categories')->onDelete('NO ACTION')->onUpdate('NO ACTION');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_question_answers');
    }
}
