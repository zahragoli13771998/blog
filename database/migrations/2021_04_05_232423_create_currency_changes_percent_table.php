<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyChangesPercentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_changes_percent', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('value');

            $table->unsignedBigInteger('global_currency_id');
            $table->unsignedInteger('interval_id');

            $table->boolean('status')->default(1)->index();

            $table->timestamps();

            $table->foreign('global_currency_id')->references('id')
                ->on('global_currencies')->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('interval_id')->references('id')
                ->on('intervals')->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_changes_percent');
    }
}
