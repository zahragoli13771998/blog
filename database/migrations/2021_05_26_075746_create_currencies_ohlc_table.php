<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrenciesOhlcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies_ohlc', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('global_currency_id');
            $table->unsignedInteger('interval_id');
            $table->double('open');
            $table->double('high');
            $table->double('low');
            $table->double('close');
            $table->bigInteger('volume');
            $table->bigInteger('market_cap');
            $table->timestamps();

            $table->foreign('global_currency_id')
                ->references('id')
                ->on('global_currencies');

            $table->foreign('interval_id')
                ->references('id')
                ->on('intervals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_currencies_ohlc');
    }
}
