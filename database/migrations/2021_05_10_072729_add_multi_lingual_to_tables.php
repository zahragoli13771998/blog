<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultiLingualToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banner_types', function (Blueprint $table) {
            $table->string('title_en')->after('title');
        });

        Schema::table('faq_categories', function (Blueprint $table) {
            $table->string('name_en')->after('name');
        });

        Schema::table('faq_question_answers', function (Blueprint $table) {
            $table->string('question_en')->after('question');
            $table->string('short_answer_en')->after('short_answer');
            $table->text('complete_answer_en')->after('complete_answer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banner_types', function (Blueprint $table) {
            $table->dropColumn('title_en');
        });

        Schema::table('faq_categories', function (Blueprint $table) {
            $table->dropColumn('name_en');
        });

        Schema::table('faq_question_answers', function (Blueprint $table) {
            $table->dropColumn('question_en');
            $table->dropColumn('short_answer_en');
            $table->dropColumn('complete_answer_en');
        });
    }
}
