<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectorInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sector_infos', function (Blueprint $table) {
            $table->id();
            $table->string('description_fa');
            $table->string('description_en');
            $table->double('market_cap');
            $table->double('trading_volume');
            $table->unsignedBigInteger('sector_id');

            $table->foreign('sector_id')->references('id')->on('sectors')
                ->onDelete('NO ACTION')->onUpdate('NO ACTION');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sector_infos');
    }
}
