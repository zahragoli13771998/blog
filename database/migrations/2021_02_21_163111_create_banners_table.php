<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->bigincrements('id');
            $table->unsignedInteger('banner_type_id');
            $table->string('image');
            $table->text('URL');
            $table->string('title_fa');
            $table->string('title_en');
            $table->text('description_fa');
            $table->text('description_en');
            $table->boolean('is_enabled')->default(true);
            $table->boolean('is_slider')->default(true);
            $table->timestamps();

            $table->foreign('banner_type_id')->references('id')
                ->on('banner_types')->onDelete('NO ACTION')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
