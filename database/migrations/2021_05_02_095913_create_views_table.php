<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('views', function (Blueprint $table) {
                $table->increments('id');
                $table->string('ip');
                $table->string('browser_agent');
                $table->string('cookie_string');
                $table->string('viewable_type');
                $table->unsignedInteger('viewable_id');
                $table->timestamps();

                $table->index(['viewable_type', 'viewable_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('views');
    }
}
