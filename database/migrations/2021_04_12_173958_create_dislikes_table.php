<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDislikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_dislikes', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->unsignedInteger('dislikeable_id');
            $table->string("dislikeable_type");

            $table->foreign('user_id')
            ->references('id')
            ->on('users');

            $table->index(['dislikeable_id', 'dislikeable_type', 'user_id']);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dislikes');
    }
}
