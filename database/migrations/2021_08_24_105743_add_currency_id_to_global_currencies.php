<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCurrencyIdToGlobalCurrencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('global_currencies', function (Blueprint $table) {
            $table->unsignedInteger('currency_id')->nullable();

            $table->foreign('currency_id')
                ->references('id')->on('currencies')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('global_currencies', function (Blueprint $table) {

            $table->dropColumn('currency_id');
        });
    }
}
