<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddtionalPricesToGlobalCurrenciesTalbe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('global_currencies', function (Blueprint $table) {
            $table->double('rial_price')->default(0)->after('price');
            $table->double('btc_price')->after('rial_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('global_currencies', function (Blueprint $table) {
            $table->dropColumn(['rial_price', 'btc_price']);
        });
    }
}
