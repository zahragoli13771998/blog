<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GlobalCurrencySectorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_currency_sector', function (Blueprint $table) {
            $table->unsignedBigInteger('global_currency_id');
            $table->unsignedBigInteger('sector_id');

            $table->foreign('global_currency_id')->references('id')->on('global_currencies')
                ->onDelete('NO ACTION')->onUpdate('NO ACTION');
            $table->foreign('sector_id')->references('id')->on('sectors')
                ->onDelete('NO ACTION')->onUpdate('NO ACTION');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_currency_sector');
    }
}
