<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGcurrenciesRamzinexInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gcurrencies_ramzinex_info', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('global_currency_id');
            $table->double('open')->nullable();
            $table->double('close')->nullable();
            $table->double('buy')->nullable();
            $table->double('sell')->nullable();
            $table->double('high')->nullable();
            $table->double('low')->nullable();
            $table->double('quote_volume')->nullable();
            $table->double('base_volume')->nullable();
            $table->double('change_percent')->nullable();
            $table->timestamps();
            $table->foreign('global_currency_id')
                ->references('id')
                ->on('global_currencies')
                ->onDelete('NO ACTION')
                ->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gcurrencies_ramzinex_info');
    }
}
