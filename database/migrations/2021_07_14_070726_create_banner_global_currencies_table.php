<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerGlobalCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_global_currencies', function (Blueprint $table) {
            $table->unsignedBigInteger('banner_id');
            $table->unsignedBigInteger('global_currency_id');
            $table->foreign('banner_id')->references('id')->on('banners')->onDelete('NO ACTION')->onUpdate('NO ACTION');
            $table->foreign('global_currency_id')->references('id')->on('global_currencies')->onUpdate('NO ACTION')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_global_currencies');
    }
}
