<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_currencies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rank')->nullable();
            $table->integer('pair_id')->nullable();
            $table->integer('price_precision')->nullable();
            $table->double('price')->nullable();
            $table->double('rial_price')->nullable();
            $table->double('btc_price')->nullable();
            $table->string('name_en')->nullable();
            $table->string('name_fa')->nullable();
            $table->string('symbol_id')->nullable();
            $table->string('symbol')->unique()->nullable();
            $table->string('icon')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_fa')->nullable();
            $table->double('total_supply')->nullable();
            $table->double('market_dominance')->nullable();
            $table->double('total_volume')->nullable();
            $table->double('daily_trading_volume')->nullable();
            $table->double('coin_circulation')->nullable();
            $table->boolean('is_hidden')->default(false);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_currencies');
    }
}
