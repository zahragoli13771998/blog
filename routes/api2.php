<?php

use App\Http\Controllers\API2\CommentController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API2\GlobalCurrencyController;
use App\Http\Controllers\API2\BannerController;
use App\Http\Controllers\API2\FaqQuestionAnswerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('global-currencies')->name('global_currencies.')->group(function () {
    Route::get('/comments/list', [GlobalCurrencyController::class, 'getComments'])->name('list_v2');
    Route::middleware(['user_authentication'])->group(function () {
        Route::prefix('comments')->name('comments.')->group(function () {
            Route::POST('/add', [GlobalCurrencyController::class, 'addComment'])->name('add_v2');
        });
    });
});

Route::prefix('faq')->name('faq_v2.')->group(function () {
    Route::get('/comments/list', [FaqQuestionAnswerController::class, 'getComment'])->name('list');
    Route::prefix('question')->group(function () {
        Route::middleware(['user_authentication'])->group(function () {
            Route::prefix('comments')->name('comments.')->group(function () {
                Route::POST('/add', [FaqQuestionAnswerController::class, 'addComment'])->name('add_v2');
            });
        });
    });
});

Route::prefix('banners')->name('banners_v2.')->group(function () {
    Route::get('/comments/list', [BannerController::class, 'getComment'])->name('comment_list_v2');

    Route::middleware(['user_authentication'])->group(function () {
        Route::prefix('comments')->name('comments.')->group(function () {
            Route::POST('/add', [BannerController::class, 'addComment'])->name('add_v2');
        });
    });
});

Route::prefix('comments')->name('comments_v2.')->group(function () {
    Route::get('/replies', [CommentController::class, 'getCommentReplies'])->name('replies.list');
});
Route::get('user_likes_list', [\App\Http\Controllers\API3\UserListsController::class, 'getUsersLikesList']);
Route::get('user_dislikes_list', [\App\Http\Controllers\API3\UserListsController::class, 'getUsersDisLikesList']);
Route::get('user_favored_list', [\App\Http\Controllers\API3\UserListsController::class, 'getUsersFavoritesList']);
Route::get('user_answered_poll_list', [\App\Http\Controllers\API3\UserListsController::class, 'getUserAnsweredPool']);
Route::prefix('comments_report')->name('comments_report.')->group(function () {
    Route::get('/report', [CommentController::class, 'getCommentReplies'])->name('report.list');
    Route::POST('/create', [\App\Http\Controllers\API\CommentReportController::class, 'create'])->name('create2');
//    Route::POST('/dislike', [CommentController::class, 'dislikeComments'])->name('dislike');
});
Route::get('/rrrr1',function (){
    return 'hii';
})->name('testy11');
