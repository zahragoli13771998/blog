<?php

use App\Http\Controllers\API\GlobalCurrencyController;
use App\Http\Controllers\GameController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StorageManagerController;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/storage/{path}', [StorageManagerController::class, 'showFile'])
    ->where([
        'path' => '.*'
    ])
    ->name('storage.show.file');

Route::get('/exchange/getUserGame',[\App\Http\Controllers\ApiKeyController::class,'gameUser'])->name('gameUser');



Route::get('/exchange/tv_market_global2/{symbol}', [GlobalCurrencyController::class, 'tradingViewBySymbol'])->middleware('s_cache:300');
Route::get('/exchange/tv_market/{pair_id}', [GlobalCurrencyController::class, 'tradingViewByPairId'])->middleware('s_cache:300');
Route::get('exchange/pt1/market',[\App\Http\Controllers\VueController::class,'pwaIndex']);
Route::pattern('ex', '.*');
Route::any('/pt{ex}', [\App\Http\Controllers\VueController::class, 'pwaIndex'])->name('pwa_index');
Route::get('play', function () {
    return redirect('https://ramzinex.com/');
});
Route::prefix('comments_report')->name('comments_report.')->group(function () {
//    Route::get('/report', [CommentController::class, 'getCommentReplies'])->middleware('s_cache:300')->name('report.list');
    Route::POST('/create', [\App\Http\Controllers\API\CommentReportController::class, 'create'])->name('create');
//    Route::POST('/dislike', [CommentController::class, 'dislikeComments'])->name('dislike');
});

Route::pattern('ex', '.*');
Route::any('/register_with_invitation={ex}', [\App\Http\Controllers\ExcelController::class, 'test'])->name('test*');

//Route::pattern('ex', '.*');
Route::get('/test_new_url',[\App\Http\Controllers\ExcelController::class, 'testUrl'])->name('test**');

//Route::get('hiiii',[\App\Http\Controllers\ExcelController::class,'testUrl']);
Route::get('/testLock1',[\App\Http\Controllers\ApiKeyController::class, 'test1'])->name('test1');
Route::get('/testLock2',[\App\Http\Controllers\ApiKeyController::class, 'test2'])->name('test2');
Route::get('/rrrr',function (){
    return 'hii';
})->name('test3');
