<?php

use App\Http\Controllers\Admin\BannerGCController;
use App\Http\Controllers\Admin\CurrencyOHLCController;
use App\Http\Controllers\Admin\GainerController;
use App\Http\Controllers\Admin\LooserController;
use App\Http\Controllers\Admin\TrendController;
use App\Http\Controllers\Admin\TrendingCurrencyController;
use App\Http\Controllers\Admin\CurrencyController;
use App\Http\Controllers\GameController;
use App\Jobs\UpdateLeftOverGlobalCurrenciesJob;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Admin\GlobalCurrencyController;
use App\Http\Controllers\CurrencyChangeController;
use App\Http\Controllers\IntervalController;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\BannerTypeController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\CommentController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\Admin\GCurrencyInfoController;
use App\Http\Controllers\FaqQuestionAnswerController;
use App\Http\Controllers\FaqCategoryController;

//Todo Add more security
Route::get('/login', [AuthenticatedSessionController::class, 'create'])
    ->middleware('guest')
    ->name('admin.login');

Route::post('/login', [AuthenticatedSessionController::class, 'store'])
    ->middleware('guest');

/** Authenticated user sections */
Route::prefix('panel')->middleware(['is_authenticated'])->name('admin.')->group(function () {
    Route::get('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->name('logout');
    Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard');
    Route::prefix('game')->group(function () {
        //show all game users
        Route::get('/users', [GameController::class, 'users'])->name('game-users.index');
        Route::get('/scores/{user}', [GameController::class, 'scores'])->name('game-users.scores');
        Route::delete('/scores/delete/{score}', [GameController::class, 'deleteScores'])->name('game-users.scores.destroy');

    });

    Route::prefix('global-currencies')->name('global-currencies.')->group(function () {
        //show all global currencies
        Route::get('/', [GlobalCurrencyController::class, 'index'])->name('index');
        //create and store route
        Route::get('/create', [GlobalCurrencyController::class, 'create'])->name('create');
        Route::post('/create', [GlobalCurrencyController::class, 'store'])->name('store');
        //show one global currency route
        Route::get('/show/{currency}', [GlobalCurrencyController::class, 'show'])->name('show');
        //edit and update route
        Route::get('/edit/{currency}/', [GlobalCurrencyController::class, 'edit'])->name('edit');
        Route::post('/edit/{currency}', [GlobalCurrencyController::class, 'update'])->name('update');
        //destroy one global currency
        Route::delete('/delete/{currency}', [GlobalCurrencyController::class, 'destroy'])->name('destroy');
        //Todo:show banners with related gc
        Route::get('/show_banner_gc/{global_currency_id}', [BannerGCController::class, 'showGCWithBanners'])->name('show_banner_gc');
        Route::get('/add_banners/{global_currency_id}', [BannerGCController::class, 'selectBanners'])->name('add_banners');
        Route::post('/add_banners/{global_currency_id}', [BannerGCController::class, 'addBannerToGlobalCurrency']);
        Route::get('attach_banner/{global_currency_id}', [GlobalCurrencyController::class, 'attachBanner']);
        Route::get('/make_hidden/{global_currency_id}', [GlobalCurrencyController::class, 'makeGlobalCurrencyHidden'])->name('make_hidden');

    });

    //IntervalController
    Route::prefix('currency_change_percent')->name('currency_change_percent.')->group(function () {
        Route::get('/', [CurrencyChangeController::class, 'show'])->name('show');
        Route::get('/create', [CurrencyChangeController::class, 'create'])->name('create');
        Route::post('/create', [CurrencyChangeController::class, 'store'])->name('store');
        Route::get('/edit/{currencyChangePercent}', [CurrencyChangeController::class, 'edit'])->name('edit');
        Route::put('/edit/{currencyChangePercent}', [CurrencyChangeController::class, 'update'])->name('update');
        Route::get('/delete/{currencyChangePercent}', [CurrencyChangeController::class, 'destroy'])->name('destroy');
    });

    Route::prefix('intervals')->name('intervals.')->group(function () {
        Route::get('/', [IntervalController::class, 'show'])->name('show');
        Route::get('/create', [IntervalController::class, 'create'])->name('create');
        Route::post('/create', [IntervalController::class, 'store'])->name('store');
        Route::get('/edit/{interval}', [IntervalController::class, 'edit'])->name('edit');
        Route::put('/edit/{interval}', [IntervalController::class, 'update'])->name('update');
        Route::get('/delete/{interval}', [IntervalController::class, 'destroy'])->name('destroy');
    });

    Route::prefix('banner')->name('banner.')->group(function () {

        Route::prefix('types')->name('types.')->group(function () {
            Route::get('/', [BannerTypeController::class, 'show'])->name('show');
            Route::get('/create', [BannerTypeController::class, 'create'])->name('create');
            Route::post('/create', 'BannerTypeController@store')->name('store');
            Route::get('/edit/{bannerType}', 'BannerTypeController@edit')->name('edit');
            Route::put('/edit/{bannerType}', 'BannerTypeController@update')->name('update');
            Route::get('/delete/{bannerType}', 'BannerTypeController@destroy')->name('destroy');
        });

        Route::get('/', [BannerController::class, 'show'])->name('show');
        Route::get('active_banners', [BannerController::class, 'showActiveBanners'])->name('active');
        Route::get('create', [BannerController::class, 'create'])->name('create');
        Route::post('create', [BannerController::class, 'add']);
        Route::get('edit/{banner}', [BannerController::class, 'edit'])->name('edit');
        Route::post('edit/{banner}', [BannerController::class, 'update']);
        Route::get('edit2/{banner}', [BannerController::class, 'update2'])->name('edit2');
        Route::get('delete/{banner}', [BannerController::class, 'delete'])->name('delete');
        Route::get('toggle/{banner}', [BannerController::class, 'toggleBanners'])->name('toggle');
        Route::get('slider_toggle/{banner}', [BannerController::class, 'toggleSliderBanners'])->name('toggle-sliders');
        Route::get('add_global_currencies/{banner_id}', [BannerGCController::class, 'selectGlobalCurrencies'])->name('add_global_currencies');
        Route::post('add_global_currencies/{banner_id}', [BannerGCController::class, 'addGlobalCurrencyToBanner']);
    });

//    Route::prefix('faq')->name('faq.')->group(function () {
//
//        Route::prefix('category')->name('category.')->group(function () {
//            Route::get('/', [FaqCategoryController::class, 'show'])->name('show_categories');
//            Route::get('create', [FaqCategoryController::class, 'create'])->name('create');
//            Route::post('create', [FaqCategoryController::class, 'add']);
//            Route::get('edit/{faq_category}', [FaqCategoryController::class, 'edit'])->name('edit');
//            Route::post('edit/{faq_category}', [FaqCategoryController::class, 'update']);
//            Route::get('delete/{faq_category}', [FaqCategoryController::class, 'delete'])->name('delete');
//        });
//
//        Route::prefix('question_answer')->name('question_answer.')->group(function () {
//            Route::get('/', [FaqQuestionAnswerController::class, 'showQuestions'])->name('show_questions');
//            Route::get('create', [FaqQuestionAnswerController::class, 'create'])->name('create');
//            Route::post('create', [FaqQuestionAnswerController::class, 'add']);
//            Route::get('edit/{faq_question_answer}', [FaqQuestionAnswerController::class, 'edit'])->name('edit');
//            Route::post('edit/{faq_question_answer}', [FaqQuestionAnswerController::class, 'update'])->name('update');
//            Route::get('delete/{faq_question_answer}', [FaqQuestionAnswerController::class, 'delete'])->name('delete');
//            Route::get('/selected/{faq_question_answer}', [FaqQuestionAnswerController::class, 'showSelectedQuestion'])->name('feedback');
//
//            Route::prefix('deeplink')->name('deeplink.')->group(function () {
//                Route::get('/create/{id}', [FaqQuestionAnswerController::class, 'createDeeplink'])->name('create');
//                Route::post('/store', [FaqQuestionAnswerController::class, 'storeDeeplink'])->name('store');
//                Route::get('delete/{deeplink_id}', [FaqQuestionAnswerController::class, 'deleteDeeplink'])->name('delete');
//            });
//        });
//    });

    Route::prefix('comments')->name('comments.')->group(function () {
        Route::get('/', [CommentController::class, 'showComments'])->name('show');
        Route::get('/show/survey', [CommentController::class, 'showSurvey'])->name('survey');
        Route::get('/toggle/{comment_id}', [CommentController::class, 'toggleComment'])->name('toggle');
        Route::post('/toggle/multiple', [CommentController::class, 'toggleMultipleComment'])->name('toggle.multiple');
    });

    Route::prefix('trend')->name('trend.')->group(function () {
        Route::get('/', [TrendController::class, 'show'])->name('show');
        Route::get('/create', [TrendController::class, 'create'])->name('create');
        Route::post('/create', [TrendController::class, 'add'])->name('store');
        Route::get('/edit/{id}', [TrendController::class, 'edit'])->name('edit');
        Route::post('/edit/{id}', [TrendController::class, 'update'])->name('update');
        Route::get('/delete/{id}', [TrendController::class, 'delete'])->name('delete');
    });

    Route::prefix('gainer')->name('gainer.')->group(function () {
        Route::get('/', [GainerController::class, 'show'])->name('show');
        Route::get('/create', [GainerController::class, 'create'])->name('create');
        Route::post('/create', [GainerController::class, 'add'])->name('store');
        Route::get('/edit/{id}', [GainerController::class, 'edit'])->name('edit');
        Route::post('/edit/{id}', [GainerController::class, 'update'])->name('update');
        Route::get('/delete/{id}', [GainerController::class, 'delete'])->name('delete');
    });

    Route::prefix('looser')->name('looser.')->group(function () {
        Route::get('/', [LooserController::class, 'show'])->name('show');
        Route::get('/create', [LooserController::class, 'create'])->name('create');
        Route::post('/create', [LooserController::class, 'add'])->name('store');
        Route::get('/edit/{id}', [LooserController::class, 'edit'])->name('edit');
        Route::post('/edit/{id}', [LooserController::class, 'update'])->name('update');
        Route::get('/delete/{id}', [LooserController::class, 'delete'])->name('delete');
    });

    Route::prefix('trending_currency')->name('trending_currency.')->group(function () {
        Route::get('/', [TrendingCurrencyController::class, 'show'])->name('show');
        Route::get('create', [TrendingCurrencyController::class, 'add'])->name('create');
        Route::post('/create', [TrendingCurrencyController::class, 'create'])->name('store');
        Route::get('edit/{id}', [TrendingCurrencyController::class, 'edit'])->name('edit');
        Route::post('/edit/{id}', [TrendingCurrencyController::class, 'update'])->name('update');
        Route::get('/delete/{id}', [TrendingCurrencyController::class, 'delete'])->name('delete');
    });

    Route::prefix('tag')->name('tag.')->group(function () {
        Route::get('show', [TagController::class, 'showTag'])->name('show');
        Route::get('/gc_create', [TagController::class, 'showCreateGCTag'])->name('global_currency.create');
        Route::post('/gc_create', [TagController::class, 'addGlobalCurrencyTag']);
        Route::get('/banner_create', [TagController::class, 'showCreateBannerTag'])->name('banner.create');
        Route::post('/banner_create', [TagController::class, 'addBannerTag']);
        Route::get('/edit/{id}', [TagController::class, 'editTag'])->name('edit');
        Route::post('/edit/{id}', [TagController::class, 'updateTag']);
        Route::delete('delete/{id}', [TagController::class, 'deleteTag'])->name('delete');
    });

    Route::prefix('currency_ramzinex_info')->name('ramzinex_info.')->group(function () {
        Route::get('/', [GCurrencyInfoController::class, 'show'])->name('show');
        Route::get('/create', [GCurrencyInfoController::class, 'create'])->name('create');
        Route::post('/create', [GCurrencyInfoController::class, 'store'])->name('store');
        Route::get('/edit/{id}', [GCurrencyInfoController::class, 'edit'])->name('edit');
        Route::put('/edit/{id}', [GCurrencyInfoController::class, 'update'])->name('update');
        Route::get('delete/{id}', [GCurrencyInfoController::class, 'destroy'])->name('destroy');
    });

    Route::prefix('ohlc')->name('ohlc.')->group(function () {
        Route::get('/', [CurrencyOHLCController::class, 'show'])->name('show');
        Route::get('/create', [CurrencyOHLCController::class, 'create'])->name('create');
        Route::post('/create', [CurrencyOHLCController::class, 'store']);
        Route::get('/edit/{id}', [CurrencyOHLCController::class, 'edit'])->name('edit');
        Route::post('/edit/{id}', [CurrencyOHLCController::class, 'update']);


        Route::get('delete/{id}', [CurrencyOHLCController::class, 'delete'])->name('delete');
    });

    Route::prefix('ckeditor')->name('ckeditor.')->group(function () {
        Route::POST('/store', 'CkeditorController@storeFile')->name('file.store');
    });

    Route::prefix('currency')->name('currency.')->group(function () {
        Route::get('/', [CurrencyController::class, 'show'])->name('show');
        Route::get('show', [CurrencyController::class, 'showCurrency'])->name('show_currencies');
        Route::get('/create', [CurrencyController::class, 'create'])->name('create');
        Route::post('/create', [CurrencyController::class, 'store'])->name('store');
        Route::get('/edit/{currency_id}', [CurrencyController::class, 'edit'])->name('edit');
        Route::post('/edit/{currency_id}', [CurrencyController::class, 'update'])->name('update');
        Route::get('/delete/{currency_id}', [CurrencyController::class, 'destroy'])->name('delete');
    });

    Route::prefix('sector')->name('sector.')->group(function () {
        Route::get('/list', [App\Http\Controllers\Admin\SectorController::class, 'show'])->name('list');
        Route::get('/create', [App\Http\Controllers\Admin\SectorController::class, 'create'])->name('create');
        Route::post('/create', [App\Http\Controllers\Admin\SectorController::class, 'store'])->name('store');
        Route::get('/edit/{id}', [App\Http\Controllers\Admin\SectorController::class, 'edit'])->name('edit');
        Route::post('/edit/{id}', [App\Http\Controllers\Admin\SectorController::class, 'update'])->name('update');
        Route::get('global_currencies/{id}', [App\Http\Controllers\Admin\SectorGCController::class, 'showAttached'])->name('show_attached');
        Route::get('attach_global_currencies/{id}', [App\Http\Controllers\Admin\SectorGCController::class, 'selectGlobalCurrencies'])->name('attach');
        Route::post('attach_global_currencies/{id}', [App\Http\Controllers\Admin\SectorGCController::class, 'attachGlobalCurrencies']);
        Route::get('edit_attached/{id}', [App\Http\Controllers\Admin\SectorGCController::class, 'edit'])->name('edit_attached');
        Route::post('edit_attached/{id}', [App\Http\Controllers\Admin\SectorGCController::class, 'update']);
        Route::get('delete_attached/{id}/{gc-id}', [App\Http\Controllers\Admin\SectorGCController::class, 'delete'])->name('detach');

        Route::delete('/delete/{id}', [App\Http\Controllers\Admin\SectorController::class, 'destroy'])->name('delete');
    });

    Route::get('update-gc', [GlobalCurrencyController::class, 'updateglobalsprice']);
    Route::get('dydx', function () {
        dispatch(new UpdateLeftOverGlobalCurrenciesJob);
    });

});

Route::get('verify_survey/{id}',[CommentController::class,'verifySurvey'])->name('toggleSurvey2');
Route::get('verify_survey/{id}',[Ad::class,'verifySurvey'])->name('toggleSurvey2');
Route::get('verify_survey/{id}',[CommentController::class,'verifySurvey'])->name('toggleSurvey2');

Route::get('test_shet',function (){
return "hii";
});
