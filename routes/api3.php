<?php

use App\Http\Controllers\API\CommentController;

use App\Http\Controllers\API3\GlobalCurrencyController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API3\BannerController;
use App\Http\Controllers\API\FaqCategoryController;
use App\Http\Controllers\API\FaqQuestionAnswerController;
use App\Http\Controllers\API\UserProfileController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('api3_global-currencies')->name('global_currencies_v3.')->group(function () {
    Route::get('/list/{id?}', [GlobalCurrencyController::class, 'index'])->name('list');
    Route::get('/get_likes_list', [GlobalCurrencyController::class, 'getGlobalLikedList'])->name('list_like');
    Route::get('/get_fave', [GlobalCurrencyController::class, 'getGlobalFavoredList'])->name('list_fav');
    Route::get('/get_dislikes_list', [GlobalCurrencyController::class, 'GetGlobalDislikeList'])->name('list_dislike');
    Route::get('/test', [GlobalCurrencyController::class, 'checkIfGCIsLiked'])->name('list_isliked');
    Route::get('/detail/{currency_id}', [GlobalCurrencyController::class, 'show'])->name('get_detail');
    Route::get('/comments/list', [GlobalCurrencyController::class, 'getComments'])->name('list_get_comment');
    Route::get('related_banners/{currency_id}', [GlobalCurrencyController::class, 'showRelatedBanners'])->name('banner-list');
    Route::middleware(['user_authentication'])->group(function () {
        Route::POST('/like', [GlobalCurrencyController::class, 'like'])->name('likes.add');
        Route::POST('/is-liked', [GlobalCurrencyController::class, 'isLiked'])->name('likes.is.liked');
        Route::POST('/dislike', [GlobalCurrencyController::class, 'disLike'])->name('dislike.add');
        Route::POST('/is-disliked', [GlobalCurrencyController::class, 'isDisliked'])->name('dislike.is.disliked');
        Route::POST('/include_to_ramzinex', [GlobalCurrencyController::class, 'includeToRamzinex'])->name('include_to_ramzinex');

        Route::POST('/favorite', [GlobalCurrencyController::class, 'addToFavorite'])->name('favorite.add');
        Route::POST('/is_favored', [GlobalCurrencyController::class, 'isAddedToFavorites'])->name('favorite.is.favored');

        Route::prefix('comments')->name('comments_v3.')->group(function () {
            Route::POST('/add', [GlobalCurrencyController::class, 'addComment'])->name('add');
            Route::POST('/edit', [GlobalCurrencyController::class, 'updateComment'])->name('edit');
            Route::get('/delete/{id}', [GlobalCurrencyController::class, 'deleteComment'])->name('delete');
        });
    });
});

Route::prefix('api3_faq')->name('faq_v3.')->group(function () {

    Route::prefix('categories')->group(function () {
        Route::get('/{parent_id?}', [FaqCategoryController::class, 'showCategories'])->name('get.list');
        Route::get('/questions/{category_id}', [FaqCategoryController::class, 'showCategoryAndRelatedQuestions'])->name('get.category.questions.list');//Todo may change
    });

    Route::prefix('api3_question')->group(function () {
//        Route::get('/question_ids', [FaqQuestionAnswerController::class, 'getQuestionsId'])->name('get.question.ids');
        Route::get('/comments/list', [FaqQuestionAnswerController::class, 'getComment'])->name('list');
        Route::get('{id}', [FaqQuestionAnswerController::class, 'showQuestion'])->name('get.question');

        Route::middleware(['user_authentication'])->group(function () {
            Route::POST('/like', [FaqQuestionAnswerController::class, 'like'])->name('likes.add');
            Route::POST('/is-liked', [FaqQuestionAnswerController::class, 'isLiked'])->name('likes.is.liked');
            Route::POST('/dislike', [FaqQuestionAnswerController::class, 'disLike'])->name('dislike.add');
            Route::POST('/is-disliked', [FaqQuestionAnswerController::class, 'isDisliked'])->name('dislike.is.disliked');

            Route::POST('/favorite', [FaqQuestionAnswerController::class, 'addToFavorite'])->name('favorite.add');
            Route::POST('/is_favored', [FaqQuestionAnswerController::class, 'isAddedToFavorites'])->name('favorite.is.favored');

            Route::prefix('comments')->name('comments.')->group(function () {
                Route::POST('/add', [FaqQuestionAnswerController::class, 'addComment'])->name('add');
                Route::POST('/edit', [FaqQuestionAnswerController::class, 'updateComment'])->name('edit');
                Route::get('/delete/{id}', [FaqQuestionAnswerController::class, 'deleteComment'])->name('delete');
            });
        });
    });

    Route::get('/search', [FaqQuestionAnswerController::class, 'searchQuestionAndCategories'])->name('search');
});

Route::prefix('api3-banners')->name('banners_v3.')->group(function () {
    Route::get('/', [BannerController::class, 'showActiveBanners'])->name('list');
    Route::get('/detail', [BannerController::class, 'getBanner'])->name('show');
    Route::get('/comments/list', [BannerController::class, 'getComment'])->name('list_banners');

});
Route::prefix('api3_user')->name('users_v3.')->group(function () {
    Route::get('/profile', [UserProfileController::class, 'getUserProfile'])->name('profile.show');
    Route::get('/favorites', [UserProfileController::class, 'getUserFavorites'])->name('profile.favorites');
});

Route::prefix('api3_comments')->name('comments_v3.')->group(function () {
    Route::get('/replies', [CommentController::class, 'getCommentReplies'])->name('replies.list');
    Route::POST('/like', [CommentController::class, 'likeComments'])->name('like');
    Route::POST('/dislike', [CommentController::class, 'dislikeComments'])->name('dislike');
});

Route::get('/banner-types', [BannerController::class, 'showBannerTypes'])->name('banners.types.show_v3');

Route::get('user_likes_list', [\App\Http\Controllers\API3\UserListsController::class, 'getUsersLikesList']);
Route::get('user_dislikes_list', [\App\Http\Controllers\API3\UserListsController::class, 'getUsersDisLikesList']);
Route::get('user_favored_list', [\App\Http\Controllers\API3\UserListsController::class, 'getUsersFavoritesList']);

Route::get('test',function (){
   return view('comment.reply');
});
