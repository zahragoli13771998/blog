<?php

use App\Http\Controllers\API\CommentController;
use App\Http\Controllers\API\CurrencyConversionController;
use App\Http\Controllers\GameController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\GlobalCurrencyController;
use App\Http\Controllers\API\BannerController;
use App\Http\Controllers\API\FaqCategoryController;
use App\Http\Controllers\API\FaqQuestionAnswerController;
use App\Http\Controllers\API\StateCurrenciesController;
use App\Http\Controllers\API\UserProfileController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('currencies')->group(function () {
    Route::get('/gainers', [StateCurrenciesController::class, 'gainer']);
    Route::get('/loosers', [StateCurrenciesController::class, 'looser']);
    Route::get('/trends', [StateCurrenciesController::class, 'lastAddedCurrencies']);
    Route::get('/global_trends', [StateCurrenciesController::class, 'getGlobalTrends']);
    Route::get('/daily_global_trends', [StateCurrenciesController::class, 'globalTrending']);
    Route::get('per_global_trends/{id}', [StateCurrenciesController::class, 'getGlobalTrends2']);
    Route::get('/currency_trends', [StateCurrenciesController::class, 'currencyTrends']);
    Route::get('/daily_trends', [StateCurrenciesController::class, 'lastAddedCurrencies']);
});

Route::prefix('global-currencies')->name('global_currencies.')->group(function () {
    Route::get('/list/{id?}', [GlobalCurrencyController::class, 'index'])->name('list');
    Route::get('/detail/{currency_id}', [GlobalCurrencyController::class, 'show'])->name('get_detail');
    Route::POST('/convert', [CurrencyConversionController::class, 'convertCurrencies'])->name('conversion');
    Route::get('/comments/list', [GlobalCurrencyController::class, 'getComments'])->name('comment_list2');
    Route::get('/comments/poll', [GlobalCurrencyController::class, 'showPoll'])->name('list2');
    Route::post('/comments/answer_Poll', [GlobalCurrencyController::class, 'answerPoll'])->name('list3');
    Route::get('/currency_ids_list', [GlobalCurrencyController::class, 'ramzinexCurrenciesWithGlobals'])->name('currency_id_list');
    Route::get('/pair_ids_list', [GlobalCurrencyController::class, 'ramzinexPairsWithGlobals'])->name('pair_id_list');
    Route::get('/ramzinex_currencies/{id}', [GlobalCurrencyController::class, 'ramzinexCurrencies'])->name('ramzinexCurrencies');
    Route::get('related_banners/{currency_id}', [GlobalCurrencyController::class, 'showRelatedBanners'])->name('banner-list');

    Route::middleware(['user_authentication'])->group(function () {
        Route::POST('/like', [GlobalCurrencyController::class, 'like'])->name('likes.add');
        Route::POST('/is-liked', [GlobalCurrencyController::class, 'isLiked'])->name('likes.is.liked');
        Route::POST('/dislike', [GlobalCurrencyController::class, 'disLike'])->name('dislike.add');
        Route::POST('/is-disliked', [GlobalCurrencyController::class, 'isDisliked'])->name('dislike.is.disliked');
        Route::POST('/include_to_ramzinex', [GlobalCurrencyController::class, 'includeToRamzinex'])->name('include_to_ramzinex');

        Route::POST('/favorite', [GlobalCurrencyController::class, 'addToFavorite'])->name('favorite.add');
        Route::POST('/is_favored', [GlobalCurrencyController::class, 'isAddedToFavorites'])->name('favorite.is.favored');

        Route::prefix('comments')->name('comments.')->group(function () {
            Route::POST('/add', [GlobalCurrencyController::class, 'addComment'])->name('add');
            Route::POST('/edit', [GlobalCurrencyController::class, 'updateComment'])->name('edit');
            Route::get('/delete/{id}', [GlobalCurrencyController::class, 'deleteComment'])->name('delete');
        });
    });
});

Route::prefix('faq')->name('faq.')->group(function () {

    Route::prefix('categories')->group(function () {
        Route::get('/{parent_id?}', [FaqCategoryController::class, 'showCategories'])->name('get.list');
        Route::get('/questions/{category_id}', [FaqCategoryController::class, 'showCategoryAndRelatedQuestions'])->name('get.category.questions.list');//Todo may change
    });

    Route::prefix('question')->group(function () {
        Route::get('/question_ids', [FaqQuestionAnswerController::class, 'getQuestionsId'])->name('get.question.ids');
        Route::get('/comments/list', [FaqQuestionAnswerController::class, 'getComment'])->name('list');
        Route::get('/frequent_questions', [FaqQuestionAnswerController::class, 'getFrequentQuestions'])->name('frequent_questions');
        Route::get('{id}', [FaqQuestionAnswerController::class, 'showQuestion'])->name('get.question');

        Route::middleware(['user_authentication'])->group(function () {
            Route::POST('/like', [FaqQuestionAnswerController::class, 'like'])->name('likes.add');
            Route::POST('/is-liked', [FaqQuestionAnswerController::class, 'isLiked'])->name('likes.is.liked');
            Route::POST('/dislike', [FaqQuestionAnswerController::class, 'disLike'])->name('dislike.add');
            Route::POST('/is-disliked', [FaqQuestionAnswerController::class, 'isDisliked'])->name('dislike.is.disliked');

            Route::POST('/favorite', [FaqQuestionAnswerController::class, 'addToFavorite'])->name('favorite.add');
            Route::POST('/is_favored', [FaqQuestionAnswerController::class, 'isAddedToFavorites'])->name('favorite.is.favored');

            Route::prefix('comments')->name('comments.')->group(function () {
                Route::POST('/add', [FaqQuestionAnswerController::class, 'addComment'])->name('add');
                Route::POST('/edit', [FaqQuestionAnswerController::class, 'updateComment'])->name('edit');
                Route::get('/delete/{id}', [FaqQuestionAnswerController::class, 'deleteComment'])->name('delete');
            });
        });
    });

    Route::get('/search', [FaqQuestionAnswerController::class, 'searchQuestionAndCategories'])->name('search');
});

Route::prefix('banners')->name('banners.')->group(function () {
    Route::get('/', [BannerController::class, 'showActiveBanners'])->name('list');
    Route::get('/detail', [BannerController::class, 'getBanner'])->name('show');
    Route::get('/sliders', [BannerController::class, 'getSliders'])->name('slides');
    Route::get('/comments/list', [BannerController::class, 'getComment'])->name('list2');

    Route::middleware(['user_authentication'])->group(function () {
        Route::POST('/like', [BannerController::class, 'like'])->name('likes.add');
        Route::POST('/is-liked', [BannerController::class, 'isLiked'])->name('likes.is.liked');
        Route::POST('/dislike', [BannerController::class, 'disLike'])->name('dislike.add');
        Route::POST('/is-disliked', [BannerController::class, 'isDisliked'])->name('dislike.is.disliked');
        Route::POST('/favorite', [BannerController::class, 'addToFavorite'])->name('favorite.add');
        Route::POST('/is_favored', [BannerController::class, 'isAddedToFavorites'])->name('favorite.is.favored');

        Route::prefix('comments')->name('comments.')->group(function () {
            Route::POST('/add', [BannerController::class, 'addComment'])->name('add');
            Route::POST('/edit', [BannerController::class, 'updateComment'])->name('edit');
            Route::get('/delete/{id}', [BannerController::class, 'deleteComment'])->name('delete');
        });
    });
});
Route::prefix('user')->name('users.')->group(function () {
    Route::get('/profile', [UserProfileController::class, 'getUserProfile'])->name('profile.show');
    Route::get('/favorites', [UserProfileController::class, 'getUserFavorites'])->name('profile.favorites');
});

Route::prefix('comments')->name('comments.')->group(function () {
    Route::get('/replies', [CommentController::class, 'getCommentReplies'])->name('replies.list');
    Route::POST('/like', [CommentController::class, 'likeComments'])->name('like');
    Route::POST('/dislike', [CommentController::class, 'dislikeComments'])->name('dislike');
});


Route::get('/banner-types', [BannerController::class, 'showBannerTypes'])->name('banners.types.show');

Route::group(['prefix' => 'jobvacancy'], function () {
    Route::get('/list', [App\Http\Controllers\API\JobVacancyRequestController::class, 'show'])->name('jobvacancy.list');
    Route::get('/list_only_job_types', [App\Http\Controllers\API\JobVacancyRequestController::class, 'showOnlyJobTypes'])->name('jobvacancy.job_type_list');
    Route::get('/vacancy_detail', [App\Http\Controllers\API\JobVacancyRequestController::class, 'getJobVacancyDetail'])->name('jobvacancy.vacancy_detail');
    Route::get('/detail/{id}', [App\Http\Controllers\API\JobVacancyRequestController::class, 'getEachJobVacancyDetail'])->name('jobvacancy.EachVacancy_detail');
    Route::get('/vacancy_search', [App\Http\Controllers\API\JobVacancyRequestController::class, 'search'])->name('jobvacancy.type_search');
    Route::Post('/job_request/store', [App\Http\Controllers\API\JobVacancyRequestController::class, 'store'])->name('request.store');

});
Route::prefix('sectors')->name('sectors')->group(function () {
    Route::get('/list', [\App\Http\Controllers\API\SectorController::class, 'getSectorList'])->name('list');
    Route::get('/detail', [\App\Http\Controllers\API\SectorController::class, 'getSector'])->name('detail');
    Route::get('/search', [\App\Http\Controllers\API\SectorController::class, 'filterSectors'])->name('search');
});
Route::get('/online_support', function () {
    return view('online_support3');
});
Route::get('/pwa_code_version', [App\Http\Controllers\ApiKeyController::class, 'pwaCodeVersion'])->name('pwaCodeVersion');

Route::get('export', [StateCurrenciesController::class, 'exportExcelView']);
Route::get('export-excel', [StateCurrenciesController::class, 'exportExcel'])->name('export.excel');

Route::get('exchange/pt1/market', [\App\Http\Controllers\VueController::class, 'pwaIndex']);


Route::get('rmzx_pair', [GlobalCurrencyController::class, 'test']);


Route::get('commentsToCsv',[\App\Http\Controllers\Admin\CommentsToCsvController::class,'getComments']);
Route::get('ramzinexcurrencies',[\App\Http\Controllers\API\RamzinexCurrenciesGlobalsController::class,'getNotIncludedCurrencies']);
Route::get('getTopRankedGlobals',[\App\Http\Controllers\API\RamzinexCurrenciesGlobalsController::class,'showGlobals']);



Route::get('/testJavaCorrrrre',[\App\Http\Controllers\API\JavaCoreController::class,'testCurl']);
Route::get('test-video',function (){
    return response()->json([
        'status' => 0,
        'data' => [
          'video1'=>"https://api-two.ramzinex.com/storage/faq_question_answer/video/RfVM2TsxDenoo6yeOcgLEUIMUYlVKVby0GsDTS9m.mp4",
          'video2'=>"https://api-two.ramzinex.com/storage/faq_question_answer/video/900x1200LQ.mp4",
            ]
    ]);
});

Route::get('getCoinGeckoData',[\App\logic\GlobalCurrencyCoinGeckoCompleteManager::class,'createGlobals']);

