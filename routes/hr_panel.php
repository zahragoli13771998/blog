<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Auth\AuthenticatedSessionController;
use Illuminate\Support\Facades\Redis;

Route::get('/test_route',function (){
    return "hiizahra";
});
Route::get('/login', [AuthenticatedSessionController::class, 'createHR'])
    ->middleware('guest')
    ->name('hr.login');

Route::post('/login', [AuthenticatedSessionController::class, 'storeHR'])
    ->middleware('guest');

Route::prefix('panel')->middleware(['is_authenticated'])->name('hr_panel.')->group(function () {
    Route::get('/logout', [AuthenticatedSessionController::class, 'destroyHR'])
        ->name('logout');
    Route::get('/jobtype/list', [App\Http\Controllers\Admin\JobTypesController::class, 'show'])->name('jobtype.list');
    Route::get('/jobtype/{id}/edit', [App\Http\Controllers\Admin\JobTypesController::class, 'edit'])->name('jobtype.edit');
    Route::get('/jobtype/create', [App\Http\Controllers\Admin\JobTypesController::class, 'create'])->name('jobtype.create');
    Route::get('/jobtype/store', [App\Http\Controllers\Admin\JobTypesController::class, 'store'])->name('jobtype.store');
    Route::post('/jobtype/{id}/edit', [App\Http\Controllers\Admin\JobTypesController::class, 'update'])->name('jobtype.update');
    Route::delete('/jobtype/{id}/delete', [App\Http\Controllers\Admin\JobTypesController::class, 'destroy'])->name('jobtype.delete');


    Route::get('/resume/download/{id}', [App\Http\Controllers\Admin\UserJobController::class, 'getFile'])->name('resume.download');
    Route::delete('/delete/{id}', [App\Http\Controllers\Admin\UserJobController::class, 'destroy'])->name('request.delete');
    Route::get('/update/accept/{id}', [App\Http\Controllers\Admin\UserJobController::class, 'updateStatusForAccept'])->name('request.update_state.accept');
    Route::get('/update/reject/{id}', [App\Http\Controllers\Admin\UserJobController::class, 'updateStatusForReject'])->name('request.update_state.reject');
    Route::get('/resumes/jobtype/{id}', [App\Http\Controllers\Admin\UserJobController::class, 'resumeShow'])->name('admin.resume.jobtype.list');
    Route::get('/resumes/jobvacancy/{id}', [App\Http\Controllers\Admin\UserJobController::class, 'show'])->name('admin.resume.jobvacancy.list');
    Route::prefix('ckeditor')->name('ckeditor.')->group(function () {
        Route::POST('/store', [App\Http\Controllers\Admin\CkeditorController::class,'storeFile'])->name('file.store');
    });
    Route::get('jobvacancy/list/{id}', [App\Http\Controllers\Admin\JobVacancyController::class, 'show'])->name('jobvacancy_list');
    Route::get('/jobvacancy/{id}/edit', [App\Http\Controllers\Admin\JobVacancyController::class, 'edit'])->name('jobvacancy.edit');
    Route::get('/jobvacancy/{id}/status/edit', [App\Http\Controllers\Admin\JobVacancyController::class, 'updateStatus'])->name('jobvacancy.status.edit');
    Route::post('/jobvacancy/{id}/update', [App\Http\Controllers\Admin\JobVacancyController::class, 'update'])->name('jobvacancy.update');
    Route::get('/jobvacancy/create', [App\Http\Controllers\Admin\JobVacancyController::class, 'create'])->name('jobvacancy.create');
    Route::post('/jobvacancy/create', [App\Http\Controllers\Admin\JobVacancyController::class, 'store'])->name('jobvacancy.store');
    Route::delete('/jobvacancy/{id}/delete', [App\Http\Controllers\Admin\JobVacancyController::class, 'destroy'])->name('jobvacancy.delete');

    Route::prefix('head-hunted')->name('head_hunted.')->group(function () {
        Route::get('/', [\App\Http\Controllers\Admin\HeadHuntedController::class, 'index'])->name('index');
        Route::get('/create/', [\App\Http\Controllers\Admin\HeadHuntedController::class, 'create'])->name('create');
        Route::post('/', [\App\Http\Controllers\Admin\HeadHuntedController::class, 'store'])->name('store');
        Route::get('edit/{headHunted}', [\App\Http\Controllers\Admin\HeadHuntedController::class, 'edit'])->name('edit');
        Route::put('update/{id}', [\App\Http\Controllers\Admin\HeadHuntedController::class, 'update'])->name('update');
        Route::get('/accepted/{headHunted}', [\App\Http\Controllers\Admin\HeadHuntedController::class, 'accepted'])->name('accepted.view');
        Route::put('/accepted/{id}', [\App\Http\Controllers\Admin\HeadHuntedController::class, 'updateAccepted'])->name('accepted');
        Route::get('rejected/{headHunted}', [\App\Http\Controllers\Admin\HeadHuntedController::class, 'rejected'])->name('rejected.view');
        Route::put('rejected/{id}', [\App\Http\Controllers\Admin\HeadHuntedController::class, 'updateRejected'])->name('rejected');
    });

});

Route::get('/publish', function () {
    // ...

    Redis::publish('test-channel', json_encode([
        'key' => 'keysss',
        'message' => [
        'name' => 'Adam Wathan'
            ]
    ]));
});
