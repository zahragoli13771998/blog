<?php

use App\Http\Controllers\Admin\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Admin\marketingCkeditorController;
use App\Http\Controllers\FaqCategoryController;
use App\Http\Controllers\FaqQuestionAnswerController;
use Illuminate\Support\Facades\Route;

Route::get('login', [\App\Http\Controllers\Admin\Auth\AuthenticatedSessionController::class, 'createMarketingAuth'])->name('login');
Route::post('login', [\App\Http\Controllers\Admin\Auth\AuthenticatedSessionController::class, 'storeMarketingAuth'])->name('admin_login_post');



//Route::middleware('is_marketing_admin')->name('admin.')->group(function () {
    Route::get('signout', [\App\Http\Controllers\Admin\Auth\AuthenticatedSessionController::class, 'destroyMarketing'])->name('signout');

    Route::prefix('panel')->name('marketing.')->group(function () {
        Route::get('/logout', [AuthenticatedSessionController::class, 'destroyMarketingAuth'])
            ->name('logout');
        Route::prefix('faq')->name('faq.')->group(function () {

            Route::prefix('ckeditor')->name('ckeditor.')->group(function () {
                Route::POST('/store', [marketingCkeditorController::class,'storeFile'])->name('file.store-image');
            });

            Route::prefix('category')->name('category.')->group(function () {
                Route::get('/', [FaqCategoryController::class, 'show'])->name('show_categories');
                Route::get('create', [FaqCategoryController::class, 'create'])->name('create');
                Route::post('create', [FaqCategoryController::class, 'add']);
                Route::get('edit/{faq_category}', [FaqCategoryController::class, 'edit'])->name('edit');
                Route::post('edit/{faq_category}', [FaqCategoryController::class, 'update']);
                Route::get('delete/{faq_category}', [FaqCategoryController::class, 'delete'])->name('delete');
            });

            Route::prefix('question_answer')->name('question_answer.')->group(function () {
                Route::get('/', [FaqQuestionAnswerController::class, 'showQuestions'])->name('show_questions');
                Route::get('create', [FaqQuestionAnswerController::class, 'create'])->name('create');
                Route::post('create', [FaqQuestionAnswerController::class, 'add']);
                Route::get('edit/{faq_question_answer}', [FaqQuestionAnswerController::class, 'edit'])->name('edit');
                Route::post('edit/{faq_question_answer}', [FaqQuestionAnswerController::class, 'update'])->name('update');
                Route::get('delete/{faq_question_answer}', [FaqQuestionAnswerController::class, 'delete'])->name('delete');
                Route::get('/selected/{faq_question_answer}', [FaqQuestionAnswerController::class, 'showSelectedQuestion'])->name('feedback');

                Route::prefix('deeplink')->name('deeplink.')->group(function () {
                    Route::get('/create/{id}', [FaqQuestionAnswerController::class, 'createDeeplink'])->name('create');
                    Route::post('/store', [FaqQuestionAnswerController::class, 'storeDeeplink'])->name('store');
                    Route::get('delete/{deeplink_id}', [FaqQuestionAnswerController::class, 'deleteDeeplink'])->name('delete');
                });
            });
        });
//    });
});

